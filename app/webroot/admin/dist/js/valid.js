
function f1()
{

	var i=document.getElementById('mode').value;
	var l=document.getElementById('level').value;
	if(i!=3)
	{
		$('#options').hide();
		$('#opt1').hide();
		$('#opt2').hide();
		$('#opt3').hide();
	}
	if(i==2)
	{
		$('#img_file2').show();
	}
	if(i!=2)
	{
		$('#img_file').hide();
		$('#opt2').hide();
		$('#img_file2').hide();
		document.getElementById("img2").required = false;
	}
	if(l==1)
	{
		$('#ans2').hide();
	    $('#q2').hide();
	    $('#opt2').hide();
		$('#img_file2').hide();

		$('#ans2_audio').hide();
	    $('#q2_audio').hide();
	    $('#opt1_audio').hide();
	    $('#opt2_audio').hide();
		$('#img2_audio').hide();
		document.getElementById("img2").required = false;
	}
	else if(l!=1 && i==2)
	{
		$('#ans2').show();
		$('#q2').show();
		$('#opt2').show();
		$('#img_file2').show();

		$('#ans2_audio').show();
	    $('#q2_audio').show();
	    $('#opt1_audio').show();
	    $('#opt2_audio').show();
		$('#img2_audio').show();
		document.getElementById("mq2").required = true; 
		document.getElementById("ma2").required = true; 
		document.getElementById("img22").required = true;
		document.getElementById("img2").required = true;

	}
	else
	{
		document.getElementById("img2").required = false;
	}
}


function mode_change(mode)
{
	if(mode==3)
	{
		$('#options').show();
		$('#opt1').show();
		$('#opt2').show();
		$('#opt3').show();
	}
	else
	{
		$('#options').hide();
		$('#opt1').hide();
		$('#opt2').hide();
		$('#opt3').hide();
	}
	if(mode==2)
	{
		$('#img_file2').show();
		$('#img_file2').show();

		document.getElementById("mq2").required = true;  
		document.getElementById("ma2").required = true; 
		document.getElementById("img22").required = true;
	
	}
	else
	{
		$('#img_file2').hide();
		
		$('#img_file2').hide();	

		document.getElementById("mq2").required = false; 
		document.getElementById("ma2").required = false; 
		document.getElementById("img22").required = false;

	}
	if(mode==2 && l!=1)
	{
	
		$('#img_file2').show();
		document.getElementById("mq2").required = true; 
		document.getElementById("ma2").required = true; 
		document.getElementById("img22").required = true;
		document.getElementById("img2").required = true;
	}
	else
	{
		$('#img_file2').hide();
		$('#img_file2').hide();	
		document.getElementById("mq2").required = false; 
		document.getElementById("ma2").required = false; 
		document.getElementById("img22").required = false;
	}
}
	
function level_change(level)
{
	var i=document.getElementById('mode').value;
	if(level==1)
	{
	$('#ans2').hide();
	$('#q2').hide();
	$('#opt2').hide();
	$('#img_file2').hide();

	$('#ans2_audio').hide();
    $('#q2_audio').hide();
    $('#opt1_audio').hide();
    $('#opt2_audio').hide();
	$('#img2_audio').hide();
	document.getElementById("mq2").required = false;
	document.getElementById("ma2").required = false; 
	document.getElementById("img22").required = false;
	document.getElementById("img2").required = false;

	}
else if(level!=1 && i==2)
{
	$('#img_file2').show();



	$('#img2_audio').show();
	document.getElementById("mq2").required = true;
	document.getElementById("ma2").required = true; 
	document.getElementById("img22").required = true;
	document.getElementById("img2").required = true;
}
else
{
	document.getElementById("img2").required = false;
}
}



        function ValidateImageExtension(fileUpload,id1) {
            var allowedFiles = [".jpg", ".png", ".gif"];
            var lblError = document.getElementById("lblError"+id1);
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.value.toLowerCase())) {
            	fileUpload.value=null;
       //         	var newElm = document.createElement('input');
		     // 	var old = document.getElementById(id1);
			    // newElm.type = "file";
			    // newElm.id = id1;
			    // newElm.name = old.name;
			    // newElm.className = old.className;
			    // old.parentNode.replaceChild(newElm, old);
		        lblError.innerHTML = "Please upload image files only.";
		        setTimeout(function () {lblError.innerHTML=''}, 3000);
		        document.getElementById(id1).required = true;
                return false;
            }
            else
            {

            	if (fileUpload.files && fileUpload.files[0]) {
			        var reader = new FileReader();

			        reader.onload = function (e) {
			            $('#img2').attr('src', e.target.result);
			        }

			        reader.readAsDataURL(fileUpload.files[0]);
			    }

            }
            lblError.innerHTML = "";
            return true;
        }

        function ValidateImageExtension1(fileUpload,id1) {
    	 	
            var allowedFiles = [".jpg", ".png", ".gif"];
            var lblError = document.getElementById("lblError"+id1);
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.value.toLowerCase())) {
            	fileUpload.value=null;
       //         	var newElm = document.createElement('input');
		     // 	var old = document.getElementById(id1);
			    // newElm.type = "file";
			    // newElm.id = id1;
			    // newElm.name = old.name;
			    // newElm.className = old.className;
			    // old.parentNode.replaceChild(newElm, old);
		        lblError.innerHTML = "Please upload image files only.";
		        setTimeout(function () {lblError.innerHTML=''}, 3000);
		        document.getElementById(id1).required = true;
		        document.getElementById("myBtn").disabled = true;
  				document.getElementById("cancel_btn").disabled = true;
  				$("#img2").hide();
                return false;
            }
            else
            {
            	$("#img2").show();

            	$("#bx1").css("height", 420);
            	if (fileUpload.files && fileUpload.files[0]) {
			        var reader = new FileReader();

			        reader.onload = function (e) {
			            $('#img2').attr('src', e.target.result);
			        }

			        reader.readAsDataURL(fileUpload.files[0]);
			    }
            	document.getElementById("myBtn").disabled = false;
  				document.getElementById("cancel_btn").disabled = false;
            }
            lblError.innerHTML = "";
            return true;
        }

        function ValidateAudioExtension(fileUpload,id1) {
            var allowedFiles = [".wav",".mp3"];
            var lblError = document.getElementById("lblError"+id1);
            var regex = new RegExp("(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.value.toLowerCase())) {
            	fileUpload.value=null;
            	$('#upload'+id1).prop( "disabled", true );
       //         	var newElm = document.createElement('input');
		     // 	var old = document.getElementById(id1);
			    // newElm.type = "file";
			    // newElm.id = id1;
			    // newElm.name = old.name;
			    // newElm.className = old.className;
			    // old.parentNode.replaceChild(newElm, old);
                lblError.innerHTML = "Please upload wav or mp3 files only.";
                setTimeout(function () {lblError.innerHTML=''}, 3000);
                return false;
            }
            else
            {
            	$('#upload'+id1).prop( "disabled", false );
            }
            lblError.innerHTML = "";
            return true;
        }



function get_object(val)
{
	var tmp='';
	$("#content").val('');
		var count=1;
 		var selectedText = val.options[val.selectedIndex].innerHTML;
        var selectedValue = val.value;
        var baseurl = document.getElementById('base_url').value;
        var width1 = document.getElementById('width').value;
        var height1 = document.getElementById('height').value;
        if(width1=='')
	    {
	      width1='35px';
	      height1='35px';
	    }
	
        $.ajax({
        type:"POST",
        data:{obj:selectedText}, 
        success : function(response) {
 			obj1=$.parseJSON(response);
 			$.each( obj1, function( key, value ) {
 				// var img_path='/tagsomdev/app/webroot/uploads/Maths_Images/Variants/'+value.Variant.image;
  		 //    	$(".fff").append('<li><img src="'+img_path+'" width=65px height=53px style="cursor:pointer;" ></li>');
  		 		var img_path='/tagsomdev/app/webroot/uploads/Maths_Images/Variants/'+value.Variant.image;
  		    	
  		    	tmp+='<div id="'+count+'" class="item ui-draggable ui-draggable-handle" style="width:'+width1+';height:'+height1+';"><img src="'+img_path+'" width="'+width1+'" height="'+height1+'"  ></div>';
  		    	count++;
			});
		
 			 $("#content").html(tmp);

 			 
 			  $("#content .item").draggable({
				    connectToSortable: ".fs:not(.full)",
				    revert: "invalid",
				    helper: "clone"
				  });
        },
        error : function() {
           alert("false");
        }
    });

         $.ajax({
        type:"POST",
        data:{obj2:selectedText}, 
        success : function(response) {
        	obj1=$.parseJSON(response);
        	var img_path='/tagsomdev/app/webroot/uploads/Maths_Images/Objects/'+obj1;
        	$('#obj_img').attr('src', img_path);
        },
        error : function() {
           alert("false");
        }
    });
}



function ValidateXLSExtension(fileUpload) {

		var file_name=fileUpload.files[0].name;
		var i = file_name.lastIndexOf('.');
      	var ext=file_name.slice(i);
        if (ext!='.xls') {
        	fileUpload.value=null;
	        lblError.innerHTML = "Please upload .xls files only.";
	        setTimeout(function () {lblError.innerHTML=''}, 3000);
	        document.getElementById(id1).required = true;
            return false;
        }
        lblError.innerHTML = "";
        return true;
}

function cancelupload() {
location.reload();
}
