
Following are the Functionality stopped working

1.) Reset Password is not working in Responsive/Web App for Customer scheduling App.   --- Reset Password working fine.
2.) Search functionality removed from Calculation Failed Log and activity Log from Budget Price App admin Panel.  -- Done
3.) Record sorting on header click removed from Calculation Failed Log and activity Log from Budget Price App admin Panel. --  Done
4.) Record navigation link at the bottom of the Grid for logs are removed from Budget Price App admin Panel .  --  Done
5.) New logs are not displaying in calculation failed log and activity log in Budget Price App admin Panel. --  Done
6.) Calculation failed log is not displaying the access date time column.



Following are the Feedbacks

Budget Price App
1.) The static text for Calculation failed email  should be  "A failed attempt has been made on Budgetary price calculation. Following are the details."
2.) When Note is not entered in configuration on admin panel, then don't display Note at the bottom.


Budget Price Admin Panel.
1.) In Lead Time increase the size of Cooling class column. ONAN/ONAF is not fully visible. --  Cooling class column is not added in "Lead Time" Page. This column is in "Budgetary Price" Page
2.) Make VT GT logo size to double and align this properly with other controls and links surrounding to this.



Customer Scheduling App.
1.) When Note is not entered in configuration on admin panel, the other notes should be top aligned with Notes label. - Done
2.) When date changed , the data showing block should be Red in colour not Pink. - Done
3.) In responsive web app the Reset Password email the signature section not same as Mobile App . Make similar with Mobile App like - Done

Thanks.
Administrator
www.vtchub.com/CustomerScheduleApp

4.) While sending email to the user through application, the First name and Last name must be using CAMEL CASE. - Done


Customer Scheduling Admin Panel.
1.) Make VT GT logo size to double and align this properly with other controls and links surrounding to this. - Done
