<?php
App::uses('ModeQuestionAnswer', 'Model');

/**
 * ModeQuestionAnswer Test Case
 */
class ModeQuestionAnswerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mode_question_answer',
		'app.mode',
		'app.user_mode_grade',
		'app.user',
		'app.user_invite',
		'app.user_mode_status',
		'app.language',
		'app.story',
		'app.user_purchase',
		'app.user_story_level',
		'app.level',
		'app.grade'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ModeQuestionAnswer = ClassRegistry::init('ModeQuestionAnswer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ModeQuestionAnswer);

		parent::tearDown();
	}

}
