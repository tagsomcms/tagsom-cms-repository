<?php
App::uses('Language', 'Model');

/**
 * Language Test Case
 */
class LanguageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.language',
		'app.mode_question_answer',
		'app.story',
		'app.user_mode_grade',
		'app.user_mode_status',
		'app.user_purchase',
		'app.user_story_level'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Language = ClassRegistry::init('Language');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Language);

		parent::tearDown();
	}

}
