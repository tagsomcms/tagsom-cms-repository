<?php
App::uses('UserStoryLevel', 'Model');

/**
 * UserStoryLevel Test Case
 */
class UserStoryLevelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_story_level',
		'app.user',
		'app.user_invite',
		'app.user_mode_grade',
		'app.level',
		'app.user_mode_status',
		'app.language',
		'app.mode_question_answer',
		'app.mode',
		'app.story',
		'app.user_purchase',
		'app.grade'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserStoryLevel = ClassRegistry::init('UserStoryLevel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserStoryLevel);

		parent::tearDown();
	}

}
