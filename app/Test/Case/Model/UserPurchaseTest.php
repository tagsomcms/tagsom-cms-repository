<?php
App::uses('UserPurchase', 'Model');

/**
 * UserPurchase Test Case
 */
class UserPurchaseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_purchase',
		'app.user',
		'app.user_invite',
		'app.user_mode_grade',
		'app.level',
		'app.user_mode_status',
		'app.language',
		'app.mode_question_answer',
		'app.mode',
		'app.user_story_level',
		'app.story',
		'app.grade'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserPurchase = ClassRegistry::init('UserPurchase');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserPurchase);

		parent::tearDown();
	}

}
