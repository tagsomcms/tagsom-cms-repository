<?php
App::uses('UserInvite', 'Model');

/**
 * UserInvite Test Case
 */
class UserInviteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_invite',
		'app.user',
		'app.user_mode_grade',
		'app.user_mode_status',
		'app.user_purchase',
		'app.user_story_level'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserInvite = ClassRegistry::init('UserInvite');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserInvite);

		parent::tearDown();
	}

}
