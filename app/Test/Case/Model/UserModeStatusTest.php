<?php
App::uses('UserModeStatus', 'Model');

/**
 * UserModeStatus Test Case
 */
class UserModeStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_mode_status',
		'app.language',
		'app.mode_question_answer',
		'app.story',
		'app.user_mode_grade',
		'app.user',
		'app.user_invite',
		'app.user_purchase',
		'app.user_story_level',
		'app.level',
		'app.mode',
		'app.grade'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserModeStatus = ClassRegistry::init('UserModeStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserModeStatus);

		parent::tearDown();
	}

}
