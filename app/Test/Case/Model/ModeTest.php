<?php
App::uses('Mode', 'Model');

/**
 * Mode Test Case
 */
class ModeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mode',
		'app.mode_question_answer',
		'app.user_mode_grade',
		'app.user_story_level'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mode = ClassRegistry::init('Mode');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mode);

		parent::tearDown();
	}

}
