<?php
App::uses('Story', 'Model');

/**
 * Story Test Case
 */
class StoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.story',
		'app.language',
		'app.mode_question_answer',
		'app.user_mode_grade',
		'app.user_mode_status',
		'app.user_purchase',
		'app.user_story_level'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Story = ClassRegistry::init('Story');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Story);

		parent::tearDown();
	}

}
