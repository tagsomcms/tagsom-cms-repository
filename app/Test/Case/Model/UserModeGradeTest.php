<?php
App::uses('UserModeGrade', 'Model');

/**
 * UserModeGrade Test Case
 */
class UserModeGradeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_mode_grade',
		'app.user',
		'app.user_invite',
		'app.user_mode_status',
		'app.user_purchase',
		'app.user_story_level',
		'app.level',
		'app.story',
		'app.language',
		'app.mode_question_answer',
		'app.mode'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserModeGrade = ClassRegistry::init('UserModeGrade');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserModeGrade);

		parent::tearDown();
	}

}
