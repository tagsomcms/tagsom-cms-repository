<?php
App::uses('UserStoryLevelsController', 'Controller');

/**
 * UserStoryLevelsController Test Case
 */
class UserStoryLevelsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_story_level',
		'app.user',
		'app.user_invite',
		'app.user_mode_grade',
		'app.level',
		'app.user_mode_status',
		'app.language',
		'app.mode_question_answer',
		'app.mode',
		'app.story',
		'app.user_purchase',
		'app.grade'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
