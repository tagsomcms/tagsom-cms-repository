<?php
/**
 * UserModeGrade Fixture
 */
class UserModeGradeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'level_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'story_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'language_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'mode_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'grade' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'timestamp', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'level_id' => 1,
			'story_id' => 1,
			'language_id' => 1,
			'mode_id' => 1,
			'grade' => 1,
			'created' => '2016-03-30 10:38:46',
			'modified' => 1459314526
		),
	);

}
