<html lang="en" class="no-js">
<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('Change Scene'); ?></h1>
  <ol class="breadcrumb">
      <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">Change Scene</li>
  </ol>
</section>
<!-- End Breadcrumb -->

<?php $rand_no=rand(111,999); ?>
<section class="content">
  <div class="stories row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <?php echo $this->Form->create('Scene',array('type' => 'file')); ?>
        <div id="dvPreview"></div>
        <div class="box-body" style="height:420px;">
		
		<div class="row" style="margin-left:15px;">
		 <img alt="" src="<?php echo $this->base . "/app/webroot/uploads/".$story_path."/ChapterImage/". $scene_name.'?'.$rand_no; ?>" width="220px" height="130px" id="img2">
        </div>
		
          <div class="row">
          <div class="col-md-4" style="clear:none !important;">
          <label>Select Story Image
          </label>
        <?php
            echo $this->Form->input('image',array('type'=>'file','id'=>'file-7','label'=>false,'required'=>true,'onchange'=>'return ValidateImageExtension1(this,"file-7")')); ?>
            <div class="col-md-7" style="width:100%;margin-top:-10px;">
                <span id="lblErrorfile-7" style="color: red;"></span>
            </div>
          </div>
       
        </div>

        

        <div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'Stories','action'=> 'images/'.$sid.'/'.$language), array('class'=>'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
        </div>
        <div class="col-xs-2" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Form->Button(__("Cancel Upload"),array('type'=>'button','id'=>'cancel_btn','class'=>'btn btn-primary','onclick'=>'cancelupload()')); ?>
        </div>
        
        
        </div>


            <?php echo $this->Form->end(); ?>
        </div>
        <br><br>
      </div>
    </div>
  </div>
</section>
