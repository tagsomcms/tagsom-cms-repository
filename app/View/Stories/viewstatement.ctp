<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('View Statement'); ?></h1>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?></li>
    <li><?php echo $this -> Html -> link('Statements', array('controller' => 'stories', 'action' => 'statements/'.$sid)); ?>
    </li>
    <li class="active">View Statements</li>
  </ol>
</section>
<!-- End Breadcrumb -->

<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
  <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
  <thead>
  <tr>
      <th><?php echo ('Statement'); ?></th>
      <th><?php echo ('Audio Clip'); ?></th>
      <th><?php echo __('Save Changes'); ?></th>
      <th class="actions"><?php echo __('Delete'); ?></th>
  </tr>
  </thead>
  <tbody>
    
    <?php
      foreach($statements as $st)
      {
          echo $this->Form->create('Statement',array('type' => 'file')); ?>
            <tr>
              <td>
                  <?php
                    echo $this->Form->input('statement',array('type'=>'textarea','value'=>$st['StoryStatement']['statement'],'label'=>false,'id'=>'st'.$st['StoryStatement']['id']));
                  ?>
                  <?php
                    echo $this->Form->input('hide',array('type'=>'hidden','value'=>$st['StoryStatement']['id'],'label'=>false));
                  ?>

              </td>
              <td>
                  <?php
                    echo $this->Form->input('audio_clip',array('type'=>'file','required'=>false,'label'=>false,'onchange'=>'editstatement(this)','id'=>$st['StoryStatement']['id']));
                  ?>
                 <div class="col-xs-7" style="margin-left:5px;margin-top:3px;">
                      <span id="lblError<?php echo $st['StoryStatement']['id']; ?>" style="color: red;"></span>
                  </div>
              </td>
              <td >
                <?php echo $this->Form->submit('Save Changes',array('type'=>'submit', 'class'=>'btn btn-primary','style'=>'margin-top:-12px;')); ?>
              </td>
              <td class="actions" style="margin-top:10px;">
                  <?php echo $this->HTML->Link(__('Delete'), array('action' => 'deletestatement', $st['StoryStatement']['id'],$sid,$img_id), array('confirm' => __('Are you sure you want to delete this statement?', $st['StoryStatement']['id'],$sid,$img_id))); ?>
              </td>
            <tr>
          <?php
          echo $this->Form->end();
      }
    ?>

  </tbody>
  </table>
 
</div><br>
<?php echo $this->Html->link("Back", array('controller' => 'stories','action'=> 'statements', $sid), array( 'class' => 'btn btn-primary')); ?>
<br>
</div>
</div>
</section>
<!-- /Main content -->



<script>
        function editstatement(file)
        {
  
         var id= file.getAttribute('id');
    
               var allowedFiles = [".wav"];
              var lblError = document.getElementById("lblError"+id);
              var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
              if (!regex.test(document.getElementById(id).value.toLowerCase())) {
                  
                  var newElm = document.createElement('input');
                  var old = document.getElementById(id);
                  newElm.type = "file";
                  newElm.id = id;
                  newElm.name = old.name;
                  newElm.className = old.className;
                  old.parentNode.replaceChild(newElm, old);
                  lblError.innerHTML = "Please upload audio files only.";
                  setTimeout(function () {lblError.style.display='none'}, 3000);
                  return false;
              }
              lblError.innerHTML="";
              return false;
      }

</script>
<style>
table td.actions a {
    margin: 0 6px;
    padding: 7px 7px;
}

</style>
