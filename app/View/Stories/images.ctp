<html lang="en" class="no-js">
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
<!-- Start Breadcrumb -->

<section class="content-header">
  <h1><?php echo __('Add Story Images')." (Story : ".$storyname.") "; ?></h1>
  <ol class="breadcrumb">
      <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">Add Images</li>
  </ol>
</section>
<!-- End Breadcrumb -->

<?php 
if($language=='English' && $this->Session->read('Auth.User.type')=='admin')
{
?>
<section class="content">
  <div class="stories row">
    <div class="col-xs-12">
      <div class="box box-primary" id="bx1">
        <?php echo $this->Form->create('Story',array('type' => 'file')); ?>
        <div id="dvPreview"></div>
        <div class="box-body" style="height:180px;">
          <div class="row">
          <div class="col-md-4" style="clear:none !important;">
          <label>Select Story Image
          </label>
        <?php
            echo $this->Form->input('image',array('type'=>'file','id'=>'file-7','label'=>false,'required'=>true,'onchange'=>'return ValidateImageExtension1(this,"file-7")','style'=>'height:inherit;')); ?>
            <div class="col-md-7" style="width:100%;margin-top:-10px;">
                <br><span id="lblErrorfile-7" style="color: red;"></span>
            </div>
          </div>
          <div class="col-md-2" style="clear:none !important;">
            <?php echo $this->Form->input('image_number',array('type'=>'number','required'=>true,'id'=>'img_number','min'=>0)); ?>
          </div>
        </div>

        <div class="row">
          <img src="" width="220px" height="130px"  id='img2' style="margin-left:15px;">
        </div>

        <div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'Stories','action'=> 'index'), array('class'=>'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
        </div>
        <div class="col-xs-2" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Form->Button(__("Cancel Upload"),array('type'=>'button','id'=>'cancel_btn','class'=>'btn btn-primary','onclick'=>'cancelupload()')); ?>
        </div>
        
        
        </div>


            <?php echo $this->Form->end(); ?>
        </div>
        <br><br>
      </div>
    </div>
  </div>
</section>
<!-- /Main content -->
<?php } ?>
</head>

<?php
if(!empty($image))
{
$size=sizeof($image);
$row=ceil($size/5);
$padding=$row*60;
?>
<section class="content">
  <div class="stories row">
      <div class="col-md-12">
        <div class="box box-primary">
            <h3 style="margin-left:15px;">Manage Scenes</h3>
            <div class="carousel slide article-slide" id="article-photo-carousel" style="margin-left:15px;">
              <div class="carousel-inner cont-slider">
                <?php
                $i=0;
                foreach($image as $img)
                {
                  $rand_no=rand(1111,9999);
                    $i++;
                    if($i==1)
                    {
                        ?>
                        <div class="item active">
                          <div id="container">
                            <div class="row">
                              <div class="col-md-9">
                                <div class="boxed">
                                  <?php echo $img['StoryImage']['image_number']; ?>
                                </div>
                               
                                 <img alt="" src="<?php echo $this->base . "/app/webroot/uploads/".$storyname."/ChapterImage/". $img['StoryImage']['image'].'?'.$rand_no; ?>" width="600px" height="460px">
                                </div>
                              <div class="col-md-3 pull-right" style="padding-top:<?php echo $padding; ?>px;">
                                <?php 
                                if($language=='English' && $this->Session->read('Auth.User.type')=='admin')
                                {
                                echo $this->Form->create('ImageNumber'); 
                                echo $this->Form->input('image_number',array('type'=>'number','label'=>'Update Image Number','value'=>$img['StoryImage']['image_number'],'min'=>0));
                                echo $this->Form->input('h1',array('type'=>'hidden','value'=>$img['StoryImage']['image_number']));
                                echo $this->Form->submit('Replace Image',array('class'=>'btn btn-primary'));
                                echo $this->Form->end();
                                }
                                ?>
                              </div>
                            </div>
                          </div>

                          <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
                          <tbody>
                            <tr>
                            <td class="actions">
                                <?php echo $this->Html->link(__('Back'), array('action' => 'index'));  ?>
                                <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin'){ echo $this->Html->link(__('Remove Scene'), array('action' => 'deleteimage', $img['StoryImage']['id'],$img['StoryImage']['story_id']), array('confirm' => __('Are you sure you want to delete this scene?')));
                                   } ?>
								                <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin'){ echo $this->Html->link(__('Change Scene'), array('action' => 'changescene', $img['StoryImage']['id'],$img['StoryImage']['story_id'],$language)); } ?>
                                <?php echo $this->Html->link(__('Manage Text And Audio'), array('action' => 'statements', $img['StoryImage']['story_id'],$img['StoryImage']['id'],$img['StoryImage']['image_number'],$language)); ?>
                                <font size="4px"><?php echo $language. " Story"; ?></font>
                            </td>
                          </tr>
                          </tbody>
                          </table>

                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="item">
                          <div id="container">
                            <div class="row">
                              <div class="col-md-9">
                                <div class="boxed">
                                  <?php echo $img['StoryImage']['image_number']; ?>
                                </div>
                              
                                 <img alt="" src="<?php echo $this->base . "/app/webroot/uploads/".$storyname."/ChapterImage/". $img['StoryImage']['image'].'?'.$rand_no; ?>" width="600px" height="460px">
                                </div>
                               <div class="col-md-3 pull-right" style="padding-top:<?php echo $padding; ?>px;">
                                <?php 
                                if($language=='English' && $this->Session->read('Auth.User.type')=='admin')
                                {
                                echo $this->Form->create('ImageNumber'); 
                                echo $this->Form->input('image_number',array('type'=>'number','label'=>'Update Image Number','value'=>$img['StoryImage']['image_number'],'min'=>0));
                                echo $this->Form->input('h1',array('type'=>'hidden','value'=>$img['StoryImage']['image_number']));
                                 echo $this->Form->submit('Replace Image',array('class'=>'btn btn-primary'));
                                echo $this->Form->end();
                                }
                                ?>
                              </div>
                            </div>
                          </div>

                         <table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
                          <tbody>
                            <tr>
                            <td class="actions">

                               <?php  echo $this->Html->link(__('Back'), array('action' => 'index'));  ?>
                                 <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin'){ echo $this->Html->link(__('Remove Scene'), array('action' => 'deleteimage', $img['StoryImage']['id'],$img['StoryImage']['story_id']), array('confirm' => __('Are you sure you want to delete this scene?')));
                                   } ?>
								                <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin'){  echo $this->Html->link(__('Change Scene'), array('action' => 'changescene', $img['StoryImage']['id'],$img['StoryImage']['story_id'],$language)); } 
                                 ?>
                                <?php echo $this->Html->link(__('Manage Text And Audio'), array('action' => 'statements', $img['StoryImage']['story_id'],$img['StoryImage']['id'],$img['StoryImage']['image_number'],$language)); ?>
                                <font size="4px"><?php echo $language. " Story"; ?></font>
                            </td>
                          </tr>
                          </tbody>
                          </table>
                        </div>

                        <?php
                    }
                }
                ?>

              </div>
              <ol class="carousel-indicators" style="bottom: inherit;left: inherit;top: 0;right: -185px;width: 375px;">
                <?php
                $j=-1;
                foreach($image as $img)
                {
                  $rand_no=rand(1111,9999);
                    $j++;
                    if($j==0)
                    {
                        ?>      
                        <li class="active" data-slide-to="0" data-target="#article-photo-carousel" >
                          <div class="boxed1">
                                <?php echo $img['StoryImage']['image_number']; ?> 
                          </div>
                           <img alt="" src="<?php echo $this->base . "/app/webroot/uploads/".$storyname."/ChapterImage/". $img['StoryImage']['image'].'?'.$rand_no; ?>" style="height:50px;width:70px;">
                        </li>
                        <?php
                    }
                    else
                    {
                        ?>
                        <li class="" data-slide-to="<?php echo $j; ?>" data-target="#article-photo-carousel">
                          <div class="boxed1">
                              <?php echo $img['StoryImage']['image_number']; ?>
                          </div>
                         <img alt="" src="<?php echo $this->base . "/app/webroot/uploads/".$storyname."/ChapterImage/". $img['StoryImage']['image'].'?'.$rand_no; ?>"  style="height:50px;width:70px;">
                        </li>
                        <?php
                    }
                }
                ?>
              </ol>
            
            </div>
            <br>
            <?php 
            if($language=='English' && $this->Session->read('Auth.User.type')=='admin')
            {
              echo $this->Html->link(__('Resize Chapter Images'), array('action' => 'resize_images',$id),array('class'=>'btn btn-primary','style'=>'margin-left:25px;margin-bottom:15px;')); 
            }
            ?>
            <br>
        </div>

    </div>

</div>
</div>
<?php 
}
?>
  
<script>
$('.carousel').carousel({
  interval: false
});
</script>


<style>
form div {
    clear: both;
    /* margin-bottom: -1.2em; */
   padding: .0em; 
    vertical-align: text-top;
}
.carousel {
    width: 800px;
}
.article-slide .carousel-indicators {
    bottom: 0;
    left: 0;
    margin-left: 5px;
    width: 420px;
}
.boxed {
    border: 0.2px solid black;
    background-color: white;
    width: 80px;
    height: 80px;
    text-align: center;
    margin-top: 0px;
    position: fixed;
    font-size: 50px;
}
.boxed1 {
    border: 0.1px solid black;
    background-color: white;
    width: 25px;
    height: 25px;
    text-align: center;
    margin-top: 0px;
    position: absolute;
    font-size: 15px;
    color: black;
    Z-INDEX: 1;
    text-indent: 0px;
}
</style>

<script>
$(document).ready(function() {
  $("#img_number")
  .keyup(function() {
    var img_no=$('#img_number').val();
    if(img_no.length>0)
    {
      document.getElementById("myBtn").disabled = false;
      document.getElementById("cancel_btn").disabled = false;
    }
    else
    {
      if(document.getElementById("file-7").value == "")
      {
      document.getElementById("myBtn").disabled = true;
      document.getElementById("cancel_btn").disabled = true;
      }
    }
  });
    $("#img_number")
  .focusout(function() {
    var img_no=$('#img_number').val();
    if(img_no.length>0)
    {
      document.getElementById("myBtn").disabled = false;
      document.getElementById("cancel_btn").disabled = false;
    }
    else
    {
      if(document.getElementById("file-7").value == "")
      {
      document.getElementById("myBtn").disabled = true;
      document.getElementById("cancel_btn").disabled = true;
      }
    }
  });
  
  document.getElementById("myBtn").disabled = true;
  document.getElementById("cancel_btn").disabled = true;
  $("#img2").hide();
});       
</script>  