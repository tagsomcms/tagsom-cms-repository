
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Story'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>



<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
 		<div class="box">
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($story2['Story']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('Language'); ?></th>
		<td>
			<?php echo $this->Html->link($story2['Story']['language_id'], array('controller' => 'languages', 'action' => 'view', $story2['Story']['language_id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Story Name'); ?></th>
		<td>
			<?php echo h($story2['Story']['story_name']); ?>
		</td></tr>
    <tr><th><?php echo __('Introduction'); ?></th>
    <td>
      <?php 
      if(!empty(h($story2['Story']['introduction']))) {echo h($story2['Story']['introduction']);}
      else{echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.'; }

      ?>
    </td></tr>
    <tr><th><?php echo __('Story Image'); ?></th>
    <td>
      <?php if(!empty($story2['Story']['image'])) { ?>
          <img src="<?php echo $this->base . "/app/webroot/uploads/".$storyname."/StoryImage/StoryImage.jpg"; ?>" width="220px" height="130px"> 
      <?php } 
      else
      { ?>
          <img src="<?php echo $this->base . "/app/webroot/uploads/default_picture.png"; ?>" width="220px" height="130px"> 
      <?php }
      ?>
        
     
     
    </td></tr>
		<tr><th><?php echo __('Last Modified'); ?></th>
		<td>
			<?php echo h($story2['Story']['modified']); ?>
		</td></tr>

    <tr>
      <td colspan=2><?php echo $this->Html->link("Back", array('action'=> 'index'), array( 'class' => 'btn btn-primary')); ?></td>
    </tr>
    
	</table>
  <br>
 
</div>
</div>
</div>



