<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Story'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Story',array('type' => 'file')); ?>
<div class="box-body">
  <div class="col-md-8">
	  <?php echo $this->Form->input('story_name',array('required'=>true,'type'=>'textarea','class'=>'req')); ?>
  </div>
  <div class="col-xs-8">
    <?php  echo $this->Form->input('introduction',array('required'=>false,'type'=>'textarea','style'=>'height:150px;')); ?>
  </div>
  <div class="col-xs-8">
    <?php echo $this->Form->input('image',array('required'=>false,'type'=>'file','id'=>'story_img','onchange'=>'return ValidateImageExtension(this,"story_img")','style'=>'height:100%;'));?>
  </div>
  <div class="col-xs-7" style="margin-left:5px;">
                <span id="lblErrorstory_img" style="color: red;"></span>
  </div>
  <div class="row">
    <div class="col-xs-1" style="clear:none !important;">
      <?php echo $this->Form->submit('Save',array('class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
      <?php echo $this->Html->link("Back", array('controller' => 'Stories','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section><!-- /.content -->



<style>

.col-md-8 label:after {
  color: #e32;
  content: '*';
  display:inline;
}
</style>
