<html lang="en" class="no-js">
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('Add Statement'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
      <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?></li>
    <li><?php echo $this -> Html -> link('Statements', array('controller' => 'stories', 'action' => 'statements/'.$sid)); ?>
    </li>
    <li class="active">Add Statement</li>
  </ol>
</section>
<!-- End Breadcrumb -->


<!-- Main content -->
<section class="content">
  <div class="statements row">
 	  <div class="col-xs-12">
	    <div class="box box-primary">
        <?php echo $this->Form->create('Story',array('type' => 'file')); ?>
        <div class="box-body"> 
        	<?php
        		echo $this->Form->input('statement',array('type'=>'textarea','required'=>true));
            ?>
              <label for="file-7" style="font-size:100% !important;"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="30" height="22" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Select Audio File&hellip;</strong>
              </label>
            <?php
            echo $this->Form->input('audio_clip',array('type'=>'file','id'=>'file-7','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>false,'required'=>true,'onchange'=>'return ValidateAudioExtension(this,"file-7")'));?>
            <div class="col-xs-7" style="margin-left:5px;">
                <span id="lblErrorfile-7" style="color: red;"></span>
            </div>
        		<div class="row">
              <div class="col-xs-1" style="clear:none !important;">
                <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
              </div>
              <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
              <?php echo $this->Html->link("Back", array('controller' => 'Stories','action'=> 'statements/'.$sid), array( 'class' => 'btn btn-primary')); ?>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

<div class="statements row">
  <h3 style="margin-left:15px;"><b>Statements:</b></h3>
  <?php if(empty($arr_term)){ ?>
  <div class="row well" style="margin-right:15px;margin-left:15px;background-color: white;">
    <div class="col-xs-8">
      <h3> No statements are there in story </h3>
    </div>
  </div>
  <?php } ?>
  <?php foreach($arr_term as $term): ?>
  <div class="row well" style="margin-right:15px;margin-left:15px;background-color: white;">
    <div class="col-xs-8">
      <?php 
          echo "<h4 style='color:#2C6877;'>".$term."</h4>"; 
      ?>
    </div>
  </div>
  <?php endforeach; ?>
</div>

</section>
<!-- /Main content -->
</html>

