<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css"
        rel="stylesheet" type="text/css" />
<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.css"
        rel="stylesheet" type="text/css" />
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Story'); ?></h1>
 <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('Story',array('type'=>'file')); ?>
	<div class="box-body">
	<?php
		echo $this->Form->input('id');   
	?>
  <div class="col-md-8">
     <?php echo $this->Form->input('story_name'); ?>
     <?php echo $this->Form->input('h1',array('type'=>'hidden','value'=>'')); ?>
  </div>
  <div class="col-xs-8">
    <?php 
    if(!empty($introduction_old))
    { 
      echo $this->Form->input('introduction',array('required'=>false,'type'=>'textarea','style'=>'height:150px;'));
    }
    else
    {
      echo $this->Form->input('introduction',array('required'=>false,'type'=>'textarea','style'=>'height:150px;','value'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.'));
    } 
    ?>
    
  </div>
  <div class="col-xs-8">
    <?php if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/StoryImage/'.$image))
    { 
      $rand_no=rand(1111,9999);
      ?>
      <img src="<?php echo $this->base . "/app/webroot/uploads/".$story_path."/StoryImage/". $image.'?'.$rand_no; ?>" width="220px" height="130px" style="margin-left:8px;" id='img2'>
    <?php }
    else
    {
    ?>
    <img src="<?php echo $this->base . "/app/webroot/uploads/default_picture.png"; ?>" width="220px" height="130px" style="margin-left:8px;" id='img2'>
    <?php } ?>
    <?php  echo $this->Form->input('image',array('required'=>false,'type'=>'file','id'=>'story_img','onchange'=>'return ValidateImageExtension(this,"story_img")','label'=>false,'style'=>'color:transparent;')); ?>

    <div class="col-xs-7" style="margin-left:5px;">
                <span id="lblErrorstory_img" style="color: red;"></span>
  </div>
  </div>
  </div> 

  <div class="container-fluid">
  <div class="row" style="margin-top:-35px;">
    <div class="col-md-6">
        <h5><b>Add Language</b></h5>
        <select class="form-control" id="tagPicker" multiple="multiple" name="Language[]" >
          <?php 
            foreach($languages as $language)
            {
              if(in_array($language, $lang_array))
              {
                ?>
                 <option value="<?php echo $language; ?>" selected style="height:30px !important;"><?php echo $language; ?></option>
                <?php
              }
              else
              {
              ?>
               <option value="<?php echo $language; ?>" style="height:30px !important;"><?php echo $language; ?></option>
              <?php
              }
            }
          ?>
         
        </select>
    </div> 
  </div>
</div>

	<div class="row">
      <div class="col-xs-1" style="clear:none !important;">
        <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary')); ?>
      </div>
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
         <?php echo $this->Html->link("Back", array('controller' => 'stories','action'=> 'index'), array( 'class' => 'btn btn-primary','style'=>'margin-left:20px;')); ?>
      </div>
    </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->




<script>
//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){
           
  /* dropdown and filter select */
  var select = $('#select2').select2();
  
  /* Select2 plugin as tagpicker */
  $("#tagPicker").select2({
    closeOnSelect:true
  });

}); //script  
$(document).ready(function() {

});       
</script>      

<style>
.select2-search-choice{
      height: 30px !important;
}
form div {
    clear: both;
    margin-bottom: 0em; 
    padding: .5em;
    vertical-align: text-top;
}
.row {
    margin-right: -15px;
    margin-left: 0px;
}
</style>

