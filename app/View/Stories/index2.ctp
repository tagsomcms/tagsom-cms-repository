<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="row">
		<div class="col-sm-1" style="font-size:25px;"><?php echo __('Stories'); ?></div>
		<div class="col-sm-1" style="margin-left:15px;"><img src="<?php echo $this->base; ?>/app/webroot/img/u10.png" width="30px" height="35px"></div>
		<div class="col-sm-1">

		<?php
			if(isset($selected_language))
			{
				$select=$selected_language;
			}
			else
			{
			$select=1;
			}
			echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
			echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','value'=>$select,'options'=>$language_array,'style'=>'width: 200px;height:33px;margin-top: -5px;','onchange'=>'StoryLanguage.submit();'));
		?>
		</div>
	</div>


  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<?php
if($language_name!='English')
{
$array=array();
	foreach($english_stories as $story)
	{
		foreach($stories as $st)
		{
			if($story['Story']['story_name']==$st['Story']['story_name'])
			{
				array_push($array,$st);
			}
		}	
	}
}
else
{
	$array=$stories;
}
?>


<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box">	
 
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('story_name'); ?></th>
			<th><?php echo $this->Paginator->sort('Last modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>

	<?php 
	

	foreach ($array as $story): ?>
	<tr>
		<td><?php echo h($story['Story']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($language_name, array('controller' => 'languages', 'action' => 'view', $story['Story']['language_id'])); ?>
		</td>
		<td><?php echo h($story['Story']['story_name']); ?>&nbsp;</td>
		<td><?php echo h($story['Story']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Manage Scenes'), array('action' => 'images', $story['Story']['id'],$language_name)); ?>
			<?php if($this->Session->read('Auth.User.type')=='admin') { if($language_name=='English') { echo $this->Html->link(__('View'), array('action' => 'view', $story['Story']['id'])); } ?>
			<?php  if($language_name=='English') { echo $this->Html->link(__('Edit'), array('action' => 'edit', $story['Story']['id'])); } ?>
			<?php  if($language_name=='English') { echo $this->Html->link(__('Delete'), array('action' => 'delete', $story['Story']['id']), array('confirm' => __('Are you sure you want to delete this story?', $story['Story']['id']))); }
				}?>
			<?php echo $this->Html->link(__('Introduction'), array('action' => 'introduction_audio', $story['Story']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
</div>
</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
       "dom": '<f<t>ip>'
    } );
});
</script>