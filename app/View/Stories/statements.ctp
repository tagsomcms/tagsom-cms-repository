<?php
header ('Content-Type: text/html; charset=UTF-8'); 
echo '<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />'; 
?>

<body onload="f3()">
<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('Manage Text & Audio'); ?></h1>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    </li>
    <li class="active">Statements</li>
  </ol>
</section>
<!-- End Breadcrumb -->


<?php 

foreach($image as $img)
{
  $img_path=$img['StoryImage']['image'];
}
?>
<?php $rand_no=rand(111,999); ?>
 <img alt="" src="<?php echo $this->base . "/app/webroot/uploads/".$storyname."/ChapterImage/". $img_path.'?'.$rand_no; ?>" width="500px" height="340px" style="border-radius: 5px;margin-left:15px;">



<div class="row" style="margin-top:10px;">
  <div class="col-md-1">
  <?php echo $this->Html->link(__('Back'), array('action' => 'images/'.$sid.'/'.$language,'class'=>'btn btn-primary'),array('style'=>'background-color: #3C8DBC;color: white;border-radius: 3px;padding: 4px;margin-left:15px;')); ?>
  </div>
  <?php if($this->Session->read('Auth.User.type')=='admin')
  {
    ?>
    <div class="col-md-2 pull-left">
  <?php echo $this->Html->link(__('Remove Scene'), array('action' => 'deleteimage/'.$image_id.'/'.$sid),array('confirm' => __('Are you sure you want to delete this scene?'),'style'=>'background-color: #3C8DBC;color: white;border-radius: 3px;padding: 4px;margin-left:-10px;'));  ?>
  </div>
    <?php
  }
  ?>
  
</div>

 <!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 

<div class="row" style="margin-top:20px;">
  <div class="col-md-3">
        <?php
          if($image_number!=$first)
          {
             echo $this->Html->link(__('Previous Scene'), array('action' => 'statements/'.$sid.'/'.$previous_image_id.'/'.$previous.'/'.$language,'class'=>'btn btn-primary'),array('style'=>'background-color: #3C8DBC;color: white;border-radius: 3px;padding: 4px;margin-left:5px;')); 
          }
        ?>
  </div>
  <div class="col-md-7"></div>
  <div class="col-xs-2">
      <?php
        if($image_number!=$last)
        {
           echo $this->Html->link(__('Next Scene'), array('action' => 'statements/'.$sid.'/'.$next_image_id.'/'.$next.'/'.$language,'disabled'=>true,'class'=>'btn btn-primary'),array('style'=>'background-color: #3C8DBC;color: white;border-radius: 3px;padding: 4px;'));
        }
      ?>
  </div>
</div>  
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>

<tr>
<td colspan=2><b><?php echo $language." Story"; ?></b></td>
<td colspan=4><b><?php echo $language." Audio"; ?></b></td>
</tr>
<tr>
    <th>ID</th>
    <th>Statements:</th>
    <?php if($language=='English'){ ?>
    <th>Text</th>
    <?php } ?>
    <th>Audio</th>
    <th></th>
</tr>
</thead>
<tbody >

<?php

 foreach($statements as $statement)
 {
    if($statement['StoryStatement']['story_image_id']==$image_id)
    {
      
      ?>
      <tr>
        <?php  
        echo $this->Form->create('Statement',array('type'=>'file'));  
        ?>
        <td style="width:130px;"><?php echo $statement['StoryStatement']['id']; ?></td>
        <td id="st<?php echo $statement['StoryStatement']['id']; ?>">
          <?php echo $this->Form->input('statement',array('type'=>'textarea','label'=>false,'value'=>$statement['StoryStatement']['statement'])); ?></td>
        <td id="text<?php echo $statement['StoryStatement']['id']; ?>" <?php if($language=='Arabic') { ?> dir="rtl" <?php } ?>>
          <?php echo $statement['StoryStatement']['statement']; ?>
        <?php echo $this->Form->input('h1',array('type'=>'hidden','value'=>$statement['StoryStatement']['id'])); ?>
        <?php echo $this->Form->input('h2',array('type'=>'hidden','value'=>$statement['StoryStatement']['statement'])); 
        ?>
        <?php echo $this->Form->input('h3',array('type'=>'hidden','value'=>$statement['StoryStatement']['audio_clip'])); 
        ?>
        
        <?php
        if($statement['StoryStatement']['statement']=='No Translation'){
          echo '<hr>';
          foreach($english_statements as $english_statement)
          {
            if($statement['StoryStatement']['audio_clip']==$english_statement['StoryStatement']['audio_clip'])
            {
              echo "(".$english_statement['StoryStatement']['statement'].")";
            }
          }
        }
        ?>
        </td>
        <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin'){ ?>
        <td class="actions" style="width:50px;"><a href="#" id="edit<?php echo $statement['StoryStatement']['id']; ?>">Edit</a></td>
        <?php } ?>

        <?php if($statement['StoryStatement']['statement']=='No Translation'){?>
        <td >
          <?php echo $this->Form->input('audio_clip',array('type'=>'file','id'=>'upload','label'=>false,'style'=>'margin-top:-5px;width:inherit;height:inherit;','disabled'=>true)); ?>
        </td>
        <?php } else {  ?>
        <td>
          <?php echo $this->Form->input('audio_clip',array('type'=>'file','id'=>$statement['StoryStatement']['id'],'label'=>false,'style'=>'margin-top:-5px;width:inherit;height:inherit;','onchange'=>'return ValidateAudioExtension(this,'.$statement['StoryStatement']['id'].')')); ?>
          <div class="col-md-7" style="width:100%;margin-top:-10px;">
                <br><span id="lblError<?php echo $statement['StoryStatement']['id']; ?>" style="color: red;"></span>
            </div>
        </td>
        <?php } ?>


        <?php if($language=='English'){ ?>
<!--         <td class="actions" style="width:80px;"><?php echo $this->Form->submit('Save changes',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;')); ?></td> -->

        <?php if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$statement['StoryStatement']['audio_clip']))
          {
            $myAudioFile = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$statement['StoryStatement']['audio_clip'];
          ?>
         
            
            <td class="actions" style="width:80px;" id="btn<?php echo $statement['StoryStatement']['id']; ?>">
              <?php 
              if($statement['StoryStatement']['disable']!=1)
              {
                $rand_no=rand(1111,9999);
              ?>
              <audio controls="controls" id="<?php echo $statement['StoryStatement']['id']; ?>">  
                <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
              </audio> 

              <?php 
              }
              if($this->Session->read('Auth.User.type')=='admin' && $statement['StoryStatement']['disable']==1)
              {
                $rand_no=rand(1111,9999);
                ?>
                <audio controls="controls" id="<?php echo $statement['StoryStatement']['id']; ?>">  
                  <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
                </audio> 
                <?php
              }
              ?>
             <div class="row">
                <div class="col-md-6">
                  <?php echo $this->Form->submit('Replace Audio',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;','id'=>'upload'.$statement['StoryStatement']['id'],'disabled'=>true)); ?>
                </div>
                <div class="col-md-6">
                   <?php
                 echo $this->Html->link(__('Delete Audio'), array('action' => 'delete_audio', $story_path,$language,$statement['StoryStatement']['audio_clip'],$statement['StoryStatement']['story_id'],$image_id,$image_number), array('confirm' => __('Are you sure you want to remove audio file for this statement?')));
                    ?>
                </div>
              </div>

                
            </td>
        
        <?php }else{ ?>
           <td class="actions" style="width:80px;" id="btn<?php echo $statement['StoryStatement']['id']; ?>"><?php echo $this->Form->submit('Upload Audio',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;')); ?></td>

        <?php } ?>  
        <td class="actions" style="width:80px;" id="savebtn<?php echo $statement['StoryStatement']['id']; ?>"><?php echo $this->Form->submit('Save Changes',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;')); ?></td>



        <?php } else { 

          if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$statement['StoryStatement']['audio_clip']))
          {
            $myAudioFile = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$statement['StoryStatement']['audio_clip'];
            ?>
             <?php if($statement['StoryStatement']['statement']=='No Translation'){?>
            <td class="actions" style="width:80px;">
              <?php 
              if($statement['StoryStatement']['disable']!=1)
              {
                $rand_no=rand(1111,9999);
              ?>
              <audio controls="controls" id="<?php echo $statement['StoryStatement']['id']; ?>">  
                 <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>   
              </audio> <br>
              <?php 
              }
              if($this->Session->read('Auth.User.type')=='admin' && $statement['StoryStatement']['disable']==1)
              {
                $rand_no=rand(1111,9999);
                ?>
                <audio controls="controls" id="<?php echo $statement['StoryStatement']['id']; ?>">  
                  <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
                </audio> <br>
                <?php
              }
              ?>

              <?php echo $this->Form->submit('Replace Audio',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;', 'disabled'=>true)); 

              ?>
             
              </td>
            <?php } else {  
              $rand_no=rand(1111,9999);?>
            <td class="actions" style="width:80px;">
              <?php 
              if($statement['StoryStatement']['disable']!=1)
              {
                $rand_no=rand(1111,9999);
              ?>
              <audio controls="controls" id="<?php echo $statement['StoryStatement']['id']; ?>">  
                 <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
              </audio>
              <?php
              }
              if($this->Session->read('Auth.User.type')=='admin' && $statement['StoryStatement']['disable']==1)
              {
                $rand_no=rand(1111,9999);
                ?>
                <audio controls="controls" id="<?php echo $statement['StoryStatement']['id']; ?>">  
                  <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
                </audio> 
                <?php
              }
              ?> 
              <div class="row">
                <div class="col-md-6">
                  <?php echo $this->Form->submit('Replace Audio',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;','id'=>'upload'.$statement['StoryStatement']['id'],'disabled'=>true)); ?>
                </div>
                <div class="col-md-6">
                   <?php
                 echo $this->Html->link(__('Delete Audio'), array('action' => 'delete_audio', $story_path,$language,$statement['StoryStatement']['audio_clip'],$statement['StoryStatement']['story_id'],$image_id,$image_number), array('confirm' => __('Are you sure you want to remove audio file for this statement?')));
                    ?>
                </div>
              </div>
             
               <br><br>
    

            </td>
            <?php } } else { ?>
            <?php if($statement['StoryStatement']['statement']=='No Translation'){?>
            <td class="actions" style="width:80px;"><?php echo $this->Form->submit('Upload Audio',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;', 'disabled'=>true)); ?></td>
            <?php } else { ?>
            <td class="actions" style="width:80px;"><?php echo $this->Form->submit('Upload Audio',array('class'=>'btn btn-primary','style'=>'margin-top:-21px;','id'=>'upload'.$statement['StoryStatement']['id'],'disabled'=>true)); ?></td>
            <?php } ?>

        <?php } }?>


        <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin'){ ?>
        <td class="actions" style="width:90px;"><?php echo $this->Html->link(__('Delete Text & Audio'), array('action' => 'deletestatement/'.$statement['StoryStatement']['id'].'/'.$statement['StoryStatement']['story_id'].'/'.$image_id.'/'.$image_number),array('confirm' => __('Are you sure you want to remove this statement?'))); ?>
          <br><br>
          <?php 
          if($statement['StoryStatement']['disable']==0)
          {
            echo $this->Html->link(__('Disable Audio for VA'), array('action' => 'disable', $statement['StoryStatement']['id'],$sid,1,$image_number,$image_id));
          }
          else
          {
            echo $this->Html->link(__('Enable Audio for VA'), array('action' => 'disable', $statement['StoryStatement']['id'],$sid,0,$image_number,$image_id));
          }
          ?>
          
        </td>
        <?php }?>

        <?php  echo $this->Form->end(); ?>
      </tr>
     
      <?php
    }
 }
 $img_id=null;
 ?>
 <?php if($language=='English' && $this->Session->read('Auth.User.type')=='admin') { ?>
  <tr>
    <td>Add New Statement:</td>
    <td  colspan=3>
      <?php
      echo $this->Form->create('statement',array('type'=>'file'));  
      echo $this->Form->input('statement',array('type'=>'textarea','label'=>false,'style'=>'margin-top:-10px;','required'=>true));
      ?>
    </td>
    <td>

      <?php
      echo $this->Form->input('audio_clip',array('type'=>'file','id'=>'file-7','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>false,'required'=>false,'onchange'=>'return ValidateAudioExtension(this,"file-7")','style'=>'margin-top:-4px;width:inherit;height:inherit;'));?>
      <div class="col-xs-12">
          <span id="lblErrorfile-7" style="color: red;"></span>
      </div>
    </td>
    <td class="actions">
        <?php echo $this->Form->Submit(__('Add Statement'), array('class' => 'btn btn-primary','style'=>'margin-top:-20px;')); 
              echo $this->Form->end();
        ?>
    </td>
  </tr>
  <?php } ?>
  </tbody>
  </table>
    </table>
    <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>
</div>
</section><!-- /.content -->
</body>


<?php
foreach($statements as $statement)
{
$st_id=$statement['StoryStatement']['id'];
?>    
<script>
$(document).ready(function() {
$('#st'+<?php echo $st_id; ?>).hide();
$('#savebtn'+<?php echo $st_id; ?>).hide();
});    
$('#edit'+<?php echo $st_id; ?>).click(function(){
$('#btn'+<?php echo $st_id; ?>).toggle();
$('#savebtn'+<?php echo $st_id; ?>).toggle();

$('#st'+<?php echo $st_id; ?>).toggle();
$('#text'+<?php echo $st_id; ?>).toggle();
}); 
</script>    
<?php 
}
?>


<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}


function open_fileupload(){
  $('#upload').trigger('click'); 
}
function open_fileupload1(){
  $('#file-7').trigger('click'); 
}

</script>

<style>
audio, canvas, progress, video {
    display: inline-block;
    vertical-align: baseline;
    margin-bottom: 20px;
}
</style>


