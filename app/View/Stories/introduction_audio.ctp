<?php
header ('Content-Type: text/html; charset=UTF-8'); 
echo '<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />'; 
?>

<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Introduction Audio For Story'); ?></h1>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'stories', 'action' => 'index')); ?>
    <li class="active">Introduction Audio</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="modes row">
  <div class="col-xs-12">
  <div class="box box-primary">
<?php echo $this->Form->create('Introduction',array('type'=>'file')); ?>
  <div class="box-body">
     <div class="row">
        <div class="col-md-6" style="margin-left:10px;"><p style="font-size: 110%;margin-bottom: 3px;font-weight: 700;">Introduction Text : </p>
         <p style="text-align:left;" <?php if($language_name=='Arabic') { ?> dir="rtl" <?php } ?>> <?php echo $introduction; ?></p>
        </div>
      </div>
    <div class="row">
        <div class="col-md-6">
          <?php  echo $this->Form->input('audio_clip',array('label'=>'Introduction Audio','type'=>'file','id'=>'intro_audio','onchange'=>'return ValidateAudioExtension(this,"intro_audio")','style'=>'height:100%;')); ?>
        </div>
        <div class="col-md-6" style="width:100%;margin-top:-40px;margin-left:7px;">
                <br><span id="lblErrorintro_audio" style="color: red;"></span>
        </div>
      
      <div class="col-xs-2" style="margin-left:0px;">
        <?php 
          if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/'.$language_name.'/introduction.wav'))
          {
            $rand_no=rand(1111,9999);
            $myAudioFile = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/'.$story_path.'/Introduction_Audio/'.$language_name.'/introduction.wav';
            ?>
            <?php if($disable!=1) { ?>
            <audio controls="controls" style="margin-left: 7px;margin-top: -20px;">  
                 <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />   
              </audio> <br>
            <?php } 
            if($disable==1 && $this->Session->read('Auth.User.type')=='admin')
            {?>
              <audio controls="controls" style="margin-left: 7px;margin-top: -20px;">  
                 <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />   
              </audio> <br>
            <?php }
            ?>
            <?php
            echo $this->Form->submit(__('Replace Audio'),array('class'=>'btn btn-primary'));

            if($this->Session->read('Auth.User.type')=='admin')
            {
              if($disable==1)
              {
              echo $this->Html->link("Enable Audio for VA", array('controller' => 'Stories','action'=> 'disable_introduction/'.$id.'/0'), array( 'class' => 'btn btn-primary','style'=>'margin-left:10px;'));
              }
              else
              {
               echo $this->Html->link("Disable Audio for VA", array('controller' => 'Stories','action'=> 'disable_introduction/'.$id.'/1'), array( 'class' => 'btn btn-primary','style'=>'margin-left:10px;'));
              }
            }
          } 
          else
          {
            echo $this->Form->submit(__('Upload Audio'),array('class'=>'btn btn-primary'));
          }
        ?>
     
      </div>
      <div class="col-xs-4" style="margin-left:0px;">
        <?php echo $this->Html->link("Back", array('controller' => 'Stories','action'=> 'index'), array( 'class' => 'btn btn-primary','style'=>'margin-left:10px;')); ?>
      </div>
      </div>
  </div>
</div>
</div>
</div>
</section><!-- /.content -->


