<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add School'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Schools', array('controller' => 'schools', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('School'); ?>
<div class="box-body">
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email',array('type'=>'text'));
		$verified_options=array('Inactive','Active');
		echo $this->Form->input('is_verified',array('options'=>$verified_options));
	?>
	<div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'schools','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
</div>
</div>
</div>
</div>
</section><!-- /.content -->




