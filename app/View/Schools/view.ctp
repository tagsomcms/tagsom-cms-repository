<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo __('School'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
        <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
        </li>
    <li><?php echo $this -> Html -> link('Schools', array('controller' => 'schools', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 
 <div class="schools row">
    <div class="col-xs-12">
        <div class="box">
        <table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <td><?php echo h($school['School']['id']); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Name'); ?></th>
                <td><?php echo h($school['School']['name']); ?></td>
            </tr>
            <tr>
                <th><?php echo __('Email'); ?></th>
                <td><?php echo h($school['School']['email']); ?></td>
            </tr>
           
            <tr>
                <th><?php echo __('Last Modified'); ?></th>
                <td><?php echo h($school['School']['modified']); ?></td>
            </tr>
            <tr>
                <td colspan=2> <?php echo $this->Html->link("Back", array('controller' => 'Schools','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
                </td>
            </tr>
        </table>
        </div>
    </div>
 
</div>
 

<div style="clear: both;"></div>    
</div>
</section><!-- /.content -->

