<section class="content-header">
  <h1>
    <?php echo __('Schools List'); ?>
    <small></small>
  </h2>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Schools', array('controller' => 'schools', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>



<!-- Main content -->
<section class="content">


<div class="schools row">
<div class="col-xs-12">	
<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover data display dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('Active/Inactive'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody class="searchable">
	<?php foreach ($schools as $school): ?>
	<tr>
		<td><?php echo h($school['School']['id']); ?>&nbsp;</td>
		<td><?php echo h($school['School']['name']); ?>&nbsp;</td>
		<td><?php echo h($school['School']['email']); ?>&nbsp;</td>
		<td><?php if($school['School']['is_verified']==1) { echo 'Active'; } else { echo 'Inactive'; } ?>&nbsp;</td>
		<td><?php echo h($school['School']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $school['School']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $school['School']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $school['School']['id']), array('confirm' => __('Are you sure you want to delete %s?', $school['School']['name']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
</div>
</section>

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>