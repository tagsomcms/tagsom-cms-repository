<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
// $cakeDescription = __d('cake_dev', 'Tagsom: e-learning app');
// $cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>

      <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
 
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" http-equiv="Cache-Control" >
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/bootstrap/css/bootstrap.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/AdminLTE.min.css">
    <!-- File upload -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/component.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/demo.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/plugins/iCheck/square/blue.css">
    <!-- carousel -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/carousel.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datatables/dataTables.bootstrap.css">
    
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/css/cake.new.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/colorbox.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/thumbs2.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/dist/css/thumbnail-slider.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datepicker/datepicker3.css">

	<?php
		echo $this->Html->meta('icon');

		/*echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');*/

	?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	
	<!-- Site wrapper -->
    <div class="wrapper">
	
	<?php echo $this->element('header'); ?>
  <?php echo $this->element('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<?php echo $this->Flash->render(); ?>
	<?php echo $this->fetch('content'); ?>
  </div><!-- /.content-wrapper -->
	<?php echo $this->element('footer'); ?>
    
	<?php //echo $this->element('sql_dump'); ?>
	</div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->

    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- For DateRange Picker -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/daterangepicker/moment.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/dist/js/demo.js"></script>
    <!-- file upload -->
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/dist/js/custom-file-input.js"></script>
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/dist/js/valid.js"></script>
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/dist/js/jquery.colorbox.js"></script>
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/dist/js/thumbnail-slider.js"></script>

    <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datepicker/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datepicker/bootstrap-datetimepicker.min.css">


  <script src="<?php echo $this->webroot; ?>app/webroot/js/jquery-ui.min.js"></script>
  <script src="<?php echo $this->webroot; ?>app/webroot/js/jquery-fsortable.js"></script>
     <script src="<?php echo $this->webroot; ?>app/webroot/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
  

    <script>
      $(function () {
         $("#dataTable").DataTable();
        /*$('#dataTable').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });*/
        $('#start_datetimepicker').datetimepicker({
           format: "DD/MM/YYYY hh:mm:ss A",
           defaultDate: "<?php echo date('Y-m-d h:i:s'); ?>",
           minDate: "<?php echo date('Y-m-d h:i:s'); ?>"
        });
        
        $('#dob_datetimepicker').datetimepicker({
           format: "YYYY",
           defaultDate: "<?php echo date('Y'); ?>"
        });

        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
          
        });
       
        //------------To format start date into YYYY-MM-DD format------------------
        var date = moment($('#start_date').val(), ["DD/MM/YYYY hh:mm:ss A", "YYYY-MM-DD"]);
       
        $('#end_datetimepicker').datetimepicker({
           format: "DD/MM/YYYY hh:mm:ss A",
           defaultDate: date,
           minDate: date
        });

        //-----set min date on change of startdate--------
        $("#start_datetimepicker").on("dp.change", function (e) {
            $('#end_datetimepicker').data("DateTimePicker").minDate(e.date);
        });

        //-----set max date on change of enddate--------
        $("#end_datetimepicker").on("dp.change", function (e) {
            $('#start_datetimepicker').data("DateTimePicker").maxDate(e.date);
        });
        
        $("#UserSchoolId").on('change',function(){
           $.ajax({
               method: 'POST',
               url: '<?php echo Configure::read('BASE_URL').'students/getTeacherBySchoolId/'; ?>',
               data: {'id': $("#UserSchoolId").val()},
               success: function(data){
                   $("#getTeachers").empty().append(data);
               }
               
           })
           
        });

      });


    </script>


<?php
$controller= $this->params['controller'];
$action= $this->params['action'];
if($controller=='students' && $action=='import'):
?>


<?php endif; ?>

</body>

</html>