<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Variants List'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Scene', array('controller' => 'modes', 'action' => 'manage_scene',$math_id,$story_id)); ?>
    </li>
    <li class="active">Variants List</li>
  </ol>
</section>





<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Variant</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($all_variant)) 
{  
?>
<tr>
<td colspan=4><h4>No Variants are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($all_variant as $variant)
  {
?>
  <tr>
    <td><img class="thumb" id="obj_img" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Variants/". $variant['Variant']['image']; ?>" style="width:60px;height:50px;"></td>
 
     <td class="actions">  
      <?php echo $this->Html->link("Delete", array('action'=> 'delete_variant',$variant['Variant']['id'],$math_id,$story_id), array( 'class' => 'btn btn-primary')); ?>
    </td>
    
  </tr>
<?php
  echo $this->Form->end();
  }
}
?>
<tr>

</tr>

  </tbody>
  </table>

 <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  
  <div class="row">
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-bottom:20px;margin-left:10px;">
        <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_scene',$math_id,$story_id), array( 'class' => 'btn btn-primary')); ?>
      </div>
      </div>
  </div>
</div>
</div>
</div>
</section><!-- /.content -->
