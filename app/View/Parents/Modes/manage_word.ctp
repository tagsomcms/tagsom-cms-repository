<!-- Start Breadcrumb -->
<section class="content-header">
  <div class="row">
    <div class="col-md-3">  <h1><?php echo __('Manage Word'); ?></h1></div>
    <div class="col-sm-3">
      <?php
        if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
      ?>
    </div>
    <div class="col-sm-1">
      <?php
         if(isset($selected_story))
        {
          $select1=$selected_story;
        }
        else
        {
        $select1=1;
        }
        echo $this->Form->input('story',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$stories_array,'value'=>$select1,'style'=>'height: 33px;margin-top: 25px;width: 200px;margin-left:0px;','onchange'=>'StoryLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  

  </div>
  <ol class="breadcrumb" style="margin-top:25px;">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Words</li>
  </ol>
</section>
<!-- End Breadcrumb -->



 <!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Id</th>
    <th>Word</th>
    <th>Audio</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($words)) 
{  
?>
<tr>
<td colspan=4><h4>No Words Are There For This Language Or Story</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($words as $word)
  {
?>
  <tr>
    <td><?php echo $word['Word']['id']; ?></td>
    <td><?php echo $word['Word']['word']; ?></td>
    <td>
      <?php 
        echo $this->Form->create('Words',array('name'=>'Words','type'=>'file')); 
        if($word['Word']['word']=='No Translation')
        {
          echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:-10px;','required'=>true,'id'=>$word['Word']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$word['Word']['id'].')','disabled'=>true));
        }
        else
        {
          echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:-10px;','required'=>true,'id'=>$word['Word']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$word['Word']['id'].')'));
        }

        ?>
        <div class="col-md-6" style="width:100%;">
          <br><span id="lblError<?php echo $word['Word']['id']; ?>" style="color: red;"></span>
        </div>
        <?php
        echo $this->Form->input('hidden_language',array('type'=>'hidden','value'=>$word['Word']['language_id']));
        echo $this->Form->input('hidden_story',array('type'=>'hidden','value'=>$word['Word']['story_id']));
        echo $this->Form->input('hidden_id',array('type'=>'hidden','value'=>$word['Word']['id']));
        echo $this->Form->input('hidden_word',array('type'=>'hidden','value'=>$word['Word']['word']));
        echo $this->Form->input('hidden_audio',array('type'=>'hidden','value'=>$word['Word']['audio_clip']));
      ?>
    </td>
    <td class="actions">
       <?php if($language_name=='English') { echo $this->Html->link('Edit',array('action'=>'edit_word/',$word['Word']['id'],$id,$story_id,$language_name)); ?>
      <?php  echo $this->Html->link('Delete',array('action'=>'delete_word/',$word['Word']['id'],$id,$story_id,$language_name));  }?>
      <?php 
        
          if($word['Word']['word']=='No Translation')
          {
            echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-left:-7px;','disabled'=>true)); 
          }
          else
          {
             echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-left:-7px;')); 
          }
       
      ?>
    </td>
    
      <?php echo $this->Form->end(); ?>
  </tr>
<?php
  }
}
?>

<tr>
  <td class="actions" colspan=2>
    <?php if($language_name=='English') { echo $this->Html->link(__('Add more'), array('action' => 'add_word',$id,$story_id, $language_name), array( 'class' => 'btn btn-primary')); } ?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>

  </td>
</tr>

  </tbody>
  </table>

 <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function(){
document.getElementById("select1").options[0].disabled = true;
document.getElementById("select2").options[0].disabled = true;
});
</script>



