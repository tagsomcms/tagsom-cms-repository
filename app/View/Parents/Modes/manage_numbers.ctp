<!-- Start Breadcrumb -->
<section class="content-header">
 <div class="col-md-4">  <h1><?php echo __('Manage Numbers'); ?></h1></div>
    
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Numbers</li>
  </ol>
</section>
<!-- End Breadcrumb -->



<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Id</th>
    <th>Number</th>
    <th>Select Audio</th>
    <th>Upload Audio</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($numbers)) 
{  
?>
<tr>
<td colspan=4><h4>No Numbers are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($numbers as $number)
  {
?>
  <tr>
    <td><?php echo $number['Number']['id']; ?></td>
    <td><?php echo $number['Number']['number']; ?></td>
    <td>
      <?php 
        echo $this->Form->create('Number',array('name'=>'Number','type'=>'file')); 
        echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:-10px;','required'=>true,'id'=>'number_audio','onchange'=>'return ValidateAudioExtension(this,"number_audio")'));
        echo $this->Form->input('hidden_audio',array('type'=>'hidden','value'=>$number['Number']['audio_clip']));
        ?>
        <div class="col-md-6" style="width:100%;margin-top:-20px;">
          <br><span id="lblErrornumber_audio" style="color: red;"></span>
        </div>
        
    </td>
    <td>
      <?php 
      if(file_exists(WWW_ROOT.'uploads/Numbers_Audio/'.$number['Number']['audio_clip']))
      {
        echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      else
      {
        echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      ?>
    </td>
    <td class="actions">  
      <?php echo $this->Html->link('Delete',array('action'=>'delete_number',$id,$number['Number']['id'])); ?>
    </td>
    
  </tr>
<?php
  echo $this->Form->end();
  }
}
?>
<tr>
  <td class="actions" colspan=2>
    <?php echo $this->Html->link(__('Add more'), array('action' => 'add_numbers',$id), array( 'class' => 'btn btn-primary')); ?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </td>
</tr>

  </tbody>
  </table>

  <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  
  </p>
  
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>
</div>
</section><!-- /.content -->





