<!-- Content Header (Page header) -->
<?php
$options=array('Select From Existing Title');
foreach($settings1 as $setting)
{
  array_push($options,$setting['GeneralSettings']['title']);
}

?>
<section class="content-header">
  <div class="row">
    <div class="col-md-4">  <h1><?php echo __('General Settings'); ?></h1></div>
    <div class="col-sm-3">
      <?php
        if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
      ?>
    </div>
    <div class="col-sm-1">
      <?php
      
        echo $this->Form->input('titles',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$options,'style'=>'width: 200px;height:33px;margin-top: 25px;','onchange'=>'StoryLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  </div>

  <ol class="breadcrumb">
        <li>
          <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
          </li>
        <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
        </li>
        <li class="active">General Settings</li>
  </ol>
</section>





<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>

      <th>Title</th>
      <th>Audio</th>
      <th>Actions</th>

 
</tr>
</thead>
<tbody>

<?php
foreach($settings as $setting)
{
  $title=$setting['GeneralSettings']['title']; 
?>
  <tr>
    <td colspan=3><?php echo "<h4 style='color:black;'><b>Title : ".$title."</b></h4>";?></td>
  </tr>
  <tr>
    <td><?php echo $setting['GeneralSettings']['string']; ?></td>
    <td>
      <div class="row">
        <div class="col-xs-4" style="clear:none !important;">
          <?php echo $this->Form->create('Settings_Form',array('type'=>'file')); ?>
          <?php if($setting['GeneralSettings']['title']=='No Translation'){ 
              echo $this->Form->input('audio_clip',array('type'=>'file','style'=>'width:inherit;margin-top:-10px;','label'=>false,'required'=>true,'id'=>$setting['GeneralSettings']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$setting['GeneralSettings']['id'].')','disabled'=>true)); }
              else{
                echo $this->Form->input('audio_clip',array('type'=>'file','style'=>'width:inherit;margin-top:-10px;','label'=>false,'required'=>true,'id'=>$setting['GeneralSettings']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$setting['GeneralSettings']['id'].')'));
              }?>
            <div class="col-md-6" style="width:100%;margin-top:-20px;">
                <br><span id="lblError<?php echo $setting['GeneralSettings']['id']; ?>" style="color: red;"></span>
            </div>
          <?php echo $this->Form->input('hidden_id',array('type'=>'hidden','value'=>$setting['GeneralSettings']['id'])); ?>
          <?php echo $this->Form->input('hidden_string',array('type'=>'hidden','value'=>$setting['GeneralSettings']['string'])); ?>
           <?php echo $this->Form->input('hidden_language',array('type'=>'hidden','value'=>$setting['GeneralSettings']['language_id'])); ?>
             <?php echo $this->Form->input('filename',array('type'=>'hidden','value'=>$setting['GeneralSettings']['audio_clip'])); ?>

        </div>
      </div>
        </td>
        <td>
       
          <?php 
          if($setting['GeneralSettings']['title']=='No Translation')
          {
            if(file_exists(WWW_ROOT.'uploads/Settings_Audio/'.$language_name.'/'.$setting['GeneralSettings']['audio_clip']))
            {
              echo $this->Form->submit(__('Replace Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;','disabled'=>true)); 
            }
            else
            {
              echo $this->Form->submit(__('Upload Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;','disabled'=>true));
            }
          }
          else 
          {
            if(file_exists(WWW_ROOT.'uploads/Settings_Audio/'.$language_name.'/'.$setting['GeneralSettings']['audio_clip']))
            {
              echo $this->Form->submit(__('Replace Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;')); 
            }
            else
            {
              echo $this->Form->submit(__('Upload Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;'));
            }
          }
          ?>
          <?php echo $this->Form->end(); ?>
  
          <?php if($language_name=='English'){  
            echo $this->Html->link('Edit',array('action'=>'edit_setting',$setting['GeneralSettings']['id']),array('class'=>'btn btn-primary','style'=>'margin-top:-3px;')); 
            echo $this->Html->link('Delete',array('action'=>'delete_string',$setting['GeneralSettings']['id']),array('class'=>'btn btn-primary','style'=>'margin-top:-3px;margin-left:5px;')); 
          }?>

     </td>
    
    
  </tr>
<?php 
}
?>
  <tr>
    <td colspan=2>
      <?php if($language_name=='English')
      { ?>
      <div class="col-xs-2" style="clear:none !important;margin-top:20px;">
              <?php echo $this->Html->link("Add New String", array('action'=> 'add_string',$id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      </div>
       <?php } ?>
      <div class="col-xs-1" style="clear:none !important;margin-top:20px;">
              <?php echo $this->Html->link("Back", array('action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
      </div>
     
    </td>
  </tr>
  </tbody>

  </table>

 <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>

</div>

</section><!-- /.content -->



<script>
$(document).ready(function(){
document.getElementById("select2").options[0].disabled = true;
});
</script>