<!-- Start Breadcrumb -->
<section class="content-header">
 <div class="col-md-4">  <h1><?php echo __('Manage Alphabet'); ?></h1></div>
    <div class="col-sm-1">
      <?php
       if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('AlphabetLanguage',array('name'=>'AlphabetLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'AlphabetLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Alphabets</li>
  </ol>
</section>
<!-- End Breadcrumb -->



<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Id</th>
    <th>Alphabet</th>
    <th>Audio</th>
    <th>Upload Audio</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($alphabets)) 
{  
?>
<tr>
<td colspan=4><h4>No Alphabets are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($alphabets as $alphabet)
  {
?>
  <tr>
    <td><?php echo $alphabet['Alphabet']['id']; ?></td>
    <td><?php echo $alphabet['Alphabet']['alphabet']; ?></td>
    <td>
      <?php 
        echo $this->Form->create('Alphabet',array('name'=>'Alphabet','type'=>'file')); 
        echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:-10px;','required'=>true,'id'=>$alphabet['Alphabet']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$alphabet['Alphabet']['id'].')'));
        ?>
        <div class="col-md-6" style="width:100%;margin-top:-20px;">
          <br><span id="lblError<?php echo $alphabet['Alphabet']['id']; ?>" style="color: red;"></span>
        </div>
        <?php
        echo $this->Form->input('hidden_id',array('type'=>'hidden','value'=>$alphabet['Alphabet']['id']));
        echo $this->Form->input('hidden_language',array('type'=>'hidden','value'=>$alphabet['Alphabet']['language_id']));
        echo $this->Form->input('hidden_alphabet',array('type'=>'hidden','value'=>$alphabet['Alphabet']['alphabet']));
      ?>
    </td>
    <td>
      <?php if(file_exists(WWW_ROOT.'uploads/Alphabet_Audio/'.$language_name.'/'.$alphabet['Alphabet']['audio_clip']))
      {
        echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      else
      {
        echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      ?>
    </td>
    <td class="actions">  
      <?php  echo $this->Html->link('Edit',array('action'=>'edit_alphabet',$id,$alphabet['Alphabet']['id'],$language_name)); ?>
      <?php echo $this->Html->link('Delete',array('action'=>'delete_alphabet',$id,$alphabet['Alphabet']['id'],$language_name)); ?>
    </td>
    
  </tr>
<?php
  echo $this->Form->end();
  }
}
?>
<tr>
  <td class="actions" colspan=2>
    <?php echo $this->Html->link(__('Add more'), array('action' => 'add_alphabet',$id, $language_name), array( 'class' => 'btn btn-primary')); ?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </td>
</tr>

  </tbody>
  </table>

  <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  
  </p>
  
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>
</div>
</section><!-- /.content -->





