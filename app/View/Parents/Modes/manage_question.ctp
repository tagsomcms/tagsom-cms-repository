<!-- Start Breadcrumb -->
<section class="content-header">
  <div class="row">
    <div class="col-sm-3">  <h2 style="background:none;color:black;"><?php echo __('Manage Question'); ?></h2></div>
    <div class="col-sm-3">
      <?php
        if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
      ?>
    </div>
    <div class="col-sm-2">
      <?php
        if(isset($selected_story))
        {
          $select1=$selected_story;
        }
        else
        {
        $select1=0;
        }
        echo $this->Form->input('story',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$stories_array,'value'=>$select1,'style'=>'height: 33px;margin-top: 25px;width: 200px;margin-left:0px;','onchange'=>'StoryLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  

  </div>
  <ol class="breadcrumb" style="margin-top:25px;">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Question</li>
  </ol>
</section>
<!-- End Breadcrumb -->




<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Id</th>
    <th>Question/Answer</th>
    <th>Audio</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>

<?php
foreach($questions as $question)
{
  $arr=array($question['Question']['option1']=>$question['Question']['option1'],$question['Question']['option2']=>$question['Question']['option2'],$question['Question']['option3']=>$question['Question']['option3']);
  $checked=$question['Question']['answer'];
  echo $this->Form->create('Question_Answer',array('type'=>'file'));
  ?>

  <tr>
    <td><?php echo $question['Question']['id']; ?></td>
    <td><p style="width: 30em;word-wrap: break-word;"><?php echo $question['Question']['question']; ?><p></td>
    <td><?php if($question['Question']['question']!='No Translation') {echo $this->Form->input('question_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['id'].')'));  } else { echo $this->Form->input('question_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit','id'=>$question['Question']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['id'].')','disabled'=>true)); }?>
      <div class="col-md-6" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['id']; ?>" style="color: red;"></span>
      </div>
      <?php echo $this->Form->input('h1',array('label'=>false,'type'=>'hidden','value'=>$question['Question']['id'])); ?>
      
    </td>
     <td class="actions">
      <?php if($language_name=='English') { echo $this->Html->link(__('Edit'), array('action' => 'edit_question',$question['Question']['id'],$id,$story_id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      <?php echo $this->Html->link(__('Delete'), array('action' => 'delete_question',$question['Question']['id'],$id,$story_id,$language_name), array( 'class' => 'btn btn-primary')); } ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td><?php if($language_name=='English') { echo $this->Form->input('option_select1', array(
        'type' => 'radio',
        'legend' => false,
        'options'=>$arr,
        'style'=>'margin-left:0px;margin-top:-3px;width:20px;box-shadow: none;',
        'value'=>$checked
    ));  } else {
    echo $this->Form->input('option_select1', array(
      'type' => 'radio',
      'legend' => false,
      'options'=>$arr,
      'style'=>'margin-left:0px;margin-top:-3px;width:20px;box-shadow: none;pointer-events:none;',
      'value'=>$checked
    ));
    }

   ?>
    <?php if($question['Question']['question']=='No Translation')
    { ?>
    <td><?php echo $this->Form->input('option1_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['option1'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option1'].')','disabled'=>true)); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option1']; ?>" style="color: red;"></span>
      </div>
     
    <?php echo $this->Form->input('option2_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['option2'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option2'].')','disabled'=>true)); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option2']; ?>" style="color: red;"></span>
      </div>
     
    <?php echo $this->Form->input('option3_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['option3'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option3'].')','disabled'=>true)); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option3']; ?>" style="color: red;"></span>
      </div>
      

  </td>
   <td class="actions" >
      <?php echo $this->Form->Submit(__('Save Changes'),array('class'=>'btn btn-primary','style'=>'margin-left:-7px;','disabled'=>true)); ?>
    </td>
  <?php } else { ?>
     <td><?php echo $this->Form->input('option1_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['option1'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option1'].')')); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option1']; ?>" style="color: red;"></span>
      </div>
      
    <?php echo $this->Form->input('option2_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['option2'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option2'].')')); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option2']; ?>" style="color: red;"></span>
      </div>
      
    <?php echo $this->Form->input('option3_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;','id'=>$question['Question']['option3'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option3'].')')); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option3']; ?>" style="color: red;"></span>
      </div>
      
  </td>
    <td class="actions" >
      <?php echo $this->Form->Submit(__('Save Changes'),array('class'=>'btn btn-primary','style'=>'margin-left:-7px;')); ?>
    </td>

    <?php 
   
  } ?>
 
   
  </tr>

  <?php
  echo $this->Form->end();
}
?>



<tr>
  <td class="actions" colspan=2>
    <?php if($language_name=='English') {echo $this->Html->link(__('Add more'), array('action' => 'add_question',$id,$story_id, $language_id), array( 'class' => 'btn btn-primary')); }?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </td>
</tr>

  </tbody>
  </table>

 <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function(){
document.getElementById("select1").options[0].disabled = true;
document.getElementById("select2").options[0].disabled = true;
});
</script>

<style>
.radio label {
    margin: -4px 1px 27px 24px;
    line-height: 36px;
}
box-shadow: none;
</style>

