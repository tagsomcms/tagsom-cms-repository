<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="row">
    <div class="col-md-3">  <h1><?php echo __('Manage maths'); ?></h1></div>
    <div class="col-sm-1">
      <?php
        if(isset($selected_story))
        {
          $select=$selected_story;
        }
        else
        {
        $select=0;
        }
        echo $this->Form->create('Story',array('name'=>'Story')); 
        echo $this->Form->input('story',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$story_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'Story.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  </div> 
    
  <ol class="breadcrumb">
        <li>
          <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
          </li>
        <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
        </li>
        <li class="active">Manage math</li>
  </ol>
</section>


 <!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box" style="height:100%;"> 
<h4 style="color:black;margin-left:10px;"><b>Select Scene</b></h4>
<div class="row">
<?php
foreach($scenes as $scene)
{
?>
<div class="col-md-6" >
  <div class="col-md-8">
        <a class="thumbnail" href="#">
            <img class="img-responsive" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/".$story_path."/Scenes/". $scene['Scene']['image']; ?>" alt="<?php echo $scene['Scene']['image']; ?>" style="width:295px;height:160px;">

        </a>
  </div>
  <div class="col-md-4">
    <?php echo $this->Html->link("Remove Scene", array('action'=> 'remove_scene',$scene['Scene']['id']), array( 'class' => 'btn btn-primary')); ?>
    <?php echo $this->Html->link("Manage Scene", array('action'=> 'Manage_scene',$scene['Scene']['id'],$story_id), array( 'class' => 'btn btn-primary','style'=>'margin-top:5px;')); ?>
  </div>
</div>
<?php 
} 
?>
      
</div>


<div class="row">
  <div class="col-xs-1" style="clear:none !important;margin-left:20px;margin-top:20px;">
    <?php echo $this->Html->link("Add Scene", array('action'=> 'add_scene',$story_id), array( 'class' => 'btn btn-primary')); ?>
  </div>
  
  <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:20px;">
    <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </div>
</div>
<br>
</div>
</div>
</div>
</section><!-- /.content -->




