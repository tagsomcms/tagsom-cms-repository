<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Mode'); ?></h1>
<ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="modes row">
<div class="col-xs-12">
<div class="box">	
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($mode['Mode']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('Mode Name'); ?></th>
		<td>
			<?php echo h($mode['Mode']['mode_name']); ?>
		</td></tr>
		<tr><th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($mode['Mode']['modified']); ?>
		</td></tr>
	</table>
</div>
</div>
</div>
</section><!-- /.content -->