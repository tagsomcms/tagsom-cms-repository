<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Question'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Question', array('controller' => 'modes', 'action' => 'manage_question')); ?>
    </li>
    <li class="active">Add Question</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="modeQuestionAnswers row">
 	<div class="col-xs-12">
	<div class="box box-primary">
	<?php echo $this->Form->create('Question',array('type' => 'file')); ?>
	<fieldset>
	<div class="row" id="c1" style="margin-left:-7px;">
		<?php  echo $this->Form->input('question',array('required'=>true)); ?>
	</div>
	<div class="row" style="margin-left:-21px;">
		<div class="col-xs-5">
			<?php echo $this->Form->input('question_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Question)','required'=>false,'id'=>'audio1','onchange'=>'return ValidateAudioExtension(this,"audio1")')); ?>
		</div>
		
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroraudio1" style="color: red;"></span>
		</div>
	</div>
	<div class="row" id="c1" style="margin-left:-7px;">
	<?php echo $this->Form->input('answer',array('required'=>true)); ?>
	</div>
 
	<div class="row" style="margin-left:-21px;">
		<div class="col-xs-5">
			<?php echo $this->Form->input('answer_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Answer)','required'=>false,'id'=>'answer_audio1','onchange'=>'return ValidateAudioExtension(this,"answer_audio1")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroranswer_audio1" style="color: red;"></span>
		</div>
	</div> 
	
	
	<!-- mode question 1 -->
	<div class="row" style="margin-left:-21px;" id="options">
	<div class="col-xs-3" id="c1">
		<?php echo $this->Form->input('option1',array('label'=>'Option1','required'=>true)); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;" id="c1">
		<?php echo $this->Form->input('option2',array('label'=>'Option2','required'=>true)); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;" id="c1">
		<?php echo $this->Form->input('option3',array('label'=>'Option3','required'=>true)); ?>
	</div>
	</div>
	 <div class="row" style="margin-left:-21px;" id="opt1">
		<div class="col-xs-5">
			<?php echo $this->Form->input('option1_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 1)','required'=>false,'id'=>'option1_audio','onchange'=>'return ValidateAudioExtension(this,"option1_audio")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroroption1_audio" style="color: red;"></span>
		</div>
	</div>
	<div class="row" style="margin-left:-21px;" id="opt2">
		<div class="col-xs-5">
			<?php echo $this->Form->input('option2_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 2)','required'=>false,'id'=>'option2_audio','onchange'=>'return ValidateAudioExtension(this,"option2_audio")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroroption2_audio" style="color: red;"></span>
		</div>
	</div>
	<div class="row" style="margin-left:-21px;" id="opt3">
		<div class="col-xs-5">
			<?php echo $this->Form->input('option3_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 3)','required'=>false,'id'=>'option3_audio','onchange'=>'return ValidateAudioExtension(this,"option3_audio")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroroption3_audio" style="color: red;"></span>
		</div>
	</div>
	<div class="row" style="margin-left:-21px;" id="opt3">
		<div class="col-xs-5" id="c1">
			<?php echo $this->Form->input('image',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Image File','required'=>true,'id'=>'image_file','onchange'=>'return ValidateImageExtension(this,"image_file")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErrorimage_file" style="color: red;"></span>
		</div>
	</div>


	<div class="row">
	<div class="col-xs-1" style="clear:none !important;">
		<?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
	</div>
	<div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
		<?php echo $this->Html->link("Back", array('action'=> 'manage_question',$id,$story_id,$language_name), array( 'class' => 'btn btn-primary')); ?>
	</div>
	</div>

	</fieldset>
</div>
</div>
</div>
</section>
<!-- /Main content -->

<style>
form div {
    clear: both;
     margin-bottom: 0em !important; 
    padding: .5em;
    vertical-align: text-top;
}
#c1 label:after {
	color: #e32;
	content: '*';
	display:inline;
}

</style>


