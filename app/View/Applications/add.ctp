<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css"
        rel="stylesheet" type="text/css" />
<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.css"
        rel="stylesheet" type="text/css" />

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Create New Application'); ?></h1>
 <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Stories', array('controller' => 'applications', 'action' => 'index')); ?>
    </li>
    <li class="active">Create New Application</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
	<div class="box box-primary">
	<?php echo $this->Form->create('Applications',array('type'=>'file')); ?>
	<div class="box-body">
	  	<div class="col-md-6">
	     	<?php echo $this->Form->input('application_name',array('required'=>true)); ?>
	  	</div>

	    <div class="col-md-6">
	     	<?php echo $this->Form->input('client_name',array('required'=>true)); ?>
	  	</div>

	  	<div class="col-md-3" style="margin-left:5px;">
	  		<?php echo $this->Form->input('year',array('type'=>'text','div'=>false,'label'=>'Year of Development','id'=>'datepicker','required'=>true,'placeholder'=>'yyyy')); ?>
	  	</div>

		<div class="col-md-6">
	  	 	<?php  echo $this->Form->input('image',array('required'=>false,'type'=>'file','id'=>'story_img','onchange'=>'return ValidateImageExtension(this,"story_img")','label'=>'Application Logo')); ?>

		    <div class="col-xs-6" style="margin-left:5px;">
	            <span id="lblErrorstory_img" style="color: red;"></span>
		  	</div>
	  	</div>


  	</div>  
  	<table style="margin-left:20px;">
	  	<tr>
		  	<td>
	        	<?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary')); ?>
	      	</td>
	      	<td >
	    	 	<?php echo $this->Html->link("Back", array('controller' => 'applications','action'=> 'index'), array( 'class' => 'btn btn-primary','style'=>'margin-top:10px;')); ?>
	      	</td>
      	</tr>
      	</table>

	</div>
</div>
</div>
</div>
</section><!-- /.content -->




<script>

$(document).ready(function() {

	$("#datepicker").datepicker({
		minViewMode: 2,
     	format: 'yyyy'
     });
		
});       
</script>      

<style>
.select2-search-choice{
      height: 30px !important;
}
form div {
    clear: both;
    margin-bottom: 0em; 
    padding: .5em;
    vertical-align: text-top;
}
.row {
    margin-right: -15px;
    margin-left: 0px;
}
</style>

