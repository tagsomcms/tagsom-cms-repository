<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css"
        rel="stylesheet" type="text/css" />
<link href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.css"
        rel="stylesheet" type="text/css" />

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Assign Stories To Application'); ?></h1>
 <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    </li>
    <li><?php echo $this -> Html -> link('Applications', array('controller' => 'applications', 'action' => 'index')); ?>
    </li>
    <li class="active">Assign Stories</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="stories row">
  <div class="col-xs-12">
  <div class="box box-primary">
  <?php echo $this->Form->create('Applications',array('type'=>'file')); ?>
  <div class="box-body">
      <div class="col-md-6" style="font-size: large;">
        Application : <?php echo $application['Application']['application_name']; ?>
      </div>

      <div class="col-md-6">
        <?php if(file_exists(WWW_ROOT.'uploads/Applications/'. $application['Application']['image'])) { ?>
        <img src="<?php echo $this->base . '/app/webroot/uploads/Applications/'.$application['Application']['image'].'?'.rand(1111,9999); ?>" width="220px" height="130px"  id='img2' >
        <?php } ?>

      </div>

      <div class="col-md-6" style="font-size: large;">Assign Stories : 
       <select class="form-control" id="tagPicker" multiple="multiple" name="stories[]" >
          <?php 
            foreach($stories as $story)
            {
              if(in_array($story['Story']['story_name'], $stories_array))
              {
                ?>
                <option value="<?php echo $story['Story']['story_name']; ?>" selected style="height:30px !important;"><?php echo $story['Story']['story_name'] ?></option>
                <?php
              }
              else
              {
              ?>
                 <option value="<?php echo $story['Story']['story_name']; ?>" style="height:30px !important;"><?php echo $story['Story']['story_name'] ?></option>
               <?php
              }
            }
          ?>
         
        </select>
      </div>


    </div>  
    <table style="margin-left:20px;">
      <tr>
        <td>
            <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary')); ?>
          </td>
          <td >
          <?php echo $this->Html->link("Back", array('controller' => 'applications','action'=> 'index'), array( 'class' => 'btn btn-primary','style'=>'margin-top:10px;')); ?>
          </td>
        </tr>
        </table>


  </div>
</div>
</div>
</div>
</section><!-- /.content -->




<script>
//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){
           
  /* dropdown and filter select */
  var select = $('#select2').select2();
  
  /* Select2 plugin as tagpicker */
  $("#tagPicker").select2({
    closeOnSelect:true
  });

}); //script  

$(document).ready(function() {

  $("#datepicker").datepicker({
    minViewMode: 2,
      format: 'yyyy'
     });
    
});       
</script>      

<style>
.select2-search-choice{
      height: 30px !important;
}
form div {
    clear: both;
    margin-bottom: 0em; 
    padding: .5em;
    vertical-align: text-top;
}
.row {
    margin-right: -15px;
    margin-left: 0px;
}
</style>

