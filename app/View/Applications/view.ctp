<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Application'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Applications', array('controller' => 'applications', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="languages row">
<div class="col-xs-12">
<div class="box">	 	
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td>
				<?php echo h($application['Application']['id']); ?>
			</td>
		</tr>

		<tr>
			<th><?php echo __('Application Name'); ?></th>
			<td>
				<?php echo h($application['Application']['application_name']); ?>
			</td>
		</tr>

		<tr>
			<th><?php echo __('Client Name'); ?></th>
			<td>
				<?php echo h($application['Application']['client_name']); ?>
			</td>
		</tr>

		<tr>
			<th><?php echo __('Year'); ?></th>
			<td>
				<?php echo h($application['Application']['year']); ?>
			</td>
		</tr>

		<tr>
			<th><?php echo __('Application Logo'); ?></th>
			<td>
				<?php if(file_exists(WWW_ROOT.'uploads/Applications/'. $application['Application']['image'])) { ?>
				<img src="<?php echo $this->base . "/app/webroot/uploads/Applications/". $application['Application']['image']; ?>" width="100px" height="80px"> 
				<?php } else { echo 'No Image Available'; }?>
			</td>
		</tr>
		
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td>
				<?php echo h($application['Application']['created']); ?>
			</td>
		</tr>

		<tr>
			<th><?php echo __('Stories'); ?></th>
			<td>
				<?php foreach($stories as $story) { echo $story['ApplicationDetail']['story_name'].'<br>';} ?>
			</td>
		</tr>

		<tr>
	      	<td colspan=2>
	    	 	<?php echo $this->Html->link("Back", array('controller' => 'applications','action'=> 'index'), array( 'class' => 'btn btn-primary','style'=>'margin-top:10px;')); ?>
	      	</td>
		</tr>

	</table>

</div>
</div>
</div>
</section><!-- /.content -->


