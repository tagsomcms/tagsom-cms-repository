<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo __('Applications List'); ?>
    <small></small>
  </h2>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

    <!-- Main content -->
    <section class="content">
	 <div class="row">
	<div class="col-xs-12">
	<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id='searchable'>
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('application_name'); ?></th>
			<th><?php echo $this->Paginator->sort('client_name'); ?></th>
			<th><?php echo $this->Paginator->sort('year'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($applications as $application): ?>
	<tr>
		<td><?php echo h($application['Application']['id']); ?>&nbsp;</td>
		<td><?php echo h($application['Application']['application_name']); ?>&nbsp;</td>
		<td><?php echo h($application['Application']['client_name']); ?>&nbsp;</td>
		<td><?php echo h($application['Application']['year']); ?>&nbsp;</td>
		<td><?php echo h($application['Application']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $application['Application']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $application['Application']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $application['Application']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $application['Application']['id']))); ?>
			<?php echo $this->Html->link(__('Assign Stories'), array('action' => 'assign', $application['Application']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	</div>
	</div>
	</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>

