
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1> User Mode Grades</h1>
          <ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('User Mode Grades', array('controller' => 'user_mode_grades', 'action' => 'index')); ?>
            </li>
            <li class="active">Index</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		 <div class="row">
		<div class="col-xs-12">
		<div class="box">
		 
		 
		 <div class="userModeGrades index" style="width:100%;">
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_id'); ?></th>
			<th><?php echo $this->Paginator->sort('story_id'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('mode_id'); ?></th>
			<th><?php echo $this->Paginator->sort('grade_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userModeGrades as $userModeGrade): ?>
	<tr>
		<td><?php echo h($userModeGrade['UserModeGrade']['id']); ?>&nbsp;</td>
		<td>
			<?php if(empty($userModeGrade['User']['username'])) {echo $this->Html->link($userModeGrade['User']['email'], array('controller' => 'users', 'action' => 'view', $userModeGrade['User']['id']));}
			else {
			 	echo $this->Html->link($userModeGrade['User']['username'], array('controller' => 'users', 'action' => 'view', $userModeGrade['User']['id']));
			 } ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeGrade['Level']['level_name'], array('controller' => 'levels', 'action' => 'view', $userModeGrade['Level']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeGrade['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userModeGrade['Story']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeGrade['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userModeGrade['Language']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeGrade['Mode']['mode_name'], array('controller' => 'modes', 'action' => 'view', $userModeGrade['Mode']['id'])); ?>
		</td>
		<td><?php echo h($userModeGrade['Grade']['grade_name']); ?>&nbsp;</td>
		<td><?php echo h($userModeGrade['UserModeGrade']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userModeGrade['UserModeGrade']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userModeGrade['UserModeGrade']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userModeGrade['UserModeGrade']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userModeGrade['UserModeGrade']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>

</div>
</div>
</div>
</div>		 
</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>