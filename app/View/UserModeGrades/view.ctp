<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('User Mode Grade'); ?></h1>
  <ol class="breadcrumb">
    <li><?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
	</li>
    <li><?php echo $this -> Html -> link('User Mode Grades', array('controller' => 'user_mode_grades', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>


<!-- Main content -->
<section class="content">
 <div class="userModeGrades row">
 	<div class="col-xs-12">
		<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($userModeGrade['UserModeGrade']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('User'); ?></th>
			<td><?php echo $this->Html->link($users['User']['username'], array('controller' => 'users', 'action' => 'view', $userModeGrade['UserModeGrade']['user_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Level'); ?></th>
			<td><?php echo $this->Html->link($userModeGrade['UserModeGrade']['level_id'], array('controller' => 'levels', 'action' => 'view', $userModeGrade['UserModeGrade']['level_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Story'); ?></th>
			<td><?php echo $this->Html->link($stories['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userModeGrade['UserModeGrade']['story_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Language'); ?></th>
			<td><?php echo $this->Html->link($languages['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userModeGrade['UserModeGrade']['language_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Mode'); ?></th>
			<td><?php echo $this->Html->link($userModeGrade['UserModeGrade']['mode_id'], array('controller' => 'modes', 'action' => 'view', $userModeGrade['UserModeGrade']['mode_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Grade Id'); ?></th>
			<td><?php echo $this->Html->link($userModeGrade['UserModeGrade']['grade_id'], array('controller' => 'grades', 'action' => 'view', $userModeGrade['UserModeGrade']['grade_id'])); ?></td>

		</tr>
		<tr>
			<th><?php echo __('Created'); ?></th>
			<td><?php echo h($userModeGrade['UserModeGrade']['created']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Modified'); ?></th>
			<td><?php echo h($userModeGrade['UserModeGrade']['modified']); ?></td>
		</tr>
		<tr>
			<td >
				<?php echo $this->Html->link("Back", array('controller' => 'user_mode_grades','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
			</td>
		</tr>
	</table>
	</div>
	</div>
</div>
</section><!-- /.content -->
