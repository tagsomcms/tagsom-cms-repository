<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> <?php echo __('Edit User Mode Grade'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('User Mode Grades', array('controller' => 'user_mode_grades', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="userModeGrades row">
 	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('UserModeGrade'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id',array('type'=>'select','options'=>$user_array,'value'=>$select));
		echo $this->Form->input('level_id');
		echo $this->Form->input('story_id');
		echo $this->Form->input('language_id');
		echo $this->Form->input('mode_id');
    echo $this->Form->input('grade_id',array('type'=>'select','options'=>$grades_array,'value'=>$select1));
	?>

  <div class="row">
    <div class="col-xs-1" style="clear:none !important;">
      <?php echo $this->Form->submit('Save',array('class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
      <?php echo $this->Html->link("Back", array('action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
    </div>
  </div>
	</fieldset>
</div>
</div>
</div>
</section><!-- /.content -->


