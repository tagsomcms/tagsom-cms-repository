        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><?php echo __('Dashboard'); ?></h1>
          <ol class="breadcrumb">
      <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li class="active">Dashboard</li>
  </ol>
        </section>

        <!-- Main content -->
        <section class="content">
     <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $countstories; ?></h3>
                  <p>Stories</p>
                </div>
                <div class="icon">
                  <i class="fa fa-book"></i>
                </div>
                <?php echo $this -> Html -> link('More info <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'stories', 'action' => 'index'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
            <?php
            if($this->Session->read('Auth.User.type')=='admin')
            {
              ?>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $countlevels;?></h3>
                  <p>Levels</p>
                </div>
                <div class="icon">
                  <i class="fa fa-level-up"></i>
                </div>
                <?php echo $this -> Html -> link('More info <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'levels', 'action' => 'index'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $countusers;?></h3>
                  <p>User Registrations</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <?php echo $this -> Html -> link('More info <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'users', 'action' => 'index'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $countlanguages;?></h3>
                  <p>Languages</p>
                </div>
                <div class="icon">
                  <i class="fa fa-language"></i>
                </div>
                <?php echo $this -> Html -> link('More info <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'languages', 'action' => 'index'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-olive">
                <div class="inner">
                  <h3>&nbsp;</h3>
                  <p>Import Teachers</p>
                </div>
                <div class="icon" style="top: 0px;">
                  <i class="glyphicon glyphicon-import"></i>
                </div>
                <?php echo $this -> Html -> link('Import <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'teachers', 'action' => 'import'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
                <div class="inner">
                  <h3>&nbsp;</h3>
                  <p>Import Students</p>
                </div>
                <div class="icon" style="top: 0px;">
                  <i class="glyphicon glyphicon-import"></i>
                </div>
                <?php echo $this -> Html -> link('Import <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'students', 'action' => 'import'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
              <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-maroon">
                <div class="inner">
                  <h3>&nbsp;</h3>
                  <p>Applications</p>
                </div>
                <div class="icon" style="top: 0px;">
                  <i class="glyphicon glyphicon-phone"></i>
                </div>
                <?php echo $this -> Html -> link('More info <i class="fa fa-arrow-circle-right"></i>', array('controller' => 'applications', 'action' => 'index'), array('class'=>'small-box-footer','escape' => false)); ?>
              </div>
            </div><!-- ./col -->
            <?php
            }
            ?>
          </div>
        </section><!-- /.content -->


        <style>
        .bg-maroon {
            background-color: #900C3F!important;
        }
        </style>