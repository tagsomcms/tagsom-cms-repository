<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Grades'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Grades', array('controller' => 'grades', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="grades row">
	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('Grade'); ?>
	<div class="box-body">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('grade_name');
	?>
  <div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'grades','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->