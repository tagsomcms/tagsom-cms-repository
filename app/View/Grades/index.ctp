<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Grades'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Grades', array('controller' => 'grades', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="grades row">
 	<div class="col-xs-12">
	<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('grade_name'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php 
	foreach ($grades as $grade): 

	?>

	<tr>
		<td><?php echo h($grade['Grade']['id']); ?>&nbsp;</td>
		<td><?php echo h($grade['Grade']['grade_name']); ?>&nbsp;</td>
		<td><?php echo h($grade['Grade']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $grade['Grade']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $grade['Grade']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $grade['Grade']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $grade['Grade']['id']))); ?>
		</td>
	</tr>
<?php  endforeach; ?>
	</tbody>
	</table>

</div>
</div>
</div>
</section><!-- /.content -->


<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>