<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Grades'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Grades', array('controller' => 'grades', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="grades row">
<div class="col-xs-12">
<div class="box"> 	
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($grade['Grade']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('Grade Name'); ?></th>
		<td>
			<?php echo h($grade['Grade']['grade_name']); ?>
		</td></tr>
		<tr><th><?php echo __('Last Modified'); ?></th>
		<td>
			<?php echo h($grade['Grade']['modified']); ?>
		</td></tr>
	</table>
</div>
</div>	
</div>
</section><!-- /.content -->