<?php
$controller= $this->params['controller'];
$schools=$users=$parents=$teachers=$children=$students=$user_invites=$user_mode_grades=$user_mode_statuses=$user_purchases=$user_story_levels=$stories=$levels=$modes=$languages=$mode_question_answers=$grades=$freebooks=$api_token=$voice_actors=$apps="";
switch ($controller) {
	case 'schools':
		$schools='active';
		break;
  case 'apps':
    $apps='active';
    break;
	case 'parents':
		$parents='active';
		break;
	case 'children':
		$children='active';
		break;
	case 'students':
		$students='active';
		break;		
	case 'teachers':
		$teachers='active';
		break;	
	case 'users':
		$users='active';
		break;
	case 'user_invites':
		$user_invites='active';
		break;	
	case 'user_mode_grades':
		$user_mode_grades='active';
		break;		
	case 'user_mode_statuses':
		$user_mode_statuses='active';
		break;	
	case 'user_purchases':
		$user_purchases='active';
		break;	
	case 'user_story_levels':
		$user_story_levels='active';
		break;	
	case 'stories':
		$stories='active';
		break;
	case 'levels':
		$levels='active';
		break;	
	case 'modes':
		$modes='active';
		break;
	case 'languages':
		$languages='active';
		break;	
	case 'mode_question_answers':
		$mode_question_answers='active';
		break;		
	case 'grades':
		$grades='active';
		break;	
	case 'freebooks':
		$freebooks='active';
		break;	
  case 'api_token':
    $api_token='active';
    break;  
  case 'voice_actors':
    $voice_actors='active';
    break;
    
	default:
		break;
}

?>


<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel" style="min-height:65px;">
            <div class="pull-left image">
              <?php if(empty($this->Session->read('Auth.User.profile_pic')))
                {
                  ?>
                    <img src="<?php echo $this->webroot; ?>app/webroot/img/user.png" class="user-image" alt="User Image">
                  <?php
                }
                else
                {
                  ?>
                    <img src="<?php echo $this->webroot; ?>app/webroot/uploads/profile/<?php echo $this->Session->read('Auth.User.profile_pic'); ?>" class="img-circle" alt="User Image" style="height:45px;">
                  <?php
                }
                ?>
            </div>
            <div class="pull-left info">
              <p>
                <?php 
                  if(empty($this->Session->read('Auth.User.first_name')))
                  {
                    echo 'Voice Actor';
                  }
                  else
                  {
                    echo $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
                  }
                  ?>
               </p>
              <i class="fa fa-circle text-success" ></i>&nbsp;&nbsp;&nbsp;Online
            </div>
          </div>
          <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

          <!------Admin Menu Start------>
          
          <?php if($this->Session->read('Auth.User.type')=='admin')
          {
          ?>
          <li class="treeview <?php echo $schools; ?>" >
              <a href="#">
                <i class="fa fa-user"></i> <span>Schools</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="">
                <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New School',
                    array('controller' => 'schools', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				    echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Schools',
                    array('controller' => 'schools', 'action' => 'index'),
                    array('escape' => false)
                  );
                  ?>
                </li>
              </ul>
            </li>
          
          
          
          
          
          <li class="treeview <?php echo $parents; ?>">
              <a href="#">
                <i class="fa fa-user"></i> <span>Parents</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="" >
                <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Parent',
                    array('controller' => 'parents', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				    echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Parents',
                    array('controller' => 'parents', 'action' => 'index'),
                    array('escape' => false)
                  );
                  ?>
                </li>
              </ul>
            </li>
            
            <li class="treeview <?php echo $children; ?>">
              <a href="#">
                <i class="fa fa-user"></i> <span>Children</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="" >
                <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Child',
                    array('controller' => 'children', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				    echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Children',
                    array('controller' => 'children', 'action' => 'index'),
                    array('escape' => false)
                  );
                  ?>
                </li>
              </ul>
            </li>
            
            
            <li class="treeview <?php echo $teachers; ?>">
              <a href="#">
                <i class="fa fa-user"></i> <span>Teachers</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="" >
                <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Teacher',
                    array('controller' => 'teachers', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				    echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Teachers',
                    array('controller' => 'teachers', 'action' => 'index'),
                    array('escape' => false)
                  );
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Import Teachers',
                    array('controller' => 'teachers', 'action' => 'import'),
                    array('escape' => false)
                  );
                  ?>
                </li>
              </ul>
            </li>
            
            
            
            <li class="treeview <?php echo $students; ?>">
              <a href="#">
                <i class="fa fa-user"></i> <span>Students</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="" >
                <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Student',
                    array('controller' => 'students', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				    echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Students',
                    array('controller' => 'students', 'action' => 'index'),
                    array('escape' => false)
                  );
                   echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Import Students',
                    array('controller' => 'students', 'action' => 'import'),
                    array('escape' => false)
                  );
                  ?>
                </li>
              </ul>
            </li>
            

            
          
            <li class="treeview <?php echo $users; ?>">
              <a href="#">
                <i class="fa fa-user"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="" >
                <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New User',
                    array('controller' => 'users', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				    echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Users',
                    array('controller' => 'users', 'action' => 'index'),
                    array('escape' => false)
                  );
                  ?>
                </li>
              </ul>
            </li>
            
            <li class="treeview <?php echo $user_invites; ?>">
              <a href="#">
                <i class="fa fa-plus"></i>
                <span>User Invites</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List User Invites',
                    array('controller' => 'user_invites', 'action' => 'index'),
                    array('escape' => false)
                  );
				  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;New User Invite',
                    array('controller' => 'user_invites', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				  ?>	
				  </li>
              </ul>
            </li>
            
            <li class="treeview <?php echo $user_mode_grades; ?>">
              <a href="#">
                <i class="fa fa-mortar-board"></i>
                <span>User Mode Grades</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List User Mode Grades',
                    array('controller' => 'user_mode_grades', 'action' => 'index'),
                    array('escape' => false)
                  ); 
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New User Mode Grades',
                    array('controller' => 'user_mode_grades', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				  ?>	
				  </li>
                <!-- <a href="#"><i class="fa fa-circle-o text-aqua"></i> Active Offers</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-green"></i> Redeem Requests</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Redeemed Offers</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Users</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Vendors</a></li> -->
              </ul>
            </li>
            
            <li class="treeview <?php echo $user_mode_statuses; ?>">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>User Mode Status</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List User Mode Statuses',
                    array('controller' => 'user_mode_statuses', 'action' => 'index'),
                    array('escape' => false,'style'=>'white-space: initial;')
                  ); 
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New User Mode Statuses',
                    array('controller' => 'user_mode_statuses', 'action' => 'add'),
                    array('escape' => false,'style'=>'white-space: initial;')
                  ); 
				  ?>	
				  </li>
              </ul>
            </li>
            
            <!-- <li class="treeview <?php echo $user_purchases; ?>">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>User Purchase</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List User Purchases',
                    array('controller' => 'user_purchases', 'action' => 'index'),
                    array('escape' => false)
                  );
				  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New User Purchases',
                    array('controller' => 'user_purchases', 'action' => 'add'),
                    array('escape' => false)
                  );
				  ?>	
				  </li>
              </ul>
            </li> -->
            
            <li class="treeview <?php echo $user_story_levels; ?>">
              <a href="#">
                <i class="fa fa-level-up"></i>
                <span>User Story Levels</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List User Story Levels',
                    array('controller' => 'user_story_levels', 'action' => 'index'),
                    array('escape' => false)
                  );
				  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New User Story Levels',
                    array('controller' => 'user_story_levels', 'action' => 'add'),
                    array('escape' => false)
                  );
				  ?>	
				  </li>
              </ul>
            </li>

            <?php
            }
            ?>
            
            <li class="treeview <?php echo $stories; ?>">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Stories</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>

                 <?php if($this->Session->read('Auth.User.type')=='admin') {echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Story',
                    array('controller' => 'stories', 'action' => 'add'),
                    array('escape' => false)
                  ); }
				          echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Stories',
                    array('controller' => 'stories', 'action' => 'index'),
                    array('escape' => false)
                  );
                   echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;Import Stories',
                    array('controller' => 'stories', 'action' => 'import_stories'),
                    array('escape' => false,'style'=>'white-space: initial;')
                  );

                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;Sync Stories With POEditor',
                    array('controller' => 'stories', 'action' => 'sync_to_poeditor'),
                    array('escape' => false,'style'=>'white-space: initial;')
                  );
				  ?>	
				  </li>
              </ul>
            </li>
            

            <?php if($this->Session->read('Auth.User.type')=='admin')
            {
            ?>

             <li class="treeview <?php echo $levels; ?>">
              <a href="#">
                <i class="fa fa-share"></i>
                <span>Levels</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Level',
                    array('controller' => 'levels', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Levels',
                    array('controller' => 'levels', 'action' => 'index'),
                    array('escape' => false)
                  );
				  ?>	
				  </li>
              </ul>
            </li>

            <?php
            }
            ?>
            
            <li class="treeview <?php echo $modes; ?>">
              <a href="#">
                <i class="fa fa-circle-o"></i>
                <span>Modes</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php if($this->Session->read('Auth.User.type')=='admin') { echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Mode',
                    array('controller' => 'modes', 'action' => 'add'),
                    array('escape' => false)
                  ); }
				            echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Modes',
                    array('controller' => 'modes', 'action' => 'index'),
                    array('escape' => false)
                  );
                   echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;Sync Minigames With POEditor',
                    array('controller' => 'modes', 'action' => 'sync_to_poeditor'),
                    array('escape' => false,'style'=>'white-space: initial;')
                  );

				  ?>	
				  </li>
              </ul>
            </li>



            <?php if($this->Session->read('Auth.User.type')=='admin')
            {
            ?>

            <li class="treeview <?php echo $languages; ?>">
              <a href="#">
                <i class="fa fa-language"></i>
                <span>Languages</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Language',
                    array('controller' => 'languages', 'action' => 'add'),
                    array('escape' => false)
                  ); 
				            echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Languages',
                    array('controller' => 'languages', 'action' => 'index'),
                    array('escape' => false)
                  );
				  ?>	
				  </li>
              </ul>
            </li>


          
          <!--Voice Actors-->
            <li class="treeview <?php echo $voice_actors; ?>">
              <a href="#">
                <i class="fa fa-language"></i>
                <span>Voice Actors</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php 
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Voice Actors',
                    array('controller' => 'users', 'action' => 'voice_actors'),
                    array('escape' => false)
                  );
                  ?>  
                </li>
              </ul>
            </li>


            <?php
            }
            ?>

            <?php if($this->Session->read('Auth.User.type')=='admin')
            {
            ?> 

            <li class="treeview <?php echo $grades; ?>">
              <a href="#">
                <i class="fa fa-mortar-board"></i>
                <span>Grades</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                 <?php echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;New Grade',
                    array('controller' => 'grades', 'action' => 'add'),
                    array('escape' => false)
                  ); 
			              echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Grades',
                    array('controller' => 'grades', 'action' => 'index'),
                    array('escape' => false)
                  );
				  ?>	
				  </li>
              </ul>
            </li>
            
            
            <li class="treeview <?php echo $freebooks; ?>">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Freebooks</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                <?php 
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Add Freebook',
                    array('controller' => 'freebooks', 'action' => 'add'),
                    array('escape' => false)
                  ); 
                  // echo $this->Html->link(
                  //   '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Assign Freebook ',
                  //   array('controller' => 'freebooks', 'action' => 'assign'),
                  //   array('escape' => false)
                  // ); 
		              echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Freebooks',
                    array('controller' => 'freebooks', 'action' => 'index'),
                    array('escape' => false)
                  );
				        ?>	
				        </li>
              </ul>
            </li>



            <li class="treeview <?php echo $api_token; ?>">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>POEditor Api</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <?php 
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Add Api_Token',
                    array('controller' => 'Apitokens', 'action' => 'add'),
                    array('escape' => false)
                  ); 
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Assign Project Name',
                    array('controller' => 'Apitokens', 'action' => 'add_project'),
                    array('escape' => false)
                  );

                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;List Api_Tokens',
                    array('controller' => 'Apitokens', 'action' => 'index'),
                    array('escape' => false)
                  ); 

                  // echo $this->Html->link(
                  //   '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Import To POEditor',
                  //   array('controller' => 'Apitokens', 'action' => 'import'),
                  //   array('escape' => false)
                  // ); 
                ?>  
                </li>
              </ul>
            </li>



             <li class="treeview <?php echo $apps; ?>">
              <a href="#">
                <i class="fa fa-apple"></i>
                <span>Apps</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>

                 <?php if($this->Session->read('Auth.User.type')=='admin') {echo $this->Html->link(
                    '<i class="fa fa-circle-o text-aqua"></i>&nbsp;Create New Application',
                    array('controller' => 'applications', 'action' => 'add'),
                    array('escape' => false)
                  ); }
                  echo $this->Html->link(
                    '<i class="fa fa-circle-o text-green"></i>&nbsp;List Applications',
                    array('controller' => 'applications', 'action' => 'index'),
                    array('escape' => false)
                  );
                
          ?>  
          </li>
              </ul>
            </li>

          
            <?php 
            }
            ?>

            
           <!--- Admin Menu End----> 
                
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      
