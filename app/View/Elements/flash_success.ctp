<div class="alert alert-success alert-dismissable">
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
<h4>	<i class="icon fa fa-check"></i> Message</h4>
    <?php echo $message; ?>
  </div>