<header class="main-header">
	<a class="logo" href="/tagsomdev">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>T</b>A</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Tagsom</b> App</span>
    </a>
	
	
	
    <!-- Logo -->
    <?php 
    //echo $this->Html->link( $this->Html->tag('span', 'Cake', array('class' => 'logo-mini')),        array('controller' => 'users', 'action' => 'index'),        array('class' => 'logo', 'escape' => FALSE)      ); 
    //echo $this->Html->link('users', array('controller'=>'users','action'=>'index'));
         ?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
 
      
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php if(empty($this->Session->read('Auth.User.profile_pic')))
              {
                ?>
                  <img src="<?php echo $this->webroot; ?>app/webroot/img/user.png" class="user-image" alt="User Image">
                <?php
              }
              else
              {
                ?>
                  <img src="<?php echo $this->webroot; ?>app/webroot/uploads/profile/<?php echo $this->Session->read('Auth.User.profile_pic'); ?>" class="user-image" alt="User Image">
                <?php
              }
              ?>
            	
              <!-- <img src="<?php echo $this->webroot; ?>admin/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <?php if(empty($this->Session->read('Auth.User.first_name')))
              {
                ?>
                <span class="hidden-xs">Voice Actor</span>
                <?php
              }
              else
              {
                ?>
                <span class="hidden-xs"><?php echo $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name'); ?></span>
                <?php
              }
              ?>
              
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if(empty($this->Session->read('Auth.User.profile_pic')))
                {
                  ?>
                    <img src="<?php echo $this->webroot; ?>app/webroot/img/user.png" class="user-image" alt="User Image">
                  <?php
                }
                else
                {
                  ?>
                    <img src="<?php echo $this->webroot; ?>app/webroot/uploads/profile/<?php echo $this->Session->read('Auth.User.profile_pic'); ?>" class="img-circle" alt="User Image">
                  <?php
                }
                ?>
              	
                <p>
                  <?php 
                  if(empty($this->Session->read('Auth.User.first_name')))
                  {
                    echo 'Voice Actor';
                  }
                  else
                  {
                      echo $this->Session->read('Auth.User.first_name').' '.$this->Session->read('Auth.User.last_name');
                  }
                  ?>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                	<?php
                    echo $this->Html->link(
                      'Profile',
                      array('controller' => 'users','action' => 'profile'),
                      array('class' => 'btn btn-default btn-flat')
                    );
                  ?>
                </div>
                <div class="pull-right">
                  <?php
                    echo $this->Html->link(
                      'Sign out',
                      array('controller' => 'users','action' => 'logout'),
                      array('class' => 'btn btn-default btn-flat')
                    );
                  ?>  
                </div>
              </li>
            </ul>
          </li>
       

          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>

    </nav>
  </header>