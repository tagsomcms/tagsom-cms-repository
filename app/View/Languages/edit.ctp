<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Language'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Languages', array('controller' => 'languages', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="languages row">
 	<div class="col-xs-12">
<div class="box box-primary">
	<div class="box-body">
	<?php echo $this->Form->create('Language',array('type'=>'file')); ?>
  <div class="box-body">
    <div class="col-md-6">
      <?php echo $this->Form->input('language_name',array('required'=>true)); ?>
    </div>
   
    <div class="row">
      <div class="col-md-6" style="clear:none !important;">
        <?php echo $this->Form->input('flag',array('id'=>'flag_icon','type'=>'file','label'=>'Select Flag','required'=>true,'onchange'=>'return ValidateImageExtension(this,"flag_icon")','style'=>'height:inherit;')); ?>
      </div>
      <div class="col-md-2" style="clear:none !important;margin-top:35px;">
        <img src="<?php echo $this->base . "/app/webroot/uploads/Language_Flags/". $image; ?>" width="30px" height="30px">
      </div>
    </div>
    <div class="col-xs-7" style="margin-left:5px;">
        <span id="lblErrorflag_icon" style="color: red;"></span>
    </div>
        
    <div class="row">
      <div class="col-md-1">
        <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
      </div>
      <div class="col-xs-2" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'Languages','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
      </div>
    </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->