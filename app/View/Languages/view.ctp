<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Language'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Languages', array('controller' => 'languages', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="languages row">
<div class="col-xs-12">
<div class="box">	 	
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($language['Language']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('Language Flag'); ?></th>
		<td>
	<?php if(empty($language['Language']['flag'])){echo 'No Flag';}else{  ?><img src="<?php echo $this->base . "/app/webroot/uploads/Language_Flags/". $language['Language']['flag']; ?>" width="30px" height="30px"> <?php } ?>
		</td></tr>
		<tr><th><?php echo __('Language Name'); ?></th>
		<td>
			<?php echo h($language['Language']['language_name']); ?>
		</td></tr>



		
		<tr><th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($language['Language']['modified']); ?>
		</td></tr>
	</table>

</div>
</div>
</div>
</section><!-- /.content -->