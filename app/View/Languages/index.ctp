<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Languages'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Languages', array('controller' => 'languages', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="languages row">
	<div class="col-xs-12">
		<div class="box">	
	<table cellpadding="0" cellspacing="0"  class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo 'Flag'; ?></th>
			<th><?php echo $this->Paginator->sort('language_name'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($languages as $language): ?>
	<tr>
		<td><?php echo h($language['Language']['id']); ?>&nbsp;</td>
		<td><?php if(empty($language['Language']['flag'])){echo 'No Flag';}else{  ?><img src="<?php echo $this->base . "/app/webroot/uploads/Language_Flags/". $language['Language']['flag']; ?>" width="30px" height="30px"> <?php } ?></td>
		<td><?php echo h($language['Language']['language_name']); ?>&nbsp;</td>
		<td><?php echo h($language['Language']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $language['Language']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $language['Language']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $language['Language']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $language['Language']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
</div>
</div>
</div>
</section><!-- /.content -->


<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>