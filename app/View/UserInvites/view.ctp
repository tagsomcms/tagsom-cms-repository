        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo __('User Invite'); ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(
                    'User Invite',
                    array('controller' => 'user_invites', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="invoice">
		<div class="userInvites row invoice-info">
			<table style="padding: 10px; width: 50%">
				<tr>
					<th><?php echo __('Id'); ?></th>
					<td><?php echo h($userInvite['UserInvite']['id']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('User'); ?></th>
					<td><?php echo $this->Html->link($userInvite['UserInvite']['user_id'], array('controller' => 'users', 'action' => 'view', $userInvite['UserInvite']['user_id'])); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Invite Emailid'); ?></th>
					<td><?php echo h($userInvite['UserInvite']['invite_emailid']); ?></td>
				</tr>
				<tr>
					<th><?php echo __('Invite Status'); ?></th>
					<td><?php echo h($userInvite['UserInvite']['invite_status']); ?></td>
				</tr>
			</table>
			
			
			
	<!-- <dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($userInvite['UserInvite']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userInvite['User']['username'], array('controller' => 'users', 'action' => 'view', $userInvite['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invite Emailid'); ?></dt>
		<dd>
			<?php echo h($userInvite['UserInvite']['invite_emailid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Invite Status'); ?></dt>
		<dd>
			<?php echo h($userInvite['UserInvite']['invite_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($userInvite['UserInvite']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($userInvite['UserInvite']['modified']); ?>
			&nbsp;
		</dd>
	</dl> -->
</div>
        </section><!-- /.content -->


<!-- <div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User Invite'), array('action' => 'edit', $userInvite['UserInvite']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User Invite'), array('action' => 'delete', $userInvite['UserInvite']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userInvite['UserInvite']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List User Invites'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Invite'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div> -->
