        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo __('Add User Invite'); ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(
                    'User Invite',
                    array('controller' => 'user_invites', 'action' => 'index')); ?>
            </li>
            <li class="active">Add</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		 <div class="userInvites form">
		 	<div class="col-xs-12">
		 		<div class="box box-primary">
<?php echo $this->Form->create('UserInvite'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('user_id',array('type'=>'select','options'=>$user_array));
		echo $this->Form->input('invite_emailid');
		//echo $this->Form->input('invite_status');
	?>
	</fieldset>
<div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'user_invites','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
</div>
</div>
</div>
        </section><!-- /.content -->