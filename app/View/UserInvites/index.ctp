        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo __('User Invites'); ?>
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(
                    'Users',
                    array('controller' => 'users', 'action' => 'index')); ?>
            </li>
            <li class="active">Invites</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		<div class="row">
		<div class="col-xs-12">
		<div class="box">
	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Invited By'); ?></th>
			<th><?php echo $this->Paginator->sort('Invited To'); ?></th>
			<th><?php echo $this->Paginator->sort('invite_status'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userInvites as $userInvite): ?>
	<tr>
		<td><?php echo h($userInvite['UserInvite']['id']); ?>&nbsp;</td>
		<td>
			<?php if(!empty($userInvite['User']['username'])){echo $this->Html->link($userInvite['User']['username'], array('controller' => 'users', 'action' => 'view', $userInvite['User']['id'])); } else {
				echo $this->Html->link($userInvite['User']['email'], array('controller' => 'users', 'action' => 'view', $userInvite['User']['id']));} ?>
		</td>
		<td><?php echo h($userInvite['UserInvite']['invite_emailid']); ?>&nbsp;</td>
		<td><?php  if($userInvite['UserInvite']['invite_status']==0){echo 'Not Accepted'; }else{echo 'Accepted'; } ?>&nbsp;</td>
		<td><?php echo h($userInvite['UserInvite']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userInvite['UserInvite']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userInvite['UserInvite']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userInvite['UserInvite']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userInvite['UserInvite']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	</div> <!-- / .box-->
		</div> <!-- / .col-xs-12-->
		</div>	<!-- / .row-->
        </section><!-- /.content -->


<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
       "dom": '<lf<t>ip>'
    } );
});
</script>