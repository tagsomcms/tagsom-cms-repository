        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo __('Edit User Invite'); ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(
                    'User Invite',
                    array('controller' => 'user_invites', 'action' => 'index')); ?>
            </li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		 <div class="userInvites row">
		 	<div class="col-xs-12">
		<div class="box box-primary">
<?php echo $this->Form->create('UserInvite'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id',array('type'=>'select','options'=>$user_array,'value'=>$select));
		echo $this->Form->input('invite_emailid');
		//echo $this->Form->input('invite_status');
	?>
	</fieldset>
<div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'user_invites','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
</div>
</div>
</div>
        </section><!-- /.content -->



<!-- <div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('UserInvite.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('UserInvite.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List User Invites'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div> -->
