<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('User Mode Status'); ?></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Examples</a></li>
    <li class="active">Blank page</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 
<div class="userModeStatuses row">
<div class="col-xs-12">
		<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('story_id'); ?></th>
			<th><?php echo $this->Paginator->sort('answer'); ?></th>
			<th><?php echo $this->Paginator->sort('user_attempt'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userModeStatuses as $userModeStatus): ?>
	<tr>
		<td><?php echo h($userModeStatus['UserModeStatus']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userModeStatus['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userModeStatus['Language']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeStatus['Level']['level_name'], array('controller' => 'levels', 'action' => 'view', $userModeStatus['Level']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeStatus['User']['username'], array('controller' => 'users', 'action' => 'view', $userModeStatus['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userModeStatus['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userModeStatus['Story']['id'])); ?>
		</td>
		<td><?php echo h($userModeStatus['UserModeStatus']['answer']); ?>&nbsp;</td>
		<td><?php echo h($userModeStatus['UserModeStatus']['user_attempt']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userModeStatus['UserModeStatus']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userModeStatus['UserModeStatus']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userModeStatus['UserModeStatus']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userModeStatus['UserModeStatus']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>

</div>
</div>	
</div>
</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>