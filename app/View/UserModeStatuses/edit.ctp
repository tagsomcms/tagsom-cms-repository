<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit User Mode Status'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('User Mode Status', array('controller' => 'user_mode_statuses', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="userModeStatuses row">
 	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('UserModeStatus',array('name'=>'UserModeStatus')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('language_id');
		echo $this->Form->input('level_id');
		echo $this->Form->input('story_id',array('type'=>'select','id'=>'st1'));
		echo $this->Form->input('mode_question_answer_id',array('type'=>'select','options'=>$question_array,'id'=>'questions','value'=>$selected_question));
		echo $this->Form->input('answer',array('type'=>'select','id'=>'answers','options'=>$answers_array,'value'=>$selected_answer));
    echo $this->Form->input('user_id',array('type'=>'select','options'=>$user_array,'value'=>$select));
		echo $this->Form->input('user_attempt',array('min'=>0));
    echo $this->Form->input('h1',array('type'=>'hidden','id'=>'h1','value'=>$first_question));
    echo $this->Form->input('h2',array('type'=>'hidden','id'=>'h2','value'=>$first_answer));
	?>
	<div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'user_mode_statuses','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

	</div>
	</fieldset>
</div>
</div>
</div>
</section><!-- /.content -->


<script>


$('#st1').change(function(){
  $('#questions').html('');
   $('#answers').html('');
var stid=$('#st1').val();
$.ajax({
  dataType: 'json',
  type: 'POST',
  data: {st1:stid},
  success: function(response) 
  {
    $('#h1').val(response.questions[0]);
    $('#h2').val(response.answers[0]);
    jQuery.each(response.questions, function(index, item) {

      $('#questions').append('<option value="' + index + '">' + item + '</option>');
    });
    jQuery.each(response.answers, function(index, item) {

        $('#answers').append('<option value="' + index + '">' + item + '</option>');
    });
  },

  });  

});

$('#questions').change(function(){
  $('#answers').html('');
  
  var qid=$('#questions').find("option:selected").text();
   $('#h1').val(qid);
$.ajax({
  dataType: 'json',
  type: 'POST',
  data: {q_id:qid},
  success: function(response) 
  {
  $('#h2').val(response.answers[0]);
    jQuery.each(response.answers, function(index, item) {

        $('#answers').append('<option value="' + index + '">' + item + '</option>');
    });

  },

  });  

});

$('#answers').change(function(){
   var qid=$('#answers').find("option:selected").text();
   $('#h2').val(qid);

});
</script>
