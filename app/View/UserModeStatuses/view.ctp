        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><?php echo __('User Mode Status'); ?></h1>
          <ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('User Mode Status', array('controller' => 'user_mode_statuses', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		 <div class="userModeStatuses row">
		 	<div class="col-xs-12">
		 		<div class="box">
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($userModeStatus['UserModeStatus']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Language'); ?></th>
			<td><?php echo $this->Html->link($userModeStatus['UserModeStatus']['language_id'], array('controller' => 'languages', 'action' => 'view', $userModeStatus['UserModeStatus']['language_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Level'); ?></th>
			<td><?php echo $this->Html->link($userModeStatus['UserModeStatus']['level_id'], array('controller' => 'levels', 'action' => 'view', $userModeStatus['UserModeStatus']['level_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('User'); ?></th>
			<td><?php echo $this->Html->link($userModeStatus['UserModeStatus']['user_id'], array('controller' => 'users', 'action' => 'view', $userModeStatus['UserModeStatus']['user_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Story'); ?></th>
			<td><?php echo $this->Html->link($userModeStatus['UserModeStatus']['story_id'], array('controller' => 'stories', 'action' => 'view', $userModeStatus['UserModeStatus']['story_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Mode Question Answer'); ?></th>
			<td><?php echo $this->Html->link($userModeStatus['UserModeStatus']['mode_question_answer_id'], array('controller' => 'mode_question_answers', 'action' => 'view', $userModeStatus['UserModeStatus']['mode_question_answer_id'])); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Answer'); ?></th>
			<td><?php echo h($userModeStatus['UserModeStatus']['answer']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('User Attempt'); ?></th>
			<td><?php echo h($userModeStatus['UserModeStatus']['user_attempt']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Last Modified'); ?></th>
			<td><?php echo h($userModeStatus['UserModeStatus']['modified']); ?></td>
		</tr>
	</table>
</div>
</div>	
</div>
</section><!-- /.content -->
