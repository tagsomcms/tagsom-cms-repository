<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Level'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Levels', array('controller' => 'levels', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="levels row">
 	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Level'); ?>
	<div class="box-body">
	<?php
		echo $this->Form->input('level_name');
	?>

  <div class="row">
    <div class="col-md-2" style="clear:none !important;">
      <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
    </div>
    <div class="col-md-2" style="clear:none !important;margin-top:20px;margin-left:-30px;">
        <?php echo $this->Html->link("Back", array('controller' => 'levels','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
    </div>
  </div>

	</div>
</div>
</div>
</div>
</section><!-- /.content -->