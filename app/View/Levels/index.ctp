<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Levels'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Levels', array('controller' => 'levels', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="levels row">
	<div class="col-xs-12">
		<div class="box">
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_name'); ?></th>
			<th><?php echo $this->Paginator->sort('last modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($levels as $level): ?>
	<tr>
		<td><?php echo h($level['Level']['id']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['level_name']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $level['Level']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $level['Level']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $level['Level']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $level['Level']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
</div>
</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>