<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Level'); ?></h1>
  <ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('Users', array('controller' => 'users', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="levels row">
 	<div class="col-xs-12">
		 		<div class="box">
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($level['Level']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('Level Name'); ?></th>
		<td>
			<?php echo h($level['Level']['level_name']); ?>
		</td></tr>
		<tr><th><?php echo __('Last Modified'); ?></th>
		<td>
			<?php echo h($level['Level']['modified']); ?>
		</td></tr>
	</table>
</div>
</div>
</div>
<div class="related">
	<h3><?php echo __('Related User Mode Grades'); ?></h3>
	<?php if (!empty($level['UserModeGrade'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th><?php echo __('Story Id'); ?></th>
		<th><?php echo __('Language Id'); ?></th>
		<th><?php echo __('Mode Id'); ?></th>
		<th><?php echo __('Grade Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($level['UserModeGrade'] as $userModeGrade): ?>
		<tr>
			<td><?php echo $userModeGrade['id']; ?></td>
			<td><?php echo $userModeGrade['user_id']; ?></td>
			<td><?php echo $userModeGrade['level_id']; ?></td>
			<td><?php echo $userModeGrade['story_id']; ?></td>
			<td><?php echo $userModeGrade['language_id']; ?></td>
			<td><?php echo $userModeGrade['mode_id']; ?></td>
			<td><?php echo $userModeGrade['grade_id']; ?></td>
			<td><?php echo $userModeGrade['created']; ?></td>
			<td><?php echo $userModeGrade['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'user_mode_grades', 'action' => 'view', $userModeGrade['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'user_mode_grades', 'action' => 'edit', $userModeGrade['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'user_mode_grades', 'action' => 'delete', $userModeGrade['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userModeGrade['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User Mode Grade'), array('controller' => 'user_mode_grades', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related User Mode Statuses'); ?></h3>
	<?php if (!empty($level['UserModeStatus'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Language Id'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Story Id'); ?></th>
		<th><?php echo __('Mode Question Answer Id'); ?></th>
		<th><?php echo __('Answer'); ?></th>
		<th><?php echo __('User Attempt'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($level['UserModeStatus'] as $userModeStatus): ?>
		<tr>
			<td><?php echo $userModeStatus['id']; ?></td>
			<td><?php echo $userModeStatus['language_id']; ?></td>
			<td><?php echo $userModeStatus['level_id']; ?></td>
			<td><?php echo $userModeStatus['user_id']; ?></td>
			<td><?php echo $userModeStatus['story_id']; ?></td>
			<td><?php echo $userModeStatus['mode_question_answer_id']; ?></td>
			<td><?php echo $userModeStatus['answer']; ?></td>
			<td><?php echo $userModeStatus['user_attempt']; ?></td>
			<td><?php echo $userModeStatus['created']; ?></td>
			<td><?php echo $userModeStatus['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'user_mode_statuses', 'action' => 'view', $userModeStatus['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'user_mode_statuses', 'action' => 'edit', $userModeStatus['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'user_mode_statuses', 'action' => 'delete', $userModeStatus['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userModeStatus['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User Mode Status'), array('controller' => 'user_mode_statuses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div style="clear: both;"></div>

</section><!-- /.content -->




