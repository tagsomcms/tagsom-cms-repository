<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit User Purchase'); ?></h1>
  <ol class="breadcrumb">
    	<li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    	<li><?php echo $this -> Html -> link('User Purchase', array('controller' => 'user_purchases', 'action' => 'index')); ?>
    	</li>
    	<li class="active">Edit</li>
  	  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="userPurchases row">
<div class="col-xs-12">
<div class="box box-primary">	
<?php echo $this->Form->create('UserPurchase'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('language_id');
		echo $this->Form->input('purchase_status');
		echo $this->Form->input('story_id');
		echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));
	?>
	</fieldset>
</div>
</div>
</div>
</section><!-- /.content -->