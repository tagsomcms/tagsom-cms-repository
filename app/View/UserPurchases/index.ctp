<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('User Purchases'); ?></h1>
  <ol class="breadcrumb">
    	<li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    	<li><?php echo $this -> Html -> link('User Purchase', array('controller' => 'user_purchases', 'action' => 'index')); ?>
    	</li>
    	<li class="active">Index</li>
  	  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="userPurchases row">
 	<div class="col-xs-12">
		<div class="box">
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('purchase_status'); ?></th>
			<th><?php echo $this->Paginator->sort('story_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userPurchases as $userPurchase): ?>
	<tr>
		<td><?php echo h($userPurchase['UserPurchase']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userPurchase['User']['username'], array('controller' => 'users', 'action' => 'view', $userPurchase['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userPurchase['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userPurchase['Language']['id'])); ?>
		</td>
		<td><?php echo h($userPurchase['UserPurchase']['purchase_status']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userPurchase['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userPurchase['Story']['id'])); ?>
		</td>
		<td><?php echo h($userPurchase['UserPurchase']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userPurchase['UserPurchase']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userPurchase['UserPurchase']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userPurchase['UserPurchase']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userPurchase['UserPurchase']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->