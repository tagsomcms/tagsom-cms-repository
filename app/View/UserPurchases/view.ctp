<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('User Purchase'); ?></h1>
  <ol class="breadcrumb">
    	<li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    	<li><?php echo $this -> Html -> link('User Purchase', array('controller' => 'user_purchases', 'action' => 'index')); ?>
    	</li>
    	<li class="active">View</li>
  	  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="userPurchases row">
 	<div class="col-xs-12">
	<div class="box">
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($userPurchase['UserPurchase']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('User'); ?></th>
		<td>
			<?php echo $this->Html->link($userPurchase['User']['username'], array('controller' => 'users', 'action' => 'view', $userPurchase['User']['id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Language'); ?></th>
		<td>
			<?php echo $this->Html->link($userPurchase['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userPurchase['Language']['id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Purchase Status'); ?></th>
		<td>
			<?php echo h($userPurchase['UserPurchase']['purchase_status']); ?>
		</td></tr>
		<tr><th><?php echo __('Story'); ?></th>
		<td>
			<?php echo $this->Html->link($userPurchase['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userPurchase['Story']['id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Last Modified'); ?></th>
		<td>
			<?php echo h($userPurchase['UserPurchase']['modified']); ?>
		</td></tr>
	</table>
</div>
</div>
</div>
</section><!-- /.content -->