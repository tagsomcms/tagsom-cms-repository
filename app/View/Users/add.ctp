<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add User'); ?></h1>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Users', array('controller' => 'users', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
  <div class="col-xs-12">
  <div class="box box-primary">
<?php echo $this->Form->create('User'); ?>
<div class="box-body">
  <?php
    echo $this->Form->input('first_name');
    echo $this->Form->input('last_name');
    echo $this->Form->input('username');
    echo $this->Form->input('email');
    // echo $this->Form->input('relationship');
    // echo $this->Form->input('login_portal');
    $options=array('admin','child','parent','teacher','student');
    echo $this->Form->input('password');
    echo $this->Form->input('role',array('options'=>$options));
    echo $this->Form->input('free_book');
  ?>
  <div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'users','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
  </div>
</div>
</div>
</div>
</section><!-- /.content -->
