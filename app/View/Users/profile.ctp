<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
   <?php echo __('User Profile'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Users', array('controller' => 'users', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="users row">
 	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('User',array('type' => 'file')); ?>
	<div class="box-body">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('username');
		echo $this->Form->input('email');
		echo $this->Form->input('profile_pic',array('type'=>'file','style'=>'height:100%;'));
		//echo $this->Form->input('password');
		echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));
	?>
</div>
</div>
</div>
</div>
</section><!-- /.content -->
