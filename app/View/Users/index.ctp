<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo __('Users List'); ?>
    <small></small>
  </h2>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Users', array('controller' => 'users', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

        <!-- Main content -->
        <section class="content">
		 <div class="row">
		<div class="col-xs-12">
		<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id='searchable'>
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('first_name'); ?></th>
			<th><?php echo $this->Paginator->sort('last_name'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('relationship'); ?></th>
			<th><?php echo $this->Paginator->sort('login_portal'); ?></th> -->
            <th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('free_book'); ?></th>
			<th><?php echo $this->Paginator->sort('is_verified','Status'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<!-- <td><?php echo h($user['User']['relationship']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['login_portal']); ?>&nbsp;</td> -->
		<td><?php echo h($user['User']['type']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['free_book']); ?>&nbsp;</td>
		<td><?php if($user['User']['is_verified']==1){echo "Active"; }else{echo "Inactive";}  ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	</div>
	</div>
	</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>

