        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo __('Teacher'); ?>
          </h1>
          <ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('Teachers', array('controller' => 'teachers', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		 
		 <div class="users row">
		 	<div class="col-xs-12">
		 		<div class="box">
			 	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<td><?php echo h($user['User']['id']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('First Name'); ?></th>
						<td><?php echo h($user['User']['first_name']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Last Name'); ?></th>
						<td><?php echo h($user['User']['last_name']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Email'); ?></th>
						<td><?php echo h($user['User']['email']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Last Modified'); ?></th>
						<td><?php echo h($user['User']['modified']); ?></td>
					</tr>
					<tr>
		                <td colspan=2> <?php echo $this->Html->link("Back", array('controller' => 'Teachers','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
		                </td>
		            </tr>
				</table>
				</div>
		 	</div>
		 
</div>
	<div style="clear: both;"></div>	
</div>
</section><!-- /.content -->