<style>
    .pagination > li:first-child > a, .pagination > li:first-child > span {margin-top: 20px;}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Import Teachers'); ?></h1>
  <ol class="breadcrumb">
    <li>
        <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
        </li>
    <li><?php echo $this -> Html -> link('Teachers', array('controller' => 'teachers', 'action' => 'index')); ?>
    </li>
    <li class="active">Import</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
<div class="col-xs-12">
<!-- <div class="container"> -->
    <div class="box">
      <div class="panel panel-default">
        <div class="panel-heading"><strong>Upload Excel Sheet of Teachers</strong>
            <span class="pull-right">
               <a href="<?php echo $this->webroot; ?>files/teacher_format.xls " class="btn btn-primary" style="padding-bottom: inherit;padding-top: inherit;">Download XLS Format</a>
            </span>
            
        </div>
        <div class="panel-body">
          <h4>Select files from your computer</h4>
          <?php 
                echo $this->Form->create('Teacher',array('type'=>'file')); 
                echo $this->Form->input('upload',array('type'=>'file','style'=>'height:100%;','onchange'=>'return ValidateXLSExtension(this)'));
                ?>
                <div class="col-md-7" style="width:100%;margin-top:-10px;">
                <br><span id="lblError" style="color: red;"></span>
                </div>
                <div class="row">
                  <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
                    <?php echo $this->Html->link("Back", array('controller' => 'teachers','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
                  </div>
                  <div class="col-md-1" style="clear:none !important;">
                    <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
                  </div> 
                </div>
                
        </div>
      </div>
     </div> 
      
    <!-- </div> --> 
    <!-- /container -->  
    
    
   <?php if(isset($teachers) && !empty($teachers) ): ?> 
    
    <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Following data imported into Teachers table</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>School Email ID</th>
                        <th>Contact Number</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach($teachers as $std): ?>
                      <tr>
                        <td><?php echo $std['User']['first_name']; ?></td>
                        <td><?php echo $std['User']['last_name']; ?></td>
                        <td><?php echo $std['User']['gender']; ?></td>
                        <td><?php echo $std['User']['email']; ?></td>
                        <td><?php echo $std['User']['contact_no']; ?></td>
                      </tr>
                     <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>School Email ID</th>
                        <th>Contact Number</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    <?php endif; ?>
    
    <?php if(isset($not_inserted_records) && !empty($not_inserted_records) ): ?> 
    
    <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Following data is not imported into Teachers table</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>School Email ID</th>
                        <th>Contact Number</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach($not_inserted_records as $records): ?>
                      <tr>
                        <td><?php echo $records['User']['first_name']; ?></td>
                        <td><?php echo $records['User']['last_name']; ?></td>
                        <td><?php echo $records['User']['gender']; ?></td>
                        <td><?php echo $records['User']['email']; ?></td>
                        <td><?php echo $records['User']['contact_no']; ?></td>
                      </tr>
                     <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>School Email ID</th>
                        <th>Contact Number</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    <?php endif; ?>
    
    
    
    
</div>
</div>
</section>
