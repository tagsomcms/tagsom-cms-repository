<!-- Content Header (Page header) -->
<?php
$options=array('Select From Existing Title');
foreach($settings1 as $setting)
{
  array_push($options,$setting['GeneralSettings']['title']);
}

?>
<section class="content-header">
  <div class="row">
    <div class="col-md-4">  <h1><?php echo __('General Settings'); ?></h1></div>
    <div class="col-sm-3">
      <?php
        if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
      ?>
    </div>
    <div class="col-sm-1">
      <?php
        if(isset($selected_title))
        {
          $select1=$selected_title;
        }
        else
        {
        $select1=1;
        }
        echo $this->Form->input('titles',array('value'=>$select1,'id'=>'select2','label'=>false,'type'=>'select','options'=>$options,'style'=>'width: 200px;height:33px;margin-top: 25px;','onchange'=>'StoryLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  </div>

  <ol class="breadcrumb">
        <li>
          <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
          </li>
        <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
        </li>
        <li class="active">General Settings</li>
  </ol>
</section>





<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
<thead>
<tr>
      <th>Id</th>
      <th>Title</th>
      <th>String & Audio</th>
      <th>Actions</th>

 
</tr>
</thead>
<tbody>

<?php
foreach($settings as $setting)
{

  $title=$setting['GeneralSettings']['title']; 
?>

  <tr>
    <td>
      <?php echo $setting['GeneralSettings']['id']; ?>
      
      
    </td>
    <td>
      <?php echo "<h5 style='color:black;'><b>Title : ".$title."</b></h5>";?>
    </td>
    <td>
      <div class="row">
        <div class="col-xs-4" >
          <b style="margin-left:5px;"><?php echo $setting['GeneralSettings']['string']; ?></b><br><br>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4" style="clear:none !important;">
          <?php echo $this->Form->create('Settings_Form',array('type'=>'file')); ?>
          <?php if($setting['GeneralSettings']['title']=='No Translation' || $setting['GeneralSettings']['string']=='No Translation'){ 
              echo $this->Form->input('audio_clip',array('type'=>'file','style'=>'width:inherit;margin-top:-10px;height:inherit;','label'=>false,'required'=>true,'id'=>$setting['GeneralSettings']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$setting['GeneralSettings']['id'].')','disabled'=>true)); }
              else{
                echo $this->Form->input('audio_clip',array('type'=>'file','style'=>'width:inherit;margin-top:-10px;height:inherit;','label'=>false,'required'=>true,'id'=>$setting['GeneralSettings']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$setting['GeneralSettings']['id'].')'));

                if(file_exists(WWW_ROOT.'uploads/Settings_Audio/'.$language_name.'/'.$setting['GeneralSettings']['audio_clip']))
                {
                  $rand_no=rand(1111,9999);
                  $myAudioFile = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Settings_Audio/'.$language_name.'/'.$setting['GeneralSettings']['audio_clip'];
                  if($setting['GeneralSettings']['disable']!=1)
                  {
                  ?>
                  <audio controls="controls" id="<?php echo $setting['GeneralSettings']['id']; ?>">  
                     <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />   
                  </audio> 
                  <?php
                  }
                  if($setting['GeneralSettings']['disable']==1 && $this->Session->read('Auth.User.type')=='admin')
                  {
                  ?>
                  <audio controls="controls" id="<?php echo $setting['GeneralSettings']['id']; ?>">  
                     <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />   
                  </audio> 
                  <?php
                  }
                }

              }?>
            <div class="col-md-6" style="width:100%;margin-top:-20px;">
                <br><span id="lblError<?php echo $setting['GeneralSettings']['id']; ?>" style="color: red;"></span>
            </div>
          <?php echo $this->Form->input('hidden_id',array('type'=>'hidden','value'=>$setting['GeneralSettings']['id'])); ?>
          <?php echo $this->Form->input('hidden_string',array('type'=>'hidden','value'=>$setting['GeneralSettings']['string'])); ?>
           <?php echo $this->Form->input('hidden_language',array('type'=>'hidden','value'=>$setting['GeneralSettings']['language_id'])); ?>
             <?php echo $this->Form->input('filename',array('type'=>'hidden','value'=>$setting['GeneralSettings']['audio_clip'])); ?>

        </div>
      </div>
        </td>
        <td class="actions">
          <?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin'){  
            echo $this->Html->link('Edit',array('action'=>'edit_setting',$setting['GeneralSettings']['id']),array('class'=>'btn btn-primary','style'=>'margin-top:-3px;')); 
            echo $this->Html->link('Delete',array('action'=>'delete_string',$setting['GeneralSettings']['id']),array('class'=>'btn btn-primary','style'=>'margin-top:-3px;margin-left:5px;','confirm' =>('Are you sure you want to delete this settings?'))); 
          }?>
          <br><br>
          <?php 
          if($setting['GeneralSettings']['title']=='No Translation' || $setting['GeneralSettings']['string']=='No Translation')
          {
            if(file_exists(WWW_ROOT.'uploads/Settings_Audio/'.$language_name.'/'.$setting['GeneralSettings']['audio_clip']))
            {
              echo $this->Form->submit(__('Replace Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;','disabled'=>true)); 
            }
            else
            {
              echo $this->Form->submit(__('Upload Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;','disabled'=>true));
            }
          }
          else 
          {
            if(file_exists(WWW_ROOT.'uploads/Settings_Audio/'.$language_name.'/'.$setting['GeneralSettings']['audio_clip']))
            {
              echo $this->Form->submit(__('Replace Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;')); 
              if($this->Session->read('Auth.User.type')=='admin' && $language_name=='English')
              {
                if($setting['GeneralSettings']['disable']==0)
                {
                  echo $this->Html->link(__('Disable Audio'), array('action' => 'disable_settings', $id,$setting['GeneralSettings']['id'],1));
                }
                else
                {
                  echo $this->Html->link(__('Enable Audio'), array('action' => 'disable_settings',$id, $setting['GeneralSettings']['id'],0));
                }
              }
            }
            else
            {
              echo $this->Form->submit(__('Upload Audio'),array('class'=>'btn btn-primary','style'=>'margin-top:-13px;margin-left:-10px;'));
            }
          }
          ?>
          <?php echo $this->Form->end(); ?>
  
     </td>
    
    
  </tr>
<?php 
}
?>
 
  </tbody>
 <tr>
    <td colspan=3>
      <?php if($language_name=='English')
      { ?>
      <div class="col-xs-2" style="clear:none !important;margin-top:20px;">
              <?php echo $this->Html->link("Add New String", array('action'=> 'add_string',$id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      </div>
       <?php } ?>
      <div class="col-xs-1" style="clear:none !important;margin-top:20px;">
              <?php echo $this->Html->link("Back", array('action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
      </div>
     
    </td>
  </tr>
  </table>

</div>
</div>

</div>

</section><!-- /.content -->



<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}

$(document).ready(function(){
document.getElementById("select2").options[0].disabled = true;
});
</script>

<script>
$(document).ready(function () {
$.fn.dataTableExt.sErrMode = "console";
     $('#searchable').DataTable( {
         "dom": '<lf<t>ip>'
    } );
});
</script>

