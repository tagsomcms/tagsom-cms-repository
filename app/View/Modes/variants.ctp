<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Variants List'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Scene', array('controller' => 'modes', 'action' => 'manage_scene',$math_id,$story_id)); ?>
    </li>
    <li class="active">Variants List</li>
  </ol>
</section>





<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id='searchable'>
<thead>
<tr>
    <th>ID</th>
    <th>Variant</th>
    <th>Audio</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($all_variant)) 
{  
?>
<tr>
<td colspan=4><h4>No Variants are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($all_variant as $variant)
  {
?>
  <tr>
    <td><?php echo $variant['Variant']['id']; ?></td>
    <td><img class="thumb" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Variants/". $variant['Variant']['image']; ?>" style="width:60px;height:50px;" id="img2"></td>
    <td>
      <?php
       echo $this->Form->create('VariantImage',array('name'=>'VariantImage','type'=>'file')); 
       $object_name='object_audio'.$object['MathObject']['id'];
        echo $this->Form->input('image',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:10px;height:inherit;','required'=>true,'id'=>$variant['Variant']['id'],'onchange'=>'return ValidateImageExtension(this,'.$variant['Variant']['id'].')'));
         echo $this->Form->input('hidden',array('type'=>'hidden','value'=>$variant['Variant']['image']));
        ?>

        <div class="col-md-6" style="width:100%;margin-top:-20px;">
          <br><span id="lblError<?php echo $variant['Variant']['id']; ?>" style="color: red;"></span>
        </div>

    </td>
 
     <td class="actions">  
      <?php 
      echo $this->Form->submit('Replace Image',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-left:-5px;')); 
      echo $this->Html->link("Delete", array('action'=> 'delete_variant',$variant['Variant']['id'],$math_id,$story_id,$mode_id), array( 'confirm' => __('Are you sure you want to delete this variant?'),'class' => 'btn btn-primary')); 
      ?>
    </td>
    
  </tr>
<?php
  echo $this->Form->end();
  }
}
?>
<tr>

</tr>

  </tbody>
  </table>
  
  <div class="row">
    <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-bottom:20px;margin-left:10px;">
      <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_scene',$mode_id,$math_id,$story_id), array( 'class' => 'btn btn-primary')); ?>
    </div>
    <div class="col-xs-2" style="clear:none !important;margin-top:20px;margin-bottom:20px;margin-left:10px;">
      <?php echo $this->Html->link("Resize Variants", array('controller' => 'Modes','action'=> 'resize_variants',$math_id,$story_id,$mode_id), array( 'class' => 'btn btn-primary')); ?>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section><!-- /.content -->


<script>
$(document).ready(function () {
$.fn.dataTableExt.sErrMode = "console";
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>