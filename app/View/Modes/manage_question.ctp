<?php
header ('Content-Type: text/html; charset=UTF-8'); 
echo '<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />'; 
?>

<?php 
if(!empty($story_path))
{
  $story_path1=$story_path;
}
?>
<!-- Start Breadcrumb -->
<section class="content-header">
  <div class="row">
    <div class="col-sm-3">  <h2 style="background:none;color:black;"><?php echo __('Manage Question'); ?></h2></div>
    <div class="col-sm-3">
      <?php
        if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
      ?>
    </div>
    <div class="col-sm-2">
      <?php
        if(isset($selected_story))
        {
          $select1=$selected_story;
        }
        else
        {
        $select1=0;
        }
        echo $this->Form->input('story',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$stories_array,'value'=>$select1,'style'=>'height: 33px;margin-top: 25px;width: 200px;margin-left:0px;','onchange'=>'StoryLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  

  </div>
  <ol class="breadcrumb" style="margin-top:25px;">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Question</li>
  </ol>
</section>
<!-- End Breadcrumb -->




<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 

<?php 
if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin') 
{ 
  echo $this->Html->link("Resize Question Images", array('controller' => 'Modes','action'=> 'resize_question_images',$story_id,$id), array( 'class' => 'btn btn-primary','style'=>'margin: 5px 5px 5px 5px;')); 
}
?>
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Id</th>
    <th>Question/Answer</th>
    <th>Audio</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>

<?php
foreach($questions as $question)
{
  $arr=array($question['Question']['option1']=>$question['Question']['option1'],$question['Question']['option2']=>$question['Question']['option2'],$question['Question']['option3']=>$question['Question']['option3']);
  $checked=$question['Question']['answer'];
  echo $this->Form->create('Question_Answer',array('type'=>'file'));
  ?>

  <tr>
    <td><?php echo $question['Question']['id']; ?></td>
    <td><p style="text-align:left;" <?php if($language_name=='Arabic') { ?> dir="rtl" <?php } ?>><?php echo $question['Question']['question']; ?><p></td>
    <td><?php 
      if($question['Question']['question']!='No Translation') {echo $this->Form->input('question_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['id'].')'));  
        $rand_no=rand(6666,9999);
        $Questions_Audio_File = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'.$question['Question']['question_audio'];
        if(file_exists(WWW_ROOT.'uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'. $question['Question']['question_audio'])  &&  $question['Question']['disable']==0)
        {
      ?>
        <audio controls="controls" id="Q<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Questions_Audio_File.'?'.$rand_no; ?>" />  
        </audio> 

      <?php
        }
        if($this->Session->read('Auth.User.type')=='admin' && $question['Question']['disable']==1) {?> 
        <audio controls="controls"  id="Q<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Questions_Audio_File.'?'.$rand_no; ?>" />  
        </audio> 
        <?php }
      }
      else { echo $this->Form->input('question_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['id'].')','disabled'=>true)); }?>
      <div class="col-md-6" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['id']; ?>" style="color: red;"></span>
      </div>
      <?php echo $this->Form->input('h1',array('label'=>false,'type'=>'hidden','value'=>$question['Question']['id'])); ?>
      
    </td>
     <td class="actions">
      <?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin') { echo $this->Html->link(__('Edit'), array('action' => 'edit_question',$question['Question']['id'],$id,$story_id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      <?php echo $this->Html->link(__('Delete'), array('action' => 'delete_question',$question['Question']['id'],$id,$story_id,$language_name), array('confirm' => __('Are you sure you want to delete this question?'),'class' => 'btn btn-primary')); } ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td style="text-align:left;"><?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin') { echo $this->Form->input('option_select1', array(
        'type' => 'radio',
        'legend' => false,
        'options'=>$arr,
        'style'=>'margin-left:0px;margin-top:-3px;width:20px;box-shadow: none;',
        'value'=>$checked
    ));  } else {
    echo $this->Form->input('option_select1', array(
      'type' => 'radio',
      'legend' => false,
      'options'=>$arr,
      'style'=>'margin-left:0px;margin-top:-3px;width:20px;box-shadow: none;pointer-events:none;',
      'value'=>$checked
    ));
    }

   ?>
    <?php if($question['Question']['question']=='No Translation')
    { ?>
    <td><?php echo $this->Form->input('option1_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['option1'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option1'].')','disabled'=>true)); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option1']; ?>" style="color: red;"></span>
      </div>
     
    <?php echo $this->Form->input('option2_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['option2'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option2'].')','disabled'=>true)); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option2']; ?>" style="color: red;"></span>
      </div>
     
    <?php echo $this->Form->input('option3_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['option3'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option3'].')','disabled'=>true)); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option3']; ?>" style="color: red;"></span>
      </div>
      

  </td>
   <td class="actions" >
      <?php echo $this->Form->Submit(__('Save Changes'),array('class'=>'btn btn-primary','style'=>'margin-left:-7px;','disabled'=>true)); ?>
    </td>
  <?php } else { 
      $rand_no=rand(1111,2222);
      $rand_no2=rand(3333,4444);
      $rand_no3=rand(4555,5555);
      $Option1_Audio_File = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'.$question['Question']['option1_audio'];
      $Option2_Audio_File = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'.$question['Question']['option2_audio'];
      $Option3_Audio_File = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'.$question['Question']['option3_audio'];

    ?>
     <td><?php echo $this->Form->input('option1_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['option1'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option1'].')')); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option1']; ?>" style="color: red;"></span>
      </div>
      <?php if(file_exists(WWW_ROOT.'uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'. $question['Question']['option1_audio'])  &&  $question['Question']['disable']==0)
        { ?>
      <audio controls="controls"  id="op1<?php echo $question['Question']['id']; ?>">  
        <source src="<?php echo $Option1_Audio_File.'?'.$rand_no; ?>" />  
      </audio> 
       
      <?php } ?>
      <?php if($this->Session->read('Auth.User.type')=='admin' && $question['Question']['disable']==1) {?> 
          <audio controls="controls"  id="op1<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Option1_Audio_File.'?'.$rand_no; ?>" />  
        </audio> 
        <?php } ?>
      
    <?php echo $this->Form->input('option2_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['option2'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option2'].')')); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option2']; ?>" style="color: red;"></span>
      </div>
       <?php if(file_exists(WWW_ROOT.'uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'. $question['Question']['option1_audio'])  &&  $question['Question']['disable']==0)
        { ?>
        <audio controls="controls"  id="op2<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Option2_Audio_File.'?'.$rand_no2; ?>" />  
        </audio> 
       
       <?php } ?>
        <?php if($this->Session->read('Auth.User.type')=='admin' && $question['Question']['disable']==1) {?> 
          <audio controls="controls"  id="op2<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Option2_Audio_File.'?'.$rand_no2; ?>" />  
        </audio> 
        <?php } ?>

      
    <?php echo $this->Form->input('option3_audio',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-left:15px;','id'=>$question['Question']['option3'],'onchange'=>'return ValidateAudioExtension(this,'.$question['Question']['option3'].')')); ?>
      <div class="col-md-2" style="width:100%;">
              <br><span id="lblError<?php echo $question['Question']['option3']; ?>" style="color: red;"></span>
      </div>
       <?php if(file_exists(WWW_ROOT.'uploads/Questions/'.$story_path1.'/Questions_Audio/'.$language_name.'/'. $question['Question']['option1_audio']) &&  $question['Question']['disable']==0)
        { ?>
        <audio controls="controls"  id="op3<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Option3_Audio_File.'?'.$rand_no3; ?>" />  
        </audio> 
        
       <?php } ?>
        <?php if($this->Session->read('Auth.User.type')=='admin' && $question['Question']['disable']==1) {?> 
          <audio controls="controls"  id="op3<?php echo $question['Question']['id']; ?>">  
          <source src="<?php echo $Option3_Audio_File.'?'.$rand_no3; ?>" />  
        </audio> 
        <?php } ?>
      
  </td>
    <td class="actions" >
      <?php echo $this->Form->Submit(__('Save Changes'),array('class'=>'btn btn-primary','style'=>'margin-left:-7px;')); ?>
      <br><br>
       <?php
		if($this->Session->read('Auth.User.type')=='admin')
		{
			if($question['Question']['disable']==0)
			{
			  echo $this->Html->link(__('Disable Audio'), array('action' => 'disable_question',$question['Question']['id'],$id,$story_id,1), array( 'class' => 'btn btn-primary'));  
			}
			else
			{
			  echo $this->Html->link(__('Enable Audio'), array('action' => 'disable_question',$question['Question']['id'],$id,$story_id,0), array( 'class' => 'btn btn-primary'));   
			}
		}
        ?>
    </td>

    <?php 
   
  } ?>
 
   
  </tr>

  <?php
  echo $this->Form->end();
}
?>



<tr>
  <td class="actions" colspan=2>
    <?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin') {echo $this->Html->link(__('Add more'), array('action' => 'add_question',$id,$story_id, $language_id), array( 'class' => 'btn btn-primary')); }?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </td>
</tr>

  </tbody>
  </table>

 <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  

</div>
</div>
</div>
</section><!-- /.content -->

<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}


$(document).ready(function(){
document.getElementById("select1").options[0].disabled = true;
document.getElementById("select2").options[0].disabled = true;
});
</script>

<style>
.radio label {
    margin: -4px 1px 27px 24px;
    line-height: 36px;
}
box-shadow: none;
</style>

