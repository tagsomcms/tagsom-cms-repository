<!-- Start Breadcrumb -->
<section class="content-header">
 <div class="col-md-4">  <h1><?php echo __('Manage Alphabet'); ?></h1></div>
    <div class="col-sm-1">
      <?php
       if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('AlphabetLanguage',array('name'=>'AlphabetLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'AlphabetLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Alphabets</li>
  </ol>
</section>
<!-- End Breadcrumb -->



<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
<thead>
<tr>
    <th>Id</th>
    <th>Alphabet</th>
    <th>Audio</th>
    <th>Upload Audio</th>
    <?php 
    if($this->Session->read('Auth.User.type')=='admin')
    {
      ?>
    <th>Actions</th>
    <?php 
    }
    ?>
</tr>
</thead>
<tbody>
<?php if(empty($alphabets)) 
{  
?>
<tr>
<td colspan=4><h4>No Alphabets are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($alphabets as $alphabet)
  {
?>
  <tr>
    <td><?php echo $alphabet['Alphabet']['id']; ?></td>
    <td><?php echo $alphabet['Alphabet']['alphabet']; ?></td>
    <td>
      <?php 
        echo $this->Form->create('Alphabet',array('name'=>'Alphabet','type'=>'file')); 
        echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:-10px;height:inherit;','required'=>true,'id'=>$alphabet['Alphabet']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$alphabet['Alphabet']['id'].')'));
        ?>
        <div class="col-md-6" style="width:100%;margin-top:-20px;">
          <br><span id="lblError<?php echo $alphabet['Alphabet']['id']; ?>" style="color: red;"></span>
        </div>
        <?php
        echo $this->Form->input('hidden_id',array('type'=>'hidden','value'=>$alphabet['Alphabet']['id']));
        echo $this->Form->input('hidden_language',array('type'=>'hidden','value'=>$alphabet['Alphabet']['language_id']));
        echo $this->Form->input('hidden_alphabet',array('type'=>'hidden','value'=>$alphabet['Alphabet']['alphabet']));
      ?>
    </td>
    <td>
      <?php 
      if(file_exists(WWW_ROOT.'uploads/Alphabet_Audio/'.$language_name.'/'.$alphabet['Alphabet']['audio_clip']))
      {
          echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
          $rand_no=rand(1111,9999);
          $myAudioFile = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Alphabet_Audio/'.$language_name.'/'.$alphabet['Alphabet']['audio_clip'];
          if($alphabet['Alphabet']['disable']==0)
          {
          ?>
            <audio controls="controls"  id="<?php echo $alphabet['Alphabet']['id']; ?>">  
              <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />  
            </audio> 
          <?php
          }
          if($this->Session->read('Auth.User.type')=='admin' && $alphabet['Alphabet']['disable']==1)
          {
            ?>
            <audio controls="controls"  id="<?php echo $alphabet['Alphabet']['id']; ?>">  
              <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />  
            </audio> 
            <?php
          }
      }
      else
      {
        echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      ?>
    </td>

    <td class="actions">  
      <?php  echo $this->Html->link('Edit',array('action'=>'edit_alphabet',$id,$alphabet['Alphabet']['id'],$language_name)); ?>
      <?php echo $this->Html->link('Delete',array('action'=>'delete_alphabet',$id,$alphabet['Alphabet']['id'],$language_name), array('confirm' => __('Are you sure you want to delete this alphabet?'))); 
      ?>
      <br><br>

      <?php
      if(file_exists(WWW_ROOT.'uploads/Alphabet_Audio/'.$language_name.'/'.$alphabet['Alphabet']['audio_clip']))
      {
        if($alphabet['Alphabet']['disable']==1)
        {
          echo $this->Html->link('Enable Audio',array('action'=>'disable_alphabet',$id,$alphabet['Alphabet']['id'],$language_name,0));   
        } 
        else
        {
          echo $this->Html->link('Disable Audio',array('action'=>'disable_alphabet',$id,$alphabet['Alphabet']['id'],$language_name,1));   
        }
      }
      ?>
    </td>

  </tr>
<?php
  echo $this->Form->end();
  }
}
?>


  </tbody>
  <tr>
  <td class="actions" colspan=2>
    <?php echo $this->Html->link(__('Add more'), array('action' => 'add_alphabet',$id, $language_name), array( 'class' => 'btn btn-primary')); ?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </td>
</tr>
  </table>
  

</div>
</div>
</div>
</section><!-- /.content -->

<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}

$(document).ready(function () {
$.fn.dataTableExt.sErrMode = "console";
     $('#searchable').DataTable( {
         "dom": '<lf<t>ip>'
    } );
});
</script>

