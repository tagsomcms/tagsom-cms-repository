<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Objects List'); ?></h1>
  <?php if($this->Session->read('Auth.User.type')=='admin') {  ?>
  <div class="col-sm-1">
      <?php
       if(isset($select))
        {
          $select=$select;
        }
        else
        {
        $select=1;
        $selected_language='English';
        }
        echo $this->Form->create('ObjectForm',array('name'=>'ObjectForm')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'ObjectForm.submit();'));
        echo $this->Form->end();
      ?>
	</div> 

  <?php } ?>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Maths', array('controller' => 'modes', 'action' => 'manage_math',$mode_id,$story_id,$selected_language)); ?>
    </li>
    <li class="active">Objects List</li>
  </ol>
</section>



<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
<thead>
<tr>
    <th>Id</th>
    <th>Image</th>
    <th>Object</th>
    <th>Audio</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>
<?php 
foreach($objects as $object)
{
  $random_no=rand(11111,55555);
?>
  <tr>
    <td style="text-align:center;"><?php echo $object['MathObject']['id']; ?></td>
    <td style="text-align:center;"><img class="thumb" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Objects/". $object['MathObject']['image'].'?'.$random_no; ?>" style="width:60px;height:50px;"></td>
    <td style="text-align:center;"><?php echo $object['MathObject']['title']; ?></td>
    <td>
    	<?php
    	 $rand_no=rand(1111,9999);
		  $myAudioFile = 'http://52.51.216.184'.$this->base.'/app/webroot/uploads/Maths_Images/Objects_Audio/'.$selected_language.'/'.$object['MathObject']['audio_clip'];
		  if(file_exists(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/'.$selected_language.'/'.$object['MathObject']['audio_clip']))
		  {
	  	?>

 		<audio controls="controls" id="<?php echo $object['MathObject']['id']; ?>">  
		   <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
		</audio> 
		<?php } ?>
		<?php 
    	 echo $this->Form->create('Object_Audio',array('name'=>'Object_Audio','type'=>'file')); 
    	 $object_name='object_audio'.$object['MathObject']['id'];
        echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:10px;height:inherit;','required'=>true,'id'=>$object['MathObject']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$object['MathObject']['id'].')'));
         echo $this->Form->input('hidden_audio',array('type'=>'hidden','value'=>$object['MathObject']['audio_clip']));
        ?>

        <div class="col-md-6" style="width:100%;margin-top:-20px;">
          <br><span id="lblError<?php echo $object['MathObject']['id']; ?>" style="color: red;"></span>
        </div>
		
    </td>
    <td class="actions">
    	<?php 
    	if(file_exists(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/'.$selected_language.'/'.$object['MathObject']['audio_clip']))
		  	{
    			echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
    		}
    		else
    		{
    			echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
    		}
    	?>
      <?php
      if($this->Session->read('Auth.User.type')=='admin')
      {
      echo $this->Html->link("Edit", array('action'=> 'edit_object',$object['MathObject']['id'],$mode_id,$story_id), array('class' => 'btn btn-primary','style'=>'margin-top: 10px;'));
    	echo $this->Html->link("Delete", array('action'=> 'delete_object',$object['MathObject']['id'],$mode_id,$story_id), array( 'confirm' => __('Are you sure you want to delete this object?'),'class' => 'btn btn-primary','style'=>'margin-top: 10px;')); 
      }
      ?>
    </td>
  </tr>
<?php 
echo $this->Form->end();
}
?>
  </tbody>
  </table>

  
  <div class="row">
    <?php if($this->Session->read('Auth.User.type')=='admin') { ?>
  	<div class="col-xs-2">
  		<?php echo $this->Html->link("Add Object", array('action'=> 'add_object',$story_id,$mode_id), array( 'class' => 'btn btn-primary','style'=>'margin-left: 50px;margin-top: 10px;')); ?>
  	</div>
    <?php } ?>
      <div class="col-xs-2" style="clear:none !important;margin-bottom:20px;">
        <?php echo $this->Html->link("Back", array('action'=> 'manage_math',$mode_id,$story_id,$selected_language), array( 'class' => 'btn btn-primary','style'=>'margin-left: 50px;margin-top: 10px;')); ?>
      </div>
       <div class="col-xs-2" style="clear:none !important;margin-bottom:20px;">
         <?php echo $this->Html->link('Resize Objects', array('controller' => 'modes', 'action' => 'resize_objects',$mode_id,$story_id),array('class'=>'btn btn-primary','style'=>'margin-top: 10px;')); ?>
       </div>
      </div>
  </div>
</div>
</div>
</div>
</section><!-- /.content -->


<style>
form div {
padding:0 !important;
}
</style>


<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}

$(document).ready(function () {
$.fn.dataTableExt.sErrMode = "console";
     $('#searchable').DataTable( {
         "dom": '<lf<t>ip>'
    } );
});
</script>





