<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit String'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li class="active">Edit string</li>
  </ol>
</section>

<?php
$options=array('Select From Existing Title');
foreach($settings as $setting)
{
  array_push($options,$setting['GeneralSettings']['title']);
}

?>

<!-- Main content -->
<section class="content">
<div class="modes row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Settings',array('type'=>'file')); ?>
	<div class="box-body">

  <div class="row">
    <div class="col-xs-5" style="clear:none !important;">
      <?php echo $this->Form->input('title',array('placeholder'=>'New Title')); ?>
    </div>
    <div class="col-xs-1" style="clear:none !important;margin-top:15px;"><h2 style="color:black;">OR</h2></div>
    <div class="col-xs-5" style="clear:none !important;margin-left:10px;">
      <?php echo $this->Form->input('title_list',array('type'=>'select','options'=>$options,'id'=>'select1','value'=>$selected_title)); ?>
    </div>
    <div class="col-xs-6" id="c1">
       <?php echo $this->Form->input('string',array('required'=>true,'type'=>'textarea','value'=>$find_string['GeneralSettings']['string'])); ?>
       <?php echo $this->Form->input('hidden_string',array('type'=>'hidden','value'=>$find_string['GeneralSettings']['string'])); ?>
       <?php echo $this->Form->input('hidden_title',array('type'=>'hidden','value'=>$find_string['GeneralSettings']['title'])); ?>
    </div>
  </div>
     
    

    <div class="row">
      <div class="col-xs-2" style="clear:none !important;">
        <?php echo $this->Form->submit(__('Edit String'),array('class'=>'btn btn-primary')); ?>
      </div>
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;">
        <?php echo $this->Html->link("Back", array('action'=> 'manage_setting',$id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      </div>
      </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function(){
document.getElementById("select1").options[0].disabled = true;
});
</script>

<style>
#c1 label:after {
  color: #e32;
  content: '*';
  display:inline;
}
</style>