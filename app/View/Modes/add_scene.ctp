<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('Add Scene'); ?></h1>
  <ol class="breadcrumb">
      <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Add Scenes</li>
  </ol>
</section>
<!-- End Breadcrumb -->

<section class="content">
  <div class="stories row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <?php echo $this->Form->create('Images',array('type' => 'file')); ?>
        <div id="dvPreview"></div>
        <div class="box-body" style="height:200px;">
          <div class="row">
            <div class="col-md-4" style="clear:none !important;">
              <?php echo $this->Form->input('image.',array('type'=>'file','id'=>'file-7','label'=>'Select Scene','required'=>true,'onchange'=>'return ValidateImageExtension(this,"file-7")','multiple'=>true)); ?>
              <div class="col-md-7" style="width:100%;margin-top:-20px;">
                  <br><span id="lblErrorfile-7" style="color: red;"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1" style="clear:none !important;margin-top:-20px;">
              <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
            </div>
            <div class="col-xs-4" style="clear:none !important;margin-left:10px;">
              <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_math',$id,$story_id,'English'), array('class'=>'btn btn-primary')); ?>
            </div>
          </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <br><br>
      </div>
    </div>
  </div>
</section>
<!-- /Main content -->

</head>

