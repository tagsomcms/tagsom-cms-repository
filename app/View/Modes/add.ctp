<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Mode'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="modes row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Mode'); ?>
	<div class="box-body">
	<?php
		echo $this->Form->input('mode_name',array('required'=>true,'class'=>'c1'));
	?>
    <div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

  </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->

<style>
.c1 label:after {
  color: #e32;
  content: '*';
  display:inline;
}
</style>