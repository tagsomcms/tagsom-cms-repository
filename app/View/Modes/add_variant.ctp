<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Variant'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Scene', array('controller' => 'modes', 'action' => 'manage_scene',$math_id,$story_id)); ?>
    </li>
    <li class="active">Add Variant</li>
  </ol>
</section>





<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
<thead>
<tr>
    <th>Object</th>
    <th>Title</th>
    <th>Image</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($objects)) 
{  
?>
<tr>
<td colspan=4><h4>No Objects are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($objects as $object)
  {
?>
  <tr>
    <td><img class="thumb" id="obj_img" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Objects/". $object['MathObject']['image']; ?>" style="width:60px;height:50px;"></td>
    <td><?php echo $object['MathObject']['title']; ?></td>
    <td>
      <?php 
        echo $this->Form->create('Variants',array('type'=>'file')); 
        echo $this->Form->input('image.',array('label'=>false,'type'=>'file','multiple','id'=>'object_image','onchange'=>'return ValidateImageExtension(this,"object_image")','required'=>true)); 
        echo $this->Form->input('h2',array('type'=>'hidden','value'=>$object['MathObject']['id']));
        ?>

        <div class="col-md-6" style="width:100%;margin-top:-20px;">
                <br><span id="lblErrorobject_image" style="color: red;"></span>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php 
            foreach($variants as $variant)
            {
              if($variant['Variant']['object_id']==$object['MathObject']['id'])
              {
              ?>
              <img class="thumb"  src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Variants/". $variant['Variant']['image']; ?>" style="width:40px;height:30px;">
              <?php
              }
            }
            ?>
          </div>
        </div>
        
    </td>
     <td class="actions">  
      <?php echo $this->Form->submit(__('Add Variant'),array('class'=>'btn btn-primary')); ?>
    </td>
    
  </tr>
<?php
  echo $this->Form->end();
  }
}
?>
<tr>

</tr>

  </tbody>
  </table>

 <p>
  <?php
  echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
  ));
  ?>  </p>
  <div class="paging">
  <?php
    echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
  ?>
  </div>
  
  <div class="row">
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-bottom:20px;margin-left:10px;">
        <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_scene',$mode_id,$math_id,$story_id), array( 'class' => 'btn btn-primary')); ?>
      </div>
      </div>
  </div>
</div>
</div>
</div>
</section><!-- /.content -->
