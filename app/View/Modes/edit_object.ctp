<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Object'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Scene', array('controller' => 'modes', 'action' => 'manage_scene',$scene_id,$story_id)); ?>
    </li>
    <li class="active">Edit Object</li>
  </ol>
</section>
<?php $rand_no=rand(1111,9999); ?>
<!-- Main content -->
<section class="content">
<div class="modes row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Objects',array('type'=>'file')); ?>
	<div class="box-body">
    <div class="row">
      <div class="col-md-6">
	     <?php echo $this->Form->input('title',array('required'=>true,'value'=>$object['MathObject']['title'])); ?>
      </div>

        <div class="col-md-6">
          <?php  echo $this->Form->input('image',array('label'=>'Select Object','type'=>'file','id'=>'object_image','onchange'=>'return ValidateImageExtension(this,"object_image")')); ?>
        </div>
        <div class="col-md-6" style="width:100%;margin-top:-40px;">
          <br><span id="lblErrorobject_image" style="color: red;"></span>
          <br>
          <img style="width: 130px;height: inherit;margin-left: 15px;" src="<?php echo $this->base .'/app/webroot/uploads/Maths_Images/Objects/'.$object['MathObject']['image'].'?'.$rand_no; ?>"  id="img2">
        </div>

    </div>

    <div class="row">
      <div class="col-xs-2" style="clear:none !important;">
        <?php echo $this->Form->submit(__('Change Object'),array('class'=>'btn btn-primary')); ?>
      </div>
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
        <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_objects',$mode_id,$scene_id,$story_id,English), array( 'class' => 'btn btn-primary')); ?>
      </div>
      </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->