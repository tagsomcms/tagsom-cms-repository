<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="row">
		<div class="col-md-2">  <h1><?php echo __('Modes'); ?></h1></div>
		<div class="col-sm-3">
			<?php
				if(isset($selected_language))
				{
					$select=$selected_language;
				}
				else
				{
				$select=1;
				}
				echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
				echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
			?>
		</div>
		<div class="col-sm-1">
			<?php
				echo $this->Form->input('story',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$stories_array,'style'=>'height: 33px;margin-top: 25px;width: 200px;margin-left: 0px;','onchange'=>'StoryLanguage.submit();'));
			?>
		</div>
	</div>

 
<ol class="breadcrumb">
        <li>
        	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
        	</li>
        <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
        </li>
        <li class="active">Index</li>
</ol>
</section>

<?php if(empty($story_id))
{
	$story_id=0;
}
?>
<!-- Main content -->
<section class="content">
 <div class="modes row">
<div class="col-xs-12">
		<div class="box"> 	
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
<thead>
<tr>
	<th><?php echo $this->Paginator->sort('id'); ?></th>
	<th><?php echo $this->Paginator->sort('mode_name'); ?></th>
	<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
	<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
</tr>
</thead>
<tbody>
<?php

if($this->Session->read('Auth.User.type')=='admin')
{
	foreach ($modes as $mode): 
	?>
	<tr>
	<td><?php echo h($mode['Mode']['id']); ?>&nbsp;</td>
	<td><?php echo h($mode['Mode']['mode_name']); ?>&nbsp;</td>
	<td><?php echo h($mode['Mode']['modified']); ?>&nbsp;</td>
	<td class="actions" >
		<?php if(empty($english_story_id)) { $english_story_id=$story_id;} ?>
		<?php if($mode['Mode']['mode_name']=='Spelling') { echo $this->Html->link(__('Manage Words'), array('action' => 'manage_word', $mode['Mode']['id'],$story_id,$language_name)); } ?>
		<?php if($mode['Mode']['mode_name']=='Maths') { echo $this->Html->link(__('Manage Maths'), array('action' => 'manage_math', $mode['Mode']['id'],$english_story_id,$language_name)); } ?>
		<?php if($mode['Mode']['mode_name']=='Questions') { echo $this->Html->link(__('Manage Questions'), array('action' => 'manage_question', $mode['Mode']['id'],$story_id,$language_name)); } ?>
		<?php if($mode['Mode']['mode_name']=='General Settings') { echo $this->Html->link(__('Manage Settings'), array('action' => 'manage_setting', $mode['Mode']['id'],$language_name)); } ?>
		<?php if($mode['Mode']['mode_name']=='Alphabet') { echo $this->Html->link(__('Manage Alphabet'), array('action' => 'manage_alphabet', $mode['Mode']['id'],$language_name)); } ?>
		<?php if($mode['Mode']['mode_name']=='Numbers') { echo $this->Html->link(__('Manage Numbers'), array('action' => 'manage_numbers', $mode['Mode']['id'],$language_name)); } ?>

		<?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin')
			 { ?>
		<?php //echo $this->Html->link(__('View'), array('action' => 'view', $mode['Mode']['id'])); ?>
		<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $mode['Mode']['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mode['Mode']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $mode['Mode']['id']))); ?>
		<?php } ?>
	</td>
	</tr>
	<?php
	endforeach; 
}
else
{
	foreach ($modes as $mode): 

?>
<tr>
<td><?php echo h($mode['Mode']['id']); ?>&nbsp;</td>
<td><?php echo h($mode['Mode']['mode_name']); ?>&nbsp;</td>
<td><?php echo h($mode['Mode']['modified']); ?>&nbsp;</td>
<td class="actions">
	<?php if($mode['Mode']['mode_name']=='Spelling') { echo $this->Html->link(__('Manage Words'), array('action' => 'manage_word', $mode['Mode']['id'],$story_id,$language_name)); } ?>
	<?php if($mode['Mode']['mode_name']=='Maths') { echo $this->Html->link(__('Manage Maths'), array('action' => 'manage_math', $mode['Mode']['id'],$story_id,$language_name)); } ?>
	<?php if($mode['Mode']['mode_name']=='Questions') { echo $this->Html->link(__('Manage Questions'), array('action' => 'manage_question', $mode['Mode']['id'],$story_id,$language_name)); } ?>
	<?php if($mode['Mode']['mode_name']=='General Settings') { echo $this->Html->link(__('Manage Settings'), array('action' => 'manage_setting', $mode['Mode']['id'],$language_name)); } ?>
	<?php if($mode['Mode']['mode_name']=='Alphabet') { echo $this->Html->link(__('Manage Alphabet'), array('action' => 'manage_alphabet', $mode['Mode']['id'],$language_name)); } ?>
	<?php if($mode['Mode']['mode_name']=='Numbers') { echo $this->Html->link(__('Manage Numbers'), array('action' => 'manage_numbers', $mode['Mode']['id'],$language_name)); } ?>

	<?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin')
		 { ?>
	<?php //echo $this->Html->link(__('View'), array('action' => 'view', $mode['Mode']['id'])); ?>
	<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $mode['Mode']['id'])); ?>
	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mode['Mode']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $mode['Mode']['id']))); ?>
</td>
</tr>
<?php
} endforeach; 
}
?>
</tbody>
</table>

</div>
</div>
</div>
</section><!-- /.content -->

<script>
$(document).ready(function(){
document.getElementById("select1").options[0].disabled = true;
document.getElementById("select2").options[0].disabled = true;
});
</script>


<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>