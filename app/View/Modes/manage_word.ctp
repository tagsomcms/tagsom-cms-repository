<?php
header ('Content-Type: text/html; charset=UTF-8'); 
echo '<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />'; 
?>


<!-- Start Breadcrumb -->
<section class="content-header">
  <div class="row">
    <div class="col-md-3">  <h1><?php echo __('Manage Word'); ?></h1></div>
    <div class="col-sm-3">
      <?php
        if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('StoryLanguage',array('name'=>'StoryLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'StoryLanguage.submit();'));
      ?>
    </div>
    <div class="col-sm-1">
      <?php
         if(isset($selected_story))
        {
          $select1=$selected_story;
        }
        else
        {
        $select1=1;
        }
        echo $this->Form->input('story',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$stories_array,'value'=>$select1,'style'=>'height: 33px;margin-top: 25px;width: 200px;margin-left:0px;','onchange'=>'StoryLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div>
  

  </div>
  <ol class="breadcrumb" style="margin-top:25px;">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Words</li>
  </ol>
</section>
<!-- End Breadcrumb -->



 <!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
<thead>
<tr>
    <th>Id</th>
    <th>Word</th>
    <th>Audio</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($words)) 
{  
?>
<tr>
<td colspan=4><h4>No Words Are There For This Language Or Story</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($words as $word)
  {
?>
  <tr>
    <td><?php echo $word['Word']['id']; ?></td>
    <td style="text-align:left;" <?php if($language_name=='Arabic') { ?> dir="rtl" <?php } ?>><?php echo $word['Word']['word']; ?>
      <?php 
        if($word['Word']['word']=='No Translation') { 
          foreach($all_words as $all_word)
          { 
            if($word['Word']['story']==$all_word['Word']['id'])
            {
              echo '<br>('.$all_word['Word']['word'].')';
            }
          }
        } 
       ?>
    </td>
    <td>
      <?php 
        echo $this->Form->create('Words',array('name'=>'Words','type'=>'file')); 
        if($word['Word']['word']=='No Translation')
        {
          echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-top:-10px;margin-left:10px;','required'=>true,'id'=>$word['Word']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$word['Word']['id'].')','disabled'=>true));
        }
        else
        {
          echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;height:inherit;margin-top:-10px;margin-left:10px;','required'=>true,'id'=>$word['Word']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$word['Word']['id'].')'));
        }

        ?>

        <?php
        if(empty($story_path2))
        {
          $story_path=$story_path1;
        }
        else
        {
          $story_path=$story_path2;
        }

        //add audio option
        if(file_exists(WWW_ROOT.'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name.'/'. $word['Word']['audio_clip']))
        {
            $rand_no=rand(1111,9999);
           $myAudioFile = 'http://'.$_SERVER['SERVER_NAME'].$this->base.'/app/webroot/uploads/Words/Words_Audio/'.$story_path.'/'.$language_name.'/'.$word['Word']['audio_clip'];
           ?>
            <?php  if($word['Word']['disable']!=1) { ?>
            <audio controls="controls" id="<?php echo $word['Word']['id']; ?>">  
                <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />  
            </audio> 
            <?php } 
            if($this->Session->read('Auth.User.type')=='admin' && $word['Word']['disable']==1)
            {
              ?>
              <audio controls="controls" id="<?php echo $word['Word']['id']; ?>">  
                <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" />  
              </audio> 
              <?php
            }
            ?>
           <?php
        }
        ?>
        <div class="col-md-6" style="width:100%;">
          <br><span id="lblError<?php echo $word['Word']['id']; ?>" style="color: red;"></span>
        </div>

        <?php
        echo $this->Form->input('hidden_language',array('type'=>'hidden','value'=>$word['Word']['language_id']));
        echo $this->Form->input('hidden_story',array('type'=>'hidden','value'=>$word['Word']['story_id']));
        echo $this->Form->input('hidden_id',array('type'=>'hidden','value'=>$word['Word']['id']));
        echo $this->Form->input('hidden_word',array('type'=>'hidden','value'=>$word['Word']['word']));
        echo $this->Form->input('hidden_audio',array('type'=>'hidden','value'=>$word['Word']['audio_clip']));
      ?>

    </td>
    <td class="actions">
       <?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin') { echo $this->Html->link('Edit',array('action'=>'edit_word/',$word['Word']['id'],$id,$story_id,$language_name)); ?>
      <?php 
          echo $this->Html->link(__('Delete'), array('action' => 'delete_word/', $word['Word']['id'],$id,$story_id,$language_name), array('confirm' => __('Are you sure you want to delete this word?')));

          if($word['Word']['disable']==1)
          {
            echo "<br><br>".$this->Html->link('Enable Audio ',array('action'=>'disable_word/',$word['Word']['id'],$id,$story_id,$language_name,0));  
          }
          else
          {
            echo "<br><br>".$this->Html->link('Disable Audio ',array('action'=>'disable_word/',$word['Word']['id'],$id,$story_id,$language_name,1));  
          }
      }?>
      <?php 
        
          if($word['Word']['word']=='No Translation')
          {
            echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-left:-7px;','disabled'=>true)); 
          }
          else
          {
            if(file_exists(WWW_ROOT.'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name.'/'. $word['Word']['audio_clip']))
            {
              echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-left:-7px;')); 
            }
            else
            {
              echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-left:-7px;')); 
            }
             
          }
       
      ?>
    </td>
    
      <?php echo $this->Form->end(); ?>
  </tr>
<?php
  }
}
?>
</tbody>
<tr>
  <td class="actions" colspan=2>
    <?php if($language_name=='English' && $this->Session->read('Auth.User.type')=='admin') { echo $this->Html->link(__('Add more'), array('action' => 'add_word',$id,$story_id, $language_name), array( 'class' => 'btn btn-primary')); } ?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>

  </td>
</tr>

</table>
  

</div>
</div>
</div>
</section><!-- /.content -->

<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}

$(document).ready(function(){
document.getElementById("select1").options[0].disabled = true;
document.getElementById("select2").options[0].disabled = true;
});
</script>

<script>
$(document).ready(function () {
$.fn.dataTableExt.sErrMode = "console";
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>







