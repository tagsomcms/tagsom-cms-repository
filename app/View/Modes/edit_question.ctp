<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Question'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Question', array('controller' => 'modes', 'action' => 'manage_question')); ?>
    </li>
    <li class="active">Edit Question</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="modeQuestionAnswers row">
 	<div class="col-xs-12">
	<div class="box box-primary">
	<?php echo $this->Form->create('Question',array('type' => 'file')); ?>
	<fieldset>
	<div class="row q1" style="margin-left:-7px;">
		<div class="col-xs-6">
			<?php  echo $this->Form->input('question',array('required'=>true,'value'=>$question_array['Question']['question'])); ?>
		</div>
	</div>
	<div class="row a1" style="margin-left:-7px;">
		<div class="col-xs-6">
			<?php echo $this->Form->input('answer',array('required'=>true,'value'=>$question_array['Question']['answer'])); ?>
		</div>
	</div>
	
	<!-- mode question 1 -->
	<div class="row"  id="options">
	<div class="col-xs-3">
		<?php echo $this->Form->input('option1',array('label'=>'Option1','required'=>true,'value'=>$question_array['Question']['option1'])); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;">
		<?php echo $this->Form->input('option2',array('label'=>'Option2','required'=>true,'value'=>$question_array['Question']['option2'])); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;">
		<?php echo $this->Form->input('option3',array('label'=>'Option3','required'=>true,'value'=>$question_array['Question']['option3'])); ?>
	</div>
	</div>
	<?php $rand_no=rand(1111,5555); ?>
	<div class="row">
      <div class="col-md-6">
       <?php echo $this->Form->input('image1',array('label'=>'Select Image','type'=>'file','required'=>false,'id'=>'question_image','onchange'=>'return ValidateImageExtension(this,"question_image")'));?>
      </div>
       <div class="col-md-6" style="width:100%;margin-top:-20px;">
                <br><span id="lblErrorquestion_image" style="color: red;"></span>
        </div>
        <img style="width: 200px;height: inherit;margin-left: 15px;" src="<?php echo $this->base .'/app/webroot/uploads/Questions/'.$story_path.'/Questions_Image/'.$image_name.'?'.$rand_no; ?>"  id="img2">
    </div>

	<div class="row">
	<div class="col-xs-1" style="clear:none !important;">
		<?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
	</div>
	<div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
		<?php echo $this->Html->link("Back", array('action'=> 'manage_question',$id,$story_id,$language_name), array( 'class' => 'btn btn-primary')); ?>
	</div>
	</div>

	</fieldset>
</div>
</div>
</div>
</section>
<!-- /Main content -->

<style>
form div {
    clear: both;
     margin-bottom: 0em !important; 
    padding: .5em;
    vertical-align: text-top;
}
.col-xs-6 label:after {
	color: #e32;
	content: '*';
	display:inline;
}
.col-xs-3 label:after {
	color: #e32;
	content: '*';
	display:inline;
}
</style>


