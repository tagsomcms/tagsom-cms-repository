<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="row">
    <div class="col-md-5">  <h1><?php echo __('Add Object To Scene'); ?></h1></div>
  </div>
  <div class="row">
    <div class="col-sm-1" style="width:160px;">
      <?php
        if(isset($selected_story))
        {
          $select=$selected_story;
        }
        else
        {
        $select=0;
        }
        if(isset($row)) 
        { 
          $width=700/$column;
          $height=525/$row;
        }
        else
        {
          $width=700/$find_grid['Grids']['col'];
          $height=525/$find_grid['Grids']['row'];
        }

        echo $this->Form->create('Story',array('name'=>'Story','id'=>'Story')); 
        echo $this->Form->input('base_url',array('type'=>'hidden','value'=>$base,'id'=>'base_url'));
        echo $this->Form->input('hidden_row',array('type'=>'hidden','value'=>$row,'id'=>'hidden_row'));
        echo $this->Form->input('hidden_column',array('type'=>'hidden','value'=>$column,'id'=>'hidden_column'));
        echo $this->Form->input('scene_hidden',array('type'=>'hidden','value'=>$find_scene['Scene']['id'],'id'=>'scene_hidden'));
        echo $this->Form->input('width',array('type'=>'hidden','value'=>$width,'id'=>'width'));
        echo $this->Form->input('height',array('type'=>'hidden','value'=>$height,'id'=>'height'));

        echo $this->Form->input('story',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$story_array,'value'=>$select,'style'=>'width: 130px;height:33px;margin-top: 18px;','onchange'=>'Story.submit();'));
      ?>
    </div>
    <div class="col-sm-1" style="width:210px;margin-top:7px;">
      <?php
        if(isset($selected_scene))
        {
          $select1=$selected_scene;
        }
        else
        {
        $select1=0;
        }
        echo $this->Form->input('scene',array('id'=>'select2','label'=>false,'type'=>'select','options'=>$scene_array,'value'=>$select1,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'Story.submit();'));
      ?>
    </div>
    <div class="col-sm-1" style="width:210px;margin-top:7px;">
      <?php
        if(isset($selected_object))
        {
          $select2=$selected_object;
        }
        else
        {
        $select2=0;
        }
        echo $this->Form->input('object',array('id'=>'select3','label'=>false,'type'=>'select','options'=>$object_array,'value'=>$select2,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'get_object(this);'));

      ?>
    </div>
  </div> 
    
  <ol class="breadcrumb">
        <li>
          <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
          </li>
        <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
        </li>
        <li class="active">Manage Scene</li>
  </ol>
</section>

<?php if(!empty($find_scene))
{?>
<div class="row" style="margin-left:15px;">

  <div class="col-xs-3" style="width:21.5%;">
    <?php echo $this->Form->input('row',array('label'=>false,'type'=>'text','placeholder'=>'Enter Row','id'=>'row1')); ?>
  </div>
  <div class="col-xs-3" >
    <?php echo $this->Form->input('column',array('label'=>false,'type'=>'text','placeholder'=>'Enter Column','id'=>'column1')); ?>
  </div>
  <div class="col-xs-3" >
    <?php echo $this->Form->submit(__('Create Grid'),array('class'=>'btn btn-primary','style'=>'margin-top:-10px;','id'=>'grid_create'));
    echo $this->Form->end(); ?>
  </div>
</div>

<div class="row" style="margin-left:10px;">
  <div class="col-md-2">
    <?php if(!empty($find_object['MathObject']['image'])) {?>
    <img class="thumb" id="obj_img" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Objects/". $find_object['MathObject']['image']; ?>" style="width:130px;height:110px;">
    <?php } ?>
  </div>

  <div class="col-md-5" style="width:37%;">
     <!-- <div id="content" style="width:380px;top:0px;left:0px;height:115px;">
    </div> -->

    <div class="row" style="margin-left:15px;">
      <div class="col-md-12" id="content" style="width:380px;top:0px;left:0px;height:115px;">
       
      </div>
     
   
      <div class="col-md-6"><?php echo $this->Html->link("Variants", array('action'=> 'variants',$find_scene['Scene']['id'],$find_scene['Scene']['story_id'],$mode_id), array( 'class' => 'btn btn-primary','style'=>'margin-top: 10px;')); ?></div>
      <div class="col-md-6"><?php echo $this->Html->link("Add Variant", array('action'=> 'add_variant',$find_scene['Scene']['id'],$find_scene['Scene']['story_id'],$mode_id), array( 'class' => 'btn btn-primary','style'=>'margin-top: 10px;')); ?>
      </div>
    </div>

  </div>

  <div class="col-md-2" id="trash" >
    <img  src="<?php echo $this->base . "/app/webroot/img/trash1.png"; ?>" style="margin-left:15px;width:120px;height:120px;">

  </div>

  

  <div class="col-md-9" style="margin-top:20px;">

<?php if((isset($row)) || (!empty($find_grid))) { 
      if(empty($row))
      {
        $row=$find_grid['Grids']['row'];
        $column=$find_grid['Grids']['col'];
      }
      $width=700/$column;
      $height=525/$row;


  ?>

   
    <div id="container"style="background-image: url('/tagsomdev/app/webroot/uploads/Maths_Images/<?php echo $story_path; ?>/Scenes/<?php echo $find_scene['Scene']['image'];?>');background-size: cover;
    background-repeat: no-repeat;width:700px;height:525px;
    ">

    <?php
    for($i=1;$i<=$row;$i++)
    {
      ?>
      <div class="row" style="margin-left:0px;">
        <?php
        for($j=1;$j<=$column;$j++)
        {
          ?>
            <?php
            if(empty($grids))
            {
            ?>

            <?php if(($i==1 && $j==1) || ($i==1 && $j==2)) {?>
            <div class="droppable col-md-2 thumb" id="undrop" >
            </div>
            <?php } else { ?>

            <div class="droppable col-md-2 thumb" id="s<?php echo $i.$j; ?>" >
            </div>
            <?php } ?>

            <script>

            $("#content .item").draggable({
              connectToSortable: ".fs:not(.full)",
              revert: "invalid",
              helper: "clone"
            });
            </script>
            <?php
            }
            else
            {
            ?>
            
              <?php
                foreach($grids as $grid)
                {
                  if($grid['Grids']['x']==$i && $grid['Grids']['y']==$j)
                  {
                    ?>
                  <div class="droppable col-md-2" id="s<?php echo $i.$j; ?>" >
                    <?php echo $grid['Grids']['image']; ?>
                  
                  </div>

                    <?php
                  }
                }
                ?>
          
            <?php
            }
            ?>
            
          <?php
        }
        ?>
      </div>
      <?php
    }
    ?>

</div>
<?php } else{?>
      <img class="img-responsive" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/".$story_path."/Scenes/". $find_scene['Scene']['image']; ?>" alt="<?php echo $find_scene['Scene']['image']; ?>" width="700px" height="550px"><?php } ?>
      <br>
      

    <div class="row" >
      <div class="col-md-1">
        <?php echo $this->Html->link("Back", array('action'=> 'manage_math/'.$mode_id.'/'.$story_id.'/English'), array( 'class' => 'btn btn-primary','style'=>'margin-bottom: 10px;')); ?>
      </div>
      <div class="col-md-2"><button id="save" class="btn btn-primary" style="margin-bottom:10px;">Save Grid</button></div>
      <div class="col-md-2"><?php echo $this->Html->link("Delete Grid", array('action'=> 'delete_grid',$find_scene['Scene']['id'],$id,$story_id,$mode_id), array( 'class' => 'btn btn-primary','style'=>'margin-bottom:10px;margin-left:-35px;')); ?>
      </div>
    </div>


  </div>

  <div class="col-md-3">
    <div class="row well" style="margin-right:25px;">
      <h4 style="color:black;"><b><center>OBJECTS</center></b></h4>
      <!-- slider -->

    <div style="max-width:900px;margin:0 auto;padding:100px 0;">

        <div style="float:left;">
            <div id="thumbs2" style="margin-top:-100px;height:420px;width:180px;">
                <div class="inner">
                    <ul style="margin-top:-100px;">
                       <?php 
                       $c1=0;
                        foreach($objects as $object)
                        {
                        ?>
                        <li <?php if($c1==0) { ?>style="margin-top: 120px;"<?php } ?>>
                            <div class="containerdiv">
                              <img class="thumb" src="<?php echo $this->base . "/app/webroot/uploads/Maths_Images/Objects/". $object['MathObject']['image']; ?>" style="width: 105px;height:75px;">
                              <?php
                                  $thumb_img=$this->Html->image("/app/webroot/img/close3.png",array('style'=>'width:20px;height:20px;','class'=>'cornerimage'));
                                  echo $this->Html->link( $thumb_img, array('controller'=>'Modes','action'=>'delete_object',$object['MathObject']['id'],$id,$story_id,$mode_id), array('escape'=>false));
                              ?>
                            </div>
                        </li>
                        <?php 
                        $c1++;
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>

        <div style="clear:both;"></div>

    </div>
    <!--End Slider-->

    </div>


  </div>
  <br><br><br>
</div>
<?php
}
else
{
  ?>
  <div style="margin-left:20px;"><h4>No Scenes are there in this story</h4><br></div>
<?php
}
?>

<style>
.containerdiv {
  border: 0;
  float: left;
  position: relative;
} 
.cornerimage {
  border: 0;
  position: absolute;
  top: 0;
  right: -130px;
 } 
 .containerdiv1 {
  border: 0;
  float: left;
  position: relative;
} 
.cornerimage1 {
  border: 0;
  position: absolute;
  top: 0;
  right: -66px;
 } 
</style>
</body>



<script>
$(document).ready(function() {

 $('#trash').droppable({
    over: function(event, ui) {
     
      ui.draggable.remove();
        
    }
});

document.getElementById("grid_create").disabled = true;
 $("#row1").keyup(function(){

      var row_value=document.getElementById('row1').value;
      var col_value=document.getElementById('column1').value;

        if(row_value=='' || col_value=='')
        {
          document.getElementById("grid_create").disabled = true;
        }
        else
        {
          if(isNaN(row_value) || isNaN(col_value) || row_value % 1 !==0 || col_value % 1 !==0)
          {
            document.getElementById("grid_create").disabled = true;
            alert('please enter only integer value.');
           
          }
          else
          {
            document.getElementById("grid_create").disabled = false;
          }
        }
    
    });

  $("#column1").keyup(function(){
      var row_value=document.getElementById('row1').value;
      var col_value=document.getElementById('column1').value;

        if(row_value=='' || col_value=='')
        {
          document.getElementById("grid_create").disabled = true;
        }
        else
        {
          if(isNaN(row_value) || isNaN(col_value) || row_value % 1 !==0 || col_value % 1 !==0)
          {
            document.getElementById("grid_create").disabled = true;
            alert('please enter only integer value.');
           
          }
          else
          {
            document.getElementById("grid_create").disabled = false;
          }
        }
    });


  $('#save').click(function () {
    
      <?php
        for($i=1;$i<=$row;$i++)
        {
          for($j=1;$j<=$column;$j++)
          {
           
          ?>

            var image = $('#s'+<?php echo $i.$j; ?>).html();
            var x='<?php echo $i; ?>';
            var y='<?php echo $j; ?>';
            var row='<?php echo $row; ?>';
            var col='<?php echo $column; ?>';
            var scene_id = document.getElementById('scene_hidden').value;
            var img = $('#s'+<?php echo $i.$j; ?>).children().html();
            var src=$('#s'+<?php echo $i.$j; ?>).find("img").attr('src');

            $.ajax({
            type:"POST",
            data:{scene:scene_id,x:x,y:y,image:image,row:row,col:col,src:src}, 
            success : function() {
              document.getElementById('column1').value='';
              document.getElementById('row1').value='';
            }
            });


          <?php
          }
        }
        ?>
        alert('Grid is being creating so please wait for few seconds.');
  });



    //for draggable items
    var width1='<?php echo $width; ?>';
    var height1='<?php echo $height; ?>';
    if(width1=='')
    {
      width1='35px';
      height1='35px';
    }
    else
    {
      width1=width1+'px';
      height1=height1+'px';
    }
   
    var baseurl = document.getElementById('base_url').value;
    var x = document.getElementById("select3").selectedIndex;
    var y = document.getElementById("select3").options;
    var selectedText=y[x].text;
    var count=1;
    $.ajax({
        type:"POST",
        data:{obj:selectedText}, 
        success : function(response) {
        $("#content .item").val('');
        obj1=$.parseJSON(response);
        $.each( obj1, function( key, value ) {

            var img_path='/tagsomdev/app/webroot/uploads/Maths_Images/Variants/'+value.Variant.image;
            $("#content").append('<div id="'+count+'" class="item ui-draggable ui-draggable-handle" ><img src="'+img_path+'" width="'+width1+'" height="'+height1+'" id="img1" ></div>');

            
            count++;

            $("#content .item").draggable({
              connectToSortable: ".fs:not(.full)",
              revert: "invalid",
              helper: "clone"
            });
        });

        },
        error : function() {
           alert("false");
        }
    });

  //for drag and drop
  $(".fs").fsortable({
    connectWith: ".fs",
    tolerance: "pointer",
    size: 5
  }).disableSelection();

  // $("#content .item").draggable({
  //   connectToSortable: ".fs:not(.full)",
  //   revert: "invalid",
  //   helper: "clone"
  // });
  $( ".droppable" ).droppable({
    drop: function( event, ui ) {
      //Get the position before changing the DOM
      // var p1 = ui.draggable.parent().offset();
      //Move the element to the new parent
      $(this).append(ui.draggable);
      var text = '<div class="item">'+jQuery(ui.draggable).html()+'</div>';
    //  console.log(ui.draggable);
       $("#content").append(text);
        $("#content .item").draggable({
              connectToSortable: ".fs:not(.full)",
              revert: "invalid",
              helper: "clone"
            });



      //Get the postion after changing the DOM
      // var p2 = ui.draggable.parent().offset();
      //Set the position relative to the change
       event.preventDefault();
    }
  });

  $( "#undrop" ).droppable({
  disabled: true
  });

});
</script>


<style>
* {
  margin: 0;
  padding: 0;
}

.demo {
  position: relative;
  width: <?php echo $width."px"; ?>;
  height: <?php echo $height."px"; ?>;
  float: left;
  margin-right: 15px;
}

.bg {
  position: absolute;
  left: 20px;
  top: 20px;
  width: <?php echo $width."px"; ?>;
  height: <?php echo $height."px"; ?>;
  z-index: -1;
}
.bg div {
  float: left;
  width: <?php echo $width."px"; ?>;
  height: <?php echo $height."px"; ?>;
  border: 1px solid #aaa;
  padding: 20px;
  margin: 10px;
  text-align: center;
  opacity: 0.4;
}
.fs {
  position: absolute;
  width: <?php echo $width."px"; ?>;
  height: <?php echo $height."px"; ?>;
  border: 1px solid #aaa;
}
.fsortable-empty {
  float: left;
  height: <?php echo $height."px"; ?>;
  width:  <?php echo $width."px"; ?>;
  padding: 20px;
  margin: 10px;
  border: 1px solid #999;
}

.item {
  height: <?php echo $height."px"; ?>;
  width:  <?php echo $width."px"; ?>;
  border: 1px solid #000;
  text-align: center;
  float: left;
  background-color: #fff;
  margin-left: -17px;
  margin-right:30px;
}


.droppable
{
  height: <?php echo $height."px"; ?>;
  width:  <?php echo $width."px"; ?>;
    border: 1px solid;
/*  margin-bottom: 10px;
  margin-top: 10px;*/
}
</style>

