<!-- Start Breadcrumb -->
<section class="content-header">
 <div class="col-md-4">  <h1><?php echo __('Manage Numbers'); ?></h1></div>
   <div class="col-sm-1">
      <?php
       if(isset($selected_language))
        {
          $select=$selected_language;
        }
        else
        {
        $select=1;
        }
        echo $this->Form->create('AlphabetLanguage',array('name'=>'AlphabetLanguage')); 
        echo $this->Form->input('language',array('id'=>'select1','label'=>false,'type'=>'select','options'=>$language_array,'value'=>$select,'style'=>'width: 200px;height:33px;margin-top: 18px;','onchange'=>'AlphabetLanguage.submit();'));
        echo $this->Form->end();
      ?>
    </div> 
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Manage Numbers</li>
  </ol>
</section>
<!-- End Breadcrumb -->



<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box"> 
 
<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
<thead>
<tr>
    <th>Id</th>
    <th>Number</th>
    <th>Select Audio</th>
    <th>Upload Audio</th>
    <th>Actions</th>
   
</tr>
</thead>
<tbody>
<?php if(empty($numbers)) 
{  
?>
<tr>
<td colspan=4><h4>No Numbers are there</h4></td>
</tr>
<?php
}
else
{
?>
<?php
  foreach($numbers as $number)
  {
?>
  <tr>
    <td><?php echo $number['Number']['id']; ?></td>
    <td><?php echo $number['Number']['number']; ?></td>
    <td>
      <?php 
        echo $this->Form->create('Number',array('name'=>'Number','type'=>'file')); 
        echo $this->Form->input('audio_clip',array('label'=>false,'type'=>'file','style'=>'width:inherit;margin-top:-10px;height:inherit;','required'=>true,'id'=>$number['Number']['id'],'onchange'=>'return ValidateAudioExtension(this,'.$number['Number']['id'].')'));
        echo $this->Form->input('hidden_audio',array('type'=>'hidden','value'=>$number['Number']['audio_clip']));
        ?>
        <div class="col-md-6" style="width:100%;margin-top:-20px;">
          <br><span id="lblError<?php echo $number['Number']['id']; ?>" style="color: red;"></span>
        </div>
        
    </td>
    <td>
      <?php 
      if(file_exists(WWW_ROOT.'uploads/Numbers_Audio/'.$language_name.'/'.$number['Number']['audio_clip']))
      {
         $rand_no=rand(1111,9999);
		  $myAudioFile = 'http://52.51.216.184'.$this->base.'/app/webroot/uploads/Numbers_Audio/'.$language_name.'/'.$number['Number']['audio_clip'];
		
	  ?>

 		<audio controls="controls" id="<?php echo $number['Number']['id']; ?>">  
		   <source src="<?php echo $myAudioFile.'?'.$rand_no; ?>" type="audio/wav"/>  
		</audio> 
      <?php
        echo $this->Form->submit('Replace Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      else
      {
        echo $this->Form->submit('Upload Audio',array('label'=>false,'class'=>'btn btn-primary','style'=>'margin-top:-18px;')); 
      }
      ?>
    </td>
    <td class="actions">  
      <?php echo $this->Html->link('Delete',array('action'=>'delete_number',$id,$number['Number']['id'],$language_name), array('confirm' => __('Are you sure you want to delete this number?'))); ?>
    </td>
    
  </tr>
<?php
  echo $this->Form->end();
  }
}
?>

  </tbody>
  <tr>
  <td class="actions" colspan=2>
    <?php echo $this->Html->link(__('Add more'), array('action' => 'add_numbers',$id,$language_name), array( 'class' => 'btn btn-primary')); ?>
     <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
  </td>
</tr>

  </table>
  

</div>
</div>
</div>
</section><!-- /.content -->


<script>

$("audio").each(function(){
  $(this).bind("play",stopAll);
});

function stopAll(e){
    var currentElementId=$(e.currentTarget).attr("id");
    $("audio").each(function(){
        var $this=$(this);
        var elementId=$this.attr("id");
        if(elementId!=currentElementId){
            $this[0].pause();
        }
    });
}


$(document).ready(function () {
$.fn.dataTableExt.sErrMode = "console";
     $('#searchable').DataTable( {
         "dom": '<lf<t>ip>'
    } );
});
</script>





