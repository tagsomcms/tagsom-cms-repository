<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Word'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit Word</li>
  </ol>
</section>
<?php $rand_no=rand(1111,9999); ?>
<!-- Main content -->
<section class="content">
<div class="modes row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Words',array('type'=>'file')); ?>
	<div class="box-body">
    <div class="row">
      <div class="col-md-6" id="c1">
	     <?php echo $this->Form->input('word',array('required'=>true,'value'=>$word)); ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
       <?php echo $this->Form->input('image',array('label'=>'Word_Image','type'=>'file','required'=>false,'id'=>'word_image','onchange'=>'return ValidateImageExtension(this,"word_image")'));?>
      </div>
       <div class="col-md-6" style="width:100%;margin-top:-20px;">
                <br><span id="lblErrorword_image" style="color: red;"></span>
        </div>
        <br>
        <img  onerror="this.src='<?php echo $this->base .'/app/webroot/uploads/noimage3.png'; ?>'" style="width: 200px;height: inherit;margin-left: 15px;" src="<?php echo $this->base .'/app/webroot/uploads/Words/Words_Image/'.$story_path.'/'.$image_name.'?'.$rand_no; ?>"  id="img2">
    </div>
    <div class="row">
      <div class="col-xs-1" style="clear:none !important;">
        <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary')); ?>
      </div>
      
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
        <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_word',$mode,$story_id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      </div>
      </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->

<style>
#c1 label:after {
  color: #e32;
  content: '*';
  display:inline;
}
</style>