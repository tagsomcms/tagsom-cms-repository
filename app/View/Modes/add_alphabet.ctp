<body onload="f1()">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Alphabet'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    <li><?php echo $this -> Html -> link('Manage Alphabet', array('controller' => 'modes', 'action' => 'manage_alphabet',$id,$language_name)); ?>
    </li>
    <li class="active">Add Alphabet</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="modes row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('Alphabet',array('type'=>'file')); ?>
	<div class="box-body">
    <div class="row">
      <div class="col-md-6" id="c1">
	     <?php echo $this->Form->input('alphabet',array('required'=>true)); ?>
      </div>

        <div class="col-md-6">
          <?php  echo $this->Form->input('audio_clip',array('label'=>'Alphabet_Audio','type'=>'file','id'=>'alphabet_audio','onchange'=>'return ValidateAudioExtension(this,"alphabet_audio")')); ?>
        </div>
        <div class="col-md-6" style="width:100%;margin-top:-40px;">
                <br><span id="lblErroralphabet_audio" style="color: red;"></span>
        </div>
         
    </div>

    <div class="row">
      <div class="col-xs-1" style="clear:none !important;">
        <?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary')); ?>
      </div>
      <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
        <?php echo $this->Html->link("Back", array('controller' => 'Modes','action'=> 'manage_alphabet',$id,$language_name), array( 'class' => 'btn btn-primary')); ?>
      </div>
      </div>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->

<style>
#c1 label:after {
  color: #e32;
  content: '*';
  display:inline;
}
</style>