<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Mode'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Modes', array('controller' => 'modes', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="modes row">
	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('Mode'); ?>
	<div class="box-body">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('mode_name');
		echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));
	?>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->