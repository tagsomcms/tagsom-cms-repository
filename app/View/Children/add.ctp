<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add Child'); ?></h1>
  <ol class="breadcrumb">
    <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
      </li>
    <li><?php echo $this -> Html -> link('Children', array('controller' => 'children', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="row">
  <div class="col-xs-12">
  <div class="box box-primary">
<?php echo $this->Form->create('User'); ?>
<div class="box-body">
  <?php
    echo $this->Form->input('UserRelation.user1_id',array('options'=>$parents,'label'=>'Parent ID'));
    echo $this->Form->input('first_name');
    echo $this->Form->input('last_name');
    echo $this->Form->input('username');?>
    <!-- Date dd/mm/yyyy -->
      <div class="form-group" id="c1">
        <label>Date of Birth:</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
           <?php echo $this->Form->input('dob',array('type'=>'text','div'=>false,'label'=>false,'id'=>'datepicker','required'=>true,'placeholder'=>'yyyy-mm-dd')); ?>
        </div><!-- /.input group -->
      </div><!-- /.form group -->
    
    <?php
    
    echo $this->Form->input('language_id');
    echo $this->Form->input('level_id');
    $options=array('male'=>'male','female'=>'female');
    echo $this->Form->input('gender',array('options'=>$options));
    $verified_options=array('Inactive','Active');
    echo $this->Form->input('is_verified',array('options'=>$verified_options));
    echo $this->Form->input('type',array('type'=>'hidden','value'=>'child'));
  ?>
  <div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'children','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
  </div>
</div>
</div>
</div>
</section><!-- /.content -->


<style>
#c1 label:after {
  color: #e32;
  content: '*';
  display:inline;
}
</style>

<script>
    $(document).ready(function(){
      $("#datepicker").datepicker({
        format: 'yyyy-mm-dd',
        endDate:Date()
    });
    })
</script>