<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('User Story Level'); ?></h1>
  <ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('User Story Levels', array('controller' => 'user_story_levels', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="userModeGrades row">
 	<div class="col-xs-12">
		<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($userStoryLevel['UserStoryLevel']['id']); ?>
		</td></tr>
		<tr><th><?php echo __('User'); ?></th>
		<td>
			<?php echo $this->Html->link($users['User']['username'], array('controller' => 'users', 'action' => 'view', $userStoryLevel['UserStoryLevel']['user_id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Language'); ?></th>
		<td>
			<?php echo $this->Html->link($languages['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userStoryLevel['UserStoryLevel']['language_id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Level'); ?></th>
		<td>
			<?php echo $this->Html->link($userStoryLevel['UserStoryLevel']['level_id'], array('controller' => 'levels', 'action' => 'view', $userStoryLevel['UserStoryLevel']['level_id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Story'); ?></th>
		<td>
			<?php echo $this->Html->link($stories['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userStoryLevel['UserStoryLevel']['story_id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Mode'); ?></th>
		<td>
			<?php echo $this->Html->link($modes['Mode']['mode_name'], array('controller' => 'modes', 'action' => 'view', $userStoryLevel['UserStoryLevel']['mode_id'])); ?>
		</td></tr>
		<tr><th><?php echo __('Last Modified'); ?></th>
		<td>
			<?php echo h($userStoryLevel['UserStoryLevel']['modified']); ?>
		</td></tr>
		<tr>
			<td >
				<?php echo $this->Html->link("Back", array('controller' => 'user_story_levels','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
			</td>
		</tr>
	</table>
	</div>
	</div>
</div>
</section><!-- /.content -->
