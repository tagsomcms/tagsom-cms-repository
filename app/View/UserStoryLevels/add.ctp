<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Add User Story Level'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('User Story Levels', array('controller' => 'user_story_levels', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="userStoryLevels row">
	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('UserStoryLevel'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('user_id',array('type'=>'select','options'=>$user_array));
		echo $this->Form->input('language_id');
		echo $this->Form->input('level_id');
		echo $this->Form->input('story_id');
		echo $this->Form->input('mode_id');
	?>
	<div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'user_story_levels','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

	</div>
	</fieldset>
</div>
</div>
</div>
</section><!-- /.content -->