<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('User Story Levels'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('User Story Levels', array('controller' => 'user_story_levels', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="userStoryLevels row">
	<div class="col-xs-12">
		<div class="box">
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_id'); ?></th>
			<th><?php echo $this->Paginator->sort('story_id'); ?></th>
			<th><?php echo $this->Paginator->sort('mode_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userStoryLevels as $userStoryLevel): ?>
	<tr>
		<td><?php echo h($userStoryLevel['UserStoryLevel']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userStoryLevel['User']['username'], array('controller' => 'users', 'action' => 'view', $userStoryLevel['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userStoryLevel['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $userStoryLevel['Language']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userStoryLevel['Level']['level_name'], array('controller' => 'levels', 'action' => 'view', $userStoryLevel['Level']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userStoryLevel['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $userStoryLevel['Story']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userStoryLevel['Mode']['mode_name'], array('controller' => 'modes', 'action' => 'view', $userStoryLevel['Mode']['id'])); ?>
		</td>
		<td><?php echo h($userStoryLevel['UserStoryLevel']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userStoryLevel['UserStoryLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userStoryLevel['UserStoryLevel']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userStoryLevel['UserStoryLevel']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userStoryLevel['UserStoryLevel']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
</div>
</div>
</div>

</section><!-- /.content -->

<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>