        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo __('Student'); ?>
          </h1>
          <ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li><?php echo $this -> Html -> link('Students', array('controller' => 'students', 'action' => 'index')); ?>
            </li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		 
		 <div class="users row">
		 	<div class="col-xs-12">
		 		<div class="box">
			 	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<td><?php echo h($user['User']['id']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('First Name'); ?></th>
						<td><?php echo h($user['User']['first_name']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Last Name'); ?></th>
						<td><?php echo h($user['User']['last_name']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Username'); ?></th>
						<td><?php echo h($user['User']['username']); ?></td>
					</tr>
					<tr>
						<th><?php echo __('Last Modified'); ?></th>
						<td><?php echo h($user['User']['modified']); ?></td>
					</tr>
					<tr>
		                <td colspan=2> <?php echo $this->Html->link("Back", array('controller' => 'Students','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
		                </td>
		            </tr>
				</table>
				</div>
		 	</div>
		 
</div>
		 
<!--		 
<div class="related">
	<h3><?php echo __('Related User Invites'); ?></h3>
	<?php if (!empty($user['UserInvite'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Invite Email Id'); ?></th>
		<th><?php echo __('Invite Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['UserInvite'] as $userInvite): ?>
		<tr>
			<td><?php echo $userInvite['id']; ?></td>
			<td><?php echo $userInvite['user_id']; ?></td>
			<td><?php echo $userInvite['invite_emailid']; ?></td>
			<td><?php echo $userInvite['invite_status']; ?></td>
			<td><?php echo $userInvite['created']; ?></td>
			<td><?php echo $userInvite['modified']; ?></td>
			<td class="actions">
				<?php echo $this -> Html -> link(__('View'), array('controller' => 'user_invites', 'action' => 'view', $userInvite['id'])); ?>
				<?php echo $this -> Html -> link(__('Edit'), array('controller' => 'user_invites', 'action' => 'edit', $userInvite['id'])); ?>
				<?php echo $this -> Form -> postLink(__('Delete'), array('controller' => 'user_invites', 'action' => 'delete', $userInvite['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userInvite['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this -> Html -> link(__('New User Invite'), array('controller' => 'user_invites', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>		 

<div class="related">
	<h3><?php echo __('Related User Mode Grades'); ?></h3>
	<?php if (!empty($user['UserModeGrade'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th><?php echo __('Story Id'); ?></th>
		<th><?php echo __('Language Id'); ?></th>
		<th><?php echo __('Mode Id'); ?></th>
		<th><?php echo __('Grade Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['UserModeGrade'] as $userModeGrade): ?>
		<tr>
			<td><?php echo $userModeGrade['id']; ?></td>
			<td><?php echo $userModeGrade['user_id']; ?></td>
			<td><?php echo $userModeGrade['level_id']; ?></td>
			<td><?php echo $userModeGrade['story_id']; ?></td>
			<td><?php echo $userModeGrade['language_id']; ?></td>
			<td><?php echo $userModeGrade['mode_id']; ?></td>
			<td><?php echo $userModeGrade['grade_id']; ?></td>
			<td><?php echo $userModeGrade['created']; ?></td>
			<td><?php echo $userModeGrade['modified']; ?></td>
			<td class="actions">
				<?php echo $this -> Html -> link(__('View'), array('controller' => 'user_mode_grades', 'action' => 'view', $userModeGrade['id'])); ?>
				<?php echo $this -> Html -> link(__('Edit'), array('controller' => 'user_mode_grades', 'action' => 'edit', $userModeGrade['id'])); ?>
				<?php echo $this -> Form -> postLink(__('Delete'), array('controller' => 'user_mode_grades', 'action' => 'delete', $userModeGrade['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userModeGrade['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this -> Html -> link(__('New User Mode Grade'), array('controller' => 'user_mode_grades', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="related">
	<h3><?php echo __('Related User Mode Statuses'); ?></h3>
	<?php if (!empty($user['UserModeStatus'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Language Id'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Story Id'); ?></th>
		<th><?php echo __('Mode Question Answer Id'); ?></th>
		<th><?php echo __('Answer'); ?></th>
		<th><?php echo __('User Attempt'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['UserModeStatus'] as $userModeStatus): ?>
		<tr>
			<td><?php echo $userModeStatus['id']; ?></td>
			<td><?php echo $userModeStatus['language_id']; ?></td>
			<td><?php echo $userModeStatus['level_id']; ?></td>
			<td><?php echo $userModeStatus['user_id']; ?></td>
			<td><?php echo $userModeStatus['story_id']; ?></td>
			<td><?php echo $userModeStatus['mode_question_answer_id']; ?></td>
			<td><?php echo $userModeStatus['answer']; ?></td>
			<td><?php echo $userModeStatus['user_attempt']; ?></td>
			<td><?php echo $userModeStatus['created']; ?></td>
			<td><?php echo $userModeStatus['modified']; ?></td>
			<td class="actions">
				<?php echo $this -> Html -> link(__('View'), array('controller' => 'user_mode_statuses', 'action' => 'view', $userModeStatus['id'])); ?>
				<?php echo $this -> Html -> link(__('Edit'), array('controller' => 'user_mode_statuses', 'action' => 'edit', $userModeStatus['id'])); ?>
				<?php echo $this -> Form -> postLink(__('Delete'), array('controller' => 'user_mode_statuses', 'action' => 'delete', $userModeStatus['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userModeStatus['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this -> Html -> link(__('New User Mode Status'), array('controller' => 'user_mode_statuses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="related">
	<h3><?php echo __('Related User Purchases'); ?></h3>
	<?php if (!empty($user['UserPurchase'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Language Id'); ?></th>
		<th><?php echo __('Purchase Status'); ?></th>
		<th><?php echo __('Story Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['UserPurchase'] as $userPurchase): ?>
		<tr>
			<td><?php echo $userPurchase['id']; ?></td>
			<td><?php echo $userPurchase['user_id']; ?></td>
			<td><?php echo $userPurchase['language_id']; ?></td>
			<td><?php echo $userPurchase['purchase_status']; ?></td>
			<td><?php echo $userPurchase['story_id']; ?></td>
			<td><?php echo $userPurchase['created']; ?></td>
			<td><?php echo $userPurchase['modified']; ?></td>
			<td class="actions">
				<?php echo $this -> Html -> link(__('View'), array('controller' => 'user_purchases', 'action' => 'view', $userPurchase['id'])); ?>
				<?php echo $this -> Html -> link(__('Edit'), array('controller' => 'user_purchases', 'action' => 'edit', $userPurchase['id'])); ?>
				<?php echo $this -> Form -> postLink(__('Delete'), array('controller' => 'user_purchases', 'action' => 'delete', $userPurchase['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userPurchase['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this -> Html -> link(__('New User Purchase'), array('controller' => 'user_purchases', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<div class="related">
	<h3><?php echo __('Related User Story Levels'); ?></h3>
	<?php if (!empty($user['UserStoryLevel'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover dataTable">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Language Id'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th><?php echo __('Story Id'); ?></th>
		<th><?php echo __('Mode Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['UserStoryLevel'] as $userStoryLevel): ?>
		<tr>
			<td><?php echo $userStoryLevel['id']; ?></td>
			<td><?php echo $userStoryLevel['user_id']; ?></td>
			<td><?php echo $userStoryLevel['language_id']; ?></td>
			<td><?php echo $userStoryLevel['level_id']; ?></td>
			<td><?php echo $userStoryLevel['story_id']; ?></td>
			<td><?php echo $userStoryLevel['mode_id']; ?></td>
			<td><?php echo $userStoryLevel['created']; ?></td>
			<td><?php echo $userStoryLevel['modified']; ?></td>
			<td class="actions">
				<?php echo $this -> Html -> link(__('View'), array('controller' => 'user_story_levels', 'action' => 'view', $userStoryLevel['id'])); ?>
				<?php echo $this -> Html -> link(__('Edit'), array('controller' => 'user_story_levels', 'action' => 'edit', $userStoryLevel['id'])); ?>
				<?php echo $this -> Form -> postLink(__('Delete'), array('controller' => 'user_story_levels', 'action' => 'delete', $userStoryLevel['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userStoryLevel['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this -> Html -> link(__('New User Story Level'), array('controller' => 'user_story_levels', 'action' => 'add')); ?> </li>
		</ul>
	</div>
-->
	<div style="clear: both;"></div>	
</div>
</section><!-- /.content -->