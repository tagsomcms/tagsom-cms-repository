<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo __('Free book'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Free books', array('controller' => 'freebooks', 'action' => 'index')); ?>
    </li>
    <li class="active">View</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="freebooks view">
	<div class="col-xs-12">
	<div class="box">
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td><?php echo h($freebook['Freebook']['id']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Title'); ?></th>
			<td><?php echo h($freebook['Freebook']['title']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Total Invites'); ?></th>
			<td><?php echo h($freebook['Freebook']['invited']); ?></td>
		</tr>
		<tr>
			<th><?php echo __('Last Modified'); ?></th>
			<td><?php echo h($freebook['Freebook']['modified']); ?></td>
		</tr>
	</table>
	</div>
	</div>
</div>
<div style="clear: both;"></div>
</section>