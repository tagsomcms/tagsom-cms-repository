<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo __('Free books List'); ?>
    <small></small>
  </h2>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Free books', array('controller' => 'freebooks', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
        <section class="content">
		 <div class="row">
		<div class="col-xs-12">
		<div class="box">	
<div class="freebooks index" style="width:100%;">
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('invited','Total invites'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($freebooks as $freebook): ?>
	<tr>
		<td><?php echo h($freebook['Freebook']['id']); ?>&nbsp;</td>
		<td><?php echo h($freebook['Freebook']['title']); ?>&nbsp;</td>
		<td><?php echo h($freebook['Freebook']['invited']); ?>&nbsp;</td>
		<td><?php echo h($freebook['Freebook']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $freebook['Freebook']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $freebook['Freebook']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $freebook['Freebook']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $freebook['Freebook']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	</div>
	</div>
</div>
</section><!-- /.content -->


<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
        "dom": '<lf<t>ip>'
    } );
});
</script>