<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
   <?php echo __('Edit Free book'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Free books', array('controller' => 'freebooks', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>


<!-- Main content -->
<section class="content">
 <div class="users row">
 	<div class="col-xs-12">
<div class="box box-primary">
<?php echo $this->Form->create('Freebook'); ?>
<div class="box-body">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
    echo $this->Form->input('invited',array('type'=>'number','label'=>'Total invites','min'=>0,'max'=>100,'id'=>'max_val'));
	?>
<div class="row">
        <div class="col-xs-1" style="clear:none !important;margin-top:20px;margin-left:10px;">
          <?php echo $this->Html->link("Back", array('controller' => 'freebooks','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
        </div>
        <div class="col-md-1" style="clear:none !important;">
          <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-primary'));?>
        </div> 

    </div>
</div>
</div>
</div>
</div>
</section><!-- /.content -->

<script>
$('#max_val').on("keypress", function(e){
       var currentValue = String.fromCharCode(e.which);
        var finalValue = $(this).val() + currentValue;
        if(finalValue > 100){
            e.preventDefault();
        }
    });
</script>