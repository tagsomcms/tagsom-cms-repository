<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo __('Assign Free books'); ?>
    <small></small>
  </h2>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Free books', array('controller' => 'freebooks', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
        <section class="content">
		 <div class="row">
		<div class="col-xs-12">
		<div class="box">	
<div class="freebooks index">
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('story_name'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Last Modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($books as $freebook): ?>
	<tr>

		<?php 
		$bookstatus=0;
		foreach($freebooks as $free)
		{
			if($freebook['Story']['id']==$free['AssignFreebook']['story_id'])
			{
				$bookstatus=$free['AssignFreebook']['status'];
			}
		}
		?>
		<td><?php echo h($freebook['Story']['id']); ?>&nbsp;</td>
		<td><?php echo h($freebook['Story']['story_name']); ?>&nbsp;</td>
		<td><?php if($bookstatus!=1){echo 'Not Active';}else{echo 'Active';} ?>&nbsp;</td>
		<td><?php echo h($freebook['Story']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php 
			if($bookstatus==1)
			{
				echo $this->Html->link(__('Inactive'), array('action' => 'inactive', $freebook['Story']['id'])); 
			}	
			else
			{
				echo $this->Html->link(__('Active'), array('action' => 'active', $freebook['Story']['id'])); 
			}
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	</div>
	</div>
</div>
</section><!-- /.content -->