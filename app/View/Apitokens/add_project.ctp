<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Project Name'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
       <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Api_Tokens', array('controller' => 'Apitokens', 'action' => 'index'), array('escape' => false)); ?>
      </li>
   
    <li class="active">Assign Project Name</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('form1'); ?>
<div class="box-body">
	<?php
		echo $this->Form->input('name',array('required'=>true,'value'=>$project['Project']['name']));
	?>
  <div class="row">
    <div class="col-xs-1" style="clear:none !important;">
      <?php echo $this->Form->submit('Save',array('class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
      <?php echo $this->Html->link("Back", array('controller' => 'Apitokens','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section><!-- /.content -->

