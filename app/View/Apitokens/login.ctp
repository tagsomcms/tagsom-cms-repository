<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
</head>
<body>
<script>



  window.fbAsyncInit = function() {
    FB.init({
      appId      : '164048680672795',
      xfbml      : true,
      version    : 'v2.7'
    });

    FB.getLoginStatus(function(response) { 
        if (response.status === 'connected') {
          document.getElementById('status').innerHTML ='User is Loggedin';
        } else if (response.status === 'not_authorized') {
          document.getElementById('status').innerHTML = 'We are not authorized';
        } else {
          document.getElementById('status').innerHTML = 'User Logged out';
        }
      });

    FB.api('/me', 'GET',{fields: 'first_name,last_name,name,id,picture.width(150).height(150)'},function(response) {
             console.log(response);
    document.getElementById('username').innerHTML="Username : " +response.first_name;
    document.getElementById('profile').innerHTML="<img src='" +response.picture.data.url +"' >";
    });

  };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function checkLoginState()
    {

          FB.getLoginStatus(function(response) {
            
            if (response.status === 'connected') {
              document.getElementById("logout1").style.visibility = "visible";
              document.getElementById('status').innerHTML ='User is Loggedin';
            } else if (response.status === 'not_authorized') {
              document.getElementById('status').innerHTML = 'We are not authorized';
            } else {
              document.getElementById('status').innerHTML = 'User Logged out';
            }
          });

            FB.api('/me', 'GET',{fields: 'first_name,last_name,name,id,picture.width(150).height(150)'},function(response) {
               console.log(response);
            if(response.first_name!='')
            {
            document.getElementById('username').innerHTML="Username : " +response.first_name;
            }
            if(response.picture.data.url!='')
            {
            document.getElementById('profile').innerHTML="<img src='" +response.picture.data.url +"' >";
            }
          });
    }

    function facebooklogout() {
      FB.logout(function (response) {
        location.reload(true);
      });
    }


</script>



<fb:login-button scope="public_profile,email" onlogin="checkLoginState();" onclick="checkLoginState();" id="login_button">Login With Facebook
</fb:login-button>

<div id="username">
</div>

<div id="email">
</div>

</body>


</html>