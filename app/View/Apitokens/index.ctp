<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Api Tokens'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li class="active">Index</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="stories row">
<div class="col-xs-12">
<div class="box">	
 
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable" id="searchable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('api'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions" style="color:#3C8DBC;"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($api_tokens as $api): ?>
	<tr>
		<td><?php echo h($api['Apitoken']['id']); ?>&nbsp;</td>
		<td><?php echo h($api['Apitoken']['api']); ?>&nbsp;</td>
		<td><?php if(h($api['Apitoken']['status'])==1){ echo 'Active'; }else{echo 'Inactive';} ?>&nbsp;</td>
		<td><?php echo h($api['Apitoken']['created']); ?>&nbsp;</td>
		<td><?php echo h($api['Apitoken']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php  if(h($api['Apitoken']['status'])==1) {echo $this->Html->link(__('Inactive'), array('action' => 'inactivation', $api['Apitoken']['id'])); } else { echo $this->Html->link(__('Active'), array('action' => 'activation', $api['Apitoken']['id']));} ?>
	
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $api['Apitoken']['id'])); ?>
	
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $api['Apitoken']['id'])); ?>
		
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $api['Apitoken']['id']), array('confirm' => __('Are you sure you want to delete this api?', $api['Apitoken']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
</div>
</div>
</div>
</section><!-- /.content -->


<script>
$(document).ready(function () {
     $('#searchable').DataTable( {
       "dom": '<lf<t>ip>'
    } );
});
</script>