<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Import Story Content in POEditor'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
       <li>
      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Api_Tokens', array('controller' => 'Apitokens', 'action' => 'index'), array('escape' => false)); ?>
      </li>
   
    <li class="active">Import</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
	<div class="box box-primary">
<?php echo $this->Form->create('form1'); ?>
<div class="box-body">
	
   <table style="margin-left:30px;">
     <tr>
    <td>
      <?php echo $this->Form->input('all',array('type'=>'checkbox','onclick'=>'check()','id'=>'all')); ?>
    </td></tr>
    <tr></th>
    <td>
       <?php echo $this->Form->input('stories',array('type'=>'checkbox','id'=>'story','onclick'=>'uncheck()')); ?>
    </td></tr>
    <tr>
    <td>
       <?php echo $this->Form->input('statements',array('type'=>'checkbox','id'=>'statement','onclick'=>'uncheck()')); ?>
    </td></tr>
    <tr>
    <td>
      <?php echo $this->Form->input('minigames',array('type'=>'checkbox','id'=>'minigame','onclick'=>'uncheck()')); ?>
    </td></tr>
   
  </table>


  <div class="row">
    <div class="col-xs-2" style="clear:none !important;">
      <?php echo $this->Form->submit('Import to POEditor',array('class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
      <?php echo $this->Html->link("Back", array('controller' => 'Apitokens','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section><!-- /.content -->


<script>
function check()
{
      $("#story").attr("checked", false);
      $("#statement").attr("checked", false);
      $("#minigame").attr("checked", false);
}
function uncheck()
{
      $("#all").attr("checked", false);
}


</script>
 

