<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Api Tokens'); ?></h1>
<ol class="breadcrumb">
            <li>
            	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
            	</li>
            <li>
		      <?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Api_Tokens', array('controller' => 'Apitokens', 'action' => 'index'), array('escape' => false)); ?>
		      </li>
            <li class="active">View</li>
          </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="modes row">
<div class="col-xs-12">
<div class="box">	
	<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
		<tr><th><?php echo __('Id'); ?></th>
		<td>
			<?php echo $api_tokens['Apitoken']['id']; ?>
		</td></tr>
		<tr><th><?php echo __('Api'); ?></th>
		<td>
			<?php echo $api_tokens['Apitoken']['api']; ?>
		</td></tr>
		<tr><th><?php echo __('Status'); ?></th>
		<td>
			<?php if($api_tokens['Apitoken']['status']==1) { echo 'Active'; }else{echo 'Inactive';} ?>
		</td></tr>
		<tr><th><?php echo __('Created'); ?></th>
		<td>
			<?php echo $api_tokens['Apitoken']['created']; ?>
		</td></tr>
		<tr><th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo $api_tokens['Apitoken']['modified']; ?>
		</td></tr>
	</table>
	<div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
				<?php echo $this->Html->link("Back", array('controller' => 'Apitokens','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
	</div>
</div>
</div>
</div>
</section><!-- /.content -->