<body onload="f1()">
<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('Add Mode Question Answer'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
    	</li>
    <li><?php echo $this -> Html -> link('Mode Question Answers', array('controller' => 'mode_question_answers', 'action' => 'index')); ?>
    </li>
    <li class="active">Add</li>
  </ol>
</section>
<!-- End Breadcrumb -->

<!-- Main content -->
<section class="content">
 <div class="modeQuestionAnswers row">
 	<div class="col-xs-12">
	<div class="box box-primary">
	<?php echo $this->Form->create('ModeQuestionAnswer',array('type' => 'file')); ?>
	<fieldset>
	<div class="row"  style="margin-left:-7px;">
		<?php echo $this->Form->input('mode_id',array('id'=>'mode','onchange'=>'mode_change(this.value);'));?>
	</div>
	<div class="row"  style="margin-left:-7px;">
		<?php echo $this->Form->input('story_id');  ?>
	</div>
	<div class="row"  style="margin-left:-7px;">
		<?php  echo $this->Form->input('level_id',array('id'=>'level','onchange'=>'level_change(this.value);')); ?>
	</div>
	<div class="row"  style="margin-left:-7px;">
		<?php  echo $this->Form->input('mode_question',array('required'=>true)); ?>
	</div>
	<div class="row" style="margin-left:-21px;">
		<div class="col-xs-5">
			<?php echo $this->Form->input('audio_clip',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Question)(Optional)','required'=>false,'id'=>'audio1','onchange'=>'return ValidateAudioExtension(this,"audio1")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroraudio1" style="color: red;"></span>
		</div>
	</div>
	<div class="row"  style="margin-left:-7px;">
	<?php echo $this->Form->input('mode_answer',array('required'=>true)); ?>
	</div>
 
	<div class="row" style="margin-left:-21px;">
		<div class="col-xs-5">
			<?php echo $this->Form->input('answer_audio1',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Answer)(Optional)','required'=>false,'id'=>'answer_audio1','onchange'=>'return ValidateAudioExtension(this,"answer_audio1")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroranswer_audio1" style="color: red;"></span>
		</div>
	</div> 
	
	
	<!-- mode question 1 -->
	<div class="row" style="margin-left:-21px;" id="options">
	<div class="col-xs-3">
		<?php echo $this->Form->input('mode_option1',array('label'=>'Mode option1','required'=>false)); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;">
		<?php echo $this->Form->input('mode_option2',array('label'=>'Mode option2','required'=>false)); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;">
		<?php echo $this->Form->input('mode_option3',array('label'=>'Mode option3','required'=>false)); ?>
	</div>
	<div class="col-xs-3" style="clear:none !important;margin-top:35px;">
		<b>(Mode options are optional)</b>
	</div>
	</div>
	 <div class="row" style="margin-left:-21px;" id="opt1">
		<div class="col-xs-5">
			<?php echo $this->Form->input('option_1_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 1)(Optional)','required'=>false,'id'=>'option1_audio','onchange'=>'return ValidateAudioExtension(this,"option1_audio")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroroption1_audio" style="color: red;"></span>
		</div>
	</div>
	<div class="row" style="margin-left:-21px;" id="opt2">
		<div class="col-xs-5">
			<?php echo $this->Form->input('option_2_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 2)(Optional)','required'=>false,'id'=>'option2_audio','onchange'=>'return ValidateAudioExtension(this,"option2_audio")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroroption2_audio" style="color: red;"></span>
		</div>
	</div>
	<div class="row" style="margin-left:-21px;" id="opt3">
		<div class="col-xs-5">
			<?php echo $this->Form->input('option_3_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 3)(Optional)','required'=>false,'id'=>'option3_audio','onchange'=>'return ValidateAudioExtension(this,"option3_audio")')); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
				<span id="lblErroroption3_audio" style="color: red;"></span>
		</div>
	</div>
	
	<div class="row" id="img_file1" style="margin-left:-21px;">
		<div class="col-xs-5">
			<?php echo $this->Form->input('chapter_image',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Chapter Image','id'=>'img1','onchange'=>'return ValidateImageExtension(this,"img1")','required'=>true)); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
			<span id="lblErrorimg1" style="color: red;"></span>
		</div>
	</div>

	<div class="row" id="img_file2" style="margin-left:-21px;">
		<div class="col-xs-5">
			<?php echo $this->Form->input('image_to_find',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Image to find','id'=>'img2','onchange'=>'return ValidateImageExtension(this,"img2")','required'=>true)); ?>
		</div>
		<div class="col-xs-7" style="margin-left:5px;">
			<span id="lblErrorimg2" style="color: red;"></span>
		</div>
	</div>

	<div class="row">
	<div class="col-xs-1" style="clear:none !important;">
		<?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
	</div>
	<div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
		<?php echo $this->Html->link("Back", array('controller' => 'ModeQuestionAnswers','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
	</div>
	</div>

	</fieldset>
</div>
</div>
</div>
</section>
<!-- /Main content -->

<style>
form div {
    clear: both;
     margin-bottom: 0em !important; 
    padding: .5em;
    vertical-align: text-top;
}
.col-xs-6 label:after {
	color: #e32;
	content: '*';
	display:inline;
}
</style>


