<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?php echo __('Edit Mode Question Answer'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Mode Question Answers', array('controller' => 'mode_question_answers', 'action' => 'index')); ?>
    </li>
    <li class="active">Edit</li>
  </ol>
</section>

<?php 
foreach($image as $img)
{
	$img_to_find=$img['ModeQuestionAnswer']['image_to_find'];
	$chapter_img=$img['ModeQuestionAnswer']['chapter_image'];
	$audio=$img['ModeQuestionAnswer']['audio_clip'];
	$answer_audio1=$img['ModeQuestionAnswer']['answer_audio'];
	$option1_audio=$img['ModeQuestionAnswer']['option1_audio'];
	$option2_audio=$img['ModeQuestionAnswer']['option2_audio'];
	$option3_audio=$img['ModeQuestionAnswer']['option3_audio'];
	$question=$img['ModeQuestionAnswer']['mode_question'];
	$answer=$img['ModeQuestionAnswer']['mode_answer'];
	$opt1=$img['ModeQuestionAnswer']['mode_option1'];
	$opt2=$img['ModeQuestionAnswer']['mode_option2'];
	$opt3=$img['ModeQuestionAnswer']['mode_option3'];

}
?>
<!-- Main content -->
<section class="content">
 <div class="stories row">
 	<div class="col-xs-12">
		<div class="box box-primary">
		<?php echo $this->Form->create('ModeQuestionAnswer',array('type' => 'file','id'=>'ModeQuestionAnswer')); ?>
			<div class="box-body">
			<div class="row"  style="margin-left:-7px;">
				<?php echo $this->Form->input('id');?>
			</div>
			<div class="row"  style="margin-left:-7px;">
				<?php echo $this->Form->input('mode_id',array('id'=>'mode','onchange'=>'mode_change1(this.value);'));?>
			</div>
			<div class="row"  style="margin-left:-7px;">
				<?php echo $this->Form->input('story_id');?>
			</div>
			<div class="row"  style="margin-left:-7px;">
				<?php  echo $this->Form->input('level_id',array('id'=>'level','onchange'=>'level_change(this.value);')); ?>
			</div>
			<div class="row"  style="margin-left:-7px;" id="question">
				<?php echo $this->Form->input('mode_question',array('required'=>false));?>
			</div>
			<div class="row" style="margin-left:-21px;" id="question_audio">
				<div class="col-xs-5" >
					<?php echo $this->Form->input('audio_clip',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Optional)','required'=>false,'id'=>'audio2','onchange'=>'audio_change(this,"audio2");')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;margin-top:-15px;">
						<span id="lblErroraudio" style="color:red;"></span>
				</div><br>
				<h3 id="audio-name"><?php echo $audio; ?></h3>
			</div>
			<div class="row"  style="margin-left:-21px;" id="answer">
				<div class="col-xs-12">
				<?php echo $this->Form->input('mode_answer',array('required'=>false));?>
				</div>
			</div>
	        

			<div class="row"  style="margin-left:-7px;">
				<?php echo $this->Form->input('image_to_find1',array('type'=>'hidden','value'=>$img_to_find));?>
					<?php echo $this->Form->input('count',array('type'=>'hidden','value'=>$cnt));?>
					<?php echo $this->Form->input('question_h',array('type'=>'hidden','value'=>$question));?>
					<?php echo $this->Form->input('answer_h',array('type'=>'hidden','value'=>$answer));?>
					<?php echo $this->Form->input('opt1_h',array('type'=>'hidden','value'=>$opt1));?>
					<?php echo $this->Form->input('opt2_h',array('type'=>'hidden','value'=>$opt2));?>
					<?php echo $this->Form->input('opt3_h',array('type'=>'hidden','value'=>$opt3));?>
					<?php echo $this->Form->input('chapter_h',array('type'=>'hidden','value'=>$chapter_img));?>
			</div>

			 <div class="row" style="margin-left:-21px;" id="answer_audio">
				<div class="col-xs-5">
					<?php echo $this->Form->input('answer_audio1',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Answer)(Optional)','required'=>false,'id'=>'answer_audio1','onchange'=>'return ValidateAudioExtension(this,"answer_audio1")')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;">
						<span id="lblErroranswer_audio1" style="color: red;"></span>
				</div><br>
				<h3 id="audio-name"><?php echo $answer_audio1; ?></h3>
			</div>

			<div class="row" style="margin-left:-21px;" id="options">
			<div class="col-xs-3">
				<?php echo $this->Form->input('mode_option1',array('label'=>'Mode option1')); ?>
			</div>
			<div class="col-xs-3" style="clear:none !important;">
				<?php echo $this->Form->input('mode_option2',array('label'=>'Mode option2')); ?>
			</div>
			<div class="col-xs-3" style="clear:none !important;">
				<?php echo $this->Form->input('mode_option3',array('label'=>'Mode option3')); ?>
			</div>
			<div class="col-xs-3" style="clear:none !important;margin-top:27px;">
				<b><h4>(Mode options are optional)</h4></b>
			</div>
			</div>
			<div class="row" style="margin-left:-21px;" id="op1_audio">
				<div class="col-xs-5">
					<?php echo $this->Form->input('option_1_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 1)(Optional)','required'=>false,'id'=>'option1_audio','onchange'=>'return ValidateAudioExtension(this,"option1_audio")')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;">
						<span id="lblErroroption1_audio" style="color: red;"></span>
				</div><br>
				<h3 id="audio-name"><?php echo $option1_audio; ?></h3>
			</div>
			<div class="row" style="margin-left:-21px;" id="op2_audio">
				<div class="col-xs-5">
					<?php echo $this->Form->input('option_2_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 2)(Optional)','required'=>false,'id'=>'option2_audio','onchange'=>'return ValidateAudioExtension(this,"option2_audio")')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;">
						<span id="lblErroroption2_audio" style="color: red;"></span>
				</div><br>
				<h3 id="audio-name"><?php echo $option2_audio; ?></h3>
			</div>
			<div class="row" style="margin-left:-21px;" id="op3_audio">
				<div class="col-xs-5">
					<?php echo $this->Form->input('option_3_audio',array('type'=>'file','class'=>'inputfile inputfile-6','data-multiple-caption'=>'{count} files selected','label'=>'Select Audio File(Option 3)(Optional)','required'=>false,'id'=>'option3_audio','onchange'=>'return ValidateAudioExtension(this,"option3_audio")')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;">
						<span id="lblErroroption3_audio" style="color: red;"></span>
				</div><br>
				<h3 id="audio-name"><?php echo $option3_audio; ?></h3>
			</div>

			<div class="row" id="img_file1" style="margin-left:-21px;">
				<div class="col-xs-5">
					<?php echo $this->Form->input('chapter_image',array('type'=>'file','class'=>'inputfile inputfile-6 ','data-multiple-caption'=>'{count} files selected','label'=>'Select Chapter Image','required'=>false,'id'=>'img22','onchange'=>'image_change2(this,"img22");')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;margin-top:-15px;">
						<span id="lblErrorimg1" style="color: red;"></span>
				</div>

				<?php 
				if (!empty($chapter_img))
				{
				?>
				<img src="<?php echo $this->base . "/app/webroot/uploads/".$story_path."/".$modename."/Image/". $chapter_img; ?>" width="90px" height="55px" id="image_file_name_preview2" style="margin-left:10px;margin-top:25px;">
				<?php
				}
				else{
				?>
				<img src="<?php echo $this->base . "/app/webroot/uploads/no_image1.jpg" ?>" width="90px" height="55px" id="image_file_name_preview" style="margin-left:10px;margin-top:25px;">
					<?php } ?>

			</div>
			
			<div class="row" id="img_file2" style="margin-left:-21px;">
				<div class="col-xs-5">
					<?php echo $this->Form->input('image_to_find',array('type'=>'file','class'=>'inputfile inputfile-6 ','data-multiple-caption'=>'{count} files selected','label'=>'Select Image to find','required'=>false,'id'=>'img11','onchange'=>'image_change(this,"img11");')); ?>
				</div>
				<div class="col-xs-7" style="margin-left:5px;margin-top:-15px;">
						<span id="lblErrorimg" style="color: red;"></span>
				</div>
				<?php 
				if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/'.$modename.'/Image/'.$img_to_find))
				{
				?>
				<img src="<?php echo $this->base . "/app/webroot/uploads/".$story_path."/".$modename."/Image/". $img_to_find; ?>" width="90px" height="55px" id="image_file_name_preview" style="margin-left:10px;margin-top:25px;">
				<?php
				}
				else{
				?>
				<img src="<?php echo $this->base . "/app/webroot/uploads/no_image1.jpg" ?>" width="90px" height="55px" id="image_file_name_preview" style="margin-left:10px;margin-top:25px;">
					<?php } ?>
			</div>
			
			<div class="row">
			<div class="col-xs-1" style="clear:none !important;">
				<?php echo $this->Form->submit(__('Submit'),array('class'=>'btn btn-primary','id'=>'myBtn')); ?>
			</div>
			<div class="col-xs-4" style="clear:none !important;margin-top:20px;margin-left:10px;">
				<?php echo $this->Html->link("Back", array('controller' => 'ModeQuestionAnswers','action'=> 'index'), array( 'class' => 'btn btn-primary')); ?>
			</div>
			</div>
				
			<?php echo $this->Form->end(); ?>		
			</div>
		</div>
</div>
</div>
</section><!-- /.content -->
</html>


<style>
.col-xs-6 label:after {
	color: #e32;
	content: '*';
	display:inline;
}
</style>

<script>

function f2()
{
	$('#img_file2').hide();
	var i=document.getElementById('mode').value;
	var l=document.getElementById('level').value;
	
	if(i!=2)
	{
		$('#img_file').hide();
	}
	else
	{
		$('#img_file2').show();
		$('#img_file').show();
		document.getElementById("img11").required = false;
	}
	if(i==3)
	{
		$('#options').show();
		$('#op1_audio').show();
		$('#op2_audio').show();
		$('#op3_audio').show();
	}
	if(i==1 || i==2 || i==4)
	{
		$('#options').hide();
		$('#op1_audio').hide();
		$('#op2_audio').hide();
		$('#op3_audio').hide();
	}
	if(i==2 && l!=1)
	{
		$('#img_file2').show();
	}
	else
	{
		$('#img_file2').hide();
	}
}

function mode_change1(mode)
{
	var l=document.getElementById('level').value;
	if(mode==2)
	{
		$('#img_file2').show();
		$('#img_file').show();
		document.getElementById("img11").required = true;
	}
	else
	{
		$('#img_file2').hide();
		$('#img_file').hide();
		document.getElementById("img11").required = false;
	}
	if(mode==3)
	{
		$('#options').show();
		$('#op1_audio').show();
		$('#op2_audio').show();
		$('#op3_audio').show();
	}
	if(mode==1 || mode==2 || mode==4)
	{
		$('#options').hide();
		$('#op1_audio').hide();
		$('#op2_audio').hide();
		$('#op3_audio').hide();
	}
	if(mode==2 && l!=1)
	{
		$('#img_file2').show();
	}
	else
	{
		$('#img_file2').hide();
	}

}

function image_change(input,id) {
	$('#image_file_name_preview').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_file_name_preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    var allowedFiles = [".jpg", ".png", ".gif"];
    var lblError = document.getElementById("lblErrorimg");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
    if (!regex.test(input.value.toLowerCase())) {
     	var newElm = document.createElement('input');
     	var old = document.getElementById(id);
	    newElm.type = "file";
	    newElm.id = id;
	    newElm.name = old.name;
	    newElm.className = old.className;
	    old.parentNode.replaceChild(newElm, old);
        lblError.innerHTML = "Please upload image files only.";
        setTimeout(function () {lblError.style.display='none'}, 3000);
        $('#image_file_name_preview').hide();
        return false;
    }
    lblError.innerHTML = "";
    return true;
}

function image_change2(input,id) {
	$('#image_file_name_preview2').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_file_name_preview2').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    var allowedFiles = [".jpg", ".png", ".gif"];
    var lblError = document.getElementById("lblErrorimg1");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
    if (!regex.test(input.value.toLowerCase())) {
     	var newElm = document.createElement('input');
     	var old = document.getElementById(id);
	    newElm.type = "file";
	    newElm.id = id;
	    newElm.name = old.name;
	    newElm.className = old.className;
	    old.parentNode.replaceChild(newElm, old);
        lblError.innerHTML = "Please upload image files only.";
        setTimeout(function () {lblError.style.display='none'}, 3000);
        $('#image_file_name_preview2').hide();
        return false;
    }
    lblError.innerHTML = "";
    return true;
}

function audio_change(fileUpload,id1) {
    var allowedFiles = [".wav"];
    var lblError = document.getElementById("lblErroraudio");
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
    audio.innerHTML = audioname;
    if (!regex.test(fileUpload.value.toLowerCase())) {
    	var newElm = document.createElement('input');
     	var old = document.getElementById(id1);
	    newElm.type = "file";
	    newElm.id = id1;
	    newElm.name = old.name;
	    newElm.className = old.className;
	    old.parentNode.replaceChild(newElm, old);
        lblError.innerHTML = "Please upload audio files only.";
        audio.innerHTML = "";
        setTimeout(function () {lblError.style.display='none'}, 3000);
        return false;
    }
    lblError.innerHTML = "";
    return true;
}
</script>


<style>
form div {
    clear: both;
     margin-bottom: 0em !important; 
    padding: .5em;
    vertical-align: text-top;
}
</style>

<script>
window.onload = function () {

if (!localStorage.getItem("reload")) {

    localStorage.setItem("reload", "true");
    location.reload();
    f2();
}
else {
    localStorage.removeItem("reload");
    f2();
}
}
</script>