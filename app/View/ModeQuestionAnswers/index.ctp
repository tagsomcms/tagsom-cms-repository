<!-- Start Breadcrumb -->
<section class="content-header">
  <h1><?php echo __('Mode Question Answers'); ?></h1>
  <ol class="breadcrumb">
    <li>
    	<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
   	</li>
    <li><?php echo $this -> Html -> link('Mode Question Answers', array('controller' => 'mode_question_answers', 'action' => 'index')); ?>
    </li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- End Breadcrumb -->

<!-- Main content -->
<section class="content">
<div class="modeQuestionAnswers row">
<div class="col-xs-12">
	<div class="box">	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover dataTable">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('mode_id'); ?></th>
			<th><?php echo $this->Paginator->sort('story_id'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('mode_question'); ?></th>
			<th><?php echo $this->Paginator->sort('mode_options'); ?></th>
			<th><?php echo $this->Paginator->sort('mode_answer'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($modeQuestionAnswers as $modeQuestionAnswer): ?>
	<tr>
		<td><?php echo h($modeQuestionAnswer['ModeQuestionAnswer']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($modeQuestionAnswer['Mode']['mode_name'], array('controller' => 'modes', 'action' => 'view', $modeQuestionAnswer['Mode']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($modeQuestionAnswer['Story']['story_name'], array('controller' => 'stories', 'action' => 'view', $modeQuestionAnswer['Story']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($modeQuestionAnswer['Language']['language_name'], array('controller' => 'languages', 'action' => 'view', $modeQuestionAnswer['Language']['id'])); ?>
		</td>
		<td><?php echo h($modeQuestionAnswer['ModeQuestionAnswer']['mode_question']); ?>&nbsp;</td>
		<td><?php if(!empty(h($modeQuestionAnswer['ModeQuestionAnswer']['mode_option1']))){echo h($modeQuestionAnswer['ModeQuestionAnswer']['mode_option1']).", ".h($modeQuestionAnswer['ModeQuestionAnswer']['mode_option2']).", ".h($modeQuestionAnswer['ModeQuestionAnswer']['mode_option3']);}else{echo '-';} ?>&nbsp;</td>
		<td><?php echo h($modeQuestionAnswer['ModeQuestionAnswer']['mode_answer']); ?>&nbsp;</td>
		<td><?php echo h($modeQuestionAnswer['ModeQuestionAnswer']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $modeQuestionAnswer['ModeQuestionAnswer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $modeQuestionAnswer['ModeQuestionAnswer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $modeQuestionAnswer['ModeQuestionAnswer']['id']), array('confirm' => __('Are you sure you want to delete this question?', $modeQuestionAnswer['ModeQuestionAnswer']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev(__('Previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Next'), array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
</div>
</div>
</section>
<!-- /Main content -->
