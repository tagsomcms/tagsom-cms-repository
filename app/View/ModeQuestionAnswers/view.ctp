<!-- Start Breadcrumb -->
<section class="content-header">
<h1><?php echo __('Mode Question Answer'); ?></h1>
<ol class="breadcrumb">
<li>
<?php echo $this -> Html -> link('<i class="fa fa-dashboard"></i>Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false)); ?>
</li>
<li><?php echo $this -> Html -> link('Mode Question Answers', array('controller' => 'mode_question_answers', 'action' => 'index')); ?>
</li>
    <li class="active">View</li>
  </ol>
</section>
<!-- End Breadcrumb -->


<!-- Main content -->
<section class="content">
 <div class="userModeStatuses row">
 	<div class="col-xs-12">
 		<div class="box">
		<table style="padding: 10px; width: 50%" class="table table-bordered table-hover dataTable">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<td>
					<?php echo h($modeQuestionAnswer['Question']['id']); ?>
					&nbsp;
				</td>
			</tr>
			
			<tr>
			<th><?php echo __('Story'); ?></th>
			<td>
				<?php echo $this->Html->link($modeQuestionAnswer['Question']['story_id'], array('controller' => 'stories', 'action' => 'view', $modeQuestionAnswer['Question']['story_id'])); ?>
				&nbsp;
			</td>
			</tr>
			<tr>
			<th><?php echo __('Language'); ?></th>
			<td>
				<?php echo $this->Html->link($modeQuestionAnswer['Question']['language_id'], array('controller' => 'languages', 'action' => 'view', $modeQuestionAnswer['Question']['language_id'])); ?>
				&nbsp;
			</td>
			</tr>
			<tr>
			<th><?php echo __('Mode Question'); ?></th>
			<td>
				<?php echo h($modeQuestionAnswer['Question']['question']); ?>
				&nbsp;
			</td>
			</tr>
			<tr>
			<th><?php echo __('Mode Options'); ?></th>
			<td>
				<?php if(!empty(h($modeQuestionAnswer['Question']['option1']))) { echo h($modeQuestionAnswer['Question']['option1']).", ".h($modeQuestionAnswer['Question']['option2']).", ".h($modeQuestionAnswer['Question']['option3']); }else {echo "-";} ?>
				&nbsp;
			</td>
			</tr>
			<tr>
			<th><?php echo __('Mode Answer'); ?></th>
			<td>
				<?php echo h($modeQuestionAnswer['Question']['answer']); ?>
				&nbsp;
			</td>
			</tr>
		

		</table>
		</div>
	</div>	
</div>
</section><!-- /.content -->






