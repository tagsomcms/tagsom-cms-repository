<?php
App::uses('AppController', 'Controller');
/**
 * UserPurchases Controller
 *
 * @property UserPurchase $UserPurchase
 * @property PaginatorComponent $Paginator
 */
class UserPurchasesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserPurchase->recursive = 0;
		$this->set('userPurchases', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserPurchase->exists($id)) {
			throw new NotFoundException(__('Invalid user purchase'));
		}
		$options = array('conditions' => array('UserPurchase.' . $this->UserPurchase->primaryKey => $id));
		$this->set('userPurchase', $this->UserPurchase->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserPurchase->create();
			if ($this->UserPurchase->save($this->request->data)) {
				$this->Flash->success(__('The user purchase has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user purchase could not be saved. Please, try again.'));
			}
		}
		$users = $this->UserPurchase->User->find('list',array('fields'=>array('User.username')));
		$languages = $this->UserPurchase->Language->find('list',array('fields'=>array('Language.language_name')));
		$stories = $this->UserPurchase->Story->find('list',array('fields'=>array('Story.story_name')));
		$this->set(compact('users', 'languages', 'stories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserPurchase->exists($id)) {
			throw new NotFoundException(__('Invalid user purchase'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserPurchase->save($this->request->data)) {
				$this->Flash->success(__('The user purchase has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user purchase could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserPurchase.' . $this->UserPurchase->primaryKey => $id));
			$this->request->data = $this->UserPurchase->find('first', $options);
		}
		$users = $this->UserPurchase->User->find('list',array('fields'=>array('User.username')));
		$languages = $this->UserPurchase->Language->find('list',array('fields'=>array('Language.language_name')));
		$stories = $this->UserPurchase->Story->find('list',array('fields'=>array('Story.story_name')));
		$this->set(compact('users', 'languages', 'stories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserPurchase->id = $id;
		if (!$this->UserPurchase->exists()) {
			throw new NotFoundException(__('Invalid user purchase'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserPurchase->delete()) {
			$this->Flash->success(__('The user purchase has been deleted.'));
		} else {
			$this->Flash->error(__('The user purchase could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
