<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
/**
 * Languages Controller
 *
 * @property Language $Language
 * @property PaginatorComponent $Paginator
 */
class LanguagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Language->recursive = 0;
		$this->set('languages', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Language->exists($id)) {
			throw new NotFoundException(__('Invalid language'));
		}
		$options = array('conditions' => array('Language.' . $this->Language->primaryKey => $id));
		$this->set('language', $this->Language->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('GeneralSettings');
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];

		if ($this->request->is('post')) {
			$this->loadModel('Story');
			$this->loadModel('User');
			$this->Language->create();
			$lang=$this->data['Language']['language_name'];
			$language_code=$this->Language->getLocaleCodeForDisplayLanguage($lang);
			$stories = $this->Story->find('all');
			$flag=$this->request->data['Language']['flag'];
			$tmp_name=$flag['tmp_name'];
			$file_name=$flag['name'];
			if (!file_exists(WWW_ROOT . 'uploads/Language_Flags/')) {
 				mkdir(WWW_ROOT . 'uploads/Language_Flags/', 0777, true);
			}
			move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Language_Flags/'.$file_name);
			$this->request->data['Language']['flag']=$file_name;
			$langs=$this->Language->find('first',array('conditions'=>array('language_name'=>$lang)));
			if(empty($langs))
			{
				if(!empty($language_code))
				{
					$last_language=$this->Language->find('first', array('order' => array('id' => 'DESC')));
					$next_id=$last_language['Language']['id']+1;
					$username='VoiceActor_'.$lang;
					$password=$lang.'_'.$next_id;

					$this->request->data['Language']['id']=$next_id;
					if ($this->Language->save($this->request->data)) {
						//Add Voice Actor for this language
						$this->User->create();
						$this->User->save(array('username'=>$username,'password'=>$password,'language_id'=>$next_id,'type'=>'voice_actor','is_verified'=>1));

						//add settings for this language
						$strings=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$english_id)));
						foreach($strings as $string)
						{
							$string_id=$string['GeneralSettings']['id'];
							$audio_clip=$string['GeneralSettings']['audio_clip'];
							$this->GeneralSettings->create();
							$this->GeneralSettings->save(array('language_id'=>$next_id,'title'=>'No Translation','string'=>'No Translation','audio_clip'=>$audio_clip,'setting_id'=>$string_id));
						}

						$this->Flash->success(__('The language and voice actor has been saved.'));
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Flash->error(__('The language could not be saved. Please, try again.'));
					}
				}
				else
				{
					$this->Flash->error(__('Please Enter Valid Language.'));
				}
			}
			else
			{
				$this->Flash->error(__('Language already exist.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Language->exists($id)) {
			throw new NotFoundException(__('Invalid language'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
                        $this->loadModel('User');
			$lang=$this->data['Language']['language_name'];
			$language_code=$this->Language->getLocaleCodeForDisplayLanguage($lang);
			$langs=$this->Language->find('first',array('conditions'=>array('language_name'=>$lang)));
			$language_old=$this->Language->find('first',array('conditions'=>array('id'=>$id)));
			$image=$language_old['Language']['flag'];
			$flag=$this->request->data['Language']['flag'];
			$tmp_name=$flag['tmp_name'];
			$file_name=$flag['name'];
			if (!file_exists(WWW_ROOT . 'uploads/Language_Flags/')) {
 				mkdir(WWW_ROOT . 'uploads/Language_Flags/', 0777, true);
			}
			move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Language_Flags/'.$file_name);
			
			if(!empty($language_code))
			{
				if(!empty($file_name))
				{
					$array=array('flag'=>$file_name,'language_name'=>$this->request->data['Language']['language_name']);
				}
				else
				{
					$array=array('language_name'=>$this->request->data['Language']['language_name']);
				}

				$this->Language->id=$id;
				if ($this->Language->save($array)) {
                                    $find_voice_actor=$this->User->find('first',array('conditions'=>array('language_id'=>$id,'type'=>'voice_actor')));
                                    if(empty($find_voice_actor))
                                    {
					$username='VoiceActor_'.$lang;
					$password=$lang.'_'.$id;
                                        $this->User->create();
                                        $this->User->save(array('username'=>$username,'password'=>$password,'language_id'=>$id,'type'=>'voice_actor','is_verified'=>1)); 
                                    }
                          
					$this->Flash->success(__('The language has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The language could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('Please Enter Valid Language.'));
			}
			
		} else {
			$options = array('conditions' => array('Language.' . $this->Language->primaryKey => $id));
			$this->request->data = $this->Language->find('first', $options);
			$language_old=$this->Language->find('first',array('conditions'=>array('id'=>$id)));
			$image=$language_old['Language']['flag'];
			$this->set(compact('image'));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->loadModel('GeneralSettings');
		$api=Configure :: read('api'); 
		$HttpSocket = new HttpSocket();
		$this->Language->id = $id;
		if (!$this->Language->exists()) {
			throw new NotFoundException(__('Invalid language'));
		}

		$this->loadModel('Story');
		$options = array('conditions' => array('Language.id' => $id));
		$lang=$this->Language->find('first',$options);
		$language_code=$this->Language->getLocaleCodeForDisplayLanguage($lang['Language']['language_name']);
		$stories = $this->Story->find('all');
		
		//get story id
		$project_array1=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
		$list_projects1 = json_decode($list_project1, true);		

		$strings=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$id)));
		foreach($strings as $string)
		{
			$this->GeneralSettings->id=$string['GeneralSettings']['id'];
			$this->GeneralSettings->delete();
		}

		$this->request->allowMethod('post', 'delete');
		if ($this->Language->delete()) {
				//add language
				// $response = $HttpSocket->post('https://poeditor.com/api/', ['api_token' => $api,'action'=>'delete_language','id'=>$story_id,'language'=>$language_code]);
				// $jsonarr = json_decode($response, true);
				// pr($jsonarr);
			
			$this->Flash->success(__('The language has been deleted.'));
		} else {
			$this->Flash->error(__('The language could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
