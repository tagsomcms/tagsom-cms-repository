<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
/**
 * Modes Controller
 *
 * @property Mode $Mode
 * @property PaginatorComponent $Paginator
 */
class ModesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() 
	{
		$this->loadModel('Language');
		$this->loadModel('Story');
		if($this->Session->read('Auth.User.type')=='voice_actor')
		{
		  	$actor_langid=$this->Session->read('Auth.User.language_id');
		  	$langs= $this->Language->find('first',array('conditions'=>array('Language.id' => $actor_langid)));
		  	$actor_language=$langs['Language']['language_name'];
		}   

		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		

		$this->Mode->recursive = 0;
		$this->set('modes', $this->Paginator->paginate());
		$languages= $this->Language->find('all');
		$language_array=array('Select Language');

		if(isset($actor_langid))
		{
			array_push($language_array,$actor_language);
			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$actor_langid)));
			$stories_array=array();
			foreach($stories as $story)
			{
				array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
			}
			$stories1= $this->Story->find('first',array('conditions'=>array('Story.language_id'=>$actor_langid)));
			$story_id=$stories1['Story']['id'];
			$language_name=$actor_language;
		}
		else
		{
			$cnt=1;
			foreach($languages as $language)
			{
				if($language['Language']['language_name']=='English')
				{
					$language_id=$language['Language']['id'];
					$selected_language=$cnt;
				}
				array_push($language_array,$language['Language']['language_name']);
				$cnt++;
			}

			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id)));
			$stories_array=array('Select Story');
			foreach($stories as $story)
			{
				array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
			}
			$stories1= $this->Story->find('first',array('conditions'=>array('Story.language_id'=>$language_id)));
			$story_id=$stories1['Story']['id'];
			$language_name='English';
		}

		

		$length=sizeof($language_array);
		$length1=sizeof($stories_array);	
		
		if ($this->request->is('post')) {

			$selected_language=$this->data['StoryLanguage']['language'];
			for($i=0;$i<$length;$i++)
			{
				if($selected_language==$i)
				{
				$languages= $this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
				$language_name=$languages['Language']['language_name'];
				$language_id=$languages['Language']['id'];
				}
			}

			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id)));
			$stories_array=array('Select Story');
			foreach($stories as $story)
			{
				array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
			}


			$selected_story=$this->data['StoryLanguage']['story'];
			for($i=0;$i<$length1;$i++)
			{
				if($selected_story==$i)
				{
				$selected_storyname=$stories_array[$i];	
				$stories1= $this->Story->find('first',array('conditions'=>array('story_name'=>$stories_array[$i],'language_id'=>$language_id)));
				$story_id=$stories1['Story']['id'];
				}
			}
			
			$story_find=$this->Story->find('first',array('conditions'=>array('story_name'=>$selected_storyname,'language_id'=>$english_id)));
			$english_story_id=$story_find['Story']['id'];
		}
		$this->set(compact('language_array','stories_array','language_name','story_id','selected_language','english_story_id'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mode->exists($id)) {
			throw new NotFoundException(__('Invalid mode'));
		}
		$options = array('conditions' => array('Mode.' . $this->Mode->primaryKey => $id));
		$this->set('mode', $this->Mode->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mode->create();
			if ($this->Mode->save($this->request->data)) {
				$this->Flash->success(__('The mode has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The mode could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Mode->exists($id)) {
			throw new NotFoundException(__('Invalid mode'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Mode->save($this->request->data)) {
				$this->Flash->success(__('The mode has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The mode could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Mode.' . $this->Mode->primaryKey => $id));
			$this->request->data = $this->Mode->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mode->id = $id;
		if (!$this->Mode->exists()) {
			throw new NotFoundException(__('Invalid mode'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Mode->delete()) {
			$this->Flash->success(__('The mode has been deleted.'));
		} else {
			$this->Flash->error(__('The mode could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



/**
 * Manage_Word method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_word($id = null,$story_id=null,$language_name=null) 
	{
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Word');
		$this->loadModel('ModeQuestionAnswer');

		$all_languages=array();
		$all_words=$this->Word->find('all');
		foreach($all_words as $word)
		{
			if(!in_array($word['Word']['language_id'], $all_languages))
			{
				array_push($all_languages,$word['Word']['language_id']);
			}
		}



		// $stories=$this->Story->find('all',array('conditions'=>array('language_id'=>1)));
		// foreach($stories as $story)
		// {
			
		// 	$story_name=$story['Story']['story_name'];
		// 	$story_path=str_replace(' ', '_', $story_name);
		// 	$words=$this->Word->find('all',array('conditions'=>array('story_id'=>$story['Story']['id'])));
		// 	foreach($words as $word)
		// 	{
		// 		$word_id=$word['Word']['id'];
		// 		$audio_clip=$word['Word']['audio_clip'];
		// 		$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 		unlink(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/Hindi/'.$audio_file);
		// 		$path=WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/Hindi/'.$audio_clip;
		// 		$audio=WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/Hindi/'.$audio_file;
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $audio";
		// 		shell_exec($cmd);
		// 		chmod(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/Hindi/'.$audio_file,0777);
		// 	}
		// }
		// exit;

		if($this->Session->read('Auth.User.type')=='voice_actor')
		{
		  	$actor_langid=$this->Session->read('Auth.User.language_id');
		  	$langs= $this->Language->find('first',array('conditions'=>array('Language.id' => $actor_langid)));
		  	$actor_language=$langs['Language']['language_name'];
            $language_name=$actor_language;
		}   

		$languages= $this->Language->find('all');
		$language_array=array('Select Language');

		if(isset($actor_langid))
		{
			array_push($language_array,$actor_language);
		}
		else
		{
			foreach($languages as $language)
			{
				if(in_array($language['Language']['id'], $all_languages))
				{
					array_push($language_array,$language['Language']['language_name']);
				}
			}
		}
		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		$language_id=$languages['Language']['id'];
	    $length=sizeof($language_array);
	    for($i=0;$i<$length;$i++)
		{
			if($language_name==$language_array[$i])
			{
				$selected_language=$i;
			}
		}

		if(isset($actor_langid))
		{
			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$actor_langid)));
		}
		else
		{
			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id)));
		}
	    
		$stories_array=array('Select Story');
		foreach($stories as $story)
		{
			array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
		}
		$length1=sizeof($stories_array);
		$selectedstory=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$storyname=$selectedstory['Story']['story_name'];
		$length1=sizeof($stories_array);
		for($i=0;$i<$length1;$i++)
		{
			if($stories_array[$i]==ucwords(strtolower($storyname)))
			{
				$selected_story=$i;
			}
		}

		$story_array=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_path1=str_replace(' ', '_', $story_array['Story']['story_name']);

		if ($this->request->is('post')) {
			if(empty($this->data['Words']['audio_clip']))
			{
				$selected_language=$this->data['StoryLanguage']['language'];
				for($i=0;$i<$length;$i++)
				{
					if($selected_language==$i)
					{
					$languages= $this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
					$language_name=$languages['Language']['language_name'];
					$language_id=$languages['Language']['id'];
					}
				}
				$this->set(compact('language_name'));
				$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id)));
				$stories_array=array('Select Story');
				foreach($stories as $story)
				{
					array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
				}

				$selected_story=$this->data['StoryLanguage']['story'];
				for($i=0;$i<$length1;$i++)
				{
					if($selected_story==$i)
					{
					$stories1= $this->Story->find('first',array('conditions'=>array('story_name'=>$stories_array[$i],'language_id'=>$language_id)));
					$story_id=$stories1['Story']['id'];
					}
				}
				$story_array=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
				$story_path2=str_replace(' ', '_', $story_array['Story']['story_name']);
				return $this->redirect(array('action' => 'manage_word/'.$id,$story_id,$language_name));
			}
			else
			{
			
				$find_story=$this->Story->find('first',array('conditions'=>array('id'=>$this->data['Words']['hidden_story'])));
				$story_name=$find_story['Story']['story_name'];
				$find_language=$this->Language->find('first',array('conditions'=>array('id'=>$this->data['Words']['hidden_language'])));
				$lang_name=$find_language['Language']['language_name'];
				$story_path=str_replace(' ', '_', $story_name);
				$filename=$this->request->data['Words']['hidden_audio'];
				$tmp_name=$this->request->data['Words']['audio_clip']['tmp_name'];
				if (!file_exists(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name, 0777, true);
				}

				$filename1=substr($filename,0,-4).'.mp3';
				if(!empty($tmp_name))
	   			{
	   				unlink(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename);
	   				unlink(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename1);
	   			}

				$path=WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename;
				$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
				shell_exec($cmd); 
			   
			    $path1=WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename1;
				$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
				shell_exec($cmd1);

				chmod(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename1,0777);
				chmod(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename,0777);
				
				//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$lang_name.'/'.$filename);
				$this->Word->id=$this->request->data['Words']['hidden_id'];
				if($this->Word->save(array('audio_clip'=>$filename)))
				{
					$this->Flash->success(__('Word Audio is Successfully Uploaded.'));
					return $this->redirect(array('action' => 'manage_word/'.$id,$story_id,$language_name));
				}
			}

		}


		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		if(isset($actor_langid))
		{
		    $words = $this->Word->find('all',array('conditions'=>array('Word.story_id'=>$story_id,'Word.language_id'=>$actor_langid)));
		}
		else
		{
		    $words = $this->Word->find('all',array('conditions'=>array('Word.story_id'=>$story_id,'Word.language_id'=>$language_id)));
		}
	     
	    
		$this->set(compact('all_words','id','words','story_id','language_name','language_array','stories_array','selected_language','selected_story','story_path1','story_path2'));


	}
        
        
        
        
    /**
    * Disable Word method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function disable_word($word_id=null,$id = null,$story_id=null,$language_name=null,$val=null) 
    {
        $this->loadModel('Word');
        $word=$this->Word->find('first',array('conditions'=>array('id'=>$word_id)));
        $audio_clip=$word['Word']['audio_clip'];
        $words=$this->Word->find('all',array('conditions'=>array('audio_clip'=>$audio_clip)));
        foreach($words as $word)
        {
            $this->Word->id=$word['Word']['id'];
            $this->Word->save(array('disable'=>$val));
            
        }
        return $this->redirect(array('action' => 'manage_word/'.$id.'/'.$story_id.'/'.$language_name));
    }




    /**
    * Disable Alphabet Audio Option method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function disable_alphabet($id=null,$alphabet_id = null,$language_name=null,$val=null) 
    {
        $this->loadModel('Alphabet');
        
        $this->Alphabet->id=$alphabet_id;
        $this->Alphabet->save(array('disable'=>$val));
           
        return $this->redirect(array('action' => 'manage_alphabet/'.$id.'/'.$language_name));
    }




    /**
    * Disable Alphabet Audio Option method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function disable_settings($id=null,$setting_id=null,$val=null) 
    {
        $this->loadModel('GeneralSettings');
        
        $this->GeneralSettings->id=$setting_id;
        $this->GeneralSettings->save(array('disable'=>$val));
           
        $settings=$this->GeneralSettings->find('all',array('conditions'=>array('setting_id'=>$setting_id)));
        foreach($settings as $setting)
        {
        	$this->GeneralSettings->id=$setting['GeneralSettings']['id'];
        	$this->GeneralSettings->save(array('disable'=>$val));
        }

        return $this->redirect(array('action' => 'manage_setting/'.$id));
    }




    /**
    * Disable Question Audio Option method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    public function disable_question($question_id=null,$id=null,$story_id=null,$val=null) 
    {
        $this->loadModel('Question');
        
        $this->Question->id=$question_id;
        $this->Question->save(array('disable'=>$val));
           
        $questions=$this->Question->find('all',array('conditions'=>array('story'=>$question_id)));
        foreach($questions as $question)
        {
        	$this->Question->id=$question['Question']['id'];
        	$this->Question->save(array('disable'=>$val));
        }

        return $this->redirect(array('action' => 'manage_question/'.$id.'/'.$story_id.'/English'));
    }




/**
 * Add Word method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_word($id=null,$story_id=null,$language_name=null) 
	{
		$this->loadModel('Apitoken');
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Word');
		$api=$this->Apitoken->get_active_api();
		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		$language_id=$languages['Language']['id'];
		$find_story=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$find_story['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];

		if ($this->request->is(array('post', 'put'))) 
		{
			$words=$this->Word->find('first',array('conditions'=>array('word'=>$this->request->data['Words']['word'],'story_id'=>$story_id)));
			if(empty($words))
			{
				
				$filename=str_replace(' ', '_',$this->request->data['Words']['word']).".wav";
				$tmp_name=$this->request->data['Words']['audio_clip']['tmp_name'];
				$filename1=$this->request->data['Words']['word'].".jpg";
				$tmp_name1=$this->request->data['Words']['image']['tmp_name'];
				if (!file_exists(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name, 0777, true);
				}
				if (!file_exists(WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path)) {
	 				mkdir(WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path, 0777, true);
				}

				$filename2=substr($filename,0,-4).'.mp3';
				$path=WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name.'/'.$filename;
				$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
				shell_exec($cmd); 

				$path1=WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name.'/'.$filename2;
				$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
				shell_exec($cmd1);

				//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language_name.'/'.$filename);
			
				move_uploaded_file($tmp_name1, WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path.'/'.$filename1);
	
				//add word for all languages of story
				$story_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
				foreach($story_languages as $story_lang)
				{
					$lang_id=$story_lang['Story']['language_id'];
					$st_id=$story_lang['Story']['id'];
					$this->Word->create();
					if($lang_id==$english_id)
					{
						$this->Word->save(array('word'=>$this->request->data['Words']['word'],'language_id'=>$lang_id,'story_id'=>$st_id,'audio_clip'=>$filename,'image'=>$filename1));
						$story=$this->Word->getLastInsertId();

						//get project id from poeditor
						$project_array1=array('api_token' => $api,'action'=>'list_projects');
						$HttpSocket = new HttpSocket();
						$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
						$list_projects1 = json_decode($list_project1, true);
						foreach($list_projects1['list'] as $project)
						{
							if($project['name']==$this->Session->read('project_name')){
								$projectid=$project['id'];
							}
						}
                                              
						//add word in poeditor
			    		$arr11=array("term"=>$this->request->data['Words']['word'],"context"=>"(".$story_name.")");
					    $data11 = array(
				           "api_token" => $api,
				           "action" => "add_terms",
				           "id" => $projectid,
				           "data" => json_encode(array($arr11),true));
				    	$request=array();
				    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);
					}
					else
					{
						$this->Word->save(array('word'=>'No Translation','language_id'=>$lang_id,'story_id'=>$st_id,'audio_clip'=>$filename,'story'=>$story,'image'=>$filename1));
					}
				}
			
				$this->Flash->success(__('Word Successfully added.'));
				return $this->redirect(array('action' => 'add_word/'.$id.'/'.$story_id.'/'.$language_name));
			}	
			else
			{
				$this->Flash->error(__('Word already exist'));
			}	
		}
		$this->set(compact('language_name','id','story_id'));
	}	



/**
 * Edit word method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_word($id = null,$mode=null,$story_id=null,$language_name=null) {
		$this->loadModel('Word');
		$this->loadModel('Story');
		$this->loadModel('Language');
		$this->loadModel('Apitoken');
		
		$api=$this->Apitoken->get_active_api();
		$words=$this->Word->find('first',array('conditions'=>array('id'=>$id)));
		$word=$words['Word']['word'];
		$stories=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$stories['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);
		$old_file=str_replace(' ', '_',$words['Word']['word']).'.wav';
		$old_image_file=$words['Word']['word'].'.jpg';
		$image_name=$words['Word']['image'];
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		if ($this->request->is(array('post', 'put'))) 
		{
			$new_file=str_replace(' ', '_',$this->data['Words']['word']).'.wav';
			$new_image_file=$this->data['Words']['word'].'.jpg';
			$filename1=$this->data['Words']['word'].".jpg";
			$tmp_name1=$this->request->data['Words']['image']['tmp_name'];
			rename(WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path.'/'.$old_image_file,WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path.'/'.$new_image_file);
			if(empty($tmp_name1))
			{
				rename(WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path.'/'.$old_image_file,WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path.'/'.$new_image_file);
			}
			else
			{
				if (!file_exists(WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path)) {
	 				mkdir(WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path, 0777, true);
				}
				move_uploaded_file($tmp_name1, WWW_ROOT . 'uploads/Words/Words_Image/'.$story_path.'/'.$filename1);
			}
			//add word for all languages of story
			$story_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
			foreach($story_languages as $story_lang)
			{
				$lang_id=$story_lang['Story']['language_id'];
				$st_id=$story_lang['Story']['id'];
				$word_array=$this->Word->find('first',array('conditions'=>array('id'=>$id)));
				$word_array2=$this->Word->find('first',array('conditions'=>array('story'=>$id,'story_id'=>$st_id)));
				
				if($lang_id==$english_id)
				{
					if($word!=$this->data['Words']['word'])
					{
						$other_words=$this->Word->find('all',array('conditions'=>array('audio_clip'=>$old_file)));
						foreach($other_words as $word1)
						{
							$language=$this->Language->find('first',array('conditions'=>array('id'=>$word['Word']['language_id'])));
							$this->Word->id=$word1['Word']['id'];
							
							if($language['Language']['language_name']=='English')
							{
								$this->Word->save(array('word'=>$this->data['Words']['word'],'audio_clip'=>$new_file,'image'=>$new_image_file));
							}
							else
							{
								$this->Word->save(array('audio_clip'=>$new_file,'image'=>$new_image_file));
							}
							
							$old_file1=substr($old_file,0,-4).'.mp3';
							$new_file1=substr($new_file,0,-4).'.mp3';
							rename(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language['Language']['language_name'].'/'.$old_file,WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language['Language']['language_name'].'/'.$new_file);
							rename(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language['Language']['language_name'].'/'.$old_file1,WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$language['Language']['language_name'].'/'.$new_file1);
						}
					

						//get project id from poeditor
						$project_array1=array('api_token' => $api,'action'=>'list_projects');
						$HttpSocket = new HttpSocket();
						$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
						$list_projects1 = json_decode($list_project1, true);
						foreach($list_projects1['list'] as $project)
						{
							if($project['name']==$this->Session->read('project_name')){
								$projectid=$project['id'];
							}
						}

						//delete old word from poeditor
			    		$arr11=array("term"=>$word,"context"=>"(".$story_name.")");
					    $data11 = array(
				           "api_token" => $api,
				           "action" => "delete_terms",
				           "id" => $projectid,
				           "data" => json_encode(array($arr11),true));
				    	$request=array();
				    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

				    	//add new word in poeditor
			    		$arr11=array("term"=>$this->request->data['Words']['word'],"context"=>"(".$story_name.")");
					    $data11 = array(
				           "api_token" => $api,
				           "action" => "add_terms",
				           "id" => $projectid,
				           "data" => json_encode(array($arr11),true));
				    	$request=array();
				    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);
					}
					else
					{
						$this->Word->id=$word_array2['Word']['id'];
						$this->Word->save(array('word'=>'No Translation','audio_clip'=>$new_file,'image'=>$new_image_file));
					}
					$langs=$this->Language->find('first',array('conditions'=>array('id'=>$lang_id)));
					//rename(WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$langs['Language']['language_name'].'/'.$old_file,WWW_ROOT . 'uploads/Words/Words_Audio/'.$story_path.'/'.$langs['Language']['language_name'].'/'.$new_file);
				}
			}
			
			
			$this->Flash->success(__('Word Successfully updated.'));
			return $this->redirect(array('action' => 'edit_word/'.$id.'/'.$mode.'/'.$story_id.'/'.$language_name));
		
		}

		$this->set(compact('word','mode','story_id','language_name','story_path','image_name'));
		
	}



/**
 * Delete word method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_word($id = null,$mode=null,$story_id=null,$language_name=null) 
	{
		$this->loadModel('Word');
		$this->loadModel('Story');
		$this->loadModel('Language');
		$this->loadModel('Apitoken');
		
		$api=$this->Apitoken->get_active_api();
		$stories=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$stories['Story']['story_name'];
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$words=$this->Word->find('first',array('conditions'=>array('id'=>$id)));
		$word=$words['Word']['word'];
		//get project id from poeditor
		$project_array1=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
		$list_projects1 = json_decode($list_project1, true);
		foreach($list_projects1['list'] as $project)
		{
			if($project['name']==$this->Session->read('project_name')){
				$projectid=$project['id'];
			}
		}

		//delete word from poeditor
		$arr11=array("term"=>$word,"context"=>"(".$story_name.")");
	    $data11 = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $projectid,
           "data" => json_encode(array($arr11),true));
    	$request=array();
    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

		$story_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
		foreach($story_languages as $story_lang)
		{
			$lang_id=$story_lang['Story']['language_id'];
			$st_id=$story_lang['Story']['id'];
			if($english_id==$lang_id)
			{
				$word_array=$this->Word->find('first',array('conditions'=>array('id'=>$id)));
			}
			else
			{
				$word_array=$this->Word->find('first',array('conditions'=>array('story'=>$id,'story_id'=>$st_id)));
			}
			$this->Word->id=$word_array['Word']['id'];
			$this->Word->delete();
		}
	
		$this->Flash->success(__('Word Successfully Deleted.'));
		return $this->redirect(array('action' => 'manage_word/'.$mode.'/'.$story_id.'/'.$language_name));
	}



/**
 * Manage_Question method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_question($id = null,$story_id=null,$language_name=null) {
		

		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Word');
		$this->loadModel('Question');

		$all_questions=$this->Question->find('all');
		$all_languages=array();
		foreach($all_questions as $question)
		{
			if(!in_array($question['Question']['language_id'],$all_languages))
			{
				array_push($all_languages,$question['Question']['language_id']);
			}
		}


		// $stories=$this->Story->find('all',array('conditions'=>array('language_id'=>2)));
		// foreach($stories as $story)
		// {
			
		// 	$story_name=$story['Story']['story_name'];
		// 	$story_path=str_replace(' ', '_', $story_name);
		// 	$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story['Story']['id'])));
		// 	foreach($questions as $question)
		// 	{
		// 		$question_id=$question['Question']['id'];
		// 		$question_audio=$question['Question']['question_audio'];
		// 		$question_file=substr($question_audio,0,-4).'.mp3';
		// 		$answer_audio=$question['Question']['answer_audio'];
		// 		$answer_file=substr($answer_audio,0,-4).'.mp3';
		// 		$option1_audio=$question['Question']['option1_audio'];
		// 		$option1_file=substr($option1_audio,0,-4).'.mp3';
		// 		$option2_audio=$question['Question']['option2_audio'];
		// 		$option2_file=substr($option2_audio,0,-4).'.mp3';
		// 		$option3_audio=$question['Question']['option3_audio'];
		// 		$option3_file=substr($option3_audio,0,-4).'.mp3';

		// 		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$question_file);
		// 		$path1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$question_audio;
		// 		$audio1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$question_file;
		// 		$cmd1="ffmpeg -i $path1 -acodec libmp3lame $audio1";
		// 		shell_exec($cmd1);
		// 		chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$question_file,0777);

		// 		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$answer_file);
		// 		$path2=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$answer_audio;
		// 		$audio2=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$answer_file;
		// 		$cmd2="ffmpeg -i $path2 -acodec libmp3lame $audio2";
		// 		shell_exec($cmd2);
		// 		chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$answer_file,0777);

		// 		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option1_file);
		// 		$path3=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option1_audio;
		// 		$audio3=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option1_file;
		// 		$cmd3="ffmpeg -i $path3 -acodec libmp3lame $audio3";
		// 		shell_exec($cmd3);
		// 		chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option1_file,0777);

		// 		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option2_file);
		// 		$path4=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option2_audio;
		// 		$audio4=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option2_file;
		// 		$cmd4="ffmpeg -i $path4 -acodec libmp3lame $audio4";
		// 		shell_exec($cmd4);
		// 		chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option2_file,0777);

		// 		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option3_file);
		// 		$path5=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option3_audio;
		// 		$audio5=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option3_file;
		// 		$cmd5="ffmpeg -i $path5 -acodec libmp3lame $audio5";
		// 		shell_exec($cmd5);
		// 		chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/English/'.$option3_file,0777);


		// 	}
		// }
		// exit;
		

		if($this->Session->read('Auth.User.type')=='voice_actor')
		{
		  	$actor_langid=$this->Session->read('Auth.User.language_id');
		  	$langs= $this->Language->find('first',array('conditions'=>array('Language.id' => $actor_langid)));
		  	$actor_language=$langs['Language']['language_name'];
		}   

		$language_array=array('Select Language');
		if(isset($actor_langid))
		{
			array_push($language_array,$actor_language);
		}
		else
		{
			$languages= $this->Language->find('all');
			foreach($languages as $language)
			{
				if(in_array($language['Language']['id'],$all_languages))
				{
					array_push($language_array,$language['Language']['language_name']);
				}
			}
		}
		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		$language_id=$languages['Language']['id'];
	    $length=sizeof($language_array);

	    for($i=0;$i<$length;$i++)
		{
			if($language_array[$i]==$language_name)
			{
				$selected_language=$i;
			}
		}

		$stories_array=array('Select Story');
		if(isset($actor_langid))
		{
			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$actor_langid)));
			foreach($stories as $story)
			{
				array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
			}
			
			$this->paginate = array('conditions'=>array('story_id'=>$story_id,'language_id'=>$actor_langid),
		    'limit' => 10
		    );
		    $questions = $this->paginate('Question');
		}
		else
		{
			$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id)));
			foreach($stories as $story)
			{
				array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
			}

			$this->paginate = array('conditions'=>array('story_id'=>$story_id,'language_id'=>$language_id),
		    'limit' => 10
		    );
		    $questions = $this->paginate('Question');
		}
	    
		$selectedstory=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$storyname=$selectedstory['Story']['story_name'];
		$story_path1=str_replace(' ', '_', $storyname);
		$length1=sizeof($stories_array);
		for($i=0;$i<$length1;$i++)
		{
			if($stories_array[$i]==ucwords(strtolower($storyname)))
			{
				$selected_story=$i;
			}
		}

		//$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id,'language_id'=>$language_id)));
		
	    
		if ($this->request->is(array('post', 'put'))) 
		{
			if(empty($this->data['Question_Answer']['h1']))
			{
				$selected_language=$this->data['StoryLanguage']['language'];
				$selected_story=$this->data['StoryLanguage']['story'];
				for($i=0;$i<$length;$i++)
				{
					if($selected_language==$i)
					{
						$selectedlanguage=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
						$language_id=$selectedlanguage['Language']['id'];
						$language_name=$selectedlanguage['Language']['language_name'];
					}
				}

				$stories= $this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id)));
				$stories_array=array('Select Story');
				foreach($stories as $story)
				{
					array_push($stories_array,ucwords(strtolower($story['Story']['story_name'])));
				}
				$length1=sizeof($stories_array);

				for($i=0;$i<$length1;$i++)
				{
					if($selected_story==$i)
					{
						$selectedstory=$this->Story->find('first',array('conditions'=>array('Story.story_name'=>$stories_array[$i],'language_id'=>$language_id)));
						$story_id=$selectedstory['Story']['id'];
						$story_name=$selectedstory['Story']['story_name'];
						$story_path=str_replace(' ', '_', $story_name);
					}
				}
				return $this->redirect(array('action' => 'manage_question/'.$id.'/'.$story_id.'/'.$language_name));
			}
			else
			{

				$select=$this->data['Question_Answer']['option_select1'];
				$question_array=$this->Question->find('first',array('conditions'=>array('id'=>$this->data['Question_Answer']['h1'])));
				$story_array=$this->Story->find('first',array('conditions'=>array('id'=>$question_array['Question']['story_id'])));
				$language_array=$this->Language->find('first',array('conditions'=>array('id'=>$question_array['Question']['language_id'])));
				$story_name=$story_array['Story']['story_name'];
				$story_path=str_replace(' ', '_', $story_name);
				$language_name=$language_array['Language']['language_name'];
				$question=$this->request->data['Question_Answer']['question_audio'];
				$option1=$this->request->data['Question_Answer']['option1_audio'];
				$option2=$this->request->data['Question_Answer']['option2_audio'];
				$option3=$this->request->data['Question_Answer']['option3_audio'];

				if($question_array['Question']['answer']!=$select)
				{
					if($select==$question_array['Question']['option1'])
					{
						$audio_file=$question_array['Question']['option1_audio'];
					}
					else if($select==$question_array['Question']['option2'])
					{
						$audio_file=$question_array['Question']['option2_audio'];
					}
					else if($select==$question_array['Question']['option3'])
					{
						$audio_file=$question_array['Question']['option3_audio'];
					}
					$audio_file_to_change=$question_array['Question']['answer_audio'];
					$audio_file_to_change1=substr($question_array['Question']['answer_audio'],0,-4).'.mp3';// to change extension in mp3
					$audio_file1=substr($audio_file,0,-4).'.mp3';
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$audio_file,WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$audio_file_to_change);
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$audio_file1,WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$audio_file_to_change1);//copy mp3 file
				}

				//Uploading audio files
				$question_temp=$question['tmp_name'];
				$option1_temp=$option1['tmp_name'];
				$option2_temp=$option2['tmp_name'];
				$option3_temp=$option3['tmp_name'];
			

				$this->Question->id=$this->data['Question_Answer']['h1'];
				$this->Question->save(array('answer'=>$select));
			
				if (!file_exists(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name, 0777, true);
				}
				

				if(!empty($question_temp))
	   			{
	   				$filename1=substr($question_array['Question']['question_audio'],0,-4).'.mp3';
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['question_audio']);
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1);
	   				//upload wav file
	   				$path=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['question_audio'];
					$cmd="ffmpeg -i $question_temp -b:v 64k -bufsize 64k $path";
					shell_exec($cmd);
					//upload mp3 file
					$path1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1;
					$cmd1="ffmpeg -i $question_temp -acodec libmp3lame $path1";
					shell_exec($cmd1); 
	   			}
	   			if(!empty($option1_temp))
	   			{
	   				$filename1=substr($question_array['Question']['option1_audio'],0,-4).'.mp3';
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option1_audio']);
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1);
	   				//upload wav file
	   				$path=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option1_audio'];
					$cmd="ffmpeg -i $option1_temp -b:v 64k -bufsize 64k $path";
					shell_exec($cmd); 
					//upload mp3 file
					$path1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1;
					$cmd1="ffmpeg -i $option1_temp -acodec libmp3lame $path1";
					shell_exec($cmd1);
	   			}
	   			if(!empty($option2_temp))
	   			{
	   				$filename1=substr($question_array['Question']['option2_audio'],0,-4).'.mp3';
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option2_audio']);
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1);
	   				//upload wav file
	   				$path=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option2_audio'];
					$cmd="ffmpeg -i $option2_temp -b:v 64k -bufsize 64k $path";
					shell_exec($cmd); 
					//upload mp3 file
					$path1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1;
					$cmd1="ffmpeg -i $option2_temp -acodec libmp3lame $path1";
					shell_exec($cmd1);
	   			}
	   			if(!empty($option3_temp))
	   			{
	   				$filename1=substr($question_array['Question']['option3_audio'],0,-4).'.mp3';
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option3_audio']);
	   				unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1);
	   				//upload wav file
	   				$path=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option3_audio'];
					$cmd="ffmpeg -i $option3_temp -b:v 64k -bufsize 64k $path";
					shell_exec($cmd); 
					//upload mp3 file
					$path1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1;
					$cmd1="ffmpeg -i $option3_temp -acodec libmp3lame $path1";
					shell_exec($cmd1);
	   			}

					

				// move_uploaded_file($question_temp, WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['question_audio']);
				// move_uploaded_file($option1_temp, WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option1_audio']);
				// move_uploaded_file($option2_temp, WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option2_audio']);
				// move_uploaded_file($option3_temp, WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option3_audio']);
				

				if($question_array['Question']['answer']==$question_array['Question']['option1'])
				{
					$filename1=substr($question_array['Question']['option1_audio'],0,-4).'.mp3';
					$filename2=substr($question_array['Question']['answer_audio'],0,-4).'.mp3';
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option1_audio'],WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['answer_audio']);
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1,WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename2);
				}
				elseif($question_array['Question']['answer']==$question_array['Question']['option2'])
				{
					$filename1=substr($question_array['Question']['option2_audio'],0,-4).'.mp3';
					$filename2=substr($question_array['Question']['answer_audio'],0,-4).'.mp3';
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option2_audio'],WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['answer_audio']);
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1,WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename2);
				}
				else if($question_array['Question']['answer']==$question_array['Question']['option3'])
				{
					$filename1=substr($question_array['Question']['option3_audio'],0,-4).'.mp3';
					$filename2=substr($question_array['Question']['answer_audio'],0,-4).'.mp3';
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option3_audio'],WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['answer_audio']);
					copy(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename1,WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$filename2);
				}
				$this->Flash->success(__('Audio Successfully Uploaded.'));
				return $this->redirect(array('action' => 'manage_question/'.$id.'/'.$story_id.'/'.$language_name));
			}
			//$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id,'language_id'=>$language_id)));
			$this->paginate = array('conditions'=>array('story_id'=>$story_id,'language_id'=>$language_id),
		    'limit' => 10
		    );
		    $questions = $this->paginate('Question');

			}

		$this->set(compact('language_array','stories_array','story_id','language_id','questions','id','story_name','language_name','selected_language','selected_story','story_path1','story_path'));

	}



/**
 * Resize Question Images method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function resize_question_images($story_id=null,$id=null) {
		$this->loadModel('Story');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$story=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$story['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);

		if (!file_exists(WWW_ROOT . 'uploads/Questions/'.$story_path.'/ResizedQuestions_Image/')) {
			mkdir(WWW_ROOT . 'uploads/Questions/'.$story_path.'/ResizedQuestions_Image/', 0777, true);
			chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/ResizedQuestions_Image/',0777);
		}

		$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id)));
		foreach($questions as $question)
		{
			$image_name=$question['Question']['image'];
			$temp_name=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Image/'.$image_name;
			$filename=$image_name;
			$target_filename =WWW_ROOT . 'uploads/Questions/'.$story_path.'/ResizedQuestions_Image/'.$filename;
	        $fn = $temp_name;
	        $size = getimagesize( $fn );
	        $width=750;
	        $height=506;
	        $src = imagecreatefromstring( file_get_contents( $fn ) );
	        $dst = imagecreatetruecolor( $width, $height );
	        $quality=90;
	        imagealphablending($dst, false);
			imagesavealpha($dst, true);  
			imagealphablending($src, true);
	        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	        imagedestroy( $src );
	        imagejpeg( $dst, $target_filename,$quality); // adjust format as needed
	        imagedestroy( $dst );
	        chmod(WWW_ROOT . 'uploads/Questions/'.$story_path.'/ResizedQuestions_Image/'.$filename,0777);
		}
		$this->Flash->success(__('The resized question images has been uploaded.'));
		return $this->redirect(array('action' => 'manage_question/'.$id.'/'.$story_id.'/English'));

	}




/**
 * Add Question method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_question($id=null,$story_id=null,$language_id=null) {
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('Apitoken');
		
		$api=$this->Apitoken->get_active_api();
		$lang=$this->Language->find('first',array('conditions'=>array('id'=>$language_id)));
		$language_name=$lang['Language']['language_name'];
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		if ($this->request->is(array('post', 'put'))) 
		{
			//get story,language name to get directories
			$stories=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
			$languages=$this->Language->find('first',array('conditions'=>array('id'=>$language_id)));
			$story_name=$stories['Story']['story_name'];
			$language_name=$languages['Language']['language_name'];
			$story_path=str_replace(' ', '_', $story_name);

			$cnt=rand(111,999);
			$image=$this->request->data['Question']['image'];
			$question=$this->request->data['Question']['question_audio'];
			$answer=$this->request->data['Question']['answer_audio'];
			$option_1=$this->request->data['Question']['option1'];
			$option_2=$this->request->data['Question']['option2'];
			$option_3=$this->request->data['Question']['option3'];
			$answer1=$this->request->data['Question']['answer'];
			$option1=$this->request->data['Question']['option1_audio'];
			$option2=$this->request->data['Question']['option2_audio'];
			$option3=$this->request->data['Question']['option3_audio'];

			//naming of audio files for wave
			$question_name='QuestionAudio'.$cnt.'.wav';
			$answer_name='AnswerAudio'.$cnt.'.wav';
			$option1_name='Option1Audio'.$cnt.'.wav';
			$option2_name='Option2Audio'.$cnt.'.wav';
			$option3_name='Option3Audio'.$cnt.'.wav';

			//naming of audio files for mp3
			$question_name1='QuestionAudio'.$cnt.'.mp3';
			$answer_name1='AnswerAudio'.$cnt.'.mp3';
			$option1_name1='Option1Audio'.$cnt.'.mp3';
			$option2_name1='Option2Audio'.$cnt.'.mp3';
			$option3_name1='Option3Audio'.$cnt.'.mp3';

			//Uploading audio files
			$image_temp=$image['tmp_name'];
			$question_temp=$question['tmp_name'];
			$answer_temp=$answer['tmp_name'];
			$option1_temp=$option1['tmp_name'];
			$option2_temp=$option2['tmp_name'];
			$option3_temp=$option3['tmp_name'];
			$image_name='Question'.$cnt.'_Image.jpg';

			if((!empty($option_1) && empty($option_2) && empty($option_3)) || (!empty($option_2) && empty($option_1) && empty($option_3)) || (!empty($option_3) && empty($option_1) && empty($option_2)))
			{
				$this->Flash->error(__('You have to fill all options for question'));
			}
			else if((!empty($answer1)) && (!empty($option_1) && !empty($option_2) && !empty($option_3)) && ($option_1!=$answer1 && $option_2!=$answer1 && $option_3!=$answer1)){
				$this->Flash->error(__('Atleast one option must match with mode answer for question'));
			}
			else if((!empty($option_1) && !empty($option_2) && !empty($option_3)) && ($option_1==$option_2 || $option_1==$option_3 || $option_2==$option_3)){
				$this->Flash->error(__('All options must be different'));
			}
			else
			{

				if (!file_exists(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name, 0777, true);
				}
				if (!file_exists(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Image/')) {
	 				mkdir(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Image/', 0777, true);
				}
				$path1=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_name;
				$cmd1="ffmpeg -i $question_temp -b:v 64k -bufsize 64k $path1";
				shell_exec($cmd1);
				$path2=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$answer_name;
				$cmd2="ffmpeg -i $answer_temp -b:v 64k -bufsize 64k $path2";
				shell_exec($cmd2);
				$path3=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$option1_name;
				$cmd3="ffmpeg -i $option1_temp -b:v 64k -bufsize 64k $path3";
				shell_exec($cmd3);
				$path4=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$option2_name;
				$cmd4="ffmpeg -i $option2_temp -b:v 64k -bufsize 64k $path4";
				shell_exec($cmd4);
				$path5=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$option3_name;
				$cmd5="ffmpeg -i $option3_temp -b:v 64k -bufsize 64k $path5";
				shell_exec($cmd5);


			 	$path11=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_name1;
				$cmd11="ffmpeg -i $question_temp -acodec libmp3lame $path11";
				shell_exec($cmd11);
				$path22=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$answer_name1;
				$cmd22="ffmpeg -i $answer_temp -acodec libmp3lame $path22";
				shell_exec($cmd22);
				$path33=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$option1_name1;
				$cmd33="ffmpeg -i $option1_temp -acodec libmp3lame $path33";
				shell_exec($cmd33);
				$path44=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$option2_name1;
				$cmd44="ffmpeg -i $option2_temp -acodec libmp3lame $path44";
				shell_exec($cmd44);
				$path55=WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$option3_name1;
				$cmd55="ffmpeg -i $option3_temp -acodec libmp3lame $path55";
				shell_exec($cmd55);

				$this->request->data['Question']['story_id']=$story_id;
				$this->request->data['Question']['language_id']=$language_id;
				$this->request->data['Question']['question_audio']=$question_name;
				$this->request->data['Question']['answer_audio']=$answer_name;
				$this->request->data['Question']['option1_audio']=$option1_name;
				$this->request->data['Question']['option2_audio']=$option2_name;
				$this->request->data['Question']['option3_audio']=$option3_name;
				$this->request->data['Question']['image']=$image_name;

				//add word for all languages of story
				$story_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
				foreach($story_languages as $story_lang)
				{
					$this->Question->create();
					$lang_id=$story_lang['Story']['language_id'];
					$st_id=$story_lang['Story']['id'];
					$this->Word->create();
					if($lang_id==$english_id)
					{
						$this->request->data['Question']['language_id']=$lang_id;
						$this->request->data['Question']['story_id']=$st_id;
						$this->Question->save($this->request->data);
						$story=$this->Question->getLastInsertId();

						//get project id from poeditor
						$project_array1=array('api_token' => $api,'action'=>'list_projects');
						$HttpSocket = new HttpSocket();
						$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
						$list_projects1 = json_decode($list_project1, true);
						foreach($list_projects1['list'] as $project)
						{
							if($project['name']==$this->Session->read('project_name')){
								$projectid=$project['id'];
							}
						}

						//add word in poeditor
			    		$arr1=array("term"=>$this->request->data['Question']['question'],"context"=>"Question".$cnt);
			    		$arr2=array("term"=>$this->request->data['Question']['answer'],"context"=>"Answer".$cnt);
			    		$arr3=array("term"=>$this->request->data['Question']['option1'],"context"=>"Option 1(".$cnt.")");
			    		$arr4=array("term"=>$this->request->data['Question']['option2'],"context"=>"Option 2(".$cnt.")");
			    		$arr5=array("term"=>$this->request->data['Question']['option3'],"context"=>"Option 3(".$cnt.")");

					    $data1= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr1),true));
					    $data2= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr2),true));
					    $data3= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr3),true));
					    $data4= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr4),true));
					    $data5= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr5),true));
				    	$request=array();
				    	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data1,$request);
				    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
				    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data3,$request);
				    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data4,$request);
				    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data5,$request);
					}
					else
					{
						$this->request->data['Question']['story']=$story;
						$this->request->data['Question']['language_id']=$lang_id;
						$this->request->data['Question']['story_id']=$st_id;
						$this->request->data['Question']['question']='No Translation';
						$this->request->data['Question']['answer']='No Translation';
						$this->request->data['Question']['option1']='No Translation';
						$this->request->data['Question']['option2']='No Translation';
						$this->request->data['Question']['option3']='No Translation';
						$this->Question->save($this->request->data);
					}
				}


				if($this->Question->save($this->request->data))
				{
					$this->Flash->success(__('Question successfully saved.'));
					return $this->redirect(array('action' => 'add_question',$id,$story_id,$language_id));
				}
			}
		}
		$this->set(compact('id','story_id','language_name'));
	}




/**
 * Edit Question method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_question($id=null,$mode_id=null,$story_id=null,$language_name=null) {
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('Apitoken');
		
		$api=$this->Apitoken->get_active_api();
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$stories=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$stories['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);
		$question_array=$this->Question->find('first',array('conditions'=>array('id'=>$id)));
		$image_name=$question_array['Question']['image'];
		$this->set(compact('question_array'));
		$cnt=substr($question_array['Question']['question_audio'], -7,-4);

		if ($this->request->is(array('post', 'put'))) 
		{
			$image=$this->request->data['Question']['image1'];
			$option_1=$this->request->data['Question']['option1'];
			$option_2=$this->request->data['Question']['option2'];
			$option_3=$this->request->data['Question']['option3'];
			$answer1=$this->request->data['Question']['answer'];
			$image_temp=$image['tmp_name'];

			if((!empty($option_1) && empty($option_2) && empty($option_3)) || (!empty($option_2) && empty($option_1) && empty($option_3)) || (!empty($option_3) && empty($option_1) && empty($option_2)))
			{
				$this->Flash->error(__('You have to fill all options for question'));
			}
			else if((!empty($answer1)) && (!empty($option_1) && !empty($option_2) && !empty($option_3)) && ($option_1!=$answer1 && $option_2!=$answer1 && $option_3!=$answer1)){
				$this->Flash->error(__('Atleast one option must match with mode answer for question'));
			}
			else if((!empty($option_1) && !empty($option_2) && !empty($option_3)) && ($option_1==$option_2 || $option_1==$option_3 || $option_2==$option_3)){
				$this->Flash->error(__('All options must be different'));
			}
			else
			{
				if (!file_exists(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Image/')) {
	 				mkdir(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Image/', 0777, true);
				}
				if(!empty($image_temp))
				{
					move_uploaded_file($image_temp, WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Image/'.$image_name);
				}
				//add word for all languages of story
				$story_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
				foreach($story_languages as $story_lang)
				{
					$lang_id=$story_lang['Story']['language_id'];
					$st_id=$story_lang['Story']['id'];
					$question_array1=$this->Question->find('first',array('conditions'=>array('id'=>$id)));
					$question_array2=$this->Question->find('first',array('conditions'=>array('story'=>$id)));
					
					if($lang_id==$english_id)
					{
						$this->Question->id=$question_array1['Question']['id'];
						$this->request->data['Question']['language_id']=$lang_id;
						$this->request->data['Question']['story_id']=$st_id;
						$this->Question->save($this->request->data);

						//get project id from poeditor
						$project_array1=array('api_token' => $api,'action'=>'list_projects');
						$HttpSocket = new HttpSocket();
						$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
						$list_projects1 = json_decode($list_project1, true);
						foreach($list_projects1['list'] as $project)
						{
							if($project['name']==$this->Session->read('project_name')){
								$projectid=$project['id'];
							}
						}


						//delete old questions from poeditor
			    		$arr1=array("term"=>$question_array['Question']['question'],"context"=>"Question".$cnt);
			    		$arr2=array("term"=>$question_array['Question']['answer'],"context"=>"Answer".$cnt);
			    		$arr3=array("term"=>$question_array['Question']['option1'],"context"=>"Option 1(".$cnt.")");
			    		$arr4=array("term"=>$question_array['Question']['option2'],"context"=>"Option 2(".$cnt.")");
			    		$arr5=array("term"=>$question_array['Question']['option3'],"context"=>"Option 3(".$cnt.")");

					    $data1= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr1),true));
					    $data2= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr2),true));
					    $data3= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr3),true));
					    $data4= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr4),true));
					    $data5= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr5),true));
					   	
					   	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data1,$request);
				    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
				    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data3,$request);
				    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data4,$request);
				    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data5,$request);

					    //Add new questions in poeditor
			    		//add word in poeditor
			    		$arr11=array("term"=>$this->request->data['Question']['question'],"context"=>"Question".$cnt);
			    		$arr22=array("term"=>$this->request->data['Question']['answer'],"context"=>"Answer".$cnt);
			    		$arr33=array("term"=>$this->request->data['Question']['option1'],"context"=>"Option 1(".$cnt.")");
			    		$arr44=array("term"=>$this->request->data['Question']['option2'],"context"=>"Option 2(".$cnt.")");
			    		$arr55=array("term"=>$this->request->data['Question']['option3'],"context"=>"Option 3(".$cnt.")");
	    			   
	    			    $data11= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr11),true));
					    $data22= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr22),true));
					    $data33= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr33),true));
					    $data44= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr44),true));
					    $data55= array("api_token" => $api,"action" => "add_terms","id" => $projectid,"data" => json_encode(array($arr55),true));
				    	$request=array();
		
				    	$response11 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);
				    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data22,$request);
				    	$response33 = $HttpSocket->post('https://poeditor.com/api/', $data33,$request);
				    	$response44 = $HttpSocket->post('https://poeditor.com/api/', $data44,$request);
				    	$response55 = $HttpSocket->post('https://poeditor.com/api/', $data55,$request);
					}
					else
					{
						$this->Question->id=$question_array2['Question']['id'];
						$this->request->data['Question']['language_id']=$lang_id;
						$this->request->data['Question']['story_id']=$st_id;
						$this->request->data['Question']['question']='No Translation';
						$this->request->data['Question']['answer']='No Translation';
						$this->request->data['Question']['option1']='No Translation';
						$this->request->data['Question']['option2']='No Translation';
						$this->request->data['Question']['option3']='No Translation';
						$this->Question->save($this->request->data);
					}
				}
				
			
				$this->Flash->success(__('Question successfully updated.'));
				return $this->redirect(array('action' => 'edit_question',$id,$mode_id,$story_id,$language_name));
			
			}
		}
		$this->set(compact('id','story_id','language_name','story_path','image_name'));
	}




/**
 * Delete Question method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_question($id=null,$mode_id=null,$story_id=null,$language_name=null) 
	{
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('Apitoken');
		$api=$this->Apitoken->get_active_api();

		$stories=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$stories['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$question_array=$this->Question->find('first',array('conditions'=>array('id'=>$id)));
		$cnt=substr($question_array['Question']['question_audio'], -7,-4);

		//get project id from poeditor
		$project_array1=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
		$list_projects1 = json_decode($list_project1, true);
		foreach($list_projects1['list'] as $project)
		{
			if($project['name']==$this->Session->read('project_name')){
				$projectid=$project['id'];
			}
		}

		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['question_audio']);
		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option1_audio']);
		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option2_audio']);
		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['option3_audio']);
		unlink(WWW_ROOT . 'uploads/Questions/'.$story_path.'/Questions_Audio/'.$language_name.'/'.$question_array['Question']['answer_audio']);
		//delete old questions from poeditor
		$arr1=array("term"=>$question_array['Question']['question'],"context"=>"Question".$cnt);
		$arr2=array("term"=>$question_array['Question']['answer'],"context"=>"Answer".$cnt);
		$arr3=array("term"=>$question_array['Question']['option1'],"context"=>"Option 1(".$cnt.")");
		$arr4=array("term"=>$question_array['Question']['option2'],"context"=>"Option 2(".$cnt.")");
		$arr5=array("term"=>$question_array['Question']['option3'],"context"=>"Option 3(".$cnt.")");

	    $data1= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr1),true));
	    $data2= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr2),true));
	    $data3= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr3),true));
	    $data4= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr4),true));
	    $data5= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr5),true));
	   	
	   	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data1,$request);
    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data3,$request);
    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data4,$request);
    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data5,$request);

		$story_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
		foreach($story_languages as $story_lang)
		{
			$lang_id=$story_lang['Story']['language_id'];
			$st_id=$story_lang['Story']['id'];
			if($english_id==$lang_id)
			{
				$questions=$this->Question->find('first',array('conditions'=>array('id'=>$id)));
			}
			else
			{
				$questions=$this->Question->find('first',array('conditions'=>array('story'=>$id,'story_id'=>$st_id)));
			}
			$this->Question->id=$questions['Question']['id'];
			$this->Question->delete();
		}
		$this->Flash->success(__('Question successfully removed.'));
		return $this->redirect(array('action' => 'manage_question',$mode_id,$story_id,$language_name));

	}



/**
 * Manage_Math method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_math($id = null,$story_name=null,$language_name=null) 
	{
		$this->loadModel('GeneralSettings');
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Scene');
		
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];

		$first_story=$this->Story->find('first',array('conditions'=>array('id'=>$story_name)));
		$story_id=$first_story['Story']['id'];
		$story_name=$first_story['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);

		$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$story_id)));

		$stories= $this->Story->find('all',array('conditions'=>array('language_id'=>$english_id)));
		$story_array=array();
		foreach($stories as $story)
		{
			array_push($story_array,ucwords(strtolower($story['Story']['story_name'])));
		}
		$length=sizeof($story_array);
		
		for($i=0;$i<$length;$i++)
		{
			if($story_array[$i]==ucwords(strtolower($story_name)))
			{
				$selected_story=$i;
			}
		}
		if ($this->request->is(array('post', 'put'))) 
		{
			$selected_story=$this->data['Story']['story'];
			for($i=0;$i<$length;$i++)
			{
				if($selected_story==$i)
				{
					$find_story= $this->Story->find('first',array('conditions'=>array('story_name'=>$story_array[$i],'language_id'=>$english_id)));
					$story_name=$find_story['Story']['story_name'];
					$story_path=str_replace(' ', '_', $story_name);
					$story_id=$find_story['Story']['id'];
					$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$story_id)));
					return $this->redirect(array('action' => 'manage_math',$id,$story_id,$language_name));
				}
			}
		}

		$this->set(compact('id','story_array','selected_story','story_id','scenes','story_name','story_path','language_name'));	
	}


/**
 * Resize Scenes method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function resize_scenes($story_id = null,$id=null) 
	{
		$this->loadModel('Story');
		$this->loadModel('Scene');
		$story=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$story['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);
		$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$story_id)));
		if (!file_exists(WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/ResizedScenes')) {
			mkdir(WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/ResizedScenes', 0777, true);
			chmod(WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/ResizedScenes',0777);
	    }


		foreach($scenes as $scene)
		{
			$image_name=$scene['Scene']['image'];
			$temp_name=WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/Scenes/'.$image_name;
			$filename=$image_name;
			$target_filename =WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/ResizedScenes/'.$filename;
	        $fn = $temp_name;
	        $size = getimagesize( $fn );
	        $width=1180;
	        $height=795;
	        $src = imagecreatefromstring( file_get_contents( $fn ) );
	        $dst = imagecreatetruecolor( $width, $height );
	        $quality=90;
	        imagealphablending($dst, false);
			imagesavealpha($dst, true);  
			imagealphablending($src, true);
	        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	        imagedestroy( $src );
	        imagejpeg( $dst, $target_filename,$quality); // adjust format as needed
	        imagedestroy( $dst );
	        chmod(WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/ResizedScenes/'.$filename,0777);
		}
		$this->Flash->success(__('The resized scenes has been uploaded.'));
		return $this->redirect(array('action' => 'manage_math/'.$id.'/'.$story_id.'/English'));
	}

/**
 * Add Scenes method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_scene($story_id = null,$id=null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('Story');
		if ($this->request->is(array('post', 'put'))) 
		{
			$images=$this->data['Images']['image'];
			foreach($images as $image)
			{
				$image_name=$image['name'];
				$temp_name=$image['tmp_name'];
				$count=rand(111,999);
				$stories= $this->Story->find('first',array('conditions'=>array('Story.id'=>$story_id)));
				$story_name=$stories['Story']['story_name'];
				$story_path=str_replace(' ', '_', $story_name);
				$filename=$story_name."_".$count.".jpg";
				$this->Scene->create();
				$this->Scene->save(array('story_id'=>$story_id,'image'=>$filename));
				if (!file_exists(WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/Scenes/')) {
     				mkdir(WWW_ROOT.'uploads/Maths_Images/'.$story_path.'/Scenes/', 0777, true);
			    }
				move_uploaded_file($temp_name, WWW_ROOT . 'uploads/Maths_Images/'.$story_path.'/Scenes/'.$filename);
				$this->Flash->success(__('Scene successfully added.'));
				return $this->redirect(array('action' => 'manage_math',$id,$story_id,English));
			}
		}
		$this->set(compact('id','story_id'));
	}


/**
 * Manage Scenes method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_scene($mode_id=null,$id = null,$story_id=null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('Story');
		$this->loadModel('Language');
		$this->loadModel('MathObject');
		$this->loadModel('Variant');
		$this->loadModel('Grids');

		$load_scene=$this->Scene->find('first',array('conditions'=>array('Scene.id'=>$id)));
		$scene_name=$load_scene['Scene']['image'];

	
		$base=$this->base;
		$find_object=$this->MathObject->find('first');
		$variants=$this->Variant->find('all',array('conditions'=>array('object_id'=>$find_object['MathObject']['id'])));
		$object_array=array();
		$objects=$this->MathObject->find('all');
		foreach($objects as $object)
		{
			array_push($object_array, $object['MathObject']['title']);
		}
		$length2=sizeof($object_array);

		$story=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
		$story_name=$story['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);

		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$stories= $this->Story->find('all',array('conditions'=>array('language_id'=>$english_id)));
		$story_array=array();
		foreach($stories as $story)
		{
			array_push($story_array,ucwords(strtolower($story['Story']['story_name'])));
		}
		$length=sizeof($story_array);

		for($i=0;$i<$length;$i++)
		{
			if(ucwords(strtolower($story_name))==$story_array[$i])
			{
				$selected_story=$i;
			}
		}

		$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$story_id)));
		$scene_array=array();
		foreach($scenes as $scene)
		{
			array_push($scene_array,$scene['Scene']['image']);
		}
		$length1=sizeof($scene_array);
		for($i=0;$i<$length1;$i++)
		{
			if($scene_name==$scene_array[$i])
			{
				$selected_scene=$i;
			}
		}

		$find_scene=$this->Scene->find('first',array('conditions'=>array('story_id'=>$story_id,'image'=>$scene_name)));
		$find_grid=$this->Grids->find('first',array('conditions'=>array('scene_id'=>$find_scene['Scene']['id'])));

		if ($this->request->is(array('post', 'put'))) 
		{
			$story=$this->data['Story']['story'];
			for($i=0;$i<$length;$i++)
			{
				if($story==$i)
				{
					$selected_story=$i;
					$story_name=$story_array[$i];
				}
			}
			$story_by_name=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name)));
			$story_id=$story_by_name['Story']['id'];
			$story_path=str_replace(' ', '_', $story_name);
			
			$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$story_by_name['Story']['id'])));
			$scene_array=array();
			foreach($scenes as $scene)
			{
				array_push($scene_array,$scene['Scene']['image']);
			}
			$length1=sizeof($scene_array);

			$scene=$this->data['Story']['scene'];
			for($i=0;$i<$length;$i++)
			{
				if($scene==$i)
				{
					$selected_scene=$i;
					$scene_name=$scene_array[$i];
				}
			}

			$object=$this->data['Story']['object'];
			for($i=0;$i<$length2;$i++)
			{
				if($object==$i)
				{
					$selected_object=$i;
				}
			}
			$find_object=$this->MathObject->find('first',array('conditions'=>array('title'=>$object_array[$selected_object])));
			$find_scene=$this->Scene->find('first',array('conditions'=>array('story_id'=>$story_id,'image'=>$scene_name)));
			$variants=$this->Variant->find('all',array('conditions'=>array('object_id'=>$find_object['MathObject']['id'])));
			$find_grid=$this->Grids->find('first',array('conditions'=>array('scene_id'=>$find_scene['Scene']['id'])));
			$row=$this->data['Story']['row'];
			$column=$this->data['Story']['column'];
			
		}

		if( $this->request->is('ajax') ) {
	    $variant_array=array();
	    $value =$this->request->data('obj');
	    $value2 =$this->request->data('obj2');
	    $scene_id1 =$this->request->data('scene');
	    $x =$this->request->data('x');
	    $y =$this->request->data('y');
        $row =$this->request->data('row');
	    $col =$this->request->data('col');
	    $image1 =$this->request->data('image');
	    $src =$this->request->data('src');
	    $src=basename($src);

	    if(!empty($x))
	    {
	    	$find_grid=$this->Grids->find('first',array('conditions'=>array('scene_id'=>$scene_id1,'x'=>$x,'y'=>$y)));
	    	$scene_id2=$find_grid['Grids']['id'];
	    	if(empty($find_grid))
	    	{
		    	$this->Grids->create();
		    	if(empty($src))
			    {
			    $this->Grids->save(array('scene_id'=>$scene_id1,'x'=>$x,'y'=>$y,'image'=>$image1,'row'=>$row,'col'=>$col));
			    }
			    else
			    {
		    	$this->Grids->save(array('scene_id'=>$scene_id1,'x'=>$x,'y'=>$y,'image'=>$image1,'row'=>$row,'col'=>$col,'src'=>$src));
		    	}
		    }
		    else
		    {
				$this->Grids->id=$scene_id2;
		    	$this->Grids->save(array('image'=>$image1,'src'=>$src));
		    }
	    }
	    if(!empty($value))
	    {
		    $object1=$this->MathObject->find('first',array('conditions'=>array('title'=>$value)));
		    $object_image=$object1['MathObject']['image'];
		    $object_variant=$this->Variant->find('all',array('conditions'=>array('object_id'=>$object1['MathObject']['id'])));
		     $this->autoRender = false;
		      $this->layout=null;
		    foreach($object_variant as $ob)
		    {
		     	array_push($variant_array, $ob['Variant']['image']);
		    }
		    echo json_encode($object_variant);exit;
		}
		if(!empty($value2))
		{
			$object1=$this->MathObject->find('first',array('conditions'=>array('title'=>$value2)));
		    $object_image=$object1['MathObject']['image'];
		    $object_variant=$this->Variant->find('all',array('conditions'=>array('object_id'=>$object1['MathObject']['id'])));
		    $this->autoRender = false;
		    $this->layout=null;
		    echo json_encode($object_image);exit;
		}
		
	    }
	    $grids=$this->Grids->find('all',array('conditions'=>array('scene_id'=>$find_scene['Scene']['id'])));
		if(!empty($this->data['Story']['row']))
		{
			$this->set(compact('column','row'));
		}
		$this->set(compact('mode_id','find_grid','grids','base','objects','scene','story_array','selected_story','scene_array','find_scene','story_path','selected_scene','object_array','selected_object','id','story_id','find_object','variants'));
		
	}




/**
 * Add Object method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_objects($mode_id=null,$story_id=null,$language_name=null) 
	{
		$this->loadModel('MathObject');
		$this->loadModel('Language');
		$languages= $this->Language->find('all');
		$language_array=array();

		// $objects=$this->MathObject->find('all');
		// foreach($objects as $object)
		// {
			
		// 	$audio_clip=$object['MathObject']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 	unlink(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English/'.$audio_file);
		// 	unlink(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/Hindi/'.$audio_file);
		// 	$path=WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English/'.$audio_clip;
		// 	$number_audio=WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English/'.$audio_file;
		// 	$cmd="ffmpeg -i $path -acodec libmp3lame $number_audio";
		// 	shell_exec($cmd);
			
		
		// 	$path2=WWW_ROOT.'uploads/Maths_Images/Objects_Audio/Hindi/'.$audio_clip;
		// 	$number_audio2=WWW_ROOT.'uploads/Maths_Images/Objects_Audio/Hindi/'.$audio_file;
		// 	$cmd2="ffmpeg -i $path2 -acodec libmp3lame $number_audio2";
		// 	shell_exec($cmd2);

		// 	chmod(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English/'.$audio_file,0777);
		// 	chmod(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/Hindi/'.$audio_file,0777);
			
		// }
		// exit;

		// $objects=$this->MathObject->find('all');
		// foreach($objects as $object)
		// {
			
		// 	$audio_clip=$object['MathObject']['audio_clip'];
		// 	$audio_file=str_replace(' ', '_', $audio_clip);
		// 	if($audio_clip!=$audio_file)
		// 	{
		// 		$object_id=$object['MathObject']['id'];
		// 		$this->MathObject->id=$object_id;
		// 		$this->MathObject->save(array('audio_clip'=>$audio_file));
		// 		rename(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English/'.$audio_clip,WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English/'.$audio_file);
		// 		rename(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/Hindi/'.$audio_clip,WWW_ROOT.'uploads/Maths_Images/Objects_Audio/Hindi/'.$audio_file);
		// 	}

			
		// }
		// exit;

		foreach($languages as $language)
		{
			array_push($language_array,$language['Language']['language_name']);
		}	
		$length=sizeof($language_array);

		for($i=0;$i<$length;$i++)
		{
			if($language_name==$language_array[$i])
			{
				$select=$i;
				$selected_language=$language_array[$i];
			}
		}

		if ($this->request->is(array('post', 'put'))) 
		{

			$filename=$this->request->data['Object_Audio']['hidden_audio'];
			$filename1=substr($filename,0,-4).'.mp3';
			$tmp_name=$this->request->data['Object_Audio']['audio_clip']['tmp_name'];
			if(!empty($tmp_name))
			{
				if (!file_exists(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/'.$language_name)) {
					mkdir(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/'.$language_name, 0777, true);
					chmod(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/'.$language_name,0777);
		    	}

		    	$path=WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/'.$language_name.'/'.$filename;
				$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
				shell_exec($cmd);  

			 	$path1=WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/'.$language_name.'/'.$filename1;
				$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
				shell_exec($cmd1);

		    	//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/'.$language_name.'/'.$filename);
				chmod(WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/'.$language_name.'/'.$filename,0777);
				chmod(WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/'.$language_name.'/'.$filename1,0777);
				$this->Flash->success(__('Object Audio Successfully added.'));
				return $this->redirect(array('action' => 'manage_objects',$mode_id,$story_id,$selected_language));
			}
			else
			{
				$selected_language=$this->data['ObjectForm']['language'];
				for($i=0;$i<$length;$i++)
				{
					if($selected_language==$i)
					{
						$selected_language=$language_array[$i];
					}
				}
				return $this->redirect(array('action' => 'manage_objects',$mode_id,$story_id,$selected_language));
			}
		}


		$objects=$this->MathObject->find('all');
		$this->set(compact('story_id','mode_id','objects','language_array','selected_language','select'));
	}



/**
 * Add Object method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_object($story_id = null,$mode_id=null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('MathObject');

		if ($this->request->is(array('post', 'put'))) 
		{
			$this->MathObject->create();
			$title=$this->data['Objects']['title'];
			$audio=$this->data['Objects']['audio_clip'];
			$temp_name1=$audio['tmp_name'];
			$temp_name=$this->data['Objects']['image']['tmp_name'];
			$filename=$title.".png";
			$filename1=str_replace(' ', '_', $title).".wav";
			

			$this->MathObject->save(array('title'=>$title,'image'=>$filename,'audio_clip'=>$filename1));
			if (!file_exists(WWW_ROOT.'uploads/Maths_Images/Objects/')) {
 				mkdir(WWW_ROOT.'uploads/Maths_Images/Objects/', 0777, true);
		    }
		    if (!file_exists(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English')) {
 				mkdir(WWW_ROOT.'uploads/Maths_Images/Objects_Audio/English', 0777, true);
		    }
			move_uploaded_file($temp_name, WWW_ROOT . 'uploads/Maths_Images/Objects/'.$filename);

			$path=WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/English/'.$filename1;
			$cmd="ffmpeg -i $temp_name1 -b:v 64k -bufsize 64k $path";
			shell_exec($cmd);  
			//move_uploaded_file($temp_name1, WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/English/'.$filename1);
			chmod(WWW_ROOT . 'uploads/Maths_Images/Objects_Audio/English/'.$filename1,0777);
			$this->Flash->success(__('Object successfully added.'));
			return $this->redirect(array('action' => 'manage_objects',$mode_id,$story_id,English));
		}
		$this->set(compact('story_id','mode_id'));
	}



/**
 * Edit Object method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_object($object_id=null,$mode_id=null,$story_id = null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('MathObject');
		$object=$this->MathObject->find('first',array('conditions'=>array('id'=>$object_id)));
		$filename=$object['MathObject']['image'];
		if ($this->request->is(array('post', 'put'))) 
		{
			$title=$this->data['Objects']['title'];
			$this->MathObject->id=$object_id;
			$this->MathObject->save(array('title'=>$title));
			$temp_name=$this->data['Objects']['image']['tmp_name'];

			if (!file_exists(WWW_ROOT.'uploads/Maths_Images/Objects/')) {
 				mkdir(WWW_ROOT.'uploads/Maths_Images/Objects/', 0777, true);
		    }

			move_uploaded_file($temp_name, WWW_ROOT . 'uploads/Maths_Images/Objects/'.$filename);
			chmod(WWW_ROOT . 'uploads/Maths_Images/Objects/'.$filename,0777);
			$this->Flash->success(__('Object successfully changed.'));
			return $this->redirect(array('action' => 'edit_object',$object_id,$mode_id,$story_id));
		}
		$this->set(compact('object','mode_id','story_id'));
	}


/**
 * Add Variant method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_variant($math_id=null,$story_id = null,$mode_id=null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('MathObject');
		$this->loadModel('Variant');

		$this->paginate = array(
        'limit' => 10
	    );
	    $objects = $this->paginate('MathObject');

		$variants=$this->Variant->find('all');
		if ($this->request->is(array('post', 'put'))) 
		{
			$object_id=$this->data['Variants']['h2'];

			foreach($this->data['Variants']['image'] as $file)
			{
				$temp_name=$file['tmp_name'];
				$filename=$file['name'];
				$this->Variant->create();
				$this->Variant->save(array('image'=>$filename,'object_id'=>$object_id));
				if (!file_exists(WWW_ROOT.'uploads/Maths_Images/Variants/')) {
 					mkdir(WWW_ROOT.'uploads/Maths_Images/Variants/', 0777, true);
			    }

			 //    $target_filename =WWW_ROOT . 'uploads/Maths_Images/Variants/'.$filename;
	   //          $fn = $temp_name;
	   //          $size = getimagesize( $fn );
	   //          $width=130;
	   //          $height=130;
	   //          $src = imagecreatefromstring( file_get_contents( $fn ) );
	   //          $dst = imagecreatetruecolor( $width, $height );
	   //          imagealphablending($dst, false);
				// imagesavealpha($dst, true);  
				// imagealphablending($src, true);
	   //          imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	   //          imagedestroy( $src );
	   //          imagepng( $dst, $target_filename ); // adjust format as needed
	   //          imagedestroy( $dst );

				move_uploaded_file($temp_name, WWW_ROOT . 'uploads/Maths_Images/Variants/'.$filename);
			}	
			
			$this->Flash->success(__('Variant successfully added.'));
			return $this->redirect(array('action' => 'add_variant',$math_id,$story_id,$mode_id,English));
		}
		$this->set(compact('story_id','math_id','objects','variants','mode_id'));
		
	}


/**
 * Variants method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function variants($math_id=null,$story_id = null,$mode_id=null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('MathObject');
		$this->loadModel('Variant');
	
	    $all_variant = $this->Variant->find('all');

	    if ($this->request->is(array('post', 'put'))) 
		{
			$filename=$this->data['VariantImage']['hidden'];
			$temp_name=$this->data['VariantImage']['image']['tmp_name'];

			if (!file_exists(WWW_ROOT.'uploads/Maths_Images/Variants/')) {
				mkdir(WWW_ROOT.'uploads/Maths_Images/Variants/', 0777, true);
		    }

		 //    $target_filename =WWW_ROOT . 'uploads/Maths_Images/Variants/'.$filename;
   //          $fn = $temp_name;
   //          $size = getimagesize( $fn );
   //          $width=130;
   //          $height=130;
   //          $src = imagecreatefromstring( file_get_contents( $fn ) );
   //          $dst = imagecreatetruecolor( $width, $height );
   //          imagealphablending($dst, false);
			// imagesavealpha($dst, true);  
			// imagealphablending($src, true);
   //          imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
   //          imagedestroy( $src );
   //          imagepng( $dst, $target_filename ); // adjust format as needed
   //          imagedestroy( $dst );
		    move_uploaded_file($temp_name, WWW_ROOT . 'uploads/Maths_Images/Variants/'.$filename);
            $this->Flash->success(__('Variant successfully changed.'));
		}


		$this->set(compact('story_id','math_id','all_variant','mode_id'));
		
	}



/**
 * Delete Object method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_object($id=null,$math_id=null,$story_id=null) 
	{
		$this->loadModel('MathObject');
		$this->loadModel('Variant');
		$this->MathObject->id=$id;
		$object=$this->MathObject->find('first',array('conditions'=>array('id'=>$id)));
		$filename=$object['MathObject']['image'];
		unlink(WWW_ROOT . 'uploads/Maths_Images/Objects/'.$filename);
		$this->MathObject->delete();
		$variants=$this->Variant->find('all',array('conditions'=>array('object_id'=>$id)));
		foreach($variants as $variant)
		{
			$this->Variant->id=$variant['Variant']['id'];
			$this->Variant->delete();
		}
		return $this->redirect(array('action' => 'manage_objects',$math_id,$story_id,English));
	}	



/**
 * Resize Objects method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	
	public function resize_objects($id=null,$story_id=null) 
	{
		$this->loadModel('MathObject');
		$objects=$this->MathObject->find('all');
		if (!file_exists(WWW_ROOT . 'uploads/Maths_Images/ResizedObjects/')) {
			mkdir(WWW_ROOT . 'uploads/Maths_Images/ResizedObjects/', 0777, true);
			chmod(WWW_ROOT . 'uploads/Maths_Images/ResizedObjects',0777);
	    }
		foreach($objects as $object)
		{
			$image_name=$object['MathObject']['image'];
			$temp_name=WWW_ROOT . 'uploads/Maths_Images/Objects/'.$image_name;
			$filename=$image_name;
			$target_filename =WWW_ROOT . 'uploads/Maths_Images/ResizedObjects/'.$filename;
	        $fn = $temp_name;
	        $size = getimagesize( $fn );
	        $width=130;
	        $height=130;
	        $src = imagecreatefromstring( file_get_contents( $fn ) );
	        $dst = imagecreatetruecolor( $width, $height );
	        imagealphablending($dst, false);
			imagesavealpha($dst, true);  
			imagealphablending($src, true);
	        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	        imagedestroy( $src );
	        imagepng( $dst, $target_filename); // adjust format as needed
	        imagedestroy( $dst );
	        chmod(WWW_ROOT . 'uploads/Maths_Images/ResizedObjects/'.$filename,0777);
		}
		$this->Flash->success(__('The resized objects has been uploaded.'));
		return $this->redirect(array('action' => 'manage_objects/'.$id.'/'.$story_id.'/English'));

	}



/**
 * Resize Variants method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */	
	public function resize_variants($scene_id=null,$story_id=null,$mode_id=null) 
	{
		$this->loadModel('Variant');
		$variants=$this->Variant->find('all');
		if (!file_exists(WWW_ROOT . 'uploads/Maths_Images/ResizedVariants/')) {
			mkdir(WWW_ROOT . 'uploads/Maths_Images/ResizedVariants/', 0777, true);
			chmod(WWW_ROOT . 'uploads/Maths_Images/ResizedVariants/',0777);
	    }
		foreach($variants as $variant)
		{
			$image_name=$variant['Variant']['image'];
			$temp_name=WWW_ROOT . 'uploads/Maths_Images/Variants/'.$image_name;
			$filename=$image_name;
			$target_filename =WWW_ROOT . 'uploads/Maths_Images/ResizedVariants/'.$filename;
	        $fn = $temp_name;
	        $size = getimagesize( $fn );
	        $width=130;
	        $height=130;
	        $src = imagecreatefromstring( file_get_contents( $fn ) );
	        $dst = imagecreatetruecolor( $width, $height );
	        imagealphablending($dst, false);
			imagesavealpha($dst, true);  
			imagealphablending($src, true);
	        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	        imagedestroy( $src );
	        imagepng( $dst, $target_filename); // adjust format as needed
	        imagedestroy( $dst );
	        chmod(WWW_ROOT . 'uploads/Maths_Images/ResizedVariants/'.$filename,0777);
		}
		$this->Flash->success(__('The resized variants has been uploaded.'));
		return $this->redirect(array('action' => 'variants',$scene_id,$story_id,$mode_id));
	}





/**
 * Delete Variant method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_variant($id=null,$math_id=null,$story_id=null,$mode_id=null) 
	{
		$this->loadModel('Variant');
		$this->Variant->id=$id;
		$this->Variant->delete();
		return $this->redirect(array('action' => 'variants',$mode_id,$math_id,$story_id));
	
	}	

/**
 * Remove Scenes method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function remove_scene($id = null,$mode_id=null,$story_id=null,$language=null) 
	{
		$this->loadModel('Scene');
		$this->loadModel('Grids');
		$this->Scene->id=$id;
		$grids=$this->Grids->find('all',array('conditions'=>array('scene_id'=>$id)));
		foreach($grids as $grid)
		{
			$this->Grids->id=$grid['Grids']['id'];
			$this->Grids->delete();
		}
		$this->Scene->delete();
		$this->Flash->success(__('Scene successfully removed.'));
		return $this->redirect(array('action' => 'manage_math',$mode_id,$story_id,$language));
	}


/**
 * Delete Grid method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_grid($id=null,$math_id=null,$story_id=null,$mode_id=null) 
	{
		$this->loadModel('Grids');
		$grids=$this->Grids->find('all',array('conditions'=>array('scene_id'=>$id)));
		foreach($grids as $grid)
		{
			$this->Grids->id=$grid['Grids']['id'];
			$this->Grids->delete();
		}
		$this->Flash->success(__('Grid successfully removed.'));
		return $this->redirect(array('action' => 'manage_scene',$mode_id,$math_id,$story_id));
	}	


/**
 * List Numbers method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_numbers($id=null,$language_name=null) 
	{
		$this->loadModel('Language');
		$this->loadModel('Number');

		// $numbers = $this->Number->find('all');
		// foreach($numbers as $number)
		// {
		// 	if($number['Number']['language_id']==1)
		// 	{
		// 		$audio_clip=$number['Number']['audio_clip'];
		// 		$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 		$path=WWW_ROOT . 'uploads/Numbers_Audio/Hindi/'.$audio_clip;
		// 		$number_audio=WWW_ROOT . 'uploads/Numbers_Audio/Hindi/'.$audio_file;
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $number_audio";
		// 		shell_exec($cmd);
		// 		chmod(WWW_ROOT . 'uploads/Numbers_Audio/Hindi/'.$audio_file,0777);
		// 	}
		// 	if($number['Number']['language_id']==2)
		// 	{
		// 		$audio_clip=$number['Number']['audio_clip'];
		// 		$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 		$path=WWW_ROOT . 'uploads/Numbers_Audio/English/'.$audio_clip;
		// 		$number_audio=WWW_ROOT . 'uploads/Numbers_Audio/English/'.$audio_file;
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $number_audio";
		// 		shell_exec($cmd);
		// 		chmod(WWW_ROOT . 'uploads/Numbers_Audio/Hindi/'.$audio_file,0777);
		// 	}
		// }
		// exit;
	

	    $languages= $this->Language->find('all');
		$language_array=array();
		if(isset($actor_langid))
		{
			array_push($language_array,$actor_language);
		}
		else
		{
			foreach($languages as $language)
			{
				array_push($language_array,$language['Language']['language_name']);
			}	
		}
		
		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		$language_id=$languages['Language']['id'];
		
	    $length=sizeof($language_array);
	    for($i=0;$i<$length;$i++)
		{
			if($language_name==$language_array[$i])
			{
				$selected_language=$i;
			}
		}

	    if(empty($language_name))
	    {
	    	$selected_language=0;
	    	$language_name=$language_array[0];
	    }

	    $numbers = $this->Number->find('all',array('conditions'=>array('language_id'=>$language_id)));
		if ($this->request->is(array('post', 'put'))) 
		{

			$filename=$this->request->data['Number']['hidden_audio'];
			$filename1=substr($filename,0,-4).'.mp3';
			$tmp_name=$this->request->data['Number']['audio_clip']['tmp_name'];
			if(!empty($tmp_name))
			{
				if (!file_exists(WWW_ROOT . 'uploads/Numbers_Audio/')) {
	 				mkdir(WWW_ROOT . 'uploads/Numbers_Audio/', 0777, true);
				}

				unlink(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename);
				$path=WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename;
				$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
				shell_exec($cmd); 

				unlink(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename1);
				$path1=WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename1;
				$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
				shell_exec($cmd1); 

				//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename);
				chmod(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename,0777);
				$this->Flash->success(__('Number Audio Successfully added.'));
				return $this->redirect(array('action' => 'manage_numbers/'.$id.'/'.$language_name));
			}
			else
			{
				$selected_language=$this->data['AlphabetLanguage']['language'];
				for($i=0;$i<$length;$i++)
				{
					if($selected_language==$i)
					{
						$languages2= $this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
						$language_name=$languages2['Language']['language_name'];
						return $this->redirect(array('action' => 'manage_numbers/'.$id.'/'.$language_name));
					}
				}

			}
		}
		$this->set(compact('numbers','id','language_name','language_array','selected_language'));
		
	}	


/**
 * Add Numbers method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_numbers($id=null,$language_name=null) 
	{
		$this->loadModel('Number');
		$this->loadModel('Language');
		if ($this->request->is(array('post', 'put'))) 
		{
			$languages= $this->Language->find('first',array('conditions'=>array('language_name'=>$language_name)));
			$language_id=$languages['Language']['id'];
			$numbers=$this->Number->find('first',array('conditions'=>array('number'=>$this->request->data['Number']['number'],'language_id'=>$language_id)));
			if(empty($numbers))
			{
				$filename=$this->request->data['Number']['number'].".wav";
				$tmp_name=$this->request->data['Number']['audio_clip']['tmp_name'];
				$filename1=substr($filename,0,-4).'.mp3';

				if (!file_exists(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name, 0777, true);
				}

				$path=WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename;
				$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
				shell_exec($cmd);


				$path1=WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename1;
				$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
				shell_exec($cmd1); 

				// move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename);
				chmod(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename,0777);
				chmod(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename1,0777);
				$this->Number->create();
				if($this->Number->save(array('number'=>$this->request->data['Number']['number'],'language_id'=>$language_id,'audio_clip'=>$filename)))
				{
					$this->Flash->success(__('Number Successfully added.'));
					return $this->redirect(array('action' => 'add_numbers',$id,$language_name));
				}
			}	
			else
			{
				$this->Flash->error(__('Number already exist'));
			}	
		}
		$this->set(compact('id','language_name'));
		
	
	}	


	/**
 * Delete Numbers method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_number($id=null,$number_id=null,$language_name=null) 
	{
		$this->loadModel('Number');
		$this->Number->id=$number_id;
		$number_find=$this->Number->find('first',array('conditions'=>array('id'=>$number_id)));
		$filename=$number_find['Number']['audio_clip'];
		$this->Number->delete();
		unlink(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename);
		$filename1=substr($filename,0,-4).'.mp3';
		unlink(WWW_ROOT . 'uploads/Numbers_Audio/'.$language_name.'/'.$filename1);
		$this->Flash->success(__('Number Successfully deleted.'));
		return $this->redirect(array('action' => 'manage_numbers',$id,$language_name));
	}


/**
 * Manage_Setting method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function manage_setting($id = null,$story_name=null,$language_name=null) 
	{
		$this->loadModel('GeneralSettings');
		$this->loadModel('Language');
		$languages= $this->Language->find('all');
		$language_array=array();

		// $settings=$this->GeneralSettings->find('all');
		// foreach($settings as $setting)
		// {
		// 	$audio_clip=$setting['GeneralSettings']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.wav';
		// 	$this->GeneralSettings->id=$setting['GeneralSettings']['id'];
		// 	$this->GeneralSettings->save(array('audio_clip'=>$audio_file));
		// }
		// exit;

		// $settings=$this->GeneralSettings->find('all');
		// foreach($settings as $setting)
		// {
		// 	$audio_clip=$setting['GeneralSettings']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 	$path=WWW_ROOT . 'uploads/Settings_Audio/English/'.$audio_clip;
		// 	$setting_audio=WWW_ROOT . 'uploads/Settings_Audio/English/'.$audio_file;
		// 	$cmd="ffmpeg -i $path -acodec libmp3lame $setting_audio";
		// 	shell_exec($cmd);
		// 	chmod(WWW_ROOT . 'uploads/Settings_Audio/English/'.$audio_file,0777);
		// }


		if($this->Session->read('Auth.User.type')=='voice_actor')
		{
		  	$actor_langid=$this->Session->read('Auth.User.language_id');
		  	$langs= $this->Language->find('first',array('conditions'=>array('Language.id' => $actor_langid)));
		  	$actor_language=$langs['Language']['language_name'];
		}   

		if(isset($actor_langid))
		{
			array_push($language_array,$actor_language);
			$language_id=$actor_langid;
		}
		else
		{
			foreach($languages as $language)
			{
				if($language['Language']['language_name']=='English')
				{
					$language_id=$language['Language']['id'];
					$language_name=$language['Language']['language_name'];
				}
				array_push($language_array,$language['Language']['language_name']);
			}
		}

		$length=sizeof($language_array);

		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$settings1=$this->GeneralSettings->find('all',array('fields'=>'DISTINCT title','conditions'=>array('language_id'=>$english_id)));
		$option_array=array('Select From Existing Title');
		foreach($settings1 as $setting1)
		{
		  	array_push($option_array,$setting1['GeneralSettings']['title']);
		}
		$length1=sizeof($option_array);
	
		$title1=$option_array[1];

		if(isset($actor_langid))
		{

			$setting_find=$this->GeneralSettings->find('all',array('conditions'=>array('title'=>$title1,'setting_id'=>0)));
				
			$settings=array();
			foreach($setting_find as $val)
			{
				$setting_find1=$this->GeneralSettings->find('first',array('conditions'=>array('language_id'=>$language_id,'setting_id'=>$val['GeneralSettings']['id'])));
				array_push($settings, $setting_find1);
			}
		}
		else
		{
			$settings =$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'title'=>$title1)));
		}
	    
		if ($this->request->is(array('post', 'put'))) 
		{
			if(empty($this->request->data['Settings_Form']))
			{
				$selected_language=$this->data['StoryLanguage']['language'];
				$selected_title=$this->data['StoryLanguage']['titles'];
				for($i=0;$i<$length1;$i++)
				{
					if($selected_title==$i)
					{
						$selected_title1=$option_array[$i];
					    $settings =$this->GeneralSettings->find('all',array('conditions'=>array('title'=>$selected_title1)));
					}
				}
				for($i=0;$i<$length;$i++)
				{
					if($selected_language==$i)
					{
					$languages= $this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
					$language_name=$languages['Language']['language_name'];
					$language_id=$languages['Language']['id'];
					}
				}
			}
			else
			{
				$lang=$this->Language->find('first',array('conditions'=>array('Language.id'=>$this->data['Settings_Form']['hidden_language'])));
				$language_name=$lang['Language']['language_name'];
				$temp=$this->data['Settings_Form']['audio_clip']['tmp_name'];
				$hidden_id=$this->data['Settings_Form']['hidden_id'];
				$hidden_string=$this->data['Settings_Form']['hidden_string'];
				$file_name=$this->data['Settings_Form']['filename'];
				if (!file_exists(WWW_ROOT . 'uploads/Settings_Audio/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Settings_Audio/'.$language_name, 0777, true);
				}
				chmod(WWW_ROOT . 'uploads/Settings_Audio/'.$language_name,0777);

				$filename1=substr($file_name,0,-4).'.mp3';

				unlink(WWW_ROOT . 'uploads/Settings_Audio/'.$language_name.'/'.$file_name);
				unlink(WWW_ROOT . 'uploads/Settings_Audio/'.$language_name.'/'.$filename1);

				$path=WWW_ROOT . 'uploads/Settings_Audio/'.$language_name.'/'.$file_name;
				$cmd="ffmpeg -i $temp -b:v 64k -bufsize 64k $path";
				shell_exec($cmd);

			 	$path1=WWW_ROOT . 'uploads/Settings_Audio/'.$language_name.'/'.$filename1;
				$cmd1="ffmpeg -i $temp -acodec libmp3lame $path1";
				shell_exec($cmd1);

				//move_uploaded_file($temp, WWW_ROOT . 'uploads/Settings_Audio/'.$language_name.'/'.$file_name);
				chmod(WWW_ROOT . 'uploads/Settings_Audio/'.$language_name.'/'.$file_name,0777);
				$this->Flash->success(__('Audio Successfully Uploaded.'));
				return $this->redirect(array('action' => 'manage_setting/'.$id));
			}	

			if($english_id==$language_id)
			{
                $settings =$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'title'=>$selected_title1)));
			}	
			else
			{
				$setting_find=$this->GeneralSettings->find('all',array('conditions'=>array('title'=>$selected_title1,'setting_id'=>0)));
			
				$settings=array();
				foreach($setting_find as $val)
				{
					$setting_find1=$this->GeneralSettings->find('first',array('conditions'=>array('language_id'=>$language_id,'setting_id'=>$val['GeneralSettings']['id'])));
					array_push($settings, $setting_find1);
				}
			} 
		}	
		$this->set(compact('settings','language_array','selected_language','language_name','id','settings1','selected_title'));
	}




/**
 * add new string method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function add_string($id = null,$story_name=null,$language_name=null) 
	{
		$this->loadModel('GeneralSettings');
		$this->loadModel('Language');
		$this->loadModel('Apitoken');
		$api=$this->Apitoken->get_active_api();

		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$settings=$this->GeneralSettings->find('all',array('fields'=>'DISTINCT title','conditions'=>array('language_id'=>$english_id)));
		$option_array=array('Select From Existing Title');
		foreach($settings as $setting)
		{
		  array_push($option_array,$setting['GeneralSettings']['title']);
		}
		$length=sizeof($option_array);

		if ($this->request->is(array('post', 'put'))) 
		{
			if($this->data['Settings']['title']!=null && $this->data['Settings']['title_list']!=null)
			{
				$this->Flash->error(__('Only one title must be there for string.'));
			}
			else if($this->data['Settings']['title']==null && $this->data['Settings']['title_list']==null)
			{
				$this->Flash->error(__('You must select one title for string.'));
			}
			else
			{
				if($this->data['Settings']['title']!=null)
				{
					$title=$this->data['Settings']['title'];
				}
				if($this->data['Settings']['title_list']!=null)
				{
					for($i=0;$i<$length;$i++)
					{
						if($i==$this->data['Settings']['title_list'])
						{
							$title=$option_array[$i];
						}
					}
				}
				$this->GeneralSettings->create();
				$file_name=str_replace(' ', '_', $this->data['Settings']['string']).".mp3";
				$array=array('language_id'=>$english_id,'title'=>$title,'string'=>$this->data['Settings']['string'],'audio_clip'=>$file_name);

				//get project id from poeditor
				$project_array1=array('api_token' => $api,'action'=>'list_projects');
				$HttpSocket = new HttpSocket();
				$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				$list_projects1 = json_decode($list_project1, true);
				foreach($list_projects1['list'] as $project)
				{
					if($project['name']==$this->Session->read('project_name')){
						$projectid=$project['id'];
					}
				}

				//add title in poeditor
	    		$arr11=array("term"=>$title,"context"=>"Title");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

		    	//add Strings in poeditor
	    		$arr12=array("term"=>$this->data['Settings']['string'],"context"=>"(".$title.")");
			    $data12 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr12),true));
		    	$request=array();
		    	$response23 = $HttpSocket->post('https://poeditor.com/api/', $data12,$request);

				$this->GeneralSettings->save($array);
				$setting_id=$this->GeneralSettings->getLastInsertId();
				$languages=$this->Language->find('all');

				foreach($languages as $language)
				{
					
					if($language['Language']['id']!=$english_id)
					{
						$this->GeneralSettings->create();
						$array=array('language_id'=>$language['Language']['id'],'title'=>'No Translation','string'=>'No Translation','setting_id'=>$setting_id,'audio_clip'=>$file_name);
						$this->GeneralSettings->save($array);
					}
					
				}
				
				$this->Flash->success(__('New String saved successfully.'));
				return $this->redirect(array('action' => 'add_string',$id,$story_name,$language_name));

			}
		}	
		$this->set(compact('settings','id','language_name'));
	}




	/**
 * Delete Strings method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_setting($id = null) {
		$this->loadModel('GeneralSettings');
		$this->loadModel('Apitoken');
		$this->loadModel('Language');
		$api=$this->Apitoken->get_active_api();
		$find_string=$this->GeneralSettings->find('first',array('conditions'=>array('id'=>$id)));
		$title=$find_string['GeneralSettings']['title'];
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$settings=$this->GeneralSettings->find('all',array('fields'=>'DISTINCT title','conditions'=>array('language_id'=>$english_id)));
		$option_array=array('Select From Existing Title');
		foreach($settings as $setting)
		{
		  array_push($option_array,$setting['GeneralSettings']['title']);
		}
		$length=sizeof($option_array);
		for($i=0;$i<$length;$i++)
		{
			if($option_array[$i]==$title)
			{
				$selected_title=$i;
			}
		}

		if ($this->request->is(array('post', 'put'))) 
		{
			$enter_title=$this->data['Settings']['title'];
			$new_string=$this->data['Settings']['string'];
			$old_string=$this->data['Settings']['hidden_string'];
			$old_title=$this->data['Settings']['hidden_title'];
			for($i=0;$i<$length;$i++)
			{
				if($i==$this->data['Settings']['title_list'])
				{
					$title=$option_array[$i];
				}
			}
			if(!empty($enter_title))
			{
				$title=$enter_title;
			}
			//get project id from poeditor
			$project_array1=array('api_token' => $api,'action'=>'list_projects');
			$HttpSocket = new HttpSocket();
			$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
			$list_projects1 = json_decode($list_project1, true);
			foreach($list_projects1['list'] as $project)
			{
				if($project['name']==$this->Session->read('project_name')){
					$projectid=$project['id'];
				}
			}
			if($new_string!=$old_string)
			{
				//delete String from poeditor
				$arr11=array("term"=>$old_string,"context"=>"(".$old_title.")");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "delete_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

				//add Strings in poeditor
	    		$arr11=array("term"=>$new_string,"context"=>"(".$title.")");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

			}

			if($title!=$old_title)
			{
				//add title in poeditor
	    		$arr11=array("term"=>$title,"context"=>"Title");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

		    	//delete String from poeditor
				$arr11=array("term"=>$old_string,"context"=>"(".$old_title.")");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "delete_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

				//add Strings in poeditor
	    		$arr11=array("term"=>$new_string,"context"=>"(".$title.")");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);
			}
			
			$array=array('language_id'=>$english_id,'title'=>$title,'string'=>$this->data['Settings']['string']);
			$this->GeneralSettings->id=$id;
			$this->GeneralSettings->save($array);
			$other_strings=$this->GeneralSettings->find('all',array('conditions'=>array('setting_id'=>$id)));
			foreach($other_strings as $other)
			{
				$this->GeneralSettings->id=$other['GeneralSettings']['id'];
				$array=array('language_id'=>$language['Language']['id'],'title'=>'No Translation','string'=>'No Translation');
				$this->GeneralSettings->save($array);
			}
			
			
			$this->Flash->success(__('String Updated successfully.'));
			return $this->redirect(array('action' => 'edit_setting',$id));

		}

		$this->set(compact('settings','id','language_name','find_string','selected_title'));
		
	}




	/**
 * Delete Strings method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_string($id = null) {
		$this->loadModel('GeneralSettings');
		$this->loadModel('Apitoken');
		$api=$this->Apitoken->get_active_api();

		$setting_array=$this->GeneralSettings->find('first',array('conditions'=>array('id'=>$id)));
		$title=$setting_array['GeneralSettings']['title'];
		$str=$setting_array['GeneralSettings']['string'];

		//get project id from poeditor
		$project_array1=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
		$list_projects1 = json_decode($list_project1, true);
		foreach($list_projects1['list'] as $project)
		{
			if($project['name']==$this->Session->read('project_name')){
				$projectid=$project['id'];
			}
		}

		$exist_settings=$this->GeneralSettings->find('all',array('conditions'=>array('title'=>$title)));
		$l=sizeof($exist_settings);
		if($l<2)
		{
			//delete title from poeditor
			$arr11=array("term"=>$title,"context"=>"Title");
		    $data11 = array(
	           "api_token" => $api,
	           "action" => "delete_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr11),true));
	    	$request=array();
	    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);
	    }

    	//delete Strings from poeditor
		$arr12=array("term"=>$str,"context"=>"(".$title.")");
	    $data12 = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $projectid,
           "data" => json_encode(array($arr12),true));
    	$request=array();
    	$response23 = $HttpSocket->post('https://poeditor.com/api/', $data12,$request);

		$strings=$this->GeneralSettings->find('all',array('conditions'=>array('setting_id'=>$id)));
		foreach($strings as $string)
		{
			$this->GeneralSettings->id=$string['GeneralSettings']['id'];
			$this->GeneralSettings->delete();
		}
		$this->GeneralSettings->id=$id;
		if($this->GeneralSettings->delete())
		{
			$this->Flash->success(__('Settings Successfully Deleted.'));
			return $this->redirect(array('action' => 'manage_setting/English'));
		}
		
	}



	/**
 	* Manage_Alphabet method
 	*
 	* @throws NotFoundException
 	* @param string $id
 	* @return void
 	*/
	public function manage_alphabet($id = null,$language_name=null) {
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Alphabet');

		// $alphabets=$this->Alphabet->find('all');
		// foreach($alphabets as $alphabet)
		// {
		// 	if($alphabet['Alphabet']['language_id']==2)
		// 	{
		// 		$audio_clip=$alphabet['Alphabet']['audio_clip'];
		// 		$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 		unlink(WWW_ROOT . 'uploads/Alphabet_Audio/English/'.$audio_file);
		// 		$path=WWW_ROOT . 'uploads/Alphabet_Audio/English/'.$audio_clip;
		// 		$alphabet_audio=WWW_ROOT . 'uploads/Alphabet_Audio/English/'.$audio_file;
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $alphabet_audio";
		//  		shell_exec($cmd);
		//  		chmod(WWW_ROOT . 'uploads/Alphabet_Audio/English/'.$audio_file,0777);
		// 	}
		// 	if($alphabet['Alphabet']['language_id']==1)
		// 	{
		// 		$audio_clip=$alphabet['Alphabet']['audio_clip'];
		// 		$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 		unlink(WWW_ROOT . 'uploads/Alphabet_Audio/Hindi/'.$audio_file);
		// 		$path=WWW_ROOT . 'uploads/Alphabet_Audio/Hindi/'.$audio_clip;
		// 		$alphabet_audio=WWW_ROOT . 'uploads/Alphabet_Audio/Hindi/'.$audio_file;
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $alphabet_audio";
		//  		shell_exec($cmd);
		//  		chmod(WWW_ROOT . 'uploads/Alphabet_Audio/Hindi/'.$audio_file,0777);
		// 	}
			
		// }
		// exit;

		if($this->Session->read('Auth.User.type')=='voice_actor')
		{
		  	$actor_langid=$this->Session->read('Auth.User.language_id');
		  	$langs= $this->Language->find('first',array('conditions'=>array('Language.id' => $actor_langid)));
		  	$actor_language=$langs['Language']['language_name'];
		}   

		$languages= $this->Language->find('all');
		$language_array=array();
		if(isset($actor_langid))
		{
			array_push($language_array,$actor_language);
		}
		else
		{
			foreach($languages as $language)
			{
				array_push($language_array,$language['Language']['language_name']);
			}	
		}
		
		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		$language_id=$languages['Language']['id'];
		
	    $length=sizeof($language_array);
	    if(empty($language_name))
	    {
	    	$selected_language=0;
	    	$language_name=$language_array[0];
	    }
	    if ($this->request->is(array('post', 'put'))) 
		{
			if(empty($this->data['Alphabet']['audio_clip']))
			{
				$selected_language=$this->data['AlphabetLanguage']['language'];
				for($i=0;$i<$length;$i++)
				{
					if($selected_language==$i)
					{
					$languages2= $this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
					$language_name=$languages2['Language']['language_name'];
					$language_id=$languages2['Language']['id'];
					}
				}
			}
			else
			{
				$languages2= $this->Language->find('first',array('conditions'=>array('Language.id'=>$this->request->data['Alphabet']['hidden_language'])));
				$language_name=$languages2['Language']['language_name'];
				$filename=$this->request->data['Alphabet']['hidden_alphabet'].".wav";
				$tmp_name=$this->request->data['Alphabet']['audio_clip']['tmp_name'];
				$langs=$this->Language->find('first',array('Language.language_id'=>$this->request->data['Alphabet']['hidden_language']));

				if (!file_exists(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name, 0777, true);
				}

				if(!empty($tmp_name))
				{
					$filename1=substr($filename,0,-4).'.mp3';
					unlink(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename);
					$path=WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename;
					$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
					shell_exec($cmd);
					unlink(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename1);
					$path1=WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename1;
					$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
					shell_exec($cmd1);
				}

				//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename);
				$this->Alphabet->id=$this->request->data['Alphabet']['hidden_id'];
				if($this->Alphabet->save(array('audio_clip'=>$filename)))
				{
					$this->Flash->success(__('Alphabet Audio is Successfully Uploaded.'));
					return $this->redirect(array('action' => 'manage_alphabet/'.$id.'/'.$language_name));
				}
			}
		}
		if(isset($actor_langid))
		{
		    $alphabets = $this->Alphabet->find('all',array('conditions'=>array('language_id'=>$actor_langid)));
		    $language_name=$actor_language;
		}
		else
		{
		    $alphabets = $this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
		}
		$this->set(compact('alphabets','language_name','id','language_array','selected_language'));
	}


	/**
	 * Add Alphabet method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
 	*/
	public function add_alphabet($id=null,$language_name=null) {
		$this->loadModel('Language');
		$this->loadModel('Story');
		$this->loadModel('Alphabet');
		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_name)));
		$language_id=$languages['Language']['id'];
		if ($this->request->is(array('post', 'put'))) 
		{
			$alphabets=$this->Alphabet->find('first',array('conditions'=>array('alphabet'=>$this->request->data['Alphabet']['alphabet'])));
			if(empty($alphabets))
			{
				$filename=$this->request->data['Alphabet']['alphabet'].".wav";
				$tmp_name=$this->request->data['Alphabet']['audio_clip']['tmp_name'];
				$langs=$this->Language->find('first',array('Language.language_id'=>$this->request->data['Alphabet']['hidden_language']));

				if (!file_exists(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name)) {
	 				mkdir(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name, 0777, true);
				}
				
				$path=WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename;
				$cmd="ffmpeg -i $tmp_name -b:v 64k -bufsize 64k $path";
				shell_exec($cmd); 
				
				$filename1=substr($filename,0,-4).'.mp3';
			 	$path1=WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename1;
				$cmd1="ffmpeg -i $tmp_name -acodec libmp3lame $path1";
				shell_exec($cmd1);

				$this->Alphabet->create();
				if($this->Alphabet->save(array('alphabet'=>$this->request->data['Alphabet']['alphabet'],'language_id'=>$language_id,'audio_clip'=>$filename)))
				{
					$this->Flash->success(__('Alphabet Successfully added.'));
					return $this->redirect(array('action' => 'add_alphabet/'.$id.'/'.$language_name));
				}
			}	
			else
			{
				$this->Flash->error(__('Alphabet already exist'));
			}	
		}
		$this->set(compact('language_name','id'));
	}




	/**
 * Edit Alphabet method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_alphabet($id=null,$alphabet_id=null,$language_name) {
		$this->loadModel('Alphabet');
		$this->loadModel('Language');
		$this->Alphabet->id=$alphabet_id;
		$alphabets=$this->Alphabet->find('first',array('conditions'=>array('id'=>$alphabet_id)));
		$language_id=$alphabets['Alphabet']['language_id'];
		$languages=$this->Language->find('first',array('conditions'=>array('id'=>$language_id)));
		$language_name=$languages['Language']['language_name'];
		$alphabet=$alphabets['Alphabet']['alphabet'];
		$old_file=$alphabet.'.wav';
		$old_mp3_file=$alphabet.'.mp3';
		if ($this->request->is(array('post', 'put'))) 
		{
			$new_file=$this->data['Alphabet']['alphabet'].'.wav';
			$new_mp3_file=$this->data['Alphabet']['alphabet'].'.mp3';
			if($this->Alphabet->save(array('alphabet'=>$this->data['Alphabet']['alphabet'],'audio_clip'=>$new_file)))
			{
				rename(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$old_file,WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$new_file);
				rename(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$old_mp3_file,WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$new_mp3_file);
				$this->Flash->success(__('Alphabet Successfully Updated.'));
				return $this->redirect(array('action' => 'manage_alphabet/'.$id.'/'.$language_name));
			}
		}
		$this->set(compact('alphabets','id','language_name'));
	}


	/**
 * Delete Alphabet method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_alphabet($id = null,$alphabet_id,$language_name) {
		$this->loadModel('Alphabet');
		$this->Alphabet->id=$alphabet_id;
		$alphabet_find=$this->Alphabet->find('first',array('conditions'=>array('id'=>$alphabet_id)));
		$filename=$alphabet_find['Alphabet']['audio_clip'];
		unlink(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename);
		$filename1=substr($filename,0,-4).'.mp3';
		unlink(WWW_ROOT . 'uploads/Alphabet_Audio/'.$language_name.'/'.$filename1);
		if($this->Alphabet->delete())
		{
			$this->Flash->success(__('Alphabet Successfully Deleted.'));
			return $this->redirect(array('action' => 'manage_alphabet/'.$id.'/'.$language_name));
		}
		
	}


	
/**
 * Sync Minigames with POEditor method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sync_to_poeditor() {
		$this->loadModel('Apitoken');
		$this->loadModel('Language');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('Story');
		$this->loadModel('GeneralSettings');

		$api=$this->Apitoken->get_active_api();
		$HttpSocket = new HttpSocket();
		//get project_id from POEditor for story
		$project_array2=array('api_token' => $api,'action'=>'list_projects');
		$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
		$list_projects2 = json_decode($list_project2, true);
		foreach($list_projects2['list'] as $project2)
		{
			if($project2['name']==$this->Session->read('project_name')){
				$projectid=$project2['id'];
			}
		}

		$english_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_language_id=$english_language['Language']['id'];
		//get language list from project
		$lang_array=array('api_token' => $api,'action'=>'list_languages','id'=>$projectid);
		$list_languages = $HttpSocket->post('https://poeditor.com/api/', $lang_array);
		$language_list = json_decode($list_languages, true);
		$language_array=array();
		$language_code_array=array();
		foreach($language_list['list'] as $language)
		{
			if($language['name']=='Chinese (simplified)')
			{
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage('Chinese');
				array_push($language_array,'Chinese');
				array_push($language_code_array,$language_code);
			}
			else
			{
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language['name']);
				array_push($language_array,$language['name']);
				array_push($language_code_array,$language_code);
			}
		}
		$l=sizeof($language_array);

		//view all terms for language
		for($i=0;$i<$l;$i++)
		{
			if($language_code_array[$i]!='en')
			{
				$data2 = array(
		           "api_token" => $api,
		           "action" => "view_terms",
		           "id" => $projectid,
		           "language" => $language_code_array[$i]);
		    	$request=array();
		    	$response4= $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
		    	$context_list = json_decode($response4, true);
		    	$term_array=array();
		    	$context_array=array();
		    	$translation_array=array();
		    	$cnt=0;
				foreach($context_list['list'] as $context1)
				{
		    	array_push($term_array,$context1['term']);
		    	array_push($context_array,$context1['context']);
		    	array_push($translation_array,$context1['definition']['form']);
		    	$cnt++;
		    	}
		    	

		    	$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
		    	$language_id=$languages['Language']['id'];

		    	$settings=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id)));
		    	foreach($settings as $setting)
		    	{
		    		
			    		$this->GeneralSettings->id=$setting['GeneralSettings']['id'];
			    		$english_setting=$this->GeneralSettings->find('first',array('conditions'=>array('id'=>$setting['GeneralSettings']['setting_id'])));
			    		for($j=0;$j<=$cnt;$j++)
		    			{
		    				if(!empty($translation_array[$j]))
	    					{
			    				if($term_array[$j]==$english_setting['GeneralSettings']['title'])
			    				{
			    					if(!empty($translation_array[$j]))
			    					{
			    						$this->GeneralSettings->save(array('title'=>$translation_array[$j]));
			    					}
			    					
			    				}
			    				if($term_array[$j]==$english_setting['GeneralSettings']['string'])
			    				{
			    					if(!empty($translation_array[$j]))
			    					{
			    						$this->GeneralSettings->save(array('string'=>$translation_array[$j]));
			    					}
			    					
			    				}
			    			}
		    			}
	    			
		    	}

		    	$stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$language_id)));
				foreach ($stories as $story) {
					$storyname1=$story['Story']['story_name'];
					$stories_by_name=$this->Story->find('first',array('conditions'=>array('story_name'=>$storyname1,'language_id'=>$language_id)));
					$story_id=$stories_by_name['Story']['id'];

					$words=$this->Word->find('all',array('conditions'=>array('story_id'=>$story_id)));
					$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id)));
				
					foreach($words as $word)
					{
				
							$story=$word['Word']['story'];
							$this->Word->id=$word['Word']['id'];
							$word_story=$this->Word->find('all',array('conditions'=>array('id'=>$story)));
							
							foreach($word_story as $word1)
							{
								$word_name=$word1['Word']['word'];
								for($j=0;$j<=$cnt;$j++)
				    			{
				    				if(!empty($translation_array[$j]))
				    				{
					    				if($term_array[$j]==$word_name && $context_array[$j]=='('.$storyname1.')')
					    				{

					    					if(!empty($translation_array[$j]))
					    					{
					    						$this->Word->save(array('word'=>$translation_array[$j]));
					    					}
					    					
					    				}
					    			}
				    			}
							}
						
					}

					foreach($questions as $question)
					{
						$story1=$question['Question']['story'];
						$this->Question->id=$question['Question']['id'];
						$question_story=$this->Question->find('all',array('conditions'=>array('id'=>$story1)));

						foreach($question_story as $question1)
						{
							$question2=$question1['Question']['question'];
							$answer=$question1['Question']['answer'];
							$option1=$question1['Question']['option1'];
							$option2=$question1['Question']['option2'];
							$option3=$question1['Question']['option3'];
							$question_audio=$question1['Question']['question_audio'];
							for($j=0;$j<=$cnt;$j++)
			    			{
			    				if(!empty($translation_array[$j]))
		    					{
		    						  	$question_context='Question'.substr($question_audio,-7,3);
					    				if($term_array[$j]==$question2 && $context_array[$j]==$question_context)
					    				{
					    					if(!empty($translation_array[$j]))
					    					{
					    						$this->Question->save(array('question'=>$translation_array[$j]));
					    					}
					    					
					    				}
					    			
					    				$answer_context='Answer'.substr($question_audio,-7,3);
					    				if($term_array[$j]==$answer && $context_array[$j]==$answer_context)
					    				{
					    					if(!empty($translation_array[$j]))
					    					{
					    						$this->Question->save(array('answer'=>$translation_array[$j]));
					    					}
					    					
					    				}
					    			
					    				$option1_context='Option 1('.substr($question_audio,-7,3).')';
					    				if($term_array[$j]==$option1 && $context_array[$j]==$option1_context)
					    				{
					    					if(!empty($translation_array[$j]))
					    					{
					    						$this->Question->save(array('option1'=>$translation_array[$j]));
					    					}
					    					
					    				}
					    			
					    				$option2_context='Option 2('.substr($question_audio,-7,3).')';
										if($term_array[$j]==$option2 && $context_array[$j]==$option2_context)
					    				{
					    					if(!empty($translation_array[$j]))
					    					{
					    						$this->Question->save(array('option2'=>$translation_array[$j]));
					    					}
					    					
					    				}
					    			

					    				$option3_context='Option 3('.substr($question_audio,-7,3).')';
										if($term_array[$j]==$option3 && $context_array[$j]==$option3_context)
					    				{
					    					if(!empty($translation_array[$j]))
					    					{
					    						$this->Question->save(array('option3'=>$translation_array[$j]));
					    					}
					    					
					    				}
					    			
			    				}
			    			}
						}
					}
				}
		    }
		}

		$this->Flash->success(__('Minigames successfully sync with POEditor.'));
		return $this->redirect(array('action' => 'index'));
	}

	

}
