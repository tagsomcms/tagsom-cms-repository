<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
 /**
 * Stories Controller
 *
 * @property Story $Story
 * @property PaginatorComponent $Paginator
 */
class StoriesController extends AppController {

 /**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','UploadFile');
	 

	public function import_stories() 
	{	
		$this->loadModel('StoryStatement');
	 	require_once 'excel_reader2.php';
      	if($this->request->is('post')){
      		unlink(WWW_ROOT . 'uploads/story.xls');
			move_uploaded_file($this->data['Teacher']['upload']['tmp_name'], WWW_ROOT . 'uploads/story.xls');
         	chmod(WWW_ROOT . 'uploads/story.xls', 0777);
         	if(file_exists(WWW_ROOT . 'uploads/story.xls'))
         	{
    
         		$data = new Spreadsheet_Excel_Reader(WWW_ROOT . 'uploads/story.xls');
         		
         		$count=count($data->sheets[0]['cells']);

         		$this->Story->import($data,$count);

         	// echo $data->sheets[0]['cells'][$i][1].'  -  '.$data->sheets[0]['cells'][$i][2].'  -  '.$data->sheets[0]['cells'][$i][3].'  -  '.$data->sheets[0]['cells'][$i][4];
			// echo '<br>';	 	
         	}
         	else{
         		echo 'file does not exist';
         	}

      	}

	}


/**
 * Index method
 *
 * @return void
 */
	public function index() 
	{	

		$this->loadModel('Language');
		if($this->Session->read('Auth.User.type')=='voice_actor')
		{
		  	$actor_langid=$this->Session->read('Auth.User.language_id');
		  	$langs= $this->Language->find('first',array('conditions'=>array('Language.id' => $actor_langid)));
		  	$actor_language=$langs['Language']['language_name'];
		}   

		$languages= $this->Language->find('all');
		$language_array=array();
		if(isset($actor_langid))
		{
			//list stories for only one language for which Voice actor has benn assigned
			$this->paginate = array(
	        'conditions' => array('Story.language_id' => $actor_langid),
	        'limit' => 20
		    );
			$stories = $this->paginate('Story');
		    $this->set(compact('stories'));
			array_push($language_array,$actor_language);
		}
		else
		{
			//list english story firsttime when user is admin
			$this->loadModel('Language');
			$langs= $this->Language->find('first',array('conditions'=>array('Language.language_name' => 'English')));
			$langs_id=$langs['Language']['id'];
			$this->paginate = array(
	        'conditions' => array('Story.language_id' => $langs_id),
	        'limit' => 20
		    );
			$stories = $this->paginate('Story');
		    $this->set(compact('stories'));

			foreach($languages as $language)
			{
				array_push($language_array,$language['Language']['language_name']);
			}
		}
		$length=sizeof($language_array);
		$lang_id=$langs_id;
		$language_name=$langs['Language']['language_name'];
		$this->set('language_array',$language_array);
		if ($this->request->is('post')) {
			$selected_language=$this->data['StoryLanguage']['language'];
			for($i=0;$i<$length;$i++)
			{
				if($selected_language==$i)
				{
					$language= $this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
					$lang_id=$language['Language']['id'];
					$language_name=$language['Language']['language_name'];
					// $this->Story->recursive = 0;
					// $stories=$this->paginate=array('limit'=>10, 'order' => 'Story.id DESC');
					// $this->set('stories', $this->Paginator->paginate());

					$this->paginate = array(
			        'conditions' => array('Story.language_id' => $lang_id),
			        'limit' => 20
				    );
				     
			     	$english_lang= $this->Language->find('first',array('conditions'=>array('Language.language_name' => 'English')));
					$eng_lang_id=$english_lang['Language']['id'];
				    $english_stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$eng_lang_id))); 
				    $stories = $this->paginate('Story');
				    $this->set(compact('stories','english_stories'));
				}
			}
			$this->set(compact('selected_language'));
		}
		$this->set(compact('lang_id','language_name'));

		if( $this->request->is('ajax') ) {
		    $value =$this->request->data('storyid');
		    $story=$this->Story->find('first',array('conditions'=>array('id'=>$value)));
		    if($story['Story']['active']==1)
		    {
		    	$status=0;
		    }
		    else
		    {
		    	$status=1;
		    }
		    $this->Story->id=$value;
		    $this->Story->save(array('active'=>$status));
		}


	}

 /**
 * View method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('StoryImage');
		if (!$this->Story->exists($id)) {
			throw new NotFoundException(__('Invalid story'));
		}
		$options2 = array('conditions' => array('Story.id' => $id));
		$story_name=$this->Story->find('first',$options2);
		$storyname=$story_name['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname);
		$this->set('storyname',$story_path);

		$options = array('conditions' => array('Story.' . $this->Story->primaryKey => $id));
		$this->set('story2', $this->Story->find('first', $options));
	}



	 /**
 * Upload introduction audio for story method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function introduction_audio($id = null) {
		$this->loadModel('Language');
		$options2 = array('conditions' => array('Story.id' => $id));
		$story_name=$this->Story->find('first',$options2);
		$storyname=$story_name['Story']['story_name'];
		$introduction=$story_name['Story']['introduction'];
		$story_path=str_replace(' ', '_', $storyname);
		$disable=$story_name['Story']['disable'];
		$this->set(compact('story_path'));
		$languages=$this->Language->find('first',array('conditions'=>array('id'=>$story_name['Story']['language_id'])));
		$language_name=$languages['Language']['language_name'];

		if ($this->request->is('post')) 
		{
			$tmp_name=$this->data['Introduction']['audio_clip']['tmp_name'];
			if (!file_exists(WWW_ROOT.'uploads/'.$story_path."/Introduction_Audio/".$language_name)) {
						mkdir(WWW_ROOT.'uploads/'.$story_path."/Introduction_Audio/".$language_name, 0777, true);
		    }
   			chmod(WWW_ROOT.'uploads/'.$story_path."/Introduction_Audio/".$language_name,0777);
			//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/Introduction_Audio/'.$language_name.'/introduction.wav');
			
			unlink(WWW_ROOT . 'uploads/'.$story_path.'/Introduction_Audio/'.$language_name.'/introduction.mp3');
			$path=$tmp_name;
		    $intro_audio=WWW_ROOT . 'uploads/'.$story_path.'/Introduction_Audio/'.$language_name.'/introduction.mp3';
			$cmd="ffmpeg -i $path -acodec libmp3lame $intro_audio";
			shell_exec($cmd);

			chmod(WWW_ROOT . 'uploads/'.$story_path.'/Introduction_Audio/'.$language_name.'/introduction.mp3',0777);
			$file_name='introduction.mp3';

			$this->Story->id=$id;
			if($this->Story->save(array('audio_clip'=>$file_name)))
			{
				$this->Flash->success(__('Introduction Audio Successfully Uploaded.'));
				return $this->redirect(array('action' => 'introduction_audio/'.$id));
			}
		}
		$this->set(compact('story_path','language_name','disable','id','introduction'));
	}


	public function change_extension()
	{
		$this->loadModel('MathObject');
		$this->loadModel('Language');
		$this->loadModel('Number');
		$this->loadModel('Alphabet');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('StoryTranslation');
		$this->loadModel('GeneralSettings');

		
		// // This is for change extension
		// $this->loadModel('StoryStatement');
		// $stories=$this->Story->find('all');
		// foreach($stories as $story)
		// {
		// 	$story_id=$story['Story']['id'];
		// 	$this->Story->id=$story_id;
		// 	$this->Story->save(array('audio_clip'=>'introduction.mp3'));
		// 	$statements=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$story_id)));
		// 	foreach($statements as $statement)
		// 	{
		// 		$audio_clip=$statement['StoryStatement']['audio_clip'];
		// 		$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 		$this->StoryStatement->id=$statement['StoryStatement']['id'];
		// 		$this->StoryStatement->save(array('audio_clip'=>$audio_file));
		// 	}
		// }	
	
		
		// $settings=$this->GeneralSettings->find('all');
		// foreach($settings as $setting)
		// {
		// 	$audio_clip=$setting['GeneralSettings']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 	$setting_id=$setting['GeneralSettings']['id'];
		// 	$this->GeneralSettings->id=$setting_id;
		// 	$this->GeneralSettings->save(array('audio_clip'=>$audio_file));	
		// }


		// $objects=$this->MathObject->find('all');
		// foreach($objects as $object)
		// {
		// 	$audio_clip=$object['MathObject']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 	$object_id=$object['MathObject']['id'];
		// 	$this->MathObject->id=$object_id;
		// 	$this->MathObject->save(array('audio_clip'=>$audio_file));	
		// }
		
			
		

		// $numbers = $this->Number->find('all');
		// foreach($numbers as $number)
		// {
		// 	$audio_clip=$number['Number']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 	$this->Number->id=$number['Number']['id'];
		// 	$this->Number->save(array('audio_clip'=>$audio_file));
		// }



		// $alphabets=$this->Alphabet->find('all');
		// foreach($alphabets as $alphabet)
		// {
		// 	$audio_clip=$alphabet['Alphabet']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';	
		// 	$this->Alphabet->id=$alphabet['Alphabet']['id'];
		// 	$this->Alphabet->save(array('audio_clip'=>$audio_file));
		// }


		// $words=$this->Word->find('all');
		// foreach($words as $word)
		// {
		// 	$audio_clip=$word['Word']['audio_clip'];
		// 	$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 	$this->Word->id=$word['Word']['id'];
		// 	$this->Word->save(array('audio_clip'=>$audio_file));
		// }


		// $questions=$this->Question->find('all');
		// foreach($questions as $question)
		// {
		// 	$question_id=$question['Question']['id'];
		// 	$question_audio=$question['Question']['question_audio'];
		// 	$question_file=substr($question_audio,0,-4).'.mp3';
		// 	$answer_audio=$question['Question']['answer_audio'];
		// 	$answer_file=substr($answer_audio,0,-4).'.mp3';
		// 	$option1_audio=$question['Question']['option1_audio'];
		// 	$option1_file=substr($option1_audio,0,-4).'.mp3';
		// 	$option2_audio=$question['Question']['option2_audio'];
		// 	$option2_file=substr($option2_audio,0,-4).'.mp3';
		// 	$option3_audio=$question['Question']['option3_audio'];
		// 	$option3_file=substr($option3_audio,0,-4).'.mp3';

		// 	$this->Question->id=$question_id;
		// 	$this->Question->save(array('question_audio'=>$question_file,'answer_audio'=>$answer_file,'option1_audio'=>$option1_file,'option2_audio'=>$option2_file,'option3_audio'=>$option3_file));
			
		// }

		exit;

	}


 /**
 * Add method 
 * @param no parameters are there
 * @return void
 */
	public function add() {
		
		// $stories=$this->Story->find('all',array('conditions'=>array('language_id'=>2)));
		// foreach($stories as $story)
		// {
		// 	$story_name=$story['Story']['story_name'];
		// 	$story_path=str_replace(' ', '_', $story_name);
		// 	if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/English/introduction.wav'))
		// 	{
			
		// 		$path=WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/English/introduction.wav';
		// 	    $intro_audio=WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/English/introduction.mp3';
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $intro_audio";
		// 		shell_exec($cmd);
		// 		chmod(WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/English/introduction.mp3',0777);
		// 	}
		// 	if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/Hindi/introduction.wav'))
		// 	{

		// 		$path=WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/Hindi/introduction.wav';
		// 	    $intro_audio=WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/Hindi/introduction.mp3';
		// 		$cmd="ffmpeg -i $path -acodec libmp3lame $intro_audio";
		// 		shell_exec($cmd);
		// 		chmod(WWW_ROOT.'uploads/'.$story_path.'/Introduction_Audio/Hindi/introduction.mp3',0777);

		// 	}
		// }


		// $this->loadModel('StoryStatement');
		
		// 	$story_name='How to Smell';
		// 	$story_path=str_replace(' ', '_', $story_name);
		// 	$find_story=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
		// 	foreach($find_story as $story)
		// 	{
		// 		if($story['Story']['language_id']==1)
		// 		{
		// 			chmod(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/Hindi/',0777);
		// 			$story_id=$story['Story']['id'];
		// 			$statements=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$story_id)));
		// 			foreach($statements as $statement)
		// 			{
		// 				$audio_clip=$statement['StoryStatement']['audio_clip'];
		// 				$path=WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/Hindi/'.$audio_clip;
		// 				$audio_file=substr($audio_clip,0,-4).'.mp3';
		// 				unlink(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/Hindi/'.$audio_file);
		// 			 	$statement_audio=WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/Hindi/'.$audio_file;
						
		// 				$cmd="ffmpeg -i $path -acodec libmp3lame $statement_audio";
		// 				shell_exec($cmd);
		// 				chmod(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/Hindi/'.$audio_file,0777);
		// 			}
					
		// 		}
		// 		if($story['Story']['language_id']==2)
		// 		{
		// 			$story_id=$story['Story']['id'];
		// 			$statements=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$story_id)));
		// 			foreach($statements as $statement)
		// 			{
		// 				$audio_clip=$statement['StoryStatement']['audio_clip'];

		// 				$path=WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/English/'.$audio_clip;
		// 				$audio_file=substr($audio_clip,0,-4).'.mp3';
						
		// 				unlink(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/English/'.$audio_file);
		// 				$statement_audio=WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/English/'.$audio_file;
		// 				$cmd="ffmpeg -i $path -acodec libmp3lame $statement_audio";
		// 				shell_exec($cmd);
		// 				chmod(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/English/'.$audio_file,0777);
		// 			}
		// 		}
		// 	}
		// exit;



		if ($this->request->is('post')) {
			$this->loadModel('Apitoken');
			$api=$this->Apitoken->get_active_api();
			$this->loadModel('Language');
			$this->Story->create();	
			$story_name=$this->data['Story']['story_name'];
			$story_path=str_replace(' ', '_', $story_name);
			$introduction=$this->data['Story']['introduction'];
			$options = array('conditions' => array('language_name' => 'English'));
			$lang = $this->Language->find('first',$options);
			$lang_id=$lang['Language']['id'];

			//(PO Editor)  -> Start
			//list_projects to check it is exist or not
			$project_array=array('api_token' => $api,'action'=>'list_projects');
			$HttpSocket = new HttpSocket();
			$list_project = $HttpSocket->post('https://poeditor.com/api/', $project_array);
			$list_projects = json_decode($list_project, true);
			foreach($list_projects['list'] as $project)
			{
				if($project['name']==$this->Session->read('project_name')){
					$story_exist=1;
				}
			}
			
			//add story
			if($story_exist!=1)
			{
				$arr=array('api_token' => $api,'action'=>'create_project','name'=>$this->Session->read('project_name'));
				$HttpSocket = new HttpSocket();
				$response = $HttpSocket->post('https://poeditor.com/api/', $arr);
				$jsonarr = json_decode($response, true);
			}
			else
			{
				$this->Flash->error(__('The story already exist'));
			}

				//get project id
				$project_array1=array('api_token' => $api,'action'=>'list_projects');
				$HttpSocket = new HttpSocket();
				$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				$list_projects1 = json_decode($list_project1, true);
				foreach($list_projects1['list'] as $project)
				{
					if($project['name']==$this->Session->read('project_name')){
						$projectid=$project['id'];
					}
				}

				//add_language for story (default - English)
				$project_array1=array('api_token' => $api,'id'=>$projectid,'action'=>'add_language','language'=>'en');
				$HttpSocket = new HttpSocket();
				$add_lang= $HttpSocket->post('https://poeditor.com/api/', $project_array1);

				//add story_name
	    		$arr11=array("term"=>$story_name,"context"=>"story_name");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

		    	//add introduction
	    		$arr13=array("term"=>$introduction,"context"=>"Introduction");
			    $data13 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr13),true));
		    	$request=array();
		    	$response23 = $HttpSocket->post('https://poeditor.com/api/', $data13,$request);
		    //(PO Editor)  -> End
			
				//save story data to database
				$file_name=$this->request->data['Story']['image']['name'];
				$tmp_name=$this->request->data['Story']['image']['tmp_name'];

				$tmp2=$tmp_name;
				if(empty($file_name))
				{
					$file_name=null;
				}
				else
				{
					//upload story images on server
					if (!file_exists(WWW_ROOT.'uploads/'.$story_path."/StoryImage")) {
	     				mkdir(WWW_ROOT.'uploads/'.$story_path."/StoryImage", 0777, true);
				    }
				    chmod(WWW_ROOT.'uploads/'.$story_path,0777);
				    chmod(WWW_ROOT.'uploads/'.$story_path."/StoryImage",0777);
					move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/StoryImage/StoryImage.jpg');
					chmod(WWW_ROOT . 'uploads/'.$story_path.'/StoryImage/StoryImage.jpg',0777);
					if (!file_exists(WWW_ROOT.'uploads/StoryInfo/LeadImage/')) {
	     				mkdir(WWW_ROOT.'uploads/StoryInfo/LeadImage/', 0777, true);
				    }
					$file_name='StoryImage.jpg';
				}
				
				//Save Story in database
				$lang_array=array();
				$languages=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
				$language_id=$languages['Language']['id'];
				$sname=$this->request->data['Story']['story_name'];

				$this->request->data['Story']['image']=$file_name;
				$this->request->data['Story']['language_id']=$language_id;
				$this->Story->save($this->request->data);
				
				//Add Story Names in database for translation
				$this->loadModel('StoryTranslation');
				$last_id=$this->Story->getLastInsertId();
				$this->StoryTranslation->create();
				$this->StoryTranslation->save(array('story_id'=>$last_id,'language_id'=>$language_id,'story_name'=>$sname,'translation'=>$sname));

			    $fname='Book_'.$story_name.'.jpg';
			    $src = WWW_ROOT . 'uploads/'.$story_path.'/StoryImage/StoryImage.jpg';  
				$dest = WWW_ROOT . 'uploads/StoryInfo/LeadImage/'.$fname;       
				copy($src,$dest);

				$this->Flash->success(__('The story has been saved.'));
				return $this->redirect(array('action' => 'add'));	
			
		}
		$languages = $this->Story->Language->find('list',array('fields'=>array('Language.id','Language.language_name')));
		$this->set(compact('languages'));
	}


 /**
 * Add image for story method
 * @param Int $id pass as unique story_id
 * @return void
 */
	public function images($id=null,$language=null) {
		$this->loadModel('StoryImage');
		$this->StoryImage->id = $id;
		$options = array('conditions' => array('StoryImage.story_id'=>$id),'order' => 'StoryImage.image_number ASC');
		$image=$this->StoryImage->find('all',$options);
		$options2 = array('conditions' => array('Story.id' => $id));
		$story_name=$this->Story->find('first',$options2);
		$storyname=$story_name['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname);//to get story name after replace space with '_'
		$number_array=array();
		$this->set(compact('image','storyname','id'));//to get images in ascending order on slider
		$this->set('storyname',$story_path);

		if ($this->request->is('post')) {
		
			foreach($image as $img)
			{
			array_push($number_array, $img['StoryImage']['image_number']);
			}		

			if(empty($this->data['Story']['image']['name']) && empty($this->data['ImageNumber']['image_number'])){
				$this->Flash->error(__('Please select story image'));
			}
			else
			{
				//Assign Image Numbers 
				if(empty($this->data['ImageNumber']['image_number']))
				{
					if(in_array($this->data['Story']['image_number'], $number_array))
					{
						$stories=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
						foreach($stories as $story)
						{
							$st_id=$story['Story']['id'];
							$this->StoryImage->updateAll(
					    	array('StoryImage.image_number' => 'StoryImage.image_number + 1'),array('StoryImage.image_number >=' => $this->data['Story']['image_number'],'StoryImage.story_id'=>$st_id));
						}
					}
		
					$file=$this->data['Story']['image'];			
					$count++;
					$tmp_name=$file['tmp_name'];
					$file_name=$file['name'];
					$this->StoryImage->create();
					$count=rand(111,999);
					$filename='Chapter'.$count.'.jpg';//to set chapterimage name with random number
					$stories=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
					foreach($stories as $story)
					{
						$array=array('story_id'=>$story['Story']['id'],'image'=>$filename,'image_number'=>$this->data['Story']['image_number']);
						$this->StoryImage->create();
						$this->StoryImage->save($array);
					}
					
					if (!file_exists(WWW_ROOT.'uploads/'.$story_path."/ChapterImage")) {
	     				mkdir(WWW_ROOT.'uploads/'.$story_path."/ChapterImage", 0777, true);
				    }
				    chmod(WWW_ROOT.'uploads/'.$story_path."/ChapterImage",0777);
					move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$filename);
					chmod(WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$filename,0777);

					$imgs=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$id),'limit'=>3,'order'=>'image_number ASC'));
					$counter=1;
					foreach($imgs as $img)
					{
						$img_name=$img['StoryImage']['image'];
						if($counter==1)
						{
						 	$fname='Book_'.$storyname.'.jpg';
						    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$img_name;  
							$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage1/'.$fname;       
							copy($src,$dest);
						}
						else if($counter==2)
						{
							$fname='Book_'.$storyname.'.jpg';
						    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$img_name;  
							$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage2/'.$fname;       
							copy($src,$dest);
						}
						else if($counter==3)
						{
							$fname='Book_'.$storyname.'.jpg';
						    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$img_name;  
							$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage3/'.$fname;       
							copy($src,$dest);
						}
						$counter++;
					}

					$this->Flash->success(__('Story image successfully uploaded.'));
					return $this->redirect(array('action' => 'images/'.$id.'/English'));
				}
				else
				{
					if(in_array($this->data['ImageNumber']['image_number'], $number_array))
					{
						$stories=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
						foreach($stories as $story)
						{
							$storyid=$story['Story']['id'];
							$options2 = array('conditions' => array('StoryImage.image_number'=>$this->data['ImageNumber']['h1'],'StoryImage.story_id'=>$storyid));
							$image=$this->StoryImage->find('first',$options2);
							$this->StoryImage->updateAll(
						    array('StoryImage.image_number' => 'StoryImage.image_number + 1'),array('StoryImage.image_number >=' => $this->data['ImageNumber']['image_number'],'StoryImage.story_id'=>$storyid));
							$this->StoryImage->id=$image['StoryImage']['id'];
							$array1=array('image_number'=>$this->data['ImageNumber']['image_number']);
							$this->StoryImage->save($array1);
							
							$new_images=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$id),'limit'=>3,'order'=>'image_number  ASC'));
							$counter=1;
							foreach($new_images as $new_image)
							{
								$image_name=$new_image['StoryImage']['image'];
								if($counter==1)
								{
								 	$fname='Book_'.$storyname.'.jpg';
								    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
									$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage1/'.$fname;       
									copy($src,$dest);

								}
								else if($counter==2)
								{
									$fname='Book_'.$storyname.'.jpg';
								    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
									$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage2/'.$fname;       
									copy($src,$dest);
								}
								else if($counter==3)
								{
									$fname='Book_'.$storyname.'.jpg';
								    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
									$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage3/'.$fname;       
									copy($src,$dest);
								}
								$counter++;
							}
						}
						$this->Flash->success(__('The story image number has been changed.'));
						return $this->redirect(array('action' => 'images/'.$id.'/English'));
					}
					else
					{
						$stories=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
						foreach($stories as $story)
						{
							$storyid=$story['Story']['id'];
							$options2 = array('conditions' => array('StoryImage.image_number'=>$this->data['ImageNumber']['h1'],'StoryImage.story_id'=>$storyid));
							$image=$this->StoryImage->find('first',$options2);
							$this->StoryImage->id=$image['StoryImage']['id'];
							$array1=array('image_number'=>$this->data['ImageNumber']['image_number']);
							$this->StoryImage->save($array1);
							
							$new_images=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$id),'limit'=>3,'order'=>'image_number  ASC'));
							$counter=1;
							foreach($new_images as $new_image)
							{
								$image_name=$new_image['StoryImage']['image'];
								if($counter==1)
								{
								 	$fname='Book_'.$storyname.'.jpg';
								    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
									$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage1/'.$fname;       
									copy($src,$dest);
								}
								else if($counter==2)
								{
									$fname='Book_'.$storyname.'.jpg';
								    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
									$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage2/'.$fname;       
									copy($src,$dest);
								}
								else if($counter==3)
								{
									$fname='Book_'.$storyname.'.jpg';
								    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
									$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage3/'.$fname;       
									copy($src,$dest);
								}
								$counter++;
							}
						}
						
						$this->Flash->success(__('The story image number has been changed.'));
						return $this->redirect(array('action' => 'images/'.$id.'/English'));
					}	
				}
			}
		}
		$this->set('language',$language);
	}




 /**
 * Edit image for story method
 * @param Int $id pass as story_image_id
  * @param Int $sid pass as unique story_id
 * @return void
 */
	public function resize_images($id=null) {
		$this->loadModel('StoryImage');
		$story=$this->Story->find('first',array('conditions'=>array('id'=>$id)));
		$story_name=$story['Story']['story_name'];
		$story_path=str_replace(' ', '_', $story_name);
		$images=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$id)));
		if (!file_exists(WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage')) {
			mkdir(WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage', 0777, true);
	    }
	    chmod(WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage',0777);
	    if (!file_exists(WWW_ROOT.'uploads/'.$story_path.'/ResizedStoryImage')) {
			mkdir(WWW_ROOT.'uploads/'.$story_path.'/ResizedStoryImage', 0777, true);
	    }
	    chmod(WWW_ROOT.'uploads/'.$story_path.'/ResizedStoryImage',0777);

	    if(!file_exists(WWW_ROOT.'uploads/'.$story_path.'/ResizedStoryImage/StoryImage.jpg'))
	    {
		    $temp_name=WWW_ROOT.'uploads/'.$story_path.'/StoryImage/StoryImage.jpg';
			$filename="StoryImage.jpg";
			$target_filename =WWW_ROOT.'uploads/'.$story_path.'/ResizedStoryImage/StoryImage.jpg';
	        $fn = $temp_name;
	        $size = getimagesize( $fn );
	        $width=1180;
	        $height=885;
	        $src = imagecreatefromstring( file_get_contents( $fn ) );
	        $dst = imagecreatetruecolor( $width, $height );
	        $quality=90;
	        imagealphablending($dst, false);
			imagesavealpha($dst, true);  
			imagealphablending($src, true);
	        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	        imagedestroy( $src );
	        imagejpeg( $dst, $target_filename,$quality); // adjust format as needed
	        imagedestroy( $dst );
	        chmod(WWW_ROOT.'uploads/'.$story_path.'/ResizedStoryImage/StoryImage.jpg',0777);
	    }

		foreach($images as $image)
		{
			$image_name=$image['StoryImage']['image'];
			$temp_name=WWW_ROOT.'uploads/'.$story_path.'/ChapterImage/'.$image_name;
			$filename=$image_name;
			if(file_exists(WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage/'.$filename))
			{
				unlink(WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage/'.$filename);
			}
			
			$target_filename =WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage/'.$filename;
	        $fn = $temp_name;
	        $size = getimagesize( $fn );
	        $width=1180;
	        $height=885;
	        $src = imagecreatefromstring( file_get_contents( $fn ) );
	        $dst = imagecreatetruecolor( $width, $height );
	        $quality=90;
	        imagealphablending($dst, false);
			imagesavealpha($dst, true);  
			imagealphablending($src, true);
	        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
	        imagedestroy( $src );
	        imagejpeg( $dst, $target_filename,$quality); // adjust format as needed
	        imagedestroy( $dst );
	        chmod(WWW_ROOT.'uploads/'.$story_path.'/ResizedChapterImage/'.$filename,0777);
			
		}
		$this->Flash->success(__('The resized story images has been uploaded.'));
		return $this->redirect(array('action' => 'images/'.$id.'/English'));
	}	


 /**
 * Edit image for story method
 * @param Int $id pass as story_image_id
  * @param Int $sid pass as unique story_id
 * @return void
 */
	public function editimage($id=null,$sid=null) {

		$this->loadModel('StoryImage');
		$this->StoryImage->id = $id;
		$options = array('conditions' => array('StoryImage.id'=>$id));
		$image=$this->StoryImage->find('all',$options);
		$story_image=$image[0]['StoryImage']['image'];
		$st2=$image[0]['StoryImage']['image'];
		$options2 = array('conditions' => array('Story.id' => $sid));
		$story_name=$this->Story->find('first',$options2);
		$storyname=$story_name['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname);
		$this->set('file',$image);
		if ($this->request->is('post')) {
			if(empty($this->data['Story']['image']['name'])){
				$this->Flash->error(__('Please select story image'));
			}
				$file=$this->data['Story']['image'];
				$tmp_name=$file['tmp_name'];
				$file_name=$file['name'];
				
						if (!file_exists(WWW_ROOT.'uploads/'.$story_path."/ChapterImage")) {
		     				mkdir(WWW_ROOT.'uploads/'.$story_path."/ChapterImage", 0777, true);
					    }
					    unlink(WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$story_image);
						move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$st2);
					
					$this->Flash->success(__('The story image has been changed.'));
					return $this->redirect(array('action' => 'statements/'.$sid));	
		}
		$this->set('sid',$sid);
		$this->set('storyname',$story_path);
	}

        
   
        
/**
 * Change scene for story method
 * @param Int $id pass as story_image_id
  * @param Int $sid pass as unique story_id
 * @return void
 */
	public function changescene($id=null,$sid=null,$language=null) {
            $this->loadModel('StoryImage');
            $options2 = array('conditions' => array('id' => $id));
            $scene=$this->StoryImage->find('first',$options2);
            $scene_name=$scene['StoryImage']['image'];
            $options3 = array('conditions' => array('Story.id' => $sid));
            $story_name=$this->Story->find('first',$options3);
            $storyname=$story_name['Story']['story_name'];
            $story_path=str_replace(' ', '_', $storyname);
            $this->set(compact('scene_name','story_path','language','sid'));
            if ($this->request->is('post')) 
            {
                $tmp_name=$this->request->data['Scene']['image']['tmp_name'];
                move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$scene_name);
            	chmod(WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$scene_name,0777);	

            	$imgs=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$sid),'limit'=>3,'order'=>'image_number ASC'));
            	$counter=1;
            	foreach($imgs as $img)
            	{
            		if($img['StoryImage']['image']==$scene_name)
            		{
            			$fname='Book_'.$storyname.'.jpg';
					    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$scene_name;  
						$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage'.$counter.'/'.$fname;       
						copy($src,$dest);
            		}
            		$counter++;
            	}

                $this->Flash->success(__('The scene image has been changed.'));
                return $this->redirect(array('action' => 'images/'.$sid.'/'.$language));
            }
            
        }




  	/**
 	* Disable/Enable method to disable or enable audio option 
 	* @return void
 	*/
	public function disable($id=null,$story_id=null,$val=null,$image_number=null,$image_id=null) {
		$this->loadModel('StoryStatement');
		$statement=$this->StoryStatement->find('first',array('conditions'=>array('id' => $id)));
		$clip_name=$statement['StoryStatement']['audio_clip'];
		$statements=$this->StoryStatement->find('all',array('conditions'=>array('audio_clip'=>$clip_name)));
		foreach($statements as $st)
		{
			$statement_id=$st['StoryStatement']['id']."<br>";
			
			$this->StoryStatement->id=$statement_id;
			$this->StoryStatement->save(array('disable'=>$val));
	    	
		}
		return $this->redirect(array('action' => 'statements/'.$story_id.'/'.$image_id.'/'.$image_number.'/English'));
	
	}



	/**
 	* Disable/Enable method to disable or enable introduction audio option
 	* @return void
 	*/
	public function disable_introduction($id=null,$val=null) {
		$story=$this->Story->find('first',array('conditions'=>array('id' => $id)));
		$story_name=$story['Story']['story_name'];

		$stories=$this->Story->find('all',array('conditions'=>array('story_name'=>$story_name)));
		foreach($stories as $story)
		{
			$this->Story->id=$story['Story']['id'];
			$this->Story->save(array('disable'=>$val));
		}
		return $this->redirect(array('action' => 'introduction_audio/'.$id));	
	}




 /**
 * View statement list method
 * @param Int $id pass as unique story_id 
 * @return void
 */


	public function statements($id=null,$image_id=null,$image_number=null,$language=null) {
	
		$this->loadModel('StoryStatement');
		$this->loadModel('StoryImage');
		$options2 = array('conditions' => array('Story.id' => $id));
		$story_name=$this->Story->find('first',$options2);
		$storyname=$story_name['Story']['story_name'];
		$storyname1=$storyname;
		$story_path=str_replace(' ', '_', $storyname);

		$options1 = array('conditions' => array('StoryImage.id' => $image_id));
		$story_image=$this->StoryImage->find('first',$options1);
		$storyimage=$story_image['StoryImage']['image'];
		$imagename=substr($storyimage, 0, -4);
		$cnt=rand(111,999);
		$filename=$imagename.'_Statement'.$cnt.'.mp3';

		$options = array('conditions' => array('image_number' => $image_number,'story_id'=>$id));
		if (empty($this->StoryImage->find('all',$options))) {
			$this->Flash->error('No Images are there to add statement');
		}
		$images=$this->StoryImage->find('all',$options);
		$image_id=$images[0]['StoryImage']['id'];

		$option4 = array('conditions' => array('story_id'=>$id),'order'=>'StoryImage.image_number ASC');
		$images_all=$this->StoryImage->find('all',$option4);
		$number_array=array();
		foreach($images_all as $img1)
		{
			array_push($number_array,$img1['StoryImage']['image_number']);
		}
		$l=sizeof($number_array);
		for($i=0;$i<$l;$i++)
		{
			$first=$number_array[0];
			$last=$number_array[$l-1];
			if($number_array[$i]==$image_number)
			{
				$previous=$number_array[$i-1];
				$next=$number_array[$i+1];
			}
		}
		foreach($images_all as $img1)
		{
			if($previous==$img1['StoryImage']['image_number'])
			{
				$previous_image_id=$img1['StoryImage']['id'];
			}
			if($next==$img1['StoryImage']['image_number'])
			{
				$next_image_id=$img1['StoryImage']['id'];
			}
		}

		$this->paginate = array(
        'conditions' => array('StoryStatement.story_image_id' => $image_id),
        'limit' => 5
	    );
	     
	    $statements = $this->paginate('StoryStatement');
	    $this->set(compact('statements','language','image_id','image_number','previous','next','first','last','previous_image_id','next_image_id','story_path'));

		$this->set('image',$images);
		$this->set('sid',$id);
		$this->set('storyname',$story_path);

		if ($this->request->is('post')) 
		{
			$this->loadModel('Apitoken');
			$api=$this->Apitoken->get_active_api();

			if(!empty($this->data['Statement']['h1']))
			{
				$audio_name=$this->data['Statement']['h3'];
				$tmp_name=$this->data['Statement']['audio_clip']['tmp_name'];
				if($language=='English')
				{
					$audio_name=$this->data['Statement']['h3'];
					$tmp_name=$this->data['Statement']['audio_clip']['tmp_name'];

					$stmt=$this->data['Statement']['statement'];
					$stmt_old=$this->data['Statement']['h2'];
					$this->StoryStatement->id=$this->data['Statement']['h1'];
					
					if(empty($audio_name))
					{
						$audio_name=$imagename.'_Statement'.$cnt.'.mp3';
					}

					if($stmt!=$stmt_old)
					{
						//poeditor
					$HttpSocket = new HttpSocket();
					$project_array1=array('api_token' => $api,'action'=>'list_projects');
					$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
					$list_projects1 = json_decode($list_project1, true);
					foreach($list_projects1['list'] as $project)
					{
						if($project['name']==$this->Session->read('project_name')){
							$projectid=$project['id'];
						}
					}

					//delete statement from poeditor
					$arr1=array("term"=>$stmt_old,"context"=>"(".$storyname1.")");
				    $data = array(
			           "api_token" => $api,
			           "action" => "delete_terms",
			           "id" => $projectid,
			           "data" => json_encode(array($arr1),true));
			    	$request=array();
			    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

			    	//add statement to poeditor
					$arr1=array("term"=>$stmt,"context"=>"(".$storyname1.")");
				    $data = array(
			           "api_token" => $api,
			           "action" => "add_terms",
			           "id" => $projectid,
			           "data" => json_encode(array($arr1),true));
			    	$request=array();
			    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
			    	
						//save data to database
						$stmts=$this->StoryStatement->find('all',array('conditions'=>array('StoryStatement.audio_clip'=>$audio_name)));
						foreach($stmts as $st)
						{
							if($st['StoryStatement']['story_id']==$id)
							{
							$this->StoryStatement->id=$st['StoryStatement']['id'];
							$this->StoryStatement->save(array('statement'=>$stmt));
							}
							else
							{
							$this->StoryStatement->id=$st['StoryStatement']['id'];
							$this->StoryStatement->save(array('statement'=>'No Translation'));
							}
						}
					}

					
					if (!file_exists(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language)) {
	     				mkdir(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language, 0777, true);
				    }
			    	chmod(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language,0777);
				 	
				 	unlink(WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name);
				 	$path=$tmp_name;
				    $intro_audio=WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name;
					$cmd="ffmpeg -i $path -acodec libmp3lame $intro_audio";
					shell_exec($cmd);

					//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name);
					chmod(WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name,0777);

					$this->Flash->success(__('The story statement has been changed.'));
					return $this->redirect(array('action' => 'statements/'.$id.'/'.$image_id.'/'.$image_number.'/'.$language));
				}
				else
				{

					if (!file_exists(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language)) {
	     				mkdir(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language, 0777, true);
                    }
                    chmod(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language,0777);
                    
                    unlink(WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name);
     				$path=$tmp_name;
				    $intro_audio=WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name;
					$cmd="ffmpeg -i $path -acodec libmp3lame $intro_audio";
					shell_exec($cmd);

					//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name);                         
                	chmod(WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$audio_name,0777);
                }
			}
			else
			{

			
			$find_existfile=$this->StoryStatement->find('first',array('conditions'=>array('audio_clip'=>$filename)));
			if(!empty($find_existfile))
			{
				$cnt=rand(111,999);
				$filename=$imagename.'_Statement'.$cnt.'.mp3';
			}
			
			$tmp_name=$this->data['statement']['audio_clip']['tmp_name'];
			$statement=$this->data['statement']['statement'];
			
			//save data to database
			$stories=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
			foreach($stories as $story)
			{
				$this->StoryStatement->create();
				$images=$this->StoryImage->find('first',array('conditions'=>array('story_id'=>$story['Story']['id'],'image_number'=>$image_number)));
				$img_id=$images['StoryImage']['id'];
				if($story['Story']['id']==$id)
				{
				$array=array('story_id'=>$story['Story']['id'],'audio_clip'=>$filename,'statement'=>$statement,'story_image_id'=>$img_id);
				}
				else
				{
				$array=array('story_id'=>$story['Story']['id'],'audio_clip'=>$filename,'statement'=>'No Translation','story_image_id'=>$img_id);
				}
				$this->StoryStatement->save($array);
			}
			
			if (!file_exists(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language)) {
 				mkdir(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language, 0777, true);
		    }
		    chmod(WWW_ROOT.'uploads/'.$story_path.'/StatementAudio/'.$language,0777);
	  		
		    $path=$tmp_name;
		    $intro_audio=WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$filename;
			$cmd="ffmpeg -i $path -acodec libmp3lame $intro_audio";
			shell_exec($cmd);

			//move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$filename);
			chmod(WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/'.$language.'/'.$filename,0777);
			}
			//poeditor
			$HttpSocket = new HttpSocket();
			$project_array1=array('api_token' => $api,'action'=>'list_projects');
			$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
			$list_projects1 = json_decode($list_project1, true);
			foreach($list_projects1['list'] as $project)
			{
				if($project['name']==$this->Session->read('project_name')){
					$projectid=$project['id'];
				}
			}
			
			//add statement to poeditor
			$arr1=array("term"=>$statement,"context"=>"(".$storyname1.")");
		    $data = array(
	           "api_token" => $api,
	           "action" => "add_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr1),true));

	    	$request=array();
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

			$this->Flash->success(__('The story statement has been saved.'));
			return $this->redirect(array('action' => 'statements/'.$id.'/'.$image_id.'/'.$image_number.'/'.$language));
		}
		
	}

 
 /**
 * Delete statement for story images method
 *
 * @throws NotFoundException
 * @param Int $id pass as unique story_statement_id
 * @param Int $sid pass as unique story_id
 * @param Int $img_id pass as story_image_id
 * @return void
 */
	public function deletestatement($id=null,$sid=null,$img_id=null,$image_number=null) {
		$this->loadModel('Apitoken');
		$api=$this->Apitoken->get_active_api(); 
		$this->loadModel('StoryStatement');
		$this->StoryStatement->id = $id;
		$options = array('conditions' => array('StoryStatement.id'=>$id));
		$statements=$this->StoryStatement->find('all',$options);
		$statement=$statements[0]['StoryStatement']['statement'];
		$audio_file=$statements[0]['StoryStatement']['audio_clip'];
		$options1 = array('conditions' => array('Story.id' => $sid));
		$story_name=$this->Story->find('first',$options1);
		$storyname=$story_name['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname);
		//get project_id
		$project_array2=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
		$list_projects2 = json_decode($list_project2, true);
		foreach($list_projects2['list'] as $project2)
		{
			if($project2['name']==$this->Session->read('project_name')){
				$projectid=$project2['id'];
			}
		}
		$options2 = array('conditions' => array('story_id' => $sid,'statement'=>$statement));
		$find_exist_statement=$this->StoryStatement->find('all',$options2);
		$l=sizeof($find_exist_statement);

		if($l<2)
		{
			//delete selected term
			$arr1=array("term"=>$statement,"context"=>"(".$storyname.")");
		    $data = array(
		       "api_token" => $api,
		       "action" => "delete_terms",
		       "id" => $projectid,
		       "data" => json_encode(array($arr1),true));
			$request=array();
			$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
    	}

		if (!$this->StoryStatement->exists()) {
			throw new NotFoundException(__('Invalid story statement'));
		}
		$lang_statements=$this->StoryStatement->find('all',array('conditions'=>array('audio_clip'=>$audio_file)));
		foreach($lang_statements as $stmt)
		{
			$st_id=$stmt['StoryStatement']['id'];
			$this->StoryStatement->id=$st_id;
			$this->StoryStatement->delete();
		}
		unlink(WWW_ROOT . 'uploads/'.$story_path.'/StatementAudio/English/'.$audio_file);
		
		$this->Flash->success(__('The story statement has been deleted.'));
		
		return $this->redirect(array('action' => 'statements/'.$sid.'/'.$img_id.'/'.$image_number.'/English'));
	}



 /**
 * delete Image method
 *
 * @throws NotFoundException
 * @param Int $id pass as Story_image_id
 * @param Int $sid pass as unique story_id
 * @return void
 */
	public function deleteimage($id = null,$sid=null) {
		$this->loadModel('StoryImage');
		$this->loadModel('StoryStatement');
		$this->loadModel('Apitoken');
		$api=$this->Apitoken->get_active_api(); 

		$options1 = array('conditions' => array('Story.id' => $sid));
		$story_name=$this->Story->find('first',$options1);
		$storyname=$story_name['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname);
		
		//get project_id
		$project_array2=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
		$list_projects2 = json_decode($list_project2, true);
		foreach($list_projects2['list'] as $project2)
		{
			if($project2['name']==$this->Session->read('project_name')){
				$projectid=$project2['id'];
			}
		}
		
		$options = array('conditions' => array('StoryStatement.story_image_id'=>$id));
		$statements=$this->StoryStatement->find('all',$options);
		
		foreach($statements as $statement)
		{
			$term=$statement['StoryStatement']['statement'];
			//delete selected term
			$arr1=array("term"=>$term,"context"=>"(".$storyname.")");
		    $data = array(
	           "api_token" => $api,
	           "action" => "delete_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr1),true));
	    	$request=array();
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
		}
		
		$story_images=$this->StoryImage->find('first',array('conditions'=>array('id'=>$id)));
		$images=$this->StoryImage->find('all',array('conditions'=>array('image'=>$story_images['StoryImage']['image'])));

		foreach($images as $image)
		{
			$this->StoryImage->id=$image['StoryImage']['id'];
			$this->StoryImage->delete();
		}

		$new_images=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$sid),'limit'=>3,'order'=>'image_number  ASC'));
		$counter=1;
		foreach($new_images as $new_image)
		{
			$image_name=$new_image['StoryImage']['image'];
			if($counter==1)
			{
			 	$fname='Book_'.$storyname.'.jpg';
			    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
				$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage1/'.$fname;       
				copy($src,$dest);
			}
			else if($counter==2)
			{
				$fname='Book_'.$storyname.'.jpg';
			    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
				$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage2/'.$fname;       
				copy($src,$dest);
			}
			else if($counter==3)
			{
				$fname='Book_'.$storyname.'.jpg';
			    $src = WWW_ROOT . 'uploads/'.$story_path.'/ChapterImage/'.$image_name;  
				$dest = WWW_ROOT . 'uploads/StoryInfo/LeadingPage3/'.$fname;       
				copy($src,$dest);
			}
			$counter++;
		}

		$this->Flash->success(__('The story image has been deleted.'));		
		return $this->redirect(array('action' => 'images/'.$sid.'/English'));
	}



 /**
 * Edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->loadModel('Apitoken');
		$this->loadModel('StoryStatement');
		$this->loadModel('ModeQuestionAnswer');
		$this->loadModel('StoryImage');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('StoryTranslation');

		$api=$this->Apitoken->get_active_api(); 
		$this->loadModel('Language');

		if (!$this->Story->exists($id)) {
			throw new NotFoundException(__('Invalid story'));
		}
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$options2 = array('conditions' => array('Story.id' => $id));
		$story_image = $this->Story->find('first',$options2);
		$image=$story_image['Story']['image'];
		$introduction_old=$story_image['Story']['introduction'];
		$storyname2=$story_image['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname2);
		$story_language=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname2)));
		$lang_array=array();
		foreach($story_language as $lang)
		{
			$languages=$this->Language->find('first',array('conditions'=>array('Language.id'=>$lang['Story']['language_id'])));
			array_push($lang_array, $languages['Language']['language_name']);
		}

		if ($this->request->is(array('post', 'put'))) {
	
			$introduction=$this->data['Story']['introduction'];
			$story_name=$this->data['Story']['story_name'];
			$file_name=$this->request->data['Story']['image']['name'];
			$tmp_name=$this->request->data['Story']['image']['tmp_name'];
			$story_path=str_replace(' ', '_', $story_name);
			$story_path2=str_replace(' ', '_', $storyname2);
			rename (WWW_ROOT.'uploads/'.$story_path2,WWW_ROOT.'uploads/'.$story_path);
			rename (WWW_ROOT.'uploads/Words/Words_Audio/'.$story_path2,WWW_ROOT.'uploads/Words/Words_Audio/'.$story_path);
			rename (WWW_ROOT.'uploads/Words/Words_Image/'.$story_path2,WWW_ROOT.'uploads/Words/Words_Image/'.$story_path);
			rename (WWW_ROOT.'uploads/Questions/'.$story_path2,WWW_ROOT.'uploads/Questions/'.$story_path);
			rename (WWW_ROOT.'uploads/Maths_Images/'.$story_path2,WWW_ROOT.'uploads/Maths_Images/'.$story_path);

			$old_name='Book_'.$storyname2.'.jpg';
			$new_name='Book_'.$story_name.'.jpg';

			rename (WWW_ROOT.'uploads/StoryInfo/LeadingPage1/'.$old_name,WWW_ROOT.'uploads/StoryInfo/LeadingPage1/'.$new_name);
			rename (WWW_ROOT.'uploads/StoryInfo/LeadingPage2/'.$old_name,WWW_ROOT.'uploads/StoryInfo/LeadingPage2/'.$new_name);
			rename (WWW_ROOT.'uploads/StoryInfo/LeadingPage3/'.$old_name,WWW_ROOT.'uploads/StoryInfo/LeadingPage3/'.$new_name);

			rename (WWW_ROOT.'uploads/StoryInfo/LeadImage/'.$old_name,WWW_ROOT.'uploads/StoryInfo/LeadImage/'.$new_name);
			if(empty($file_name))
			{
				$file_name='StoryImage.jpg';
				$this->request->data['Story']['image']='StoryImage.jpg';
			}
			else
			{
				if (!file_exists(WWW_ROOT.'uploads/'.$story_path."/StoryImage")) {
	     				mkdir(WWW_ROOT.'uploads/'.$story_path."/StoryImage", 0777, true);
				}
				chmod(WWW_ROOT.'uploads/'.$story_path,0777);
				move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/StoryImage/StoryImage.jpg');
				chmod(WWW_ROOT . 'uploads/'.$story_path.'/StoryImage/StoryImage.jpg',0777);
				$file_name='StoryImage.jpg';

			    $book_id=$id;
			    $fname='Book_'.$story_name.'.jpg';
			    $src = WWW_ROOT . 'uploads/'.$story_path.'/StoryImage/StoryImage.jpg';  
				$dest = WWW_ROOT . 'uploads/StoryInfo/LeadImage/'.$fname;       
				copy($src,$dest);

			}
			
			foreach($lang_array as $lang)
			{
				if(!in_array($lang, $this->data['Language']))
				{
					if($lang=='English')
					{
						$this->Flash->error(__('The Source language can not remove.'));
						return $this->redirect(array('action' => 'edit/'.$id));
					}
					else
					{
						$langs=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$lang)));
						$lg_id=$langs['Language']['id'];
						$story=$this->Story->find('first',array('conditions'=>array('language_id'=>$lg_id,'story_name'=>$storyname2)));
						$words=$this->Word->find('all',array('conditions'=>array('Word.story_id'=>$story['Story']['id'])));
						foreach($words as $word)
						{
							$this->Word->id=$word['Word']['id'];
							$this->Word->delete();
						}
						$questions1=$this->Question->find('all',array('conditions'=>array('Question.story_id'=>$story['Story']['id'])));
						foreach($questions1 as $question1)
						{
							$this->Question->id=$question1['Question']['id'];
							$this->Question->delete();
						}

						$translation=$this->StoryTranslation->find('first',array('conditions'=>array('story_id'=>$story['Story']['id'])));
						$this->StoryTranslation->id=$translation['StoryTranslation']['id'];
						$this->StoryTranslation->delete();

						$this->Story->id=$story['Story']['id'];
						$this->Story->delete();
					}
				}
			}
		
			$language_id=$this->data['Story']['language_id'];
			$options = array('conditions' => array('Language.id' => $language_id));
			$lang = $this->Language->find('first',$options);
			$story_language=$lang['Language']['language_name'];

			//get story id
			$project_array1=array('api_token' => $api,'action'=>'list_projects');
			$HttpSocket = new HttpSocket();
			$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
			$list_projects1 = json_decode($list_project1, true);
			foreach($list_projects1['list'] as $project)
			{
				if($project['name']==$this->Session->read('project_name')){
					$projectid=$project['id'];
				}
			}

			if($introduction!=$introduction_old)
			{
				//delete old terms 
				$arr1=array("term"=>$introduction_old,"context"=>"Introduction");
			    $data = array(
		           "api_token" => $api,
		           "action" => "delete_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr1),true));
		    	$request=array();
		    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

				//add new terms 
				$arr1=array("term"=>$introduction,"context"=>"Introduction");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr1),true));
		    	$request=array();
		    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

			}

			if($storyname2!=$story_name)
			{
			//delete old terms 
			$arr1=array("term"=>$storyname2,"context"=>"story_name");
		    $data = array(
	           "api_token" => $api,
	           "action" => "delete_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr1),true));
	    	$request=array();
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

			//add new terms 
			$arr1=array("term"=>$story_name,"context"=>"story_name");
		    $data = array(
	           "api_token" => $api,
	           "action" => "add_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr1),true));
	    	$request=array();
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

	    	$project_array1=array('api_token' => $api,'action'=>'view_terms','id'=>$projectid);
			$terms_array = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
			$terms = json_decode($terms_array, true);

			foreach($terms['list'] as $term)
			{
				if($term['context']=="(".$storyname2.")")
				{
					$term1=$term['term'];
					$context=$term['context'];
					//delete old terms 
					$arr1=array("term"=>$term1,"context"=>$context);
				    $data = array(
			           "api_token" => $api,
			           "action" => "delete_terms",
			           "id" => $projectid,
			           "data" => json_encode(array($arr1),true));
			    	$request=array();
			    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

			    	//add new terms 
					$arr1=array("term"=>$term1,"context"=>"(".$story_name.")");
				    $data = array(
			           "api_token" => $api,
			           "action" => "add_terms",
			           "id" => $projectid,
			           "data" => json_encode(array($arr1),true));
			    	$request=array();
			    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
				}
			}
		}

			foreach($this->data['Language'] as $lang)
			{
				if(!in_array($lang,$lang_array))
				{
					$langs=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$lang)));
					$this->Story->create();
					$this->Story->save(array('story_name'=>$storyname2,'introduction'=>$introduction,'image'=>$image,'language_id'=>$langs['Language']['id'],'active'=>0));

					$last_id=$this->Story->getLastInsertId();
					$words=$this->Word->find('all',array('conditions'=>array('Word.story_id'=>$id)));
					foreach($words as $word)
					{
						$word_name=$word['Word']['word'];
						$story=$word['Word']['id'];
						$audio_clip1=$word['Word']['audio_clip'];
						$this->Word->create();
						$this->Word->save(array('word'=>'No Translation','audio_clip'=>$audio_clip1,'language_id'=>$langs['Language']['id'],'story_id'=>$last_id,'story'=>$story));
					}

					$questions=$this->Question->find('all',array('conditions'=>array('Question.story_id'=>$id)));
					foreach($questions as $question)
					{
						$story1=$question['Question']['id'];
						$this->Question->create();
						$this->Question->save(array('image'=>$question['Question']['image'],'question'=>'No Translation','option1'=>'No Translation','option2'=>'No Translation','option3'=>'No Translation','answer'=>'No Translation','language_id'=>$langs['Language']['id'],'story_id'=>$last_id,'story'=>$story1,'question_audio'=>$question['Question']['question_audio'],'answer_audio'=>$question['Question']['answer_audio'],'option1_audio'=>$question['Question']['option1_audio'],'option2_audio'=>$question['Question']['option2_audio'],'option3_audio'=>$question['Question']['option3_audio']));
					}

					$this->StoryTranslation->create();
					$this->StoryTranslation->save(array('story_id'=>$last_id,'language_id'=>$langs['Language']['id'],'story_name'=>$storyname2,'translation'=>'No Translation'));

					//add_language in poeditor
					$lang1=$this->Language->getLocaleCodeForDisplayLanguage($lang);
					$project_array1=array('api_token' => $api,'id'=>$projectid,'action'=>'add_language','language'=>$lang1);
					$HttpSocket = new HttpSocket();
					$add_lang= $HttpSocket->post('https://poeditor.com/api/', $project_array1);
					$this->Story->getLastInsertId();
					//add images for different language story
					$images=$this->StoryImage->find('all',array('conditions'=>array('story_id'=>$id)));
					foreach($images as $image)
					{
						$image_id=$image['StoryImage']['id'];
						$this->StoryImage->create();
						$this->StoryImage->save(array('story_id'=>$this->Story->getLastInsertId(),'image_number'=>$image['StoryImage']['image_number'],'image'=>$image['StoryImage']['image']));
						//add statements for different language story
						$statements=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$id,'story_image_id'=>$image_id)));
						foreach($statements as $statement)
						{
							$this->StoryStatement->create();
							$this->StoryStatement->save(array('story_id'=>$this->Story->getLastInsertId(),'story_image_id'=>$this->StoryImage->getLastInsertId(),'statement'=>'No Translation','audio_clip'=>$statement['StoryStatement']['audio_clip']));
						}	
					}
				}
			}

			$story_array=array();
			$stories=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname2)));
			foreach($stories as $story)
			{
				if($story['Story']['language_id']==$english_id)
				{
					$this->Story->id=$story['Story']['id'];
					$this->Story->save(array('story_name'=>$story_name,'introduction'=>$introduction,'image'=>$file_name));
				}
				else
				{
					$this->Story->id=$story['Story']['id'];
					$this->Story->save(array('story_name'=>$story_name,'introduction'=>'No Translation','image'=>$file_name));
				}
				
			}
			$this->Flash->success(__('The story has been saved.'));
			return $this->redirect(array('action' => 'edit/'.$id));
			
		} else {
			$options = array('conditions' => array('Story.' . $this->Story->primaryKey => $id));
			$this->request->data = $this->Story->find('first', $options);
		}
		$languages = $this->Story->Language->find('list',array('fields'=>array('Language.id','Language.language_name')));
		$this->set(compact('languages','image','storyname2','story_path','introduction_old','lang_array'));
	}



 /**
 * Delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_audio($path=null,$language=null,$file=null,$story_id=null,$image_id=null,$image_number=null) {
		unlink(WWW_ROOT.'uploads/'.$path.'/StatementAudio/'.$language.'/'.$file);
		return $this->redirect(array('action' => 'statements',$story_id,$image_id,$image_number,$language));
		
	}



 /**
 * Delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->loadModel('Apitoken');
		$this->loadModel('StoryStatement');
		$this->loadModel('Language');
		$this->loadModel('Word');
		$this->loadModel('Question');
		$this->loadModel('Grids');
		$this->loadModel('Scene');
		$this->loadModel('StoryTranslation');

		$api=$this->Apitoken->get_active_api(); 
		$this->Story->id = $id;
		$story=$this->Story->find('first',array('conditions'=>array('Story.id'=>$id)));
		$storyname=$story['Story']['story_name'];
		$introduction=$story['Story']['introduction'];
		$story_path=str_replace(' ', '_', $storyname);

		$traslations=$this->StoryTranslation->find('all',array('conditions'=>array('story_name'=>$storyname)));
		foreach($traslations as $translation)
		{
			$this->StoryTranslation->id=$translation['StoryTranslation']['id'];
			$this->StoryTranslation->delete();
		}


		//to delete images from StoryInfo folder when delete story
		$file_name='Book_'.$storyname.'.jpg';
		unlink(WWW_ROOT.'uploads/StoryInfo/LeadImage/'.$file_name);
		unlink(WWW_ROOT.'uploads/StoryInfo/LeadingPage1/'.$file_name);
		unlink(WWW_ROOT.'uploads/StoryInfo/LeadingPage2/'.$file_name);
		unlink(WWW_ROOT.'uploads/StoryInfo/LeadingPage3/'.$file_name);
		unlink(WWW_ROOT.'uploads/'.$story_path.'/StoryImage');
		unlink(WWW_ROOT.'uploads/'.$story_path);

		//get project_id
		$project_array2=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
		$list_projects2 = json_decode($list_project2, true);
		foreach($list_projects2['list'] as $project2)
		{
			if($project2['name']==$this->Session->read('project_name')){
				$projectid=$project2['id'];
			}
		}
		
		//delete selected term
		$arr1=array("term"=>$storyname,"context"=>"story_name");
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $projectid,
           "data" => json_encode(array($arr1),true));
    	$request=array();
    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

    	//delete selected term
		$arr11=array("term"=>$introduction,"context"=>"Introduction");
	    $data11 = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $projectid,
           "data" => json_encode(array($arr11),true));
    	$request=array();
    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

    	$statements=$this->StoryStatement->find('all',array('conditions'=>array('StoryStatement.story_id'=>$id)));
		foreach($statements as $statement)
		{
			$stmt=$statement['StoryStatement']['statement'];
			//delete selected term
			$arr1=array("term"=>$stmt,"context"=>"(".$storyname.")","plural"=>"statement");
		    $data = array(
	           "api_token" => $api,
	           "action" => "delete_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr1),true));
	    	$request=array();
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
		}
	
		$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$english['Language']['id'];
		$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$storyname,'language_id'=>$english_id)));
		$english_story_id=$english_story['Story']['id'];

		$words=$this->Word->find('all',array('conditions'=>array('story_id'=>$english_story_id)));

		foreach($words as $word)
		{
			$arr1=array("term"=>$word['Word']['word'],"context"=>"(".$storyname.")","plural"=>"Word");
		    $data = array(
	           "api_token" => $api,
	           "action" => "delete_terms",
	           "id" => $projectid,
	           "data" => json_encode(array($arr1),true));
	    	$request=array();
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
	    	$word_id=$word['Word']['id'];
	    	$words_to_delete=$this->Word->find('all',array('conditions'=>array('story'=>$word_id)));
	    	foreach($words_to_delete as $w)
	    	{
	    		$this->Word->id=$w['Word']['id'];
	    		$this->Word->delete();
	    	}
	    	$this->Word->id=$word_id;
	    	$this->Word->delete();
		}

		$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$english_story_id)));

		foreach($questions as $question)
		{
			//delete old questions from poeditor
			$cnt=substr($question['Question']['question_audio'], -7, -4);
			$arr1=array("term"=>$question['Question']['question'],"context"=>"Question".$cnt,"plural"=>"(".$story_name.")");
			$arr2=array("term"=>$question['Question']['answer'],"context"=>"Answer".$cnt,"plural"=>"(".$story_name.")");
			$arr3=array("term"=>$question['Question']['option1'],"context"=>"Option 1(".$cnt.")","plural"=>"(".$story_name.")");
			$arr4=array("term"=>$question['Question']['option2'],"context"=>"Option 2(".$cnt.")","plural"=>"(".$story_name.")");
			$arr5=array("term"=>$question['Question']['option3'],"context"=>"Option 3(".$cnt.")","plural"=>"(".$story_name.")");

		    $data1= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr1),true));
		    $data2= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr2),true));
		    $data3= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr3),true));
		    $data4= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr4),true));
		    $data5= array("api_token" => $api,"action" => "delete_terms","id" => $projectid,"data" => json_encode(array($arr5),true));
		   	
		   	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data1,$request);
	    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
	    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data3,$request);
	    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data4,$request);
	    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data5,$request);
			
	    	$question_id=$question['Question']['id'];
	    	$questions_to_delete=$this->Question->find('all',array('conditions'=>array('story'=>$question_id)));
	    	foreach($questions_to_delete as $q)
	    	{
	    		$this->Question->id=$q['Question']['id'];
	    		$this->Question->delete();
	    	}
	    	$this->Question->id=$question_id;
	    	$this->Question->delete();
		}

		$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$english_story_id)));
		foreach($scenes as $scene)
		{
			$scene_id=$scene['Scene']['id'];
			$grids=$this->Grids->find('all',array('conditions'=>array('scene_id'=>$scene_id)));
			foreach($grids as $grid)
			{
				$this->Grids->id=$grid['Grids']['id'];
				$this->Grids->delete();
			}
			$this->Scene->id=$scene_id;
			$this->Scene->delete();
		}

		$stories=$this->Story->find('all',array('conditions'=>array('story_name'=>$storyname)));
		foreach($stories as $story)
		{
			$statements_to_delete=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$story['Story']['id'])));
			foreach($statements_to_delete as $st)
			{
				$this->StoryStatement->id=$st['StoryStatement']['id'];
				$this->StoryStatement->delete();
			}
			$this->Story->id=$story['Story']['id'];

			$this->Story->delete();
		}

		//Delete extra languages from POEditor
		$lang2_array=array();
		//get language list from project
		$lang_array=array('api_token' => $api,'action'=>'list_languages','id'=>$projectid);
		$list_languages = $HttpSocket->post('https://poeditor.com/api/', $lang_array);
		$language_list = json_decode($list_languages, true);
		foreach($language_list['list'] as $lan)
		{
			array_push($lang2_array, $lan['code']);
		}
		$lang1_array=array();
		$story_languages=$this->Story->find('all', array('fields'=>'DISTINCT language_id'));
		foreach($story_languages as $sl)
		{
			$lang_id=$sl['Story']['language_id'];
			$languages=$this->Language->find('first',array('conditions'=>array('id'=>$lang_id)));
			$lang1=$this->Language->getLocaleCodeForDisplayLanguage($languages['Language']['language_name']);
			array_push($lang1_array,$lang1);
		}	

		$diff=array();
		$diff=array_diff($lang2_array,$lang1_array);
		foreach($diff as $dif)
		{
			//get language list from project
			$lang_array1=array('api_token' => $api,'action'=>'delete_language','id'=>$projectid,'language'=>$dif);
			$lang_delete = $HttpSocket->post('https://poeditor.com/api/', $lang_array1);
		}
		rmdir(WWW_ROOT.'uploads/'.$story_path);
		$this->Flash->success(__('The story has been deleted.'));
		return $this->redirect(array('action' => 'index'));
	}


 /**
 * Sync_To_POEditor method
 *
 * @throws NotFoundException
 * @param  no parameters are there
 * @return void
 */
	public function sync_to_poeditor() {
		$this->loadModel('Apitoken');
		$this->loadModel('StoryStatement');
		$this->loadModel('StoryTranslation');
		$this->loadModel('Language');
		$api=$this->Apitoken->get_active_api();
		$HttpSocket = new HttpSocket();
		//get project_id from POEditor for story
		$project_array2=array('api_token' => $api,'action'=>'list_projects');
		$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
		$list_projects2 = json_decode($list_project2, true);
		foreach($list_projects2['list'] as $project2)
		{
			if($project2['name']==$this->Session->read('project_name')){
				$projectid=$project2['id'];
			}
		}

		//get language list from project
		$lang_array=array('api_token' => $api,'action'=>'list_languages','id'=>$projectid);
		$list_languages = $HttpSocket->post('https://poeditor.com/api/', $lang_array);
		$language_list = json_decode($list_languages, true);
		$language_array=array();
		$language_code_array=array();
		foreach($language_list['list'] as $language)
		{
			if($language['name']=='Chinese (simplified)')
			{
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage('Chinese');
				array_push($language_array,'Chinese');
				array_push($language_code_array,$language_code);
			}
			else
			{
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language['name']);
				array_push($language_array,$language['name']);
				array_push($language_code_array,$language_code);
			}
		}
		$l=sizeof($language_array);

		$get_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id1=$get_language['Language']['id'];

		//view all terms for language
		for($i=0;$i<$l;$i++)
		{
			if($language_code_array[$i]!='en')
			{
				$data2 = array(
		           "api_token" => $api,
		           "action" => "view_terms",
		           "id" => $projectid,
		           "language" => $language_code_array[$i]);
		    	$request=array();
		    	$response4= $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
		    	$context_list = json_decode($response4, true);
		    	$term_array=array();
		    	$translation_array=array();
		    	$cnt=0;
				foreach($context_list['list'] as $context1)
				{
		    	array_push($term_array,$context1['term']);
		    	array_push($translation_array,$context1['definition']['form']);
		    	$cnt++;
		    	}

		    	$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
				$language_id=$languages['Language']['id'];

				$english_id=array();
				$stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$language_id)));
				foreach($stories as $story)
				{
					array_push($english_id,$story['Story']['id']);
				}
		
				$translations=$this->StoryTranslation->find('all',array('conditions'=>array('language_id !=' => $english_id1)));
				foreach($translations as $translation)
				{
					$story_name=$translation['StoryTranslation']['story_name'];
					$translation_id=$translation['StoryTranslation']['id'];
					$translate=$translation['StoryTranslation']['translation'];
					
						for($j=0;$j<=$cnt;$j++)
		    			{
		    				if($term_array[$j]==$story_name)
		    				{
		    					if(!empty($translation_array[$j]))
		    					{
		    						$this->StoryTranslation->id=$translation_id;
		    						$this->StoryTranslation->save(array('translation'=>$translation_array[$j]));
		    					}
		    					
		    				}
		    			}
					
				}
				
				foreach($english_id as $arr)
				{

					//Story Introduction sync with poeditor
					$story=$this->Story->find('first',array('conditions'=>array('id'=>$arr)));
					$introduction=$story['Story']['introduction'];
					$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story['Story']['story_name'],'language_id'=>$english_id1)));
					$english_introduction=$english_story['Story']['introduction'];
					
						for($j=0;$j<=$cnt;$j++)
		    			{
		    				if($term_array[$j]==$english_introduction)
		    				{
		    					if(!empty($translation_array[$j]))
		    					{
		    						$this->Story->id=$arr;
		    						$this->Story->save(array('introduction'=>$translation_array[$j]));
		    					}
		    					
		    				}
		    			}
		    		


					//statement sync with poeditor
					$statements=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$arr)));
					foreach($statements as $statement)
					{
						$audio_name=$statement['StoryStatement']['audio_clip'];
						$statement_story_id=$statement['StoryStatement']['story_id'];
						$statement_id=$statement['StoryStatement']['id'];
						$find_story=$this->Story->find('first',array('conditions'=>array('id'=>$statement_story_id)));
						$find_english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$find_story['Story']['story_name'],'language_id'=>$english_id1)));
						$english_statement=$this->StoryStatement->find('first',array('conditions'=>array('story_id'=>$find_english_story['Story']['id'],'audio_clip'=>$audio_name)));
						$english_statement_id=$english_statement['StoryStatement']['id'];
						
							if($statement_id!=$english_statement_id)
							{
								$no_translate=$english_statement['StoryStatement']['statement'];
				    			for($j=0;$j<=$cnt;$j++)
				    			{
				    				if($term_array[$j]==$no_translate)
				    				{
				    					if(!empty($translation_array[$j]))
				    					{
				    						$this->StoryStatement->id=$statement_id;
				    						$this->StoryStatement->save(array('statement'=>$translation_array[$j]));
				    					}
				    					
				    				}
				    			}
							}
						
					}
				}
			}  	
		}
			
		$this->Flash->success(__('The story has been sync with POEditor.'));
		return $this->redirect(array('action' => 'index'));
	}
	

}
