<?php
App::uses('AppController', 'Controller');
/**
 * Parents Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class TeachersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','UploadFile');
	public $uses = array('User','Level','Story','Language','UserInvite','UserRelation','School','Country');
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	// Allow users to register and logout.
    	$this->Auth->allow('add', 'logout');
	}
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
    $users=$this->User->find('all',array('conditions'=>array('type'=>'teacher')));
    $this->set(compact('users'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid teacher'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
      $dob=$this->request->data['User']['dob'];
      $yob=date('Y', strtotime($dob));
      $this->request->data['User']['yob']=$yob;
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The teacher has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The teacher could not be saved. Please, try again.'));
			}
		}
        $schools=$this->School->find('list',array('fields'=>array('School.name')));
        $countries=$this->Country->find('list',array('fields'=>array('Country.country_name')));
        $this->set(compact('schools','countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
      $dob=$this->request->data['User']['dob'];
      $yob=date('Y', strtotime($dob));
      $this->request->data['User']['yob']=$yob;
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The teacher has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The teacher could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
        $schools=$this->School->find('list',array('fields'=>array('School.name')));
        $countries=$this->Country->find('list',array('fields'=>array('Country.country_name')));
        $this->set(compact('schools','countries'));
	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid teacher'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The teacher has been deleted.'));
		} else {
			$this->Flash->error(__('The teacher could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * import method
 *
 * @return void
 */    
  
  public function import(){
      require_once 'excel_reader2.php';
      if($this->request->is('post')){
          if($_FILES['data']['error']['Teacher']['upload']!=0){
              $this->Flash->error(__('Something went wrong while importing Teachers. Please try again.'));
              return $this->redirect(array('action' => 'import'));
          }
         move_uploaded_file($_FILES['data']['tmp_name']['Teacher']['upload'],'files/teachers.xls');
         //chmod("files/teachers.xls", 0777);
         $data = new Spreadsheet_Excel_Reader('files/teachers.xls');
         $teachers=$not_inserted_records=array();
        //echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";
        for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
        {
            //echo "<pre>";
            //print_r($data->sheets);
            if(count($data->sheets[$i]['cells'])>0) // checking sheet not empty
            {
                if($data->sheets[$i]['cells'][1][1]=="First Name" && $data->sheets[$i]['cells'][1][2]=="Last Name" && $data->sheets[$i]['cells'][1][3]=="Gender" && $data->sheets[$i]['cells'][1][4]=="Email" && $data->sheets[$i]['cells'][1][5]=="School Email ID" && $data->sheets[$i]['cells'][1][6]=="Contact Number" && $data->sheets[$i]['cells'][1][7]=="Status"){
                 
                }else{
                    $this->Flash->error(__('Format does not match. Please try again.'));
                    return $this->redirect(array('action' => 'import'));
                }
                
                 //$data->sheets[$i]['cells'] = array_values($data->sheets[$i]['cells']);


                //echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i]['cells'])."<br />";
                for($j=2;$j<=count($data->sheets[$i]['cells']);$j++) // loop used to get each row of the sheet
                {
                  
                    $first_name = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][1]));
                    $last_name = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][2]));
                    $gender = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][3]));
                    $email = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][4]));
                    $school_email = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][5]));
                    $contact_number = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][6]));
                    $status = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][7]));
                    // $contact_number='9985652352';
                    // $status=1;
                    //$data->sheets[$i]['cells'][$j][1];
                    $unique_id= strtolower($first_name).'.'.strtolower($last_name);
                    
                    $this->School->contain();
                    $school_id=$this->School->find('first',array('fields'=>array('School.id'),'conditions'=>array('School.email'=>$school_email)));
                    
                                        
                    if(!empty($school_id)){
                        $teachers[]=$newrecord=array('User'=>array(
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'gender' => $gender,
                        'email' => $email,
                        'school_id' => $school_id['School']['id'],
                        'contact_no' => $contact_number,
                        'is_verified' => $status,
                        'type' => 'teacher'
                        ));

                        //unset($newrecord['User']['email']);
                         $this->User->set($newrecord);
                         if (!$this->User->validates(array('fieldList' => array('email')))) {
               
                              $errors = $this->User->validationErrors;
                              $this->User->contain();
                              $user_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$email)));
                              $this->User->id=$user_id['User']['id'];
                              $this->User->save($newrecord);
                         }else{
                             $this->User->create();
                             if($this->User->save($newrecord)){
                                $last_insert_id=$this->User->getLastInsertId();
                                $update['User']['id']=$last_insert_id;
                                $update['User']['username']=$unique_id.$last_insert_id;
                                $this->User->save($update);
                            }
                         }
                        
                    }else{
                        $not_inserted_records[]=array('User'=>array(
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'gender' => $gender,
                        'email' => $school_email,
                        'contact_no' => $contact_number,
                        'is_verified' => '1',
                        'type' => 'teacher'
                        ));
                    }

                }
            }
        }
        $this->set('teachers',$teachers);
        if(!empty($not_inserted_records)){
            $this->set('not_inserted_records',$not_inserted_records);
        }
        $this->Flash->success(__('Teachers data has been imported successfully.'));
      }
  }

    public function export(){
      $this->layout=false;
      require_once 'php-export-data.class.php';
      
      $teachers=$this->User->find('all',array('conditions'=>array('User.type'=>'teacher'),
                                    'fields'=>array(
                                        'User.first_name', 'User.last_name', 'User.gender', 'User.email', 'User.contact_no', 'User.is_verified'),
                                    'contain'=>array(
                                    'School.email')));
      
      $exporter = new ExportDataExcel('browser', 'teachers.xls');
      $exporter->initialize(); // starts streaming data to web browser
      $exporter->addRow(array("First Name", "Last Name", "Gender", "Email","School Email ID","Contact Number","Status",));
      
      
      foreach ($teachers as $key => $data) {
          $exporter->addRow(array($data['User']['first_name'], $data['User']['last_name'], $data['User']['gender'], $data['User']['email'],$data['School']['email'],$data['User']['contact_no'],$data['User']['is_verified']));
      }
       
      $exporter->finalize(); // writes the footer, flushes remaining data to browser.
      exit(); // all done
      
  }

}
