<?php
App::uses('AppController', 'Controller');
/**
 * Children Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class ChildrenController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','UploadFile');
	public $uses = array('User','UserRelation','Language','Level');
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	// Allow users to register and logout.
    	$this->Auth->allow('add', 'logout');
	}
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
	   	$users=$this->User->find('all',array('conditions'=>array('type'=>'child')));
    	$this->set(compact('users'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			$dob=$this->request->data['User']['dob'];
			$yob=date('Y', strtotime($dob));
			$this->request->data['User']['yob']=$yob;
			$parent_id=$this->request->data['UserRelation']['user1_id'];
			$fname=$this->request->data['User']['first_name'];
			$find_user=$this->User->getUserIdbyFname($fname);
			if(!empty($find_user))
			{
				foreach($find_user as $user)
				{
					$child_id=$user['User']['id'];
					$relations=$this->UserRelation->find('first',array('conditions'=>array('user1_id'=>$parent_id,'user2_id'=>$child_id)));
					
				}
			}
				
			if(empty($relations))
			{
				if ($this->User->save($this->request->data)) {
					$userrelation=array('UserRelation'=>array(
						'user1_id'=>$this->request->data['UserRelation']['user1_id'],
						'user2_id'=>$this->User->getLastInsertId()
						));
					$this->UserRelation->create();
					$this->UserRelation->save($userrelation);
					$this->Flash->success(__('The user has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The user could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The child already exists'));
			}

				
			
		}
		$languages = $this->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->Level->find('list',array('fields'=>array('Level.level_name')));
		$parents=$this->User->find('list',array('conditions'=>array('User.type'=>'parent'),'fields'=>array('User.email')));
		$this->set(compact('parents', 'levels', 'languages'));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$dob=$this->request->data['User']['dob'];
			$yob=date('Y', strtotime($dob));
			$this->request->data['User']['yob']=$yob;
			if ($this->User->save($this->request->data)) {
				$this->UserRelation->updateAll(array('UserRelation.user1_id'=>$this->request->data['UserRelation']['user1_id']),
				array('UserRelation.user2_id'=>$this->request->data['User']['id']));
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id),'contain'=>array('Parent','Parent.User1'));
			$this->request->data = $this->User->find('first', $options);
			$languages = $this->Language->find('list',array('fields'=>array('Language.language_name')));
			$levels = $this->Level->find('list',array('fields'=>array('Level.level_name')));
			$parents=$this->User->find('list',array('conditions'=>array('User.type'=>'parent'),'fields'=>array('User.email')));
			$parent_select=$this->request->data['Parent']['User1']['id'];
			$this->set(compact('parent_select','parents', 'levels', 'languages'));
		}
	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
