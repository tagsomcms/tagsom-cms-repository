<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
 /**
 * Stories Controller
 *
 * @property Story $Story
 * @property PaginatorComponent $Paginator
 */
class ApitokensController extends AppController {

public $components = array('RequestHandler');

 /**
 * Index method
 *
 * @throws NotFoundException
 * @param no parameters are there
 * @return void
 */
	public function index() 
	{
		$api_tokens = $this->Apitoken->find('all');
		$this->set('api_tokens',$api_tokens);
	}


 /**
 * Add Api_token method
 *
 * @throws NotFoundException
 * @param no parameters are there
 * @return void
 */
	public function add() 
	{
		if ($this->request->is('post')) {
			date_default_timezone_set('Asia/Calcutta');
			$date = date('Y/m/d h:i:s', time());
			$api=$this->data['form1']['api'];
			$array=array('api'=>$api,'created'=>$date,'modified'=>$date);
			$this->Apitoken->create();
			if($this->Apitoken->save($array))
			{				
			$this->Flash->success(__('The api has been saved.'));
			return $this->redirect(array('action' => 'index'));
			}
		}
	}




 /**
 * Add project_name method
 *
 * @throws NotFoundException
 * @param no parameters are there
 * @return void
 */
	public function add_project() 
	{
		$this->loadModel('Project');
		$project=$this->Project->find('first');
		$id=$project['Project']['id'];
		$this->set(compact('project'));
		if ($this->request->is('post')) {
			date_default_timezone_set('Asia/Calcutta');
			$date = date('Y/m/d h:i:s', time());
			$api=$this->data['form1']['name'];
			$array=array('name'=>$api,'created'=>$date,'modified'=>$date);
			$this->Project->id=$id;
			if($this->Project->save($array))
			{				
			$this->Flash->success(__('The project name has been saved.'));
			return $this->redirect(array('action' => 'add_project'));
			}
		}
	}



 /**
 * Edit method to change in api_token
 *
 * @throws NotFoundException
 * @param string $id pass as a api_token unique_id
 * @return void
 */
	public function edit($id = null) {
		
		if ($this->request->is(array('post', 'put'))) {
			$this->Apitoken->id=$id;
			$array=array('api'=>$this->data['form1']['api']);
			if ($this->Apitoken->save($array)) {
				$this->Flash->success(__('The api has been changed.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The api could not be changed. Please, try again.'));
			}
		} 
		$options= array('conditions' => array('id' => $id));
		$api_token = $this->Apitoken->find('first',$options);
		$this->set(compact('api_token'));
	}


/**
 * Delete method to remove existing api_tokens
 *
 * @throws NotFoundException
 * @param string $id pass as a api_token unique_id
 * @return void
 */
	public function delete($id = null) {
		
		$this->Apitoken->id = $id;
		$this->request->allowMethod('post', 'delete');
		if ($this->Apitoken->delete()) {
			$this->Flash->success(__('The api has been deleted.'));
		} else {
			$this->Flash->error(__('The api could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



/**
 * Activation method to active api_tokens to switch POEditor account
 *
 * @throws NotFoundException
 * @param string $id pass as a api_token unique_id
 * @return void
 */
	public function activation($id=null) {
		$this->Apitoken->updateAll(  array('status' => 1), 
          array('id' => $id));
		$this->Apitoken->updateAll(  array('status' => 0), 
          array('id !=' => $id));
		return $this->redirect(array('action' => 'index'));
	}



/**
 * Inactivation method to active api_tokens to switch POEditor account
 *
 * @throws NotFoundException
 * @param string $id pass as a api_token unique_id
 * @return void
 */
	public function inactivation($id=null) {
		$this->Apitoken->updateAll(  array('status' => 0), 
          array('id' => $id));
		
		return $this->redirect(array('action' => 'index'));
	}


/**
 * view method to detail view of api_token
 *
 * @throws NotFoundException
 * @param string $id pass as a api_token unique_id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Apitoken->exists($id)) {
			throw new NotFoundException(__('Invalid mode'));
		}
		$options = array('conditions' => array('id' => $id));
		$api_tokens=$this->Apitoken->find('first', $options);
		$this->set('api_tokens',$api_tokens);
	}



/**
 * import method to get translation from poeditor
 *
 * @throws NotFoundException
 * @param string $id pass as a api_token unique_id
 * @return void
 */
	public function import_temp() {
		// if (!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }
		$this->loadModel('StoryStatement');
		$this->loadModel('Story');
		$this->loadModel('Language');
		$api=$this->Apitoken->get_active_api();
		$HttpSocket = new HttpSocket();
		//get project_id from POEditor for story
		$project_array2=array('api_token' => $api,'action'=>'list_projects');
		$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
		$list_projects2 = json_decode($list_project2, true);
		foreach($list_projects2['list'] as $project2)
		{
			if($project2['name']=='Tagsom Project'){
				$projectid=$project2['id'];
			}
		}

		//get language list from project
		$lang_array=array('api_token' => $api,'action'=>'list_languages','id'=>$projectid);
		$list_languages = $HttpSocket->post('https://poeditor.com/api/', $lang_array);
		$language_list = json_decode($list_languages, true);
		$language_array=array();
		$language_code_array=array();
		foreach($language_list['list'] as $language)
		{
			$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language['name']);
			array_push($language_array,$language['name']);
			array_push($language_code_array,$language_code);
		}
		$l=sizeof($language_array);


		//view all terms for language
		for($i=0;$i<$l;$i++)
		{
			if($language_code_array[$i]!='en')
			{
				$data2 = array(
		           "api_token" => $api,
		           "action" => "view_terms",
		           "id" => $projectid,
		           "language" => $language_code_array[$i]);
		    	$request=array();
		    	$response4= $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
		    	$context_list = json_decode($response4, true);
		    	$term_array=array();
		    	$translation_array=array();
		    	$cnt=0;
				foreach($context_list['list'] as $context1)
				{
		    	array_push($term_array,$context1['term']);
		    	array_push($translation_array,$context1['definition']['form']);
		    	$cnt++;
		    	}

		    	$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language_array[$i])));
				$stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$languages['Language']['id'])));
				foreach ($stories as $story) {
					$storyname1=$story['Story']['story_name'];
					$stories_by_name=$this->Story->find('all',array('conditions'=>array('story_name'=>$storyname1,'language_id'=>$languages['Language']['id'])));
				}

				foreach($stories_by_name as $name)
				{
					$statements=$this->StoryStatement->find('all',array('conditions'=>array('statement'=>'No Translation','story_id'=>$name['Story']['id'])));
				
			    	foreach($statements as $statement)
			    	{
			    		$st_id=$statement['StoryStatement']['id'];
			    		$audio_clip=$statement['StoryStatement']['audio_clip'];
			    		$statements1=$this->StoryStatement->find('all',array('conditions'=>array('audio_clip'=>$audio_clip)));
			    		foreach($statements1 as $statement1)
			    		{
				    		if($statement1['StoryStatement']['statement']!='No Translation')
				    		{
				    			$no_translate=$statement1['StoryStatement']['statement'];
				    			for($j=0;$j<=$cnt;$j++)
				    			{
				    				if($term_array[$j]==$no_translate)
				    				{
				    					if(!empty($translation_array[$j]))
				    					{
				    						$this->StoryStatement->id=$st_id;
				    						$this->StoryStatement->save(array('statement'=>$translation_array[$j]));
				    					}
				    				}
				    			}
				    		}
			    		}
			    	}
		    	}
			}
		}
	}



/**
 * import method to import stories,statements,minigames from CMS to POEditor
 *
 * @throws NotFoundException
 * @param no parameters are there
 * @return void
 */
	public function import() {
		if ($this->request->is(array('post', 'put'))) {
			$api=$this->Apitoken->get_active_api();
			$HttpSocket = new HttpSocket();
			$all_flag=$this->data['form1']['all'];
			$story_flag=$this->data['form1']['stories'];
			$statement_flag=$this->data['form1']['statements'];
			$minigame_flag=$this->data['form1']['minigames'];

			//load models 
			$this->loadModel('Story');
			$this->loadModel('StoryStatement');
			$this->loadModel('ModeQuestionAnswer');
			$this->loadModel('Language');

			//find all stories,statements and minigames from database
			$stories=$this->Story->find('all',array('contain' => array('StoryStatement','ModeQuestionAnswer'),'conditions'=>array('Story.language_id'=>2)));
		
			foreach($stories as $story)
			{
				$storyname=$story['Story']['story_name'];
				
				//list_projects to check it is exist in POEditor or not
				$project_array=array('api_token' => $api,'action'=>'list_projects');
				$list_project = $HttpSocket->post('https://poeditor.com/api/', $project_array);
				$list_projects = json_decode($list_project, true);
				foreach($list_projects['list'] as $project)
				{
					if($project['name']=='Tagsom Project')
					{
						$story_exist=1;
					}
				}

				//add story in POEditor if it is not exist
				if($story_exist!=1)
				{
				$arr=array('api_token' => $api,'action'=>'create_project','name'=>'Tagsom Project');
				$response = $HttpSocket->post('https://poeditor.com/api/', $arr);
				$jsonarr = json_decode($response, true);
				}

				//get project_id from POEditor for story
				$project_array2=array('api_token' => $api,'action'=>'list_projects');
				$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
				$list_projects2 = json_decode($list_project2, true);
				foreach($list_projects2['list'] as $project2)
				{
					if($project2['name']=='Tagsom Project'){
						$story_id=$project2['id'];
					}
				}

				//add_language for story (default - English)
				$lang_story=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
				foreach($lang_story as $story2)
				{
					$story_language=$story2['Story']['language_id'];
					$option1 = array('conditions' => array('Language.id' => $story_language));
					$langs=$this->Language->find('first', $option1);
					$lang_code=$this->Language->getLocaleCodeForDisplayLanguage($langs['Language']['language_name']);
					$project_array1=array('api_token' => $api,'id'=>$story_id,'action'=>'add_language','language'=>$lang_code);
					$add_lang= $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				}
				
				//add story_name
	    		$arr11=array("term"=>$storyname,"context"=>"story_name","plural"=>"story_name");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);

		    	//view all terms for statements
				$arr3=array("context"=>"statement","plural"=>"statement");
			    $data1 = array(
		           "api_token" => $api,
		           "action" => "view_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr3),true));
		    	$request=array();
		    	$response3= $HttpSocket->post('https://poeditor.com/api/', $data1,$request);
		    	$term_list = json_decode($response3, true);
		    	$term_array=array();
				foreach($term_list['list'] as $term)
				{
		    	array_push($term_array, $term['term']);
		    	}

		    	//view all terms for questions
			    $data2 = array(
		           "api_token" => $api,
		           "action" => "view_terms",
		           "id" => $story_id);
		    	$request=array();
		    	$response4= $HttpSocket->post('https://poeditor.com/api/', $data2,$request);
		    	$context_list = json_decode($response4, true);
		    	$context_array=array();
				foreach($context_list['list'] as $context1)
				{
		    	array_push($context_array, $context1['context']);
		    	}

				//when select all or minigames to import
				if($minigame_flag==1 || $all_flag==1)
				{
					foreach($story['ModeQuestionAnswer'] as $questions)
					{	
						$cnt=rand(111,999);
						$question=$questions['mode_question'];
						$answer=$questions['mode_answer'];
						$option1=$questions['mode_option1'];
						$option2=$questions['mode_option2'];
						$option3=$questions['mode_option3'];

						if(!in_array($question, $term_array))
						{
							//add questions from database to POEditor
							$context='Question'.$cnt;
							$arr1=array("term"=>$question,"context"=>$context,"plural"=>"Question");
						    $data = array(
					           "api_token" => $api,
					           "action" => "add_terms",
					           "id" => $story_id,
					           "data" => json_encode(array($arr1),true));
					    	$request=array();
					    	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
				    	}

				    	if(!in_array($answer, $term_array))
						{
					    	//add answer of question
					    	$context2='Answer'.$cnt;
							$arr2=array("term"=>$answer,"context"=>$context2,"plural"=>"Answer");
						    $data = array(
					           "api_token" => $api,
					           "action" => "add_terms",
					           "id" => $story_id,
					           "data" => json_encode(array($arr2),true));
					    	$request=array();
					    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
				    	}

				    	if(!in_array($option1, $term_array))
						{
					    	//add option 1 of question
					    	$context3='Option1(Question'.$cnt.')';
							$arr3=array("term"=>$option1,"context"=>$context3,"plural"=>"Option1");
						    $data = array(
					           "api_token" => $api,
					           "action" => "add_terms",
					           "id" => $story_id,
					           "data" => json_encode(array($arr3),true));
					    	$request=array();
					    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
				    	}

				    	if(!in_array($option2, $term_array))
						{
					    	//add option 2 of question
					    	$context4='Option2(Question'.$cnt.')';
							$arr4=array("term"=>$option2,"context"=>$context4,"plural"=>"Option2");
						    $data = array(
					           "api_token" => $api,
					           "action" => "add_terms",
					           "id" => $story_id,
					           "data" => json_encode(array($arr4),true));
					    	$request=array();
					    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
					    }
					    if(!in_array($option3, $term_array))
						{
					    	//add option 3 of question
					    	$context5='Option3(Question'.$cnt.')';
							$arr5=array("term"=>$option3,"context"=>$context5,"plural"=>"Option3");
						    $data = array(
					           "api_token" => $api,
					           "action" => "add_terms",
					           "id" => $story_id,
					           "data" => json_encode(array($arr5),true));
					    	$request=array();
					    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
					    }
					}
				}

				if($statement_flag==1 || $all_flag==1)
				{
					foreach($story['StoryStatement'] as $stmt)
					{
						$statement=$stmt['statement'];
						if(!in_array($statement, $term_array))
				    	{
				    		//add statement to poeditor
							$arr1=array("term"=>$statement,"context"=>"(".$storyname.")","plural"=>"statement");
						    $data = array(
					           "api_token" => $api,
					           "action" => "add_terms",
					           "id" => $story_id,
					           "data" => json_encode(array($arr1),true));
					    	$request=array();
					    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
				    	}
					}
				
				}


			}
		}
	}

}
