<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','UploadFile');
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	// Allow users to register and logout.
    	$this->Auth->allow('add', 'logout');
	}

	public function login() {
		$this->layout=false;
		if($this->Auth->loggedIn()){

			return $this->redirect($this->Auth->redirectUrl());
		}
	    if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	        	// did they select the remember me checkbox?
            if ($this->request->data['User']['remember_me'] == 1) {
                // remove "remember me checkbox"
                unset($this->request->data['User']['remember_me']);

                // hash the user's password
                $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                // write the cookie
                $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
            }
				
	        	if ($this->Auth->user('type') === 'admin' || $this->Auth->user('type') === 'voice_actor') {
	        		$arr=array('parents','schools','children','teachers','students','user_invites','user_mode_grades','user_mode_statuses','user_story_levels','levels','languages','mode_question_answers','grades','freebooks','Apitokens');

			    	if($this->Session->read('Auth.User.type')!='admin')
			    	{
			    		if(in_array($this->params['controller'], $arr))
			    		{
			    			return $this->redirect(array('controller'=>'pages','action' => 'nopermission'));
			    		}
			    		if($this->params['controller']=='stories' && $this->params['action']=='add' || $this->params['action']=='edit' || $this->params['action']=='delete')
			    		{
			    			return $this->redirect(array('controller'=>'pages','action' => 'nopermission'));
			    		}
			    		if($this->params['controller']=='modes' && $this->params['action']=='add' || $this->params['action']=='edit' || $this->params['action']=='delete' || $this->params['action']=='edit_word' || $this->params['action']=='delete_word' || $this->params['action']=='add_word' || $this->params['action']=='add_question' || $this->params['action']=='edit_question' || $this->params['action']=='delete_question' || $this->params['action']=='add_string' || $this->params['action']=='delete_string' || $this->params['action']=='edit_setting' || $this->params['action']=='add_alphabet' || $this->params['action']=='edit_alphabet' || $this->params['action']=='delete_alphabet' || $this->params['action']=='add_numbers' || $this->params['action']=='delete_number')			
			    		{
			    			return $this->redirect(array('controller'=>'pages','action' => 'nopermission'));
			    		}

			    	}

	        		$this->loadModel('Project');
					$project=$this->Project->find('first');
					$name=$project['Project']['name'];
					$this->Session->write('project_name', $name);
	            	return $this->redirect($this->Auth->redirectUrl());
				}
				$this->Flash->error(__('You are not authorized to access that location!.'));
				$this->redirect(array('controller'=>'users','action'=>'logout'));
	        }
	        $this->Flash->error(__('Invalid username or password, try again'));
	    }
	}

	public function logout() {
		// clear the cookie (if it exists) when logging out
    	$this->Cookie->delete('remember_me_cookie');
	    return $this->redirect($this->Auth->logout());
	}
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('users', $this->User->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$options=array('admin','child','parent','teacher','student');
			$cnt=0;
			$count=$this->request->data['User']['role'];
			foreach($options as $option)
			{
				if($cnt==$count)
				{
					echo $this->request->data['User']['type']=$option;
				}
				$cnt++;
			}
			
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}


/**
 * profile method
 *
 * @return void
 */
	public function profile() {
		$id=$this->Session->read('Auth.User.id');
		if ($this->request->is(array('post', 'put'))) {
			if(!empty($this->data['User']['profile_pic']['name'])){
				pr($this->data);
				$file=$this->data['User']['profile_pic'];
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
				if(in_array($ext, $arr_ext)){
						$file_name1=$this->UploadFile->start_upload($this->data['User']['profile_pic']);
						$this->request->data['User']['profile_pic']=$file_name1; //prepare the filename for database entry
                }
				$this->Session->write('Auth.User.profile_pic', $file_name1);
			}else{
				unset($this->request->data['User']['profile_pic']);
			}
			if ($this->User->save($this->request->data)) {

				$this->Session->write('Auth.User.first_name',$this->data['User']['first_name']);
				$this->Session->write('Auth.User.last_name',$this->data['User']['last_name']);
				$this->Flash->success(__('The profile has been saved.'));
				return $this->redirect(array('action' => 'profile'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



/**
 * Voice Actors list method
 *
 * @return void
 */
	public function voice_actors() {
		$this->loadModel('Language');
		$title = 'Voice Actors';
		$this->set(compact('title'));
		$this->User->recursive = 0;
		$languages=$this->Language->find('all');

	     
		$this->set('users', $this->User->find('all',array('conditions' => array('User.type' => 'voice_actor'))));
		$this->set(compact('languages'));
	}


/**
 * delete_voiceactor method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete_voiceactor($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The Voice Actor has been deleted.'));
		} else {
			$this->Flash->error(__('The Voice Actor could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'voice_actors'));
	}



}
