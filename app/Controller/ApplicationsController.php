<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
 /**
 * Stories Controller
 *
 * @property Story $Story
 * @property PaginatorComponent $Paginator
 */
class ApplicationsController extends AppController {

 /**
 * Components
 *
 * @var array
 */
 	public $uses = array('Application','Story','Language','ApplicationDetail');
	public $components = array('Paginator');
	 

/**
 * Index method
 *
 * @return void
 */
	public function index() 
	{
		$applications=$this->Application->find('all');
		$this->set(compact('applications'));
	}


/**
 * 	Add New Applications method
 *
 * @return void
 */
	public function add() 
	{
		if ($this->request->is('post')) 
		{
			$image_temp=$this->data['Applications']['image']['tmp_name'];
			$imagename=$this->data['Applications']['application_name'].'.jpg';
			$application_name=$this->data['Applications']['application_name'];
			$client_name=$this->data['Applications']['client_name'];
			$year=$this->data['Applications']['year'];

			$application_save=$this->Application->add_application($application_name,$client_name,$year,$imagename,$image_temp);
			
			$this->Flash->success(__('Application Successfully Created.'));
			return $this->redirect(array('action' => 'index'));
		}
	}


/**
 * 	Edit Application from cms
 *
 * @return void
 */
	public function edit($id=null) 
	{
		$application=$this->Application->find('first',array('conditions'=>array('id'=>$id)));
		$imagename=$application['Application']['image'];
		$this->set(compact('application'));
		if ($this->request->is('post')) 
		{
			$image_temp=$this->data['Applications']['image']['tmp_name'];
			$application_name=$this->data['Applications']['application_name'];
			$imagename2=$this->data['Applications']['application_name'].'.jpg';
			$client_name=$this->data['Applications']['client_name'];
			$year=$this->data['Applications']['year'];

			$application=$this->Application->edit_application($application_name,$client_name,$year,$imagename2,$image_temp,$imagename,$id);

			$this->Flash->success(__('Application Successfully Updated.'));
			return $this->redirect(array('action' => 'index'));
		}


	}



/**
 * Application Detail View method
 *
 * @return void
 */
	public function view($id=null) 
	{
		$application=$this->Application->find('first',array('conditions'=>array('id'=>$id)));
		$stories=$this->ApplicationDetail->find('all',array('conditions'=>array('application_id'=>$id)));
		$this->set(compact('application','stories'));
	}



/**
 * Assign Stories to applications method
 *
 * @return void
 */
	public function assign($id=null) 
	{
		$application=$this->Application->find('first',array('conditions'=>array('id'=>$id)));
		$language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$english_id=$language['Language']['id'];
		$stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$english_id)));
		$stories_id=array();

		$stories_array=array();
		$applications_stories=$this->ApplicationDetail->find('all',array('conditions'=>array('application_id'=>$id)));
		foreach($applications_stories as $story)
		{
			array_push($stories_array,$story['ApplicationDetail']['story_name']);
		}

		if ($this->request->is('post')) 
		{
			foreach($this->request->data['stories'] as $story)
			{
				if(!in_array($story, $stories_array))
				{
					$this->ApplicationDetail->create();
					$this->ApplicationDetail->save(array('application_id'=>$id,'story_name'=>$story));
				}
			}
			foreach($stories_array as $story)
			{
				if(!in_array($story,$this->request->data['stories']))
				{
					$find_story=$this->ApplicationDetail->find('first',array('conditions'=>array('application_id'=>$id,'story_name'=>$story)));
					$this->ApplicationDetail->id=$find_story['ApplicationDetail']['id'];
					$this->ApplicationDetail->delete();
				}
			}
			
			$this->Flash->success(__('Stories Added Successfully.'));
			return $this->redirect(array('action' => 'index'));
		}


		$this->set(compact('application','stories','stories_array'));
	}


/**
 * 	Delete Application from cms
 *
 * @return void
 */
	public function delete($id=null) 
	{
		$application=$this->Application->delete_application($id);
		$this->Flash->success(__('Application Successfully Removed.'));
		return $this->redirect(array('action' => 'index'));

	}



}

?>