<?php
App::uses('AppController', 'Controller');
/**
 * Children Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class StudentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','UploadFile');
	public $uses = array('User','UserRelation','Language','Level','School','Story','Book');
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	// Allow users to register and logout.
    	$this->Auth->allow('add', 'logout');
        
        $stories_list=$this->Story->find('all',array('contain'=>array('Language'),'conditions'=>array('Language.language_name'=>'English')));
        foreach ($stories_list as $storykey => $storyname) {
            $index=$storykey+1;
            define('BOOK'.$index, $storyname['Story']['story_name']);
        }
        
	}
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// $this->paginate=array('conditions'=>array('type'=>'student'),'contain'=>array('Teacher','Teacher.User1'),'limit'=>10);
		// $this->set('users', $this->Paginator->paginate());
        $users=$this->User->find('all',array('conditions'=>array('type'=>'student')));
        $this->set(compact('users'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
            $dob=$this->request->data['User']['dob'];
            $yob=date('Y', strtotime($dob));
            $this->request->data['User']['yob']=$yob;
			if ($this->User->save($this->request->data)) {
				$userrelation=array('UserRelation'=>array(
					'user1_id'=>$this->request->data['UserRelation']['user1_id'],
					'user2_id'=>$this->User->getLastInsertId()
					));
				$this->UserRelation->create();
				$this->UserRelation->save($userrelation);
				$this->Flash->success(__('The student has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The student could not be saved. Please, try again.'));
			}
		}
		$languages = $this->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->Level->find('list',array('fields'=>array('Level.level_name')));
        $schools=$this->School->find('list',array('fields'=>array('School.name')));
		$teachers=$this->User->find('list',array('conditions'=>array('User.type'=>'teacher'),'fields'=>array('User.email')));
		$this->set(compact('schools','teachers', 'levels', 'languages'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $dob=$this->request->data['User']['dob'];
            $yob=date('Y', strtotime($dob));
            $this->request->data['User']['yob']=$yob;
			if ($this->User->save($this->request->data)) {
				$this->UserRelation->updateAll(array('UserRelation.user1_id'=>$this->request->data['UserRelation']['user1_id']),
				array('UserRelation.user2_id'=>$this->request->data['User']['id']));
				$this->Flash->success(__('The student has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The student could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id),'contain'=>array('Teacher','Teacher.User1'));
			$this->request->data = $this->User->find('first', $options);
			$languages = $this->Language->find('list',array('fields'=>array('Language.language_name')));
			$levels = $this->Level->find('list',array('fields'=>array('Level.level_name')));
            $schools=$this->School->find('list',array('fields'=>array('School.name')));
			$teachers=$this->User->find('list',array('conditions'=>array('User.type'=>'teacher','User.school_id'=>$this->request->data['User']['school_id']),'fields'=>array('User.email')));
			$parent_select=$this->request->data['Teacher']['User1']['id'];
			$this->set(compact('parent_select','teachers', 'levels', 'languages','schools'));
		}
	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The student has been deleted.'));
		} else {
			$this->Flash->error(__('The student could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * import method
 *
 * @return void
 */    
  
  public function import(){
      require_once 'excel_reader2.php';
      if($this->request->is('post')){
          if($_FILES['data']['error']['Student']['upload']!=0){
              $this->Flash->error(__('Something went wrong while importing Students. Please try again.'));
              return $this->redirect(array('action' => 'import'));
          }
          move_uploaded_file($_FILES['data']['tmp_name']['Student']['upload'],'files/students.xls');
          //chmod("files/students.xls", 0777);
         $data = new Spreadsheet_Excel_Reader('files/students.xls');
         $students=$not_inserted_records=array();
        //echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";
        for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
        {
            //echo "<pre>";
            //print_r($data->sheets);
            if(count($data->sheets[$i]['cells'])>0) // checking sheet not empty
            {
                if(!$data->sheets[$i]['cells'][1][1]=="First Name" 
                || !$data->sheets[$i]['cells'][1][2]=="Last Name" 
                || !$data->sheets[$i]['cells'][1][3]=="Gender" 
                || !$data->sheets[$i]['cells'][1][4]=="Year of Birth" 
                || !$data->sheets[$i]['cells'][1][5]=="Teacher Email ID" 
                || !$data->sheets[$i]['cells'][1][6]=="UID" 
                || !$data->sheets[$i]['cells'][1][7]=="Language" 
                || !$data->sheets[$i]['cells'][1][8]=="Level" 
                || !$data->sheets[$i]['cells'][1][9]=="Is Unique" 
                || !$data->sheets[$i]['cells'][1][10]=="Status" 
                || !$data->sheets[$i]['cells'][1][11]=="Book 01 - ".BOOK1."" 
                || !$data->sheets[$i]['cells'][1][12]=="Book 02- ".BOOK2."" 
                || !$data->sheets[$i]['cells'][1][13]== "Book 03 - ".BOOK3."" 
                || !$data->sheets[$i]['cells'][1][14]== "Book 04 - ".BOOK4."" 
                || !$data->sheets[$i]['cells'][1][15]== "Book 05 - ".BOOK5."" 
                || !$data->sheets[$i]['cells'][1][16]== "Book 06 - ".BOOK6."" 
                || !$data->sheets[$i]['cells'][1][17]== "Book 07- ".BOOK7."" 
                || !$data->sheets[$i]['cells'][1][18]== "Book 08- ".BOOK8."" 
                || !$data->sheets[$i]['cells'][1][19]== "Book 09 - ".BOOK9."" 
                || !$data->sheets[$i]['cells'][1][20]== "Book 10 - ".BOOK10."" 
                || !$data->sheets[$i]['cells'][1][21]== "Book 11 - ".BOOK11."" 
                || !$data->sheets[$i]['cells'][1][22]== "Book 12 - ".BOOK12.""){
                            
                            // $this->Story->contain();
                            // $story1=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK1.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story2=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK2.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story3=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK3.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story4=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK4.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story5=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK5.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story6=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK6.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story7=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK7.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story8=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK8.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story9=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK9.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story10=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK10.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story11=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK11.'%'),
                            // 'fields'=> array('Story.id')));
                            // $story12=$this->Story->find('first',array('conditions'=>array('Story.story_name LIKE' => '%'.BOOK12.'%'),
                            // 'fields'=> array('Story.id')));
                            
                // }else{
                    $this->Flash->error(__('Format does not match. Please try again.'));
                    return $this->redirect(array('action' => 'import'));
                }
                
                
                //echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i]['cells'])."<br />";
                for($j=2;$j<=count($data->sheets[$i]['cells']);$j++) // loop used to get each row of the sheet
                {
                    $first_name = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][1]));
                    $last_name = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][2]));
                    $gender = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][3]));
                    $yob = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][4]));
                    $email = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][5]));
                    $uid = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][6]));
                    $language = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][7]));
                    $level = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][8]));
                    $is_unique = preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][9]));
                    $status= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][10]));
                    
                    $book1= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][11]));
                    $book2= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][12]));
                    $book3= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][13]));
                    $book4= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][14]));
                    $book5= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][15]));
                    $book6= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][16]));
                    $book7= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][17]));
                    $book8= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][18]));
                    $book9= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][19]));
                    $book10= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][20]));
                    $book11= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][21]));
                    $book12= preg_replace("/[^a-zA-Z@.0-9]/", "", utf8_encode($data->sheets[$i]['cells'][$j][22]));
                    
                    $this->Language->contain();
                    $language_id=$this->Language->find('first',array('fields'=>array('Language.id'),'conditions'=>array('Language.language_name'=>$language)));
                    $this->Level->contain();
                    $level_id=$this->Level->find('first',array('fields'=>array('Level.id'),'conditions'=>array('Level.level_name'=>$level)));
                    $english_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
                    
                    if(empty($language_id)){
                        $language_id=$english_language['Language']['id'];
                    }else{
                        $language_id = $language_id['Language']['id'];
                    }

                    if(!empty($level_id)){
                        $level_id = $level_id['Level']['id'];
                    }else{
                        $level_id ='1';
                    }
                  
                    //$data->sheets[$i]['cells'][$j][1];
                    
                    
                    $this->User->contain();
                    $teacher_id=$this->User->find('first',array('fields'=>array('User.id','User.school_id'),'conditions'=>array('User.type'=>'teacher','User.email'=>$email)));
                    

                    if(!empty($teacher_id)){
                        
                        $students[]=$newrecord=array('User'=>array(
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'gender' => $gender,
                        'yob' => date('Y',strtotime($yob)),
                        'email' => $email,
                        'username' => $uid,
                        'school_id' => $teacher_id['User']['school_id'],
                        'language_id' => $language_id,
                        'level_id' => $level_id,
                        'is_unique' => $is_unique,
                        'is_verified' => $status,
                        'type' => 'student'
                        ));
                        unset($newrecord['User']['email']);
                    
                        $this->User->set($newrecord);
                        
                        // If duplicate user id, then update it
                        if (!$this->User->validates(array('fieldList' => array('username')))) {
                            
                             //$errors = $this->User->validationErrors;
                             $this->User->contain();
                             $user_details=$this->User->findByUsername($newrecord['User']['username']);
                             $this->User->id=$user_details['User']['id'];
                             $this->User->save($newrecord);
                             $this->UserRelation->updateAll(array('UserRelation.user1_id'=>$teacher_id['User']['id']),
                            array('UserRelation.user2_id'=>$user_details['User']['id']));
                             
                             $user_relation_id = $this->UserRelation->find('first',array('conditions'=>array(
                             'UserRelation.user2_id'=>$user_details['User']['id']
                             ),
                             'fields'=> array('UserRelation.id')
                             ));
                             
                            $user_relation_id=$user_relation_id['UserRelation']['id'];
                             
                            if($book1==1){
                                $this->_saveBook($user_relation_id, $story1['Story']['id']);
                            }
                            
                            if($book2==1){
                                $this->_saveBook($user_relation_id, $story2['Story']['id']);
                            }
                            
                            if($book3==1){
                                $this->_saveBook($user_relation_id, $story3['Story']['id']);
                            }
                           
                            if($book4==1){
                                $this->_saveBook($user_relation_id, $story4['Story']['id']);
                            }
                            
                            if($book5==1){
                                $this->_saveBook($user_relation_id, $story5['Story']['id']);
                            }
                            
                            if($book6==1){
                                $this->_saveBook($user_relation_id, $story6['Story']['id']);
                            }
                            
                            if($book7==1){
                                $this->_saveBook($user_relation_id, $story7['Story']['id']);
                            }
                            
                            if($book8==1){
                                $this->_saveBook($user_relation_id, $story8['Story']['id']);
                            }
                            
                            if($book9==1){
                                $this->_saveBook($user_relation_id, $story9['Story']['id']);
                            }
                            
                            if($book10==1){
                                $this->_saveBook($user_relation_id, $story10['Story']['id']);
                            }
                            
                            if($book11==1){
                                $this->_saveBook($user_relation_id, $story11['Story']['id']);
                            }
                            
                            if($book12==1){
                                $this->_saveBook($user_relation_id, $story12['Story']['id']);
                            }
                             
                             //$unique_id= strtolower($first_name);
                             //$newrecord['User']['username']=$unique_id.substr(md5($first_name.time()), 0,4);
                             
                        }else{
                            
                        $this->User->create();
                        if($this->User->save($newrecord)){
                            $last_insert_id=$this->User->getLastInsertId();
                            //$update['User']['id']=$last_insert_id;
                            //$update['User']['username']=$unique_id.$last_insert_id;
                            //$this->User->save($update);
                            $userrelation['UserRelation']['user1_id']=$teacher_id['User']['id'];
                            $userrelation['UserRelation']['user2_id']=$last_insert_id;
                            $this->UserRelation->create();
                            $this->UserRelation->save($userrelation);
                            
                            $user_relation_id=$this->UserRelation->getLastInsertId();
                            
                            if($book1==1){
                                $this->_saveBook($user_relation_id, $story1['Story']['id']);
                            }
                            
                            if($book2==1){
                                $this->_saveBook($user_relation_id, $story2['Story']['id']);
                            }
                            
                            if($book3==1){
                                $this->_saveBook($user_relation_id, $story3['Story']['id']);
                            }
                           
                            if($book4==1){
                                $this->_saveBook($user_relation_id, $story4['Story']['id']);
                            }
                            
                            if($book5==1){
                                $this->_saveBook($user_relation_id, $story5['Story']['id']);
                            }
                            
                            if($book6==1){
                                $this->_saveBook($user_relation_id, $story6['Story']['id']);
                            }
                            
                            if($book7==1){
                                $this->_saveBook($user_relation_id, $story7['Story']['id']);
                            }
                            
                            if($book8==1){
                                $this->_saveBook($user_relation_id, $story8['Story']['id']);
                            }
                            
                            if($book9==1){
                                $this->_saveBook($user_relation_id, $story9['Story']['id']);
                            }
                            
                            if($book10==1){
                                $this->_saveBook($user_relation_id, $story10['Story']['id']);
                            }
                            
                            if($book11==1){
                                $this->_saveBook($user_relation_id, $story11['Story']['id']);
                            }
                            
                            if($book12==1){
                                $this->_saveBook($user_relation_id, $story12['Story']['id']);
                            }
                            
                        }
                        }
                        
                    }
                    else{
                        $not_inserted_records[]=array('User'=>array(
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'gender' => $gender,
                        'yob' => date('Y-m-d',strtotime($yob)),
                        'email' => $email,
                        'language_id' => $language_id,
                        'level_id' => $level_id,
                        'is_verified' => '1',
                        'type' => 'student'
                        ));
                    }

                }
            }
        }
        $this->set('students',$students);

      }
  }

  protected function _saveBook($user_relation_id,$story_id){
      $booksdata=$this->Book->find('first',array('conditions'=>array(
                    'Book.user_relation_id'=>$user_relation_id,
                    'Book.story_id' => $story_id)));
      if(empty($booksdata)){
          $book=array('Book'=>array(
                        'user_relation_id' => $user_relation_id,
                        'story_id' => $story_id
        ));
        $this->Book->create();
        $this->Book->save($book);
      }
  }
  

  public function getTeacherBySchoolId(){
      $this->layout=false;
      if($this->request->is('post')){
          $this->User->contain();
          $teachers=$this->User->find('list',array('conditions'=>array('User.type'=>'teacher','User.school_id'=>$this->request->data['id']),'fields'=>array('User.email')));
          $this->set(compact('teachers'));
          $this->render('/Elements/teachers_list');
      }
  }
  
  public function old_export(){
      $this->layout=false;
      require_once 'php-export-data.class.php';
      
      $students=$this->User->find('all',array('conditions'=>array('User.type'=>'student'),
                                    'fields'=>array(
                                        'User.first_name', 'User.last_name', 'User.gender', 'User.yob', 'User.username', 'User.language_id', 'User.level_id', 'User.is_unique', 'User.is_verified'),
                                    'contain'=>array(
                                    'Level.level_name','Language.language_name','Teacher.User1.email')));
      
      $exporter = new ExportDataExcel('browser', 'students.xls');
      $exporter->initialize(); // starts streaming data to web browser
      $exporter->addRow(array("First Name", "Last Name", "Gender", "Year of Birth","Teacher Email ID","UID","Language","Level","Is Unique","Status"));
      
      
      foreach ($students as $key => $data) {
          $exporter->addRow(array($data['User']['first_name'], $data['User']['last_name'], $data['User']['gender'], $data['User']['yob'],$data['Teacher']['User1']['email'],$data['User']['username'],$data['Language']['language_name'],$data['Level']['level_name'],$data['User']['is_unique'],$data['User']['is_verified']));
      }
       
      $exporter->finalize(); // writes the footer, flushes remaining data to browser.
      exit(); // all done
      
  }
    

  
  public function export(){
      //error_reporting(E_ALL);
      //ini_set('display_errors', TRUE);
      //ini_set('display_startup_errors', TRUE);
      //date_default_timezone_set('Europe/London');
    
    if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');
    
    /** Include PHPExcel */
    require_once 'Classes/PHPExcel.php';
    
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
    
    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Amuk Saxena")
                                 ->setLastModifiedBy("Amuk Saxena")
                                 ->setTitle("Office 2007 XLSX Test Document")
                                 ->setSubject("Office 2007 XLSX Test Document")
                                 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                 ->setKeywords("office 2007 openxml php")
                                 ->setCategory("Test result file");
                                 
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
    
    foreach(range('A','V') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
    }
    
    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    
    $objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getFill()->getStartColor()->applyFromArray(array('rgb' => '00B153'));
    
    //$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->getStartColor()->setARGB(PHPExcel_Style_Color::COLOR_GREEN);
    
    $objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
    
    // Add some data
    $objPHPExcel->getActiveSheet()->getStyle("A1:V1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'First Name')
                ->setCellValue('B1', 'Last Name')
                ->setCellValue('C1', 'Gender')
                ->setCellValue('D1', 'Year of Birth')
                ->setCellValue('E1', 'Teacher Email ID')
                ->setCellValue('F1', 'UID')
                ->setCellValue('G1', 'Language')
                ->setCellValue('H1', 'Level')
                ->setCellValue('I1', 'Is Unique')
                ->setCellValue('J1', 'Status')
                ->setCellValue('K1', 'Book 01 - '.BOOK1)
                ->setCellValue('L1', 'Book 02- '.BOOK2)
                ->setCellValue('M1', 'Book 03 - '.BOOK3)
                ->setCellValue('N1', 'Book 04 - '.BOOK4)
                ->setCellValue('O1', 'Book 05 - '.BOOK5)
                ->setCellValue('P1', 'Book 06 - '.BOOK6)
                ->setCellValue('Q1', 'Book 07- '.BOOK7)
                ->setCellValue('R1', 'Book 08- '.BOOK8)
                ->setCellValue('S1', 'Book 09 - '.BOOK9)
                ->setCellValue('T1', 'Book 10 - '.BOOK10)
                ->setCellValue('U1', 'Book 11 - '.BOOK11)
                ->setCellValue('V1', 'Book 12 - '.BOOK12);


    $students=$this->User->find('all',array('conditions'=>array('User.type'=>'student'),
                                    'fields'=>array(
                                        'User.first_name', 'User.last_name', 'User.gender', 'User.yob', 'User.username', 'User.language_id', 'User.level_id', 'User.is_unique', 'User.is_verified'),
                                    'contain'=>array(
                                    'Level.level_name','Language.language_name','Teacher.id','Teacher.User1.email',
                                    'Teacher.Book.Story.story_name','Teacher.Book.story_id')));
   
    foreach ($students as $key => $data) {
        $book1=$book2=$book3=$book4=$book5=$book6=$book7=$book8=$book9=$book10=$book11=$book12=0;
        foreach ($data['Teacher']['Book'] as $bookkey => $bookdata) {
            switch (trim($bookdata['Story']['story_name'])) {
                case BOOK1:
                    $book1=1;
                    break;
                case BOOK2:
                    $book2=1;
                    break;
                case BOOK3:
                    $book3=1;
                    break;
                case BOOK4:
                    $book4=1;
                    break;
                case BOOK5:
                    $book5=1;
                    break;
                case BOOK6:
                    $book6=1;
                    break;
                case BOOK7:
                    $book7=1;
                    break;
                case BOOK8:
                    $book8=1;
                    break;
                case BOOK9:
                    $book9=1;
                    break;
                case BOOK10:
                    $book10=1;
                    break;
                case BOOK11:
                    $book11=1;
                    break;
                case BOOK12:
                    $book12=1;
                    break;
                default:
                    break;
            }
        }
        
        $cellnumber=$key+2;
        $objPHPExcel->getActiveSheet()->getRowDimension($cellnumber)->setRowHeight(15);
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$cellnumber, $data['User']['first_name'])
                    ->setCellValue('B'.$cellnumber, $data['User']['last_name'])
                    ->setCellValue('C'.$cellnumber, $data['User']['gender'])
                    ->setCellValue('D'.$cellnumber, $data['User']['yob'])
                    ->setCellValue('E'.$cellnumber, $data['Teacher']['User1']['email'])
                    ->setCellValue('F'.$cellnumber, $data['User']['username'])
                    ->setCellValue('G'.$cellnumber, $data['Language']['language_name'])
                    ->setCellValue('H'.$cellnumber, $data['Level']['level_name'])
                    ->setCellValue('I'.$cellnumber, $data['User']['is_unique'])
                    ->setCellValue('J'.$cellnumber, $data['User']['is_verified'])
                    ->setCellValue('K'.$cellnumber, $book1)
                    ->setCellValue('L'.$cellnumber, $book2)
                    ->setCellValue('M'.$cellnumber, $book3)
                    ->setCellValue('N'.$cellnumber, $book4)
                    ->setCellValue('O'.$cellnumber, $book5)
                    ->setCellValue('P'.$cellnumber, $book6)
                    ->setCellValue('Q'.$cellnumber, $book7)
                    ->setCellValue('R'.$cellnumber, $book8)
                    ->setCellValue('S'.$cellnumber, $book9)
                    ->setCellValue('T'.$cellnumber, $book10)
                    ->setCellValue('U'.$cellnumber, $book11)
                    ->setCellValue('V'.$cellnumber, $book12);
      }


    $objPHPExcel->getActiveSheet()->setTitle('Students');
    
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="students.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    
    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }
    
}
