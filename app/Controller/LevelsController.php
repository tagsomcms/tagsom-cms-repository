<?php
App::uses('AppController', 'Controller');
/**
 * Levels Controller
 *
 * @property Level $Level
 * @property PaginatorComponent $Paginator
 */
class LevelsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Level->recursive = 0;
		$this->set('levels', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Level->exists($id)) {
			throw new NotFoundException(__('Invalid level'));
		}
		$options = array('conditions' => array('Level.' . $this->Level->primaryKey => $id));
		$this->set('level', $this->Level->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Level->create();
			$level=$this->Level->find('first',array('conditions'=>array('level_name'=>$this->request->data['Level']['level_name'])));
			if(empty($level))
			{
				if ($this->Level->save($this->request->data)) {
					$this->Flash->success(__('The level has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The level could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The level is already exist.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Level->exists($id)) {
			throw new NotFoundException(__('Invalid level'));
		}
		$level_array=array();
		$levels=$this->Level->find('all',array('conditions'=>array('id !=' => $id)));
		foreach($levels as $level)
		{
			array_push($level_array, $level['Level']['level_name']);
		}
		if ($this->request->is(array('post', 'put'))) {
			if(!in_array($this->request->data['Level']['level_name'],$level_array))
			{
				if ($this->Level->save($this->request->data)) {
					$this->Flash->success(__('The level has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The level could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The level is already exist.'));
			}
		} else {
			$options = array('conditions' => array('Level.' . $this->Level->primaryKey => $id));
			$this->request->data = $this->Level->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Level->id = $id;
		if (!$this->Level->exists()) {
			throw new NotFoundException(__('Invalid level'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Level->delete()) {
			$this->Flash->success(__('The level has been deleted.'));
		} else {
			$this->Flash->error(__('The level could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
