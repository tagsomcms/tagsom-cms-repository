<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');
/**
 * UserInvites Controller
 *
 * @property UserInvite $UserInvite
 * @property PaginatorComponent $Paginator
 */
class UserInvitesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	$this->Auth->allow('invite','activate');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserInvite->recursive = 0;
		$this->set('userInvites', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserInvite->exists($id)) {
			throw new NotFoundException(__('Invalid user invite'));
		}
		$options = array('conditions' => array('UserInvite.' . $this->UserInvite->primaryKey => $id));
		$this->set('userInvite', $this->UserInvite->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {


		$this->loadModel('User');
		$users=$this->User->find('all');
	 	$user_array=array();
	  	foreach($users as $user)
	  	{
	    	if(!empty($user['User']['email']))
	    	{
	      		array_push($user_array,$user['User']['email']);
	    	}
	  	}
	  	$this->set(compact('user_array'));
	  	$length=sizeof($user_array);

		if ($this->request->is('post')) {
			$select=$this->request->data['UserInvite']['user_id'];
			$user_email=$user_array[$select];
			$user_find=$this->User->find('first',array('conditions'=>array('email'=>$user_email)));
			$userid=$user_find['User']['id'];

			$data=$this->UserInvite->User->find('first',array('recursive'=>0,'conditions'=>array('User.id'=>$userid)));
	
			$from=$data['User']['email'];
			$to=$this->request->data['UserInvite']['invite_emailid'];
			$invite_exist=$this->UserInvite->find('first',array('conditions'=>array('invite_emailid'=>$to)));
			$find_userid=$invite_exist['UserInvite']['user_id'];
			
			$already_exist=$this->User->find('first',array('conditions'=>array('email'=>$to,'is_verified'=>1)));
			if(empty($already_exist))
			{
			$already_exist2=$this->User->find('first',array('conditions'=>array('email'=>$to,'is_verified'=>0)));
			if(empty($already_exist2))
			{
				$pwd=rand(111111,999999);
				$user=array('User'=>array('email'=>$to,'relationship'=>0,'login_portal'=>0,'role'=>'user','free_book'=>0,'status'=>0));
				App::Import('Model','User');
				$this->User=new User();
				$this->User->create();
				$this->User->save($user);
				$id=$this->User->getLastInsertId();
			}
			else
			{
				$users=$this->User->find('first',array('conditions'=>array('email'=>$to)));
				$id=$users['User']['id'];
				$pwd=rand(111111,999999);
			}
			$messagedata=$data['User'];
			$array=array('user_id'=>$userid,'book_id'=>0,'invite_emailid'=>$to,'invite_status'=>0);
			$messagedata['from']=$from;
			$messagedata['email']=$to;
			$messagedata['password']=$pwd;
			if($userid!=$find_userid)
			{
				$invite_exist2=$this->UserInvite->find('first',array('conditions'=>array('invite_emailid'=>$to,'invite_status'=>1)));
				if(empty($invite_exist2))
				{
					if($to==$from)
					{
						$this->Flash->error(__('The user invitee & invitor must be different.'));
					}
					else
					{
						$this->UserInvite->create();
						if ($this->UserInvite->save($array)) {
							$userinvite_id=$this->UserInvite->getLastInsertId();
							$messagedata["activation_url"]=Router::url('/user_invites/invite?invitestatus=true&user_id='.$id.'&invite_id='.$userinvite_id.'&pwd='.$pwd, true );
							$this->_sendemail($from,$to,'Invitation Mail',$messagedata,'user_invite');
							$this->Flash->success(__('The user invite has been saved.'));
							return $this->redirect(array('action' => 'index'));
						} else {
							$this->Flash->error(__('The user invite could not be saved. Please, try again.'));
						}
					}
				}
				else
				{
					$this->Flash->error(__('The user already exist.'));
				}
			}
			else
			{
				$this->Flash->error(__('The user invite have been already sent.'));
			}
		
		}
		else
		{
			$this->Flash->error(__('The user already exist.'));
		}
		}
		
	}



	/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->loadModel('User');
		$users=$this->User->find('all');
	 	$user_array=array();
	  	foreach($users as $user)
	  	{
	    	if(!empty($user['User']['email']))
	    	{
	      		array_push($user_array,$user['User']['email']);
	    	}
	  	}

	  	$invitefind=$this->UserInvite->find('first',array('conditions'=>array('id'=>$id)));
	  	$user_find=$this->User->find('first',array('conditions'=>array('id'=>$invitefind['UserInvite']['user_id'])));

	  	$user_email=$user_find['User']['email'];
	  	$cnt=0;
	  	foreach($user_array as $email)
	  	{
	  		if($email==$user_email)
	  		{
	  			$select=$cnt;
	  		}
	  		$cnt++;
	  	}

	  	$this->set(compact('user_array','select'));
		if (!$this->UserInvite->exists($id)) {
			throw new NotFoundException(__('Invalid user invite'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$option=$this->request->data['UserInvite']['user_id'];
			$user_email=$user_array[$option];
			$find_user=$this->User->find('first',array('conditions'=>array('email'=>$user_email)));
			$userid=$find_user['User']['id'];
			$to=$this->request->data['UserInvite']['invite_emailid'];
			$messagedata=$data['User'];
			$array=array('user_id'=>$userid,'book_id'=>0,'invite_emailid'=>$to,'invite_status'=>0);
			if ($this->UserInvite->save($array)) {
				$userinvite_id=$this->UserInvite->getLastInsertId();
				$messagedata["activation_url"]=Router::url('/user_invites/invite?invitestatus=true&user_id='.$userid.'&invite_id='.$userinvite_id, true );
				$from=$user_email;
				$this->_sendemail($from,$to,'Invitation Mail',$messagedata,'user_invite');
				$this->Flash->success(__('The user invite has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user invite could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserInvite.' . $this->UserInvite->primaryKey => $id));
			$this->request->data = $this->UserInvite->find('first', $options);
		}

	}
	
	
	public function invite(){
		$this->loadModel('User');
		$this->layout=false;
		$this->render(false);
		$user_id=isset($_GET['user_id'])?$_GET['user_id']:'';
		$invite_id=isset($_GET['invite_id'])?$_GET['invite_id']:'';
		$pwd=isset($_GET['pwd'])?$_GET['pwd']:'';


		$find_user=$this->User->find('first',array('conditions'=>array('id'=>$user_id,'is_verified'=>1)));

		if(empty($find_user))
		{
			$user_data['User']['is_verified']='1';
			$user_data['User']['password']=$pwd;
			
			$user_invite_data['UserInvite']['invite_status']='1';
		
			$this->User->id=$user_id;
			$this->User->save($user_data);
			
			$this->UserInvite->id=$invite_id;
			$this->UserInvite->save($user_invite_data);
			echo '<h2  style="color:white;border: 2px solid green;text-align: center;background-color: green;">User Activated Successfully</h2>';
		}
		else
		{
			echo '<h2 style="border: 2px solid red;text-align: center; background-color: red;color: white;">User Already Activated</h2>';
		}

	}


	public function activate(){
		$this->layout=false;
		$this->render(false);
		$invite_id=isset($_GET['invite_id'])?$_GET['invite_id']:'';
		$status=isset($_GET['invitestatus'])?$_GET['invitestatus']:false;
		$password= isset($_GET['passkey'])? base64_decode($_GET['passkey']):'';
		$email= isset($_GET['invitedmail'])? base64_decode($_GET['invitedmail']):'';
		
		if($status==false)
		return "Status must be true";
		
		if(empty($invite_id))
		return "Invite ID cannot be blank";
		
		App::Import('Model','User');
		$this->User=new User();
		
		if ($this->User->hasAny(array('User.email' => $email))){
			echo "This link has been expired!";
			exit;
		}
		
		$userinvite=$this->UserInvite->find('first',array('conditions'=>array('UserInvite.id'=>$invite_id)));
		
		$user_data['User']['email']=$userinvite['UserInvite']['invite_emailid'];
		$user_data['User']['is_verified']='1';
		$user_data['User']['password']=$password;
		$user_data['User']['type']='parent';
		$user_data['User']['free_book']='1';
		$user_data['User']['invite_target']='1';
		
		$user_invite_data['UserInvite']['id']=$invite_id;
		$user_invite_data['UserInvite']['invite_status']='1';
		
		App::Import('Model','User');
		$this->User=new User();
		
		$this->User->create();
		$this->User->save($user_data);
		
		$this->UserInvite->create();
		$this->UserInvite->save($user_invite_data);
		
		//Set the json-encoded data to be the body
		$this->response->body(json_encode(array('User Activated Successfully')));
		$this->response->statusCode(200);
		$this->response->type('application/json');
		return $this->response;
	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserInvite->id = $id;
		if (!$this->UserInvite->exists()) {
			throw new NotFoundException(__('Invalid user invite'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserInvite->delete()) {
			$this->Flash->success(__('The user invite has been deleted.'));
		} else {
			$this->Flash->error(__('The user invite could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
}
