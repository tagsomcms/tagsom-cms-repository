<?php
App::uses('AppController', 'Controller');
/**
 * Parents Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class ParentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','UploadFile');
	public $uses = array('User','Level','Story','Language','UserInvite','UserRelation');
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	// Allow users to register and logout.
    	$this->Auth->allow('add', 'logout');
	}
	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$users=$this->User->find('all',array('conditions'=>array('type'=>'parent'),'order'=>'User.id ASC'));
    	$this->set(compact('users'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$dob=$this->request->data['User']['dob'];
			$yob=date('Y', strtotime($dob));
			$this->request->data['User']['yob']=$yob;
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$dob=$this->request->data['User']['dob'];
			$yob=date('Y', strtotime($dob));
			$this->request->data['User']['yob']=$yob;
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
