<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
/**
 * ModeQuestionAnswers Controller
 *
 * @property ModeQuestionAnswer $ModeQuestionAnswer
 * @property PaginatorComponent $Paginator
 */
class ModeQuestionAnswersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * Index method (Display list of mode question answers)
 *
 * @return void
 */
	public function index() {
		$this->ModeQuestionAnswer->recursive = 0;
		$this->paginate=array('limit'=>15, 'order' => ['ModeQuestionAnswer.id' => 'DESC'],'conditions'=>array('ModeQuestionAnswer.language_id'=>2));
		$this->set('modeQuestionAnswers', $this->Paginator->paginate());

	}

/**
 * View method (Display the detail view of selected mode question answer)
 *
 * @throws NotFoundException
 * @param Integer $id pass as unique question_id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('Question');
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid mode question answer'));
		}
		$options = array('conditions' => array('id'=>$id));
		$this->set('modeQuestionAnswer', $this->Question->find('first', $options));
	}

/**
 * Add method (for add new mode_question answer)
 * @param no parameters are there
 * @return void
 */
	public function add() {
		$this->loadModel('Story');
		$this->loadModel('Mode');
		if ($this->request->is('post')) {
			$this->loadModel('Apitoken');
			$api=$this->Apitoken->get_active_api();
			$mode_id=$this->data['ModeQuestionAnswer']['mode_id'];
			$story_id=$this->data['ModeQuestionAnswer']['story_id'];
			$options2 = array('conditions' => array('Story.id' => $story_id));
			$story_name=$this->Story->find('first',$options2);
			$storyname=$story_name['Story']['story_name'];
			$story_path=str_replace(' ', '_', $storyname);
			$options3 = array('conditions' => array('Mode.id' => $mode_id));
			$mode_name=$this->Mode->find('first',$options3);
			$modename=$mode_name['Mode']['mode_name']."_Mode";

			$cnt=rand(111,999);
			$option1=$this->request->data['ModeQuestionAnswer']['mode_option1'];
			$option2=$this->request->data['ModeQuestionAnswer']['mode_option2'];
			$option3=$this->request->data['ModeQuestionAnswer']['mode_option3'];
			$answer=$this->request->data['ModeQuestionAnswer']['mode_answer'];

			if((!empty($option1) && empty($option2) && empty($option3)) || (!empty($option2) && empty($option1) && empty($option3)) || (!empty($option3) && empty($option1) && empty($option2)))
			{
				$this->Flash->error(__('You have to fill all options for question'));
			}
			else if((!empty($answer)) && (!empty($option1) && !empty($option2) && !empty($option3)) && ($option1!=$answer && $option2!=$answer && $option3!=$answer)){
				$this->Flash->error(__('Atleast one option must match with mode answer for question'));
			}
			else if((!empty($option1) && !empty($option2) && !empty($option3)) && ($option1==$option2 || $option1==$option3 || $option2==$option3)){
				$this->Flash->error(__('All options must be different'));
			}
			else
			{
				$file=$this->data['ModeQuestionAnswer']['audio_clip'];
				$tmp_name=$file['tmp_name'];
				$file_name=$file['name'];
				if(!empty($file_name))
				{
					$file_name='QuestionAudio'.$cnt.'.wav';
				}

				$file1=$this->data['ModeQuestionAnswer']['answer_audio1'];
				$temp_name11=$file1['tmp_name'];
				$file_name11=$file1['name'];
				if(!empty($file_name11))
				{
					$file_name11='AnswerAudio'.$cnt.'.wav';
				}

				$file2=$this->data['ModeQuestionAnswer']['option_1_audio'];
				$temp_name22=$file2['tmp_name'];
				$file_name22=$file2['name'];
				if(!empty($file_name22))
				{
					$file_name22='Option1Audio'.$cnt.'.wav';
				}

				$file3=$this->data['ModeQuestionAnswer']['option_2_audio'];
				$temp_name33=$file3['tmp_name'];
				$file_name33=$file3['name'];
				if(!empty($file_name33))
				{
					$file_name33='Option2Audio'.$cnt.'.wav';
				}

				$file4=$this->data['ModeQuestionAnswer']['option_3_audio'];
				$temp_name44=$file4['tmp_name'];
				$file_name44=$file4['name'];
				if(!empty($file_name44))
				{
					$file_name44='Option3Audio'.$cnt.'.wav';
				}

				$img_file=$this->data['ModeQuestionAnswer']['image_to_find'];
				$tmp_name1=$img_file['tmp_name'];
				$img=$img_file['name'];
				if(!empty($tmp_name1))
				{
				$img='QuestionImage'.$cnt.'.jpg';
				}

				$img_file2=$this->data['ModeQuestionAnswer']['chapter_image'];
				$tmp_name2=$img_file2['tmp_name'];
				$img2=$img_file2['name'];
				if(!empty($tmp_name2))
				{
				$img2='ChapterImage'.$cnt.'.jpg';
				}
				

			    //add minigames in poeditor
			    //get project_id
			    $HttpSocket = new HttpSocket();
				$project_array2=array('api_token' => $api,'action'=>'list_projects');
				$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
				$list_projects2 = json_decode($list_project2, true);
				foreach($list_projects2['list'] as $project1)
				{
					if($project1['name']=='Tagsom Project'){
						$story_id=$project1['id'];
					}
				}
				
				$terms_arr=array('api_token' => $api,'action'=>'view_terms','id'=>$story_id,'language'=>'en');
				$terms=$HttpSocket->post('https://poeditor.com/api/', $terms_arr);
				$list_terms = json_decode($terms, true);
				
				//add question to poeditor
				$context='Question'.$cnt;
				$arr1=array("term"=>$this->request->data['ModeQuestionAnswer']['mode_question'],"context"=>$context,"plural"=>"Question");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr1),true));

		    	$request=array();
		    	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add answer of question
		    	$context2='Answer'.$cnt;
				$arr2=array("term"=>$this->request->data['ModeQuestionAnswer']['mode_answer'],"context"=>$context2,"plural"=>"Answer");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr2),true));

		    	$request=array();
		    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add option 1 of question
		    	$context3='Option1(Question'.$cnt.')';
				$arr3=array("term"=>$option1,"context"=>$context3,"plural"=>"Option1");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr3),true));

		    	$request=array();
		    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add option 2 of question
		    	$context4='Option2(Question'.$cnt.')';
				$arr4=array("term"=>$option2,"context"=>$context4,"plural"=>"Option2");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr4),true));

		    	$request=array();
		    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add option 3 of question
		    	$context5='Option3(Question'.$cnt.')';
				$arr5=array("term"=>$option3,"context"=>$context5,"plural"=>"Option3");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id,
		           "data" => json_encode(array($arr5),true));

		    	$request=array();
		    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	$story_of_languages=$this->Story->find('all',array('conditions'=>array('story_name'=>$storyname)));

				foreach($story_of_languages as $story_language)
				{
					if($story_language['Story']['language_id']==2)
					{
						$this->ModeQuestionAnswer->create();
						$this->request->data['ModeQuestionAnswer']['story_id']=$story_language['Story']['id'];
						$this->request->data['ModeQuestionAnswer']['language_id']=$story_language['Story']['language_id'];
						$this->request->data['ModeQuestionAnswer']['audio_clip']=$file_name;
						$this->request->data['ModeQuestionAnswer']['answer_audio']=$file_name11;
						$this->request->data['ModeQuestionAnswer']['option1_audio']=$file_name22;
						$this->request->data['ModeQuestionAnswer']['option2_audio']=$file_name33;
						$this->request->data['ModeQuestionAnswer']['option3_audio']=$file_name44;
						$this->request->data['ModeQuestionAnswer']['chapter_image']=$img2;
						$this->request->data['ModeQuestionAnswer']['image_to_find']=$img;
						$this->request->data['ModeQuestionAnswer']['mode_question']=$this->request->data['ModeQuestionAnswer']['mode_question'];
						$this->request->data['ModeQuestionAnswer']['mode_answer']=$this->request->data['ModeQuestionAnswer']['mode_answer'];
					    $this->ModeQuestionAnswer->save($this->request->data);
					}
					else
					{
						$this->ModeQuestionAnswer->create();
						$this->request->data['ModeQuestionAnswer']['story_id']=$story_language['Story']['id'];
						$this->request->data['ModeQuestionAnswer']['language_id']=$story_language['Story']['language_id'];
						$this->request->data['ModeQuestionAnswer']['audio_clip']=$file_name;
						$this->request->data['ModeQuestionAnswer']['answer_audio']=$file_name11;
						$this->request->data['ModeQuestionAnswer']['option1_audio']=$file_name22;
						$this->request->data['ModeQuestionAnswer']['option2_audio']=$file_name33;
						$this->request->data['ModeQuestionAnswer']['option3_audio']=$file_name44;
						$this->request->data['ModeQuestionAnswer']['chapter_image']=$img2;
						$this->request->data['ModeQuestionAnswer']['image_to_find']=$img;
						$this->request->data['ModeQuestionAnswer']['mode_question']='No Translation';
						$this->request->data['ModeQuestionAnswer']['mode_answer']='No Translation';
						$this->request->data['ModeQuestionAnswer']['mode_option1']='No Translation';
						$this->request->data['ModeQuestionAnswer']['mode_option2']='No Translation';
						$this->request->data['ModeQuestionAnswer']['mode_option3']='No Translation';
					    $this->ModeQuestionAnswer->save($this->request->data);
					}
				}


				if (!file_exists(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English')) {
     				mkdir(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English', 0777, true);
				}
				if (!file_exists(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image')) {
     				mkdir(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image', 0777, true);
				}
				
				move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name);
				move_uploaded_file($temp_name11, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name11);
				move_uploaded_file($temp_name22, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name22);
				move_uploaded_file($temp_name33, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name33);
				move_uploaded_file($temp_name44, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name44);
				move_uploaded_file($tmp_name2, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.$img2);
				move_uploaded_file($tmp_name1, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.$img);

				$this->Flash->success(__('The mode question answer has been saved.'));
				return $this->redirect(array('action' => 'add'));
			}
		}
		$modes = $this->ModeQuestionAnswer->Mode->find('list',array('fields'=>array('Mode.mode_name'),'order'=>array('Mode.mode_name ASC')));
		$stories = $this->ModeQuestionAnswer->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('Story.language_id'=>2)));
		$languages = $this->ModeQuestionAnswer->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->ModeQuestionAnswer->Level->find('list',array('fields'=>array('Level.level_name')));
		$this->set(compact('modes', 'stories', 'languages','levels'));
	}

/**
 * Edit method (for edit existing mode question answer)
 *
 * @throws NotFoundException
 * @param Integer $id pass as unique question_id
 * @return void
 */
	public function edit($id = null) {
		$this->loadModel('Story');
		$this->loadModel('Mode');
		$this->loadModel('Apitoken');
		$api=$this->Apitoken->get_active_api();
		if (!$this->ModeQuestionAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid mode question answer'));
		}
		$options1 = array('conditions' => array('ModeQuestionAnswer.id' => $id));
		$mode_question=$this->ModeQuestionAnswer->find('first',$options1);
		$mode_id=$mode_question['ModeQuestionAnswer']['mode_id'];
		$question=$mode_question['ModeQuestionAnswer']['mode_question'];
		$story_id=$mode_question['ModeQuestionAnswer']['story_id'];
		$options2 = array('conditions' => array('Story.id' => $story_id));
		$story_name=$this->Story->find('first',$options2);
		$storyname=$story_name['Story']['story_name'];
		$story_path=str_replace(' ', '_', $storyname);
		$modeid=$mode_question['ModeQuestionAnswer']['mode_id'];
		$options3 = array('conditions' => array('Mode.id' => $modeid));
		$mode_name=$this->Mode->find('first',$options3);
		$modename=$mode_name['Mode']['mode_name']."_Mode";

		if ($this->request->is(array('post', 'put'))) {
			$change_story_id=$this->request->data['ModeQuestionAnswer']['story_id'];
			$change_story=$this->Story->find('first',array('conditions'=>array('id'=>$change_story_id)));
			$change_storyname=$change_story['Story']['story_name'];
			$change_stories=$this->Story->find('all',array('conditions'=>array('story_name'=>$change_storyname)));
			$modeid=$this->request->data['ModeQuestionAnswer']['mode_id'];
			$options3 = array('conditions' => array('Mode.id' => $modeid));
			$mode_name=$this->Mode->find('first',$options3);
			$modename1=$mode_name['Mode']['mode_name']."_Mode";
			$cnt=$this->request->data['ModeQuestionAnswer']['count'];

			if($modename1!=$modename)
			{
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'QuestionAudio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/English/'.'QuestionAudio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'AnswerAudio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/English/'.'AnswerAudio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'Option1Audio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/English/'.'Option1Audio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'Option2Audio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/English/'.'Option2Audio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'Option3Audio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/English/'.'Option3Audio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.'ChapterImage'.$cnt.'.jpg', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/Image/'.'ChapterImage'.$cnt.'.jpg');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.'QuestionImage'.$cnt.'.jpg', WWW_ROOT . 'uploads/'.$story_path.'/'.$modename1.'/Image/'.'QuestionImage'.$cnt.'.jpg');
			}
			if($storyname!=$change_storyname)
			{
				$change_path=str_replace(' ', '_', $change_storyname);
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'QuestionAudio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/English/'.'QuestionAudio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'AnswerAudio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/English/'.'AnswerAudio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'Option1Audio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/English/'.'Option1Audio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'Option2Audio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/English/'.'Option2Audio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.'Option3Audio'.$cnt.'.wav', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/English/'.'Option3Audio'.$cnt.'.wav');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.'ChapterImage'.$cnt.'.jpg', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/Image/'.'ChapterImage'.$cnt.'.jpg');
				copy(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.'QuestionImage'.$cnt.'.jpg', WWW_ROOT . 'uploads/'.$change_path.'/'.$modename1.'/Image/'.'QuestionImage'.$cnt.'.jpg');
			}

			$img1=$this->request->data['ModeQuestionAnswer']['image_to_find1'];
			$img2=$this->request->data['ModeQuestionAnswer']['chapter_image'];

			$img_file22=$this->request->data['ModeQuestionAnswer']['chapter_image'];
			$tmp_name22=$img_file22['tmp_name'];
			$img22='ChapterImage'.$cnt.'.jpg';

			$img_file=$this->data['ModeQuestionAnswer']['image_to_find'];
			$tmp_name1=$img_file['tmp_name'];
			$img='QuestionImage'.$cnt.'.jpg';

			$file1=$this->data['ModeQuestionAnswer']['answer_audio1'];
			$temp_name1=$file1['tmp_name'];
			$file_name1='AnswerAudio'.$cnt.'.wav';

			$file2=$this->data['ModeQuestionAnswer']['option_1_audio'];
			$temp_name2=$file2['tmp_name'];
			$file_name2='Option1Audio'.$cnt.'.wav';

			$file2=$this->data['ModeQuestionAnswer']['option_2_audio'];
			$temp_name3=$file2['tmp_name'];
			$file_name3='Option2Audio'.$cnt.'.wav';

			$file3=$this->data['ModeQuestionAnswer']['option_3_audio'];
			$temp_name4=$file3['tmp_name'];
			$file_name4='Option3Audio'.$cnt.'.wav';

			if (!file_exists(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English')) {
 				mkdir(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English', 0777, true);
			}
			if (!file_exists(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image')) {
 				mkdir(WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image', 0777, true);
			}

			if(empty($answer_audio1)){
				if(!empty($temp_name1)){
				$this->request->data['ModeQuestionAnswer']['answer_audio']=$file_name1;
				move_uploaded_file($temp_name1, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name1);
				}
			}else{
				move_uploaded_file($temp_name1, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name1);
			}
			if(empty($option1_audio)){
				if(!empty($temp_name2)){
				$this->request->data['ModeQuestionAnswer']['option1_audio']=$file_name2;
				move_uploaded_file($temp_name2, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name2);
				}
			}else{
				move_uploaded_file($temp_name2, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name2);
			}
			if(empty($option2_audio)){
				if(!empty($temp_name3)){
				$this->request->data['ModeQuestionAnswer']['option2_audio']=$file_name3;
				move_uploaded_file($temp_name3, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name3);
				}
			}else{
				move_uploaded_file($temp_name3, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name3);
			}
			if(empty($option3_audio)){
				if(!empty($temp_name4)){
				$this->request->data['ModeQuestionAnswer']['option3_audio']=$file_name4;
				move_uploaded_file($temp_name4, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name4);
				}
			}else{
				move_uploaded_file($temp_name4, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name4);
			}
		

			$file=$this->request->data['ModeQuestionAnswer']['audio_clip'];
			$tmp_name=$file['tmp_name'];
			if(!empty($tmp_name)){
				$file_name='QuestionAudio'.$cnt.'.wav';
				move_uploaded_file($tmp_name, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/English/'.$file_name);
				$audiofile=$file_name;
				$this->request->data['ModeQuestionAnswer']['audio_clip']=$audiofile;
			}
			else
			{
				if(!empty($que_audio))
				{
					$this->request->data['ModeQuestionAnswer']['audio_clip']=$que_audio;
				}
				else
				{
					$this->request->data['ModeQuestionAnswer']['audio_clip']='';
				}
			}
			
			if(empty($tmp_name22))
			{
				$this->request->data['ModeQuestionAnswer']['chapter_image']=$this->request->data['ModeQuestionAnswer']['chapter_h'];
			}
			else
			{
				$this->request->data['ModeQuestionAnswer']['chapter_image']=$img22;
			}
			if(empty($img_file))
			{
				$this->request->data['ModeQuestionAnswer']['image_to_find']=$this->request->data['ModeQuestionAnswer']['image_to_find1'];
			}
			else
			{
				$this->request->data['ModeQuestionAnswer']['image_to_find']=$img;
			}
		
			$option1=$this->request->data['ModeQuestionAnswer']['mode_option1'];
			$option2=$this->request->data['ModeQuestionAnswer']['mode_option2'];
			$option3=$this->request->data['ModeQuestionAnswer']['mode_option3'];
			$answer=$this->request->data['ModeQuestionAnswer']['mode_answer'];

			move_uploaded_file($tmp_name1, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.$img);
			move_uploaded_file($tmp_name22, WWW_ROOT . 'uploads/'.$story_path.'/'.$modename.'/Image/'.$img22);
			

			if((!empty($option1) && empty($option2) && empty($option3)) || (!empty($option2) && empty($option1) && empty($option3)) || (!empty($option3) && empty($option1) && empty($option2)))
			{
				$this->Flash->error(__('You have to fill all options for question 1'));
			}
			else if((!empty($answer)) && (!empty($option1) && !empty($option2) && !empty($option3)) && ($option1!=$answer && $option2!=$answer && $option3!=$answer)){
				$this->Flash->error(__('Atleast one option must match with mode answer for question 1'));
			}
			else
			{

				if($this->request->data['ModeQuestionAnswer']['question_h']!=$this->request->data['ModeQuestionAnswer']['mode_question'] 
				|| $this->request->data['ModeQuestionAnswer']['answer_h']!=$this->request->data['ModeQuestionAnswer']['mode_answer']
				|| $option1!=$this->request->data['ModeQuestionAnswer']['opt1_h']
				|| $option2!=$this->request->data['ModeQuestionAnswer']['opt2_h']
				|| $option3!=$this->request->data['ModeQuestionAnswer']['opt3_h']

				)
				
				{
				//add minigames in poeditor
			    //get project_id
			    $HttpSocket = new HttpSocket();
				$project_array2=array('api_token' => $api,'action'=>'list_projects');
				$list_project2 = $HttpSocket->post('https://poeditor.com/api/', $project_array2);
				$list_projects2 = json_decode($list_project2, true);
				foreach($list_projects2['list'] as $project1)
				{
					if($project1['name']=='Tagsom Project'){
						$story_id2=$project1['id'];
					}
				}

				$arr1=array("term"=>$question);
			    $data = array(
		           "api_token" => $api,
		           "action" => "view_terms",
		           "id" => $story_id2,
		           "data" => json_encode(array($arr1),true));

		    	$request=array();
		    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
		    	$terms = json_decode($response3, true);
		    	foreach($terms['list'] as $term)
		    	{
		    		if($term['term']==$question)
		    		{
		    			$count=substr($term['context'],-3);
		    		}
		    	}
		    	//delete
		    	$del=$this->deletepo($id);
		
		  		//add question to poeditor
				$context='Question'.$count;
				$arr1=array("term"=>$this->request->data['ModeQuestionAnswer']['mode_question'],"context"=>$context,"plural"=>"Question");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id2,
		           "data" => json_encode(array($arr1),true));

		    	$request=array();
		    	$response1 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add answer of question
		    	$context2='Answer'.$count;
				$arr2=array("term"=>$this->request->data['ModeQuestionAnswer']['mode_answer'],"context"=>$context2,"plural"=>"Answer");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id2,
		           "data" => json_encode(array($arr2),true));

		    	$request=array();
		    	$response2 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add option 1 of question
		    	$context3='Option1(Question'.$count.')';
				$arr3=array("term"=>$option1,"context"=>$context3,"plural"=>"Option1");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id2,
		           "data" => json_encode(array($arr3),true));

		    	$request=array();
		    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add option 2 of question
		    	$context4='Option2(Question'.$count.')';
				$arr4=array("term"=>$option2,"context"=>$context4,"plural"=>"Option2");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id2,
		           "data" => json_encode(array($arr4),true));

		    	$request=array();
		    	$response4 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);

		    	//add option 3 of question
		    	$context5='Option3(Question'.$count.')';
				$arr5=array("term"=>$option3,"context"=>$context5,"plural"=>"Option3");
			    $data = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $story_id2,
		           "data" => json_encode(array($arr5),true));

		    	$request=array();
		    	$response5 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
		    	}

		    	if($change_storyname==$storyname)
		    	{
			    	$story_languages=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));

					foreach($story_languages as $story1)
					{
						$minigame_languages=$this->ModeQuestionAnswer->find('first',array('conditions'=>array('story_id'=>$story1['Story']['id'],'chapter_image'=>$this->request->data['ModeQuestionAnswer']['chapter_h'])));
					
						$this->ModeQuestionAnswer->id=$minigame_languages['ModeQuestionAnswer']['id'];
						$this->request->data['ModeQuestionAnswer']['id']=$minigame_languages['ModeQuestionAnswer']['id'];
						$this->request->data['ModeQuestionAnswer']['story_id']=$story1['Story']['id'];
						if($story1['Story']['language_id']==2)
						{
							$this->ModeQuestionAnswer->save($this->request->data);
						}
						else
						{
							$this->request->data['ModeQuestionAnswer']['mode_question']='No Translation';
							$this->request->data['ModeQuestionAnswer']['mode_answer']='No Translation';
							$this->request->data['ModeQuestionAnswer']['mode_option1']='No Translation';
							$this->request->data['ModeQuestionAnswer']['mode_option2']='No Translation';
							$this->request->data['ModeQuestionAnswer']['mode_option3']='No Translation';

							$this->ModeQuestionAnswer->save($this->request->data);
						}
					}
				}
				else
				{
					//add minigame for new story
					$minigames=$this->ModeQuestionAnswer->find('first',array('conditions'=>array('id'=>$id)));
					foreach($change_stories as $story)
					{
						if($story['Story']['language_id']==2)
						{
							$this->ModeQuestionAnswer->create();
							$arr=array('mode_id'=>$minigames['ModeQuestionAnswer']['mode_id'],
								'story_id'=>$story['Story']['id'],
								'language_id'=>$story['Story']['language_id'],
								'level_id'=>$minigames['ModeQuestionAnswer']['level_id'],
								'mode_question'=>$minigames['ModeQuestionAnswer']['mode_question'],
								'mode_answer'=>$minigames['ModeQuestionAnswer']['mode_answer'],
								'audio_clip'=>$minigames['ModeQuestionAnswer']['audio_clip'],
								'answer_audio'=>$minigames['ModeQuestionAnswer']['answer_audio'],
								'option1_audio'=>$minigames['ModeQuestionAnswer']['option1_audio'],
								'option2_audio'=>$minigames['ModeQuestionAnswer']['option2_audio'],
								'option3_audio'=>$minigames['ModeQuestionAnswer']['option3_audio'],
								'chapter_image'=>$minigames['ModeQuestionAnswer']['chapter_image'],
								'image_to_find'=>$minigames['ModeQuestionAnswer']['image_to_find'],
								'mode_option1'=>$minigames['ModeQuestionAnswer']['mode_option1'],
								'mode_option2'=>$minigames['ModeQuestionAnswer']['mode_option2'],
								'mode_option3'=>$minigames['ModeQuestionAnswer']['mode_option3'],
								);
							$this->ModeQuestionAnswer->save($arr);
						}
						else
						{
							$this->ModeQuestionAnswer->create();
							$arr=array('mode_id'=>$minigames['ModeQuestionAnswer']['mode_id'],
								'story_id'=>$story['Story']['id'],
								'language_id'=>$story['Story']['language_id'],
								'level_id'=>$minigames['ModeQuestionAnswer']['level_id'],
								'mode_question'=>'No Translation',
								'mode_answer'=>'No Translation',
								'audio_clip'=>$minigames['ModeQuestionAnswer']['audio_clip'],
								'answer_audio'=>$minigames['ModeQuestionAnswer']['answer_audio'],
								'option1_audio'=>$minigames['ModeQuestionAnswer']['option1_audio'],
								'option2_audio'=>$minigames['ModeQuestionAnswer']['option2_audio'],
								'option3_audio'=>$minigames['ModeQuestionAnswer']['option3_audio'],
								'chapter_image'=>$minigames['ModeQuestionAnswer']['chapter_image'],
								'image_to_find'=>$minigames['ModeQuestionAnswer']['image_to_find'],
								'mode_option1'=>'No Translation',
								'mode_option2'=>'No Translation',
								'mode_option3'=>'No Translation',
								);
							$this->ModeQuestionAnswer->save($arr);
						}
					}

					//delete old minigames from old story
					$story_languages=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));
					foreach($story_languages as $story1)
					{
						$minigame_languages=$this->ModeQuestionAnswer->find('first',array('conditions'=>array('story_id'=>$story1['Story']['id'],'chapter_image'=>$this->request->data['ModeQuestionAnswer']['chapter_h'])));
					
						$this->ModeQuestionAnswer->id=$minigame_languages['ModeQuestionAnswer']['id'];
						$this->ModeQuestionAnswer->delete();
					}
				}
				$this->Flash->success(__('The mode question answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
				
			}
		} else {
			$options = array('conditions' => array('ModeQuestionAnswer.' . $this->ModeQuestionAnswer->primaryKey => $id));
			$this->request->data = $this->ModeQuestionAnswer->find('first', $options);
		}
		$modes = $this->ModeQuestionAnswer->Mode->find('list',array('fields'=>array('Mode.mode_name')));
		$stories = $this->ModeQuestionAnswer->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('Story.language_id'=>2)));
		$languages = $this->ModeQuestionAnswer->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->ModeQuestionAnswer->Level->find('list',array('fields'=>array('Level.level_name')));
		$options1 = array('conditions' => array('ModeQuestionAnswer.id' => $id));
		$image = $this->ModeQuestionAnswer->find('all',$options1);
		$this->set(compact('modes', 'stories', 'languages','levels','image','modename','levelname','story_path'));
		foreach($image as $img)
		{
			$img_to_find=$img['ModeQuestionAnswer']['image_to_find'];
			$chapter=$img['ModeQuestionAnswer']['chapter_image'];
			$audio=$img['ModeQuestionAnswer']['audio_clip'];
			$answer_audio1=$img['ModeQuestionAnswer']['answer_audio'];
			$option1_audio=$img['ModeQuestionAnswer']['option1_audio'];
			$option2_audio=$img['ModeQuestionAnswer']['option2_audio'];
			$option3_audio=$img['ModeQuestionAnswer']['option3_audio'];
			$cnt=!empty(substr($chapter,-7,-4))?substr($chapter,-7,-4):$cnt;
			// $cnt=!empty(substr($img_to_find,-7,-4))?substr($img_to_find,-7,-4):$cnt;
			// $cnt=!empty(substr($audio,-7,-4))?substr($audio,-7,-4):$cnt;
			// $cnt=!empty(substr($answer_audio1,-7,-4))?substr($answer_audio1,-7,-4):$cnt;
			// $cnt=!empty(substr($option1_audio,-7,-4))?substr($option1_audio,-7,-4):$cnt;
			// $cnt=!empty(substr($option2_audio,-7,-4))?substr($option2_audio,-7,-4):$cnt;
			// $cnt=!empty(substr($option2_audio,-7,-4))?substr($option2_audio,-7,-4):$cnt;
			// $cnt=!empty(substr($option3_audio,-7,-4))?substr($option3_audio,-7,-4):$cnt;
			if(empty($cnt))
			{
				$cnt=rand(111,999);
			}
			$this->set('cnt',$cnt);
		}

	}


/**
 * delete method (for delete existing mode question answer)
 *
 * @throws NotFoundException
 * @param Integer $id pass as unique question_id
 * @return void
 */
	public function delete($id = null) {
		$this->ModeQuestionAnswer->id = $id;
		$this->loadModel('Apitoken');
		$this->loadModel('Story');
		$api=$this->Apitoken->get_active_api();

		$options = array('conditions' => array('ModeQuestionAnswer.id' => $id));
		$mode_question_answers=$this->ModeQuestionAnswer->find('first',$options);
		$story_id1=$mode_question_answers['ModeQuestionAnswer']['story_id'];
		$chap=$mode_question_answers['ModeQuestionAnswer']['chapter_image'];
		$mode_question=$mode_question_answers['ModeQuestionAnswer']['mode_question'];
		$mode_answer=$mode_question_answers['ModeQuestionAnswer']['mode_answer'];
		$option1=$mode_question_answers['ModeQuestionAnswer']['mode_option1'];
		$option2=$mode_question_answers['ModeQuestionAnswer']['mode_option2'];
		$option3=$mode_question_answers['ModeQuestionAnswer']['mode_option3'];
		$options1 = array('conditions' => array('Story.id' => $story_id1));
		$stories=$this->Story->find('first',$options1);
		$storyname=$stories['Story']['story_name'];

		$project_array1=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
		$list_projects1 = json_decode($list_project1, true);
		foreach($list_projects1['list'] as $project)
		{
			if($project['name']=='Tagsom Project'){
				$story_id=$project['id'];
			}
		}
		
		$arr1=array("term"=>$mode_question);
	    $data = array(
           "api_token" => $api,
           "action" => "view_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr1),true));

    	$request=array();
    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
    	$terms = json_decode($response3, true);
    	foreach($terms['list'] as $term)
    	{
    		if($term['term']==$mode_question)
    		{
    			$count=substr($term['context'],-3);
    		}
    	}
    	$ans="Answer".$count;
    	$arr2=array("term"=> $mode_answer,"context"=>$ans);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr2),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);
    
    	$opt1="Option1(Question".$count.")";
    	$arr3=array("term"=> $option1,"context"=>$opt1);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr3),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

    	$opt2="Option2(Question".$count.")";
    	$arr4=array("term"=> $option2,"context"=>$opt2);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr4),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

    	$opt3="Option3(Question".$count.")";
    	$arr6=array("term"=> $option3,"context"=>$opt3);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr6),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

    	$ques="Question".$count;
    	$arr5=array("term"=> $mode_question,"context"=>$ques);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr5),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

		
		$story_languages=$this->Story->find('all',array('conditions'=>array('Story.story_name'=>$storyname)));

		foreach($story_languages as $story)
		{
			//$minigame_languages=$this->ModeQuestionAnswer->find('first',array('conditions'=>array('story_id'=>$story['Story']['id'])));
			$minigame_languages=$this->ModeQuestionAnswer->find('first',array('conditions'=>array('story_id'=>$story['Story']['id'],'chapter_image'=>$chap)));

			$this->ModeQuestionAnswer->id=$minigame_languages['ModeQuestionAnswer']['id'];
			$this->ModeQuestionAnswer->delete();
		}

			$this->Flash->success(__('The mode question answer has been deleted.'));
	
		return $this->redirect(array('action' => 'index'));
	}





/**
 * delete from poeditormethod (for delete existing mode question answer)
 *
 * @throws NotFoundException
 * @param Integer $id pass as unique question_id
 * @return void
 */
	public function deletepo($id = null) {
		$this->ModeQuestionAnswer->id = $id;
		$this->loadModel('Apitoken');
		$this->loadModel('Story');
		$api=$this->Apitoken->get_active_api();

		$options = array('conditions' => array('ModeQuestionAnswer.id' => $id));
		$mode_question_answers=$this->ModeQuestionAnswer->find('first',$options);
		$story_id1=$mode_question_answers['ModeQuestionAnswer']['story_id'];
		$mode_question=$mode_question_answers['ModeQuestionAnswer']['mode_question'];
		$mode_answer=$mode_question_answers['ModeQuestionAnswer']['mode_answer'];
		$option1=$mode_question_answers['ModeQuestionAnswer']['mode_option1'];
		$option2=$mode_question_answers['ModeQuestionAnswer']['mode_option2'];
		$options1 = array('conditions' => array('Story.id' => $story_id1));
		$stories=$this->Story->find('first',$options1);
		$storyname=$stories['Story']['story_name'];

		$project_array1=array('api_token' => $api,'action'=>'list_projects');
		$HttpSocket = new HttpSocket();
		$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
		$list_projects1 = json_decode($list_project1, true);
		foreach($list_projects1['list'] as $project)
		{
			if($project['name']=='Tagsom Project'){
				$story_id=$project['id'];
			}
		}
		
		$arr1=array("term"=>$mode_question);
	    $data = array(
           "api_token" => $api,
           "action" => "view_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr1),true));

    	$request=array();
    	$response3 = $HttpSocket->post('https://poeditor.com/api/', $data,$request);
    	$terms = json_decode($response3, true);
    	foreach($terms['list'] as $term)
    	{
    		if($term['term']==$mode_question)
    		{
    			$count=substr($term['context'],-3);
    		}
    	}
    	$ans="Answer".$count;
    	$arr2=array("term"=> $mode_answer,"context"=>$ans);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr2),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);
    
    	$opt1="Option1(Question".$count.")";
    	$arr3=array("term"=> $option1,"context"=>$opt1);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr3),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

    	$opt2="Option2(Question".$count.")";
    	$arr4=array("term"=> $option2,"context"=>$opt2);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr4),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

    	$ques="Question".$count;
    	$arr5=array("term"=> $mode_question,"context"=>$ques);
	    $data = array(
           "api_token" => $api,
           "action" => "delete_terms",
           "id" => $story_id,
           "data" => json_encode(array($arr5),true));

    	$request=array();
    	$HttpSocket->post('https://poeditor.com/api/', $data,$request);

		return true;
	}



/**
 * Sync Minigames with POEditor
 *
 * @throws NotFoundException
 * @param Integer $id pass as unique question_id
 * @return void
 */
	public function sync_to_poeditor() {
		
	}


}
