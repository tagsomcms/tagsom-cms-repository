<?php
App::uses('AppController', 'Controller');
/**
 * UserModeGrades Controller
 *
 * @property UserModeGrade $UserModeGrade
 * @property PaginatorComponent $Paginator
 */
class UserModeGradesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserModeGrade->recursive = 0;
		$this->set('userModeGrades', $this->UserModeGrade->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('User');
		$this->loadModel('Level');
                $this->loadModel('Story');
                $this->loadModel('Language');
		if (!$this->UserModeGrade->exists($id)) {
			throw new NotFoundException(__('Invalid user mode grade'));
		}
		$options = array('contain'=>array('User','Level'),'conditions' => array('UserModeGrade.' . $this->UserModeGrade->primaryKey => $id));
		$userModeGrade=$this->UserModeGrade->find('first', array('contain'=>array('User','Level'),'conditions' => array('UserModeGrade.' . $this->UserModeGrade->primaryKey => $id)));
                
		$users = $this->User->find('first',array('conditions'=>array('id'=>$userModeGrade['UserModeGrade']['user_id'])));
		$levels = $this->UserModeGrade->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->Story->find('first',array('conditions'=>array('id'=>$userModeGrade['UserModeGrade']['story_id'])));
		$languages = $this->Language->find('first',array('conditions'=>array('id'=>$userModeGrade['UserModeGrade']['language_id'])));
		$modes = $this->UserModeGrade->Mode->find('list',array('fields'=>array('Mode.mode_name')));
		$grades = $this->UserModeGrade->Grade->find('list',array('fields'=>array('Grade.grade_name')));
		$this->set(compact('users', 'levels', 'stories', 'languages', 'modes', 'grades'));
		$this->set(compact('userModeGrade'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('User');
		$this->loadModel('Grade');
		$this->loadModel('Language');
		$language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$language_id=$language['Language']['id'];
		$users = $this->User->find('all');
		$grades = $this->UserModeGrade->Grade->find('all',array('fields'=>array('DISTINCT Grade.grade_name')));
		$grades_array=array();
		foreach($grades as $grade)
		{
			array_push($grades_array,$grade['Grade']['grade_name']);
		}
		$user_array=array();
	  	foreach($users as $user)
	  	{
	  		if($user['User']['type']=='child' || $user['User']['type']=='student')
	  		{
	    		if(!empty($user['User']['email']))
		    	{
		      		array_push($user_array,$user['User']['email']);
		    	}
		    	else
		    	{
		    		if(!empty($user['User']['username']))
			    	{
			      		array_push($user_array,$user['User']['username']);
			    	}
		    	}
		    }
	  	}
		if ($this->request->is('post')) {
			
			$user_email=$user_array[$this->request->data['UserModeGrade']['user_id']];
			$user_grade=$grades_array[$this->request->data['UserModeGrade']['grade_id']];
			$find_user=$this->User->find('first',array('conditions'=>array('email'=>$user_email)));
			if(empty($find_user))
			{
				$find_user=$this->User->find('first',array('conditions'=>array('username'=>$user_email)));
			}
			$this->request->data['UserModeGrade']['user_id']=$find_user['User']['id'];
			$find_grade=$this->Grade->find('first',array('conditions'=>array('grade_name'=>$user_grade)));
			$this->request->data['UserModeGrade']['grade_id']=$find_grade['Grade']['id'];
			$this->UserModeGrade->create();	
			if ($this->UserModeGrade->save($this->request->data)) {
				$this->Flash->success(__('The user mode grade has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user mode grade could not be saved. Please, try again.'));
			}
		}
		$users = $this->User->find('all',array('fields'=>array('username','email')));
		$levels = $this->UserModeGrade->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->UserModeGrade->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('language_id'=>$language_id)));
		$languages = $this->UserModeGrade->Language->find('list',array('fields'=>array('Language.language_name')));
		$modes = $this->UserModeGrade->Mode->find('list',array('fields'=>array('Mode.mode_name')));
		$this->set(compact('users', 'levels', 'stories', 'languages', 'modes', 'grades','user_array','grades_array'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->loadModel('User');
		$this->loadModel('Grade');
		$this->loadModel('Language');
		$language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$language_id=$language['Language']['id'];
		$grades = $this->UserModeGrade->Grade->find('all',array('fields'=>array('DISTINCT Grade.grade_name')));
		$grades_array=array();
		foreach($grades as $grade)
		{
			array_push($grades_array,$grade['Grade']['grade_name']);
		}
		$users = $this->User->find('all');
		$user_array=array();
	  	foreach($users as $user)
	  	{
	  		if($user['User']['type']=='child' || $user['User']['type']=='student')
	  		{
	    		if(!empty($user['User']['email']))
		    	{
		      		array_push($user_array,$user['User']['email']);
		    	}
		    	else
		    	{
		    		if(!empty($user['User']['username']))
			    	{
			      		array_push($user_array,$user['User']['username']);
			    	}
		    	}
		    }
	  	}


		$invitefind=$this->UserModeGrade->find('first',array('conditions'=>array('id'=>$id)));
	  	$user_find=$this->User->find('first',array('conditions'=>array('id'=>$invitefind['UserModeGrade']['user_id'])));
	  	$grade_find=$this->Grade->find('first',array('conditions'=>array('id'=>$invitefind['UserModeGrade']['grade_id'])));
	  	$user_email=$user_find['User']['email'];
	  	$user_grade=$grade_find['Grade']['grade_name'];
	  	$user_username=$user_find['User']['username'];
	  	$cnt=0;
	  
	  	foreach($user_array as $email)
	  	{
	  		if($email==$user_email)
	  		{
	  			$select=$cnt;
	  			break;
	  		}
	  		if($email==$user_username)
	  		{
	  			$select=$cnt;
	  			break;
	  		}
	  		$cnt++;
	  	}

	  	$cnt1=0;
	  	foreach($grades_array as $grade)
	  	{
	  		if($grade==$user_grade)
	  		{
	  			$select1=$cnt1;
	  			break;
	  		}
	  		$cnt1++;
	  	}

	  
		if (!$this->UserModeGrade->exists($id)) {
			throw new NotFoundException(__('Invalid user mode grade'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$user_email=$user_array[$this->request->data['UserModeGrade']['user_id']];
			$user_grade=$grades_array[$this->request->data['UserModeGrade']['grade_id']];
			$find_user=$this->User->find('first',array('conditions'=>array('email'=>$user_email)));
			if(empty($find_user))
			{
				$find_user=$this->User->find('first',array('conditions'=>array('username'=>$user_email)));
			}
			$find_grade=$this->Grade->find('first',array('conditions'=>array('grade_name'=>$user_grade)));
			$this->request->data['UserModeGrade']['grade_id']=$find_grade['Grade']['id'];
			$this->request->data['UserModeGrade']['user_id']=$find_user['User']['id'];
			if ($this->UserModeGrade->save($this->request->data)) {
				$this->Flash->success(__('The user mode grade has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user mode grade could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserModeGrade.' . $this->UserModeGrade->primaryKey => $id));
			$this->request->data = $this->UserModeGrade->find('first', $options);
		}
		$levels = $this->UserModeGrade->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->UserModeGrade->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('language_id'=>$language_id)));
		$languages = $this->UserModeGrade->Language->find('list',array('fields'=>array('Language.language_name')));
		$modes = $this->UserModeGrade->Mode->find('list',array('fields'=>array('Mode.mode_name')));
		$this->set(compact('users', 'levels', 'stories', 'languages', 'modes', 'grades','select','user_array','grades_array','select1'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserModeGrade->id = $id;
		if (!$this->UserModeGrade->exists()) {
			throw new NotFoundException(__('Invalid user mode grade'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserModeGrade->delete()) {
			$this->Flash->success(__('The user mode grade has been deleted.'));
		} else {
			$this->Flash->error(__('The user mode grade could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
