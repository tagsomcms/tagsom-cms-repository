<?php
App::uses('AppController', 'Controller');
/**
 * UserStoryLevels Controller
 *
 * @property UserStoryLevel $UserStoryLevel
 * @property PaginatorComponent $Paginator
 */
class UserStoryLevelsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserStoryLevel->recursive = 0;
		$this->set('userStoryLevels', $this->UserStoryLevel->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
                $this->loadModel('User');
                $this->loadModel('Mode');
                $this->loadModel('Story');
                $this->loadModel('Language');
		if (!$this->UserStoryLevel->exists($id)) {
			throw new NotFoundException(__('Invalid user story level'));
		}
                
        $userStoryLevel=$this->UserStoryLevel->find('first', array('conditions' => array('id' => $id)));
                
		$users = $this->User->find('first',array('conditions'=>array('id'=>$userStoryLevel['UserStoryLevel']['user_id'])));
		$stories = $this->Story->find('first',array('conditions'=>array('id'=>$userStoryLevel['UserStoryLevel']['story_id'])));
		$languages = $this->Language->find('first',array('conditions'=>array('id'=>$userStoryLevel['UserStoryLevel']['language_id'])));
        $modes = $this->Mode->find('first',array('conditions'=>array('id'=>$userStoryLevel['UserStoryLevel']['mode_id'])));
		$this->set(compact('users', 'levels', 'stories', 'languages','modes'));
		$this->set(compact('userStoryLevel'));
                        
                        
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('User');
		$this->loadModel('UserRelation');
		$this->loadModel('Language');
		$this->loadModel('Story');
		$users = $this->User->find('all');
		$user_array=array();
	  	foreach($users as $user)
	  	{
	  		if($user['User']['type']=='child' || $user['User']['type']=='student')
	  		{
	    		if(!empty($user['User']['email']))
		    	{
		      		array_push($user_array,$user['User']['email']);
		    	}
		    	else
		    	{
		    		if(!empty($user['User']['username']))
			    	{
			      		array_push($user_array,$user['User']['username']);
			    	}
		    	}
		    }
	  	}
		if ($this->request->is('post')) {
			$user_name=$user_array[$this->request->data['UserStoryLevel']['user_id']];
			foreach($users as $user)
	  		{
	  			if($user['User']['email']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  			if($user['User']['username']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  		}

	  		$find_relation=$this->UserRelation->find('first',array('conditions'=>array('user2_id'=>$userid)));
	  		$relation_id=$find_relation['UserRelation']['id'];
	  		$this->request->data['UserStoryLevel']['user_id']=$userid;
	  		if(!empty($relation_id))
	  		{
	  			$this->request->data['UserStoryLevel']['user_relation_id']=$relation_id;
	  		}
			$this->UserStoryLevel->create();
			if ($this->UserStoryLevel->save($this->request->data)) {
				$this->Flash->success(__('The user story level has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user story level could not be saved. Please, try again.'));
			}
		}
		$languages = $this->UserStoryLevel->Language->find('list',array('fields'=>array('Language.language_name')));
		$english_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
		$levels = $this->UserStoryLevel->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->UserStoryLevel->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('language_id'=>$english_language['Language']['id'])));
		$modes = $this->UserStoryLevel->Mode->find('list',array('fields'=>array('Mode.mode_name')));
		$this->set(compact('users', 'languages', 'levels', 'stories', 'modes','user_array'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserStoryLevel->exists($id)) {
			throw new NotFoundException(__('Invalid user story level'));
		}
		$this->loadModel('User');
		$this->loadModel('UserRelation');
		$users = $this->User->find('all');
		$user_array=array();
	  	foreach($users as $user)
	  	{
	  		if($user['User']['type']=='child' || $user['User']['type']=='student')
	  		{
	    		if(!empty($user['User']['email']))
		    	{
		      		array_push($user_array,$user['User']['email']);
		    	}
		    	else
		    	{
		    		if(!empty($user['User']['username']))
			    	{
			      		array_push($user_array,$user['User']['username']);
			    	}
		    	}
		    }
	  	}

	  	$find_story_level=$this->UserStoryLevel->find('first',array('conditions'=>array('id'=>$id)));
	  	$user_find=$this->User->find('first',array('conditions'=>array('id'=>$find_story_level['UserStoryLevel']['user_id'])));
	  	$user_email=$user_find['User']['email'];
	  	$user_username=$user_find['User']['username'];
	  	$cnt=0;
	  
	  	foreach($user_array as $email)
	  	{
	  		if($email==$user_email)
	  		{
	  			$select=$cnt;
	  			break;
	  		}
	  		if($email==$user_username)
	  		{
	  			$select=$cnt;
	  			break;
	  		}
	  		$cnt++;
	  	}

		if ($this->request->is(array('post', 'put'))) {
			$user_name=$user_array[$this->request->data['UserStoryLevel']['user_id']];
			foreach($users as $user)
	  		{
	  			if($user['User']['email']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  			if($user['User']['username']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  		}

	  		$find_relation=$this->UserRelation->find('first',array('conditions'=>array('user2_id'=>$userid)));
	  		$relation_id=$find_relation['UserRelation']['id'];
	  		$this->request->data['UserStoryLevel']['user_id']=$userid;
	  		if(!empty($relation_id))
	  		{
	  			$this->request->data['UserStoryLevel']['user_relation_id']=$relation_id;
	  		}
			if ($this->UserStoryLevel->save($this->request->data)) {
				$this->Flash->success(__('The user story level has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user story level could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserStoryLevel.' . $this->UserStoryLevel->primaryKey => $id));
			$this->request->data = $this->UserStoryLevel->find('first', $options);
		}
		$languages = $this->UserStoryLevel->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->UserStoryLevel->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->UserStoryLevel->Story->find('list',array('fields'=>array('Story.story_name')));
		$modes = $this->UserStoryLevel->Mode->find('list',array('fields'=>array('Mode.mode_name')));
		$this->set(compact('users', 'languages', 'levels', 'stories', 'modes','user_array','select'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserStoryLevel->id = $id;
		if (!$this->UserStoryLevel->exists()) {
			throw new NotFoundException(__('Invalid user story level'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserStoryLevel->delete()) {
			$this->Flash->success(__('The user story level has been deleted.'));
		} else {
			$this->Flash->error(__('The user story level could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
