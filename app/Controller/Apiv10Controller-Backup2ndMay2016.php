<?php
App::uses('AppController', 'Controller');
/**
 * Apiv10 Controller
 *
 */
class Apiv10Controller extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('User','Level','Story','Language','UserInvite','UserRelation');
	public $components = array('RequestHandler');
	public $autoRender = false; // Do not render any view in any action of this controller
	public $layout = null; // Set layout to null to every action of this controller
	public $autoLayout = false; // Set to false to disable automatically rendering the layout around views.
	public $jsonArray = array('status' => false, 'message' => 'Something went wrong'); // Set status & message.
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	$this->Auth->allow();
		
		// Allow user to access these function without 'Authorization' header
		$allowedFunctions = array('ping','reset', 'login', 'registration', 'forgot_password','profile_edit','add_child','user_invites','getCountryByIp','uniqueid','verifycode');
		
		if (!method_exists($this, $this->params['action'])) {
			header('HTTP/1.0 404 Not Found');
			exit(json_encode(array('status' => false, 'message' => 'The requested api endpoint doesn\'t exist.')));
		}
		
		// Process all requests
		if (!in_array($this->params['action'], $allowedFunctions)) {

			// Fetch all HTTP request headers from the current request.
			$requestHeaders = apache_request_headers();

			if (isset($requestHeaders['Authorization']) && !empty($requestHeaders['Authorization'])) {
				$authorizationHeader = $requestHeaders['Authorization'];

				if (!$this->_authenticate($authorizationHeader)) {
					header('HTTP/1.0 401 Unauthorized');
					exit(json_encode(array('status' => false, 'message' => 'No valid access token provided.')));
				}
			} else {
				header('HTTP/1.0 401 Unauthorized');
				exit(json_encode(array('status' => false, 'message' => 'No authorization header sent.')));
			}
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			array_walk_recursive($this->request->data, array($this, '_trimElementOfArray'));
		}
	}
	// Remove whitespaces from start and end of a string from element
	protected function _trimElementOfArray(&$item, &$key) {
		$item = trim($item);
	}
	
	/**
	 * Output given data in JSON format.
	 *
	 * @access protected
	 *
	 * @param bool $status true|false
	 * @param string $message
	 * @param array $data The data to output in JSON format
	 * @return object
	 */
	protected function _returnJson($status = false, $message = null, $data = array()) {

		// Set status
		$this->jsonArray['status'] = $status ? 'success' : 'failure';

		// Set message
		$this->jsonArray['message'] = $message;

		// Set data (if any)
		if ($data) {
			// Replace all the 'null' values with blank string
			array_walk_recursive($data, array($this, '_replaceNullWithEmptyString'));

			// Remove whitespaces from start and end of a string from element
			array_walk_recursive($data, array($this, '_trimElementOfArray'));

			$this->jsonArray['data'] = $data;
		}

		// Set the json-encoded data to be the body
		$this->response->body(json_encode($this->jsonArray));
		$this->response->statusCode(200);
		$this->response->type('application/json');

		return $this->response;
	}
	
	/**
	 * Replace null value to blank string from all elements of associative array.
	 * This function call recursively
	 *
	 * @access protected
	 *
	 * @param string|int|null $item
	 * @param string $key
	 * @return void
	 */
	protected function _replaceNullWithEmptyString(&$item, $key) {
		if ($item === null) {
			$item = '';
		}
	}
	
	
	/**
	 * Return long-lived access token.
	 *
	 * @access protected
	 *
	 * @param int $id
	 * @return string $token
	 */
	protected function _getAccessToken($id) {
		try {
			// Make call to "_generateAccessToken" to get long-lived access token
			$access_token = $this->_generateAccessToken($id);

			// Update access token of user
			//$this->_updateToken($id, $access_token);
			// Return the newly created access token
			return $access_token;
		} catch (Exception $e) {
			return $this->_returnJson(false, $e->getMessage());
		}
	}
	
	/**
	 * Generate unique access token to access APIs
	 * It uses openssl_random_pseudo_bytes & SHA1 for generating access token
	 *
	 * @access protected
	 *
	 * @param int $id
	 * @return string $token
	 */
	protected function _generateAccessToken($id) {
		try {
			// Generate a random token
			$token = bin2hex(openssl_random_pseudo_bytes(5)) . SHA1(($id * time()));
			return substr($token, 0,5);
//            return $id;
			//            return $id;
		} catch (Exception $e) {
			return $this->_returnJson(false, $e->getMessage());
		}
	}
	
	public function getCountryByIp(){
	    $ip = $_SERVER['REMOTE_ADDR'];
		$country_details = json_decode(file_get_contents("http://ipinfo.io/14.141.58.163"));
		pr($country_details);
	}
	
	protected function _IsUnique($unique_id){
		if($this->User->hasAny(array('User.username'=>$unique_id))){
			return $this->_IsUnique($unique_id);
		}else{
			return $unique_id.rand(0, 10);
		}
	}
	
	
	public function uniqueid($unique_id){
	    $u=$this->_IsUnique($unique_id);
		echo $u;
	}
	
	/**
	 * Registration Method.
	 * 
	 * @access $email 
	 * @access $keyword , keyword ="New" for registration and keyword ="Old" for forgot password
     * @access $type , type =parent/teacher/child/student
	 */
	 
	 public function registration(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$email = isset($email)?$email:'';
			$keyword = isset($keyword)?$keyword:'';
			
			// Email should not be left blank
			if (empty($email))
            return $this->_returnJson(false, 'Please enter email address.');
			
			// Check if email address is valid
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			// Keyword should not be left blank
			if (empty($keyword))
            return $this->_returnJson(false, 'Please enter keyword.');
			
			if($keyword=="New"){
				// Check if email address already exist in database
				if ($this->User->hasAny(array('User.email' => $email, 'User.is_verified' => 1)))
            	return $this->_returnJson(false, 'An account already exists for this email id.');
			}
			
			$userArray=$userData=$messagedata=$emaildata=array();
			$password=$this->_getAccessToken(rand(100000, 999999));
			
			
			if($keyword=="New"){
				$emaildata=array('template'=>'registration','subject'=>'Tagsom -Registration','message'=>'You\'ve registered successfully.');
				$messagedata=array('email'=>$email,'password'=>$password);
                $type  = isset($type)?$type:'';
                if (empty($type))
                return $this->_returnJson(false, 'Type cannot be left blank.');
                
                
				$userArray=array('User'=>array('email'=>$email,'password'=>$password,'role'=>'user','is_verified'=>'1','type'=>$type));
			}elseif($keyword=="Old"){
				if (!$this->User->hasAny(array('User.email' => $email)))
				return $this->_returnJson(false, 'Email is not registered with ID');
				$this->User->contain();
				$userdata = $this->User->find('first', array('conditions' => array('User.email' => $email)));
				$messagedata = $userdata['User'];
				$messagedata['password']=$password;
				$userArray=array('User'=>array('id'=>$messagedata['id'],'password'=>$password));
				$emaildata=array('template'=>'forgot_password','subject'=>'Tagsom -Forgot Password','message'=>'Please check your email for new password.');
			}else{
				return $this->_returnJson(false, 'Please enter right keyword');
			}
			
			if ($this->User->save($userArray)) {
				$this->_sendemail(Configure::read('FROM'),$email,$emaildata['subject'],$messagedata,$emaildata['template']);
            	return $this->_returnJson(true, $emaildata['message'], array('userID' => $this->User->id));
        	} else {
            	return $this->_returnJson(false, 'Error, please try again.');
        	}
	 	}else{
	 		return $this->_returnJson(false, 'Error Post request');
	 	}
	 }
	
	
	
	/**
	 * Old Registration Method.
	 *
	 * @access public
	 *
	 * @param string $username
	 * @param string $email
	 * @param string $first_name
	 * @param string $last_name	  
	 * @param string $password
	 * @param string $repassword
	 * @param string $security_code
	 * @return json object
	 */
	
	public function old_registration(){
		if($this->request->is('post')){
			 
			// Declare & assign values to variables
			extract($this->request->data);
			$username = isset($username)?$username:'';
			$email = isset($email)?$email:'';
			$first_name = isset($first_name)?$first_name:'';
			$last_name = isset($last_name)?$last_name:'';
			$password = isset($password)?$password:'';
			$repassword = isset($repassword)?$repassword:'';
			$security_code = isset($security_code)?$security_code:'';
			
			if (empty($username))
            return $this->_returnJson(false, 'Please enter username.');
			
			if ($this->User->hasAny(array('User.username' => $username, 'User.is_verified' => 1)))
            return $this->_returnJson(false, 'An account already exists for this username.');
			
			if (empty($email))
            return $this->_returnJson(false, 'Please enter email address.');
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			if ($this->User->hasAny(array('User.email' => $email, 'User.is_verified' => 1)))
            return $this->_returnJson(false, 'An account already exists for this email id.');
			
			if (empty($first_name))
            return $this->_returnJson(false, 'Please enter your first name.');
			
			if (empty($last_name))
            return $this->_returnJson(false, 'Please enter your last name.');
			
			if (empty($password))
            return $this->_returnJson(false, 'Please enter password.');
			
			if (empty($repassword))
            return $this->_returnJson(false, 'Please re-enter password.');
			
			if($password!=$repassword)
			return $this->_returnJson(false, 'Password does not match.');
			
			if(empty($security_code))
			return $this->_returnJson(false, 'Please enter security code.');
			
			$userArray=array('User'=>array('username'=>$username,'email'=>$email,'password'=>$password,'role'=>'user','is_verified'=>'1',
			'first_name'=>$first_name, 'last_name'=> $last_name, 'security_code'=>$security_code));
			
			if ($this->User->save($userArray)) {
            	return $this->_returnJson(true, 'You\'ve registered successfully.', array('userID' => $this->User->id));
        	} else {
            	return $this->_returnJson(false, 'Error, please try again.');
        	}
		}
	}

	/**
	 * Login method.
	 *
	 * @access public
	 *
	 * @param string $email
	 * @param string $password
	 * @return json object
	 * 
	 * "You also need to check if new user email id is present in ""Invite"" Table or not.
		If present in invite table then award his invitee as free book. 
	 * But You also need to check the counter ragarding the free books in the User table. 
	 * And also delete the data from the invites tables"
	 * 
	 * If user has free book or not. 
	 * User can have a free book on invite also. (If his invited friend accepts his invitation and create a account on our app)
	 * 
	 */

	public function login(){
		if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$email = isset($email)?$email:'';
			$password = isset($password)?$password:'';
			if(empty($email) || empty($password)){
				return $this->_returnJson(false, 'Please enter your login credentials.');
			}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}else{
				
				// Now check if new user email id is present in UserInvite table or not.
				
				if($this->UserInvite->hasAny(array('UserInvite.invite_emailid'=>$email))){
					$userinvite=$this->UserInvite->find('first',array('conditions'=>array('UserInvite.invite_emailid'=>$email)));
					$invitee_details=$userinvite['User'];
					$this->UserInvite->deleteAll(array('UserInvite.invite_emailid'=>$email));
					$update_user['User']['id']=$invitee_details['id'];
					$update_user['User']['free_book_available']='1';
					$update_user['User']['free_book']=$invitee_details['free_book']+1;
					$this->User->save($update_user);
				}
				
				
				$result = $this->User->find('first', array(
				'conditions' => array(
					'User.email' => $email,
					'User.password' => AuthComponent::password($password),
				),
				'recursive' => -1,
			)
			);
			// Check whether a user exists
			if ($result) {
				$data = array();
				$data["User"]["id"] = $result['User']['id'];
				$data["User"]["last_login"] = date("Y-m-d H:i:s");
				$this->User->Save($data);
				return $this->_returnJson(true, 'You\'ve logged-in successfully.');
			}else {
				return $this->_returnJson(false, 'Invalid login credentials.');
			}
			}
			
		}
	}

	/**
	 * Profile edit method.
	 * 
	 * @access public
	 * 
	 * @param $username
	 * @param $email
	 * @param $currentPassword
	 * @param $newPassword
	 * @param $first_name
	 * @param $last_name
	 * @param $security_code
	 * @param $previous_security_code
	 */
	 
	public function profile_edit(){
		if($this->request->is('post')){
			extract($this->request->data);
			$username = isset($username)?$username:'';
			$email = isset($email)?$email:'';
			$newPassword = isset($newPassword)?$newPassword:'';
			$currentPassword = isset($currentPassword)?$currentPassword:'';
			$first_name = isset($first_name)?$first_name:'';
			$last_name = isset($last_name)?$last_name:'';
			$security_code = isset($security_code)?$security_code:'';
			$previous_security_code =isset($previous_security_code)?$previous_security_code:'';
			
			if(empty($email))
			return $this->_returnJson(false, 'Please enter email address.');
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			if(empty($currentPassword))
			return $this->_returnJson(false, 'Please enter your current password.');
			
			if (empty($newPassword))
			return $this->_returnJson(false, 'Please enter your new password.');
			
			if (empty($first_name))
			return $this->_returnJson(false, 'Please enter your first name.');
			
			if (empty($last_name))
			return $this->_returnJson(false, 'Please enter your last name.');
			
			if(empty($previous_security_code))
			return $this->_returnJson(false, 'Please enter your current security code.');
			
			if (!$this->User->hasAny(array('User.email' => $email, 'User.security_code' => $previous_security_code)))
            return $this->_returnJson(false, 'Current security code does not match, please try again.');
			
			if (empty($security_code))
			return $this->_returnJson(false, 'Please enter your new security_code.');
			
			$userdetails=$this->User->find('first',array(
			'conditions' => array(
				'User.email' => $email
			),
			'recursive' => -1
			));
			
			
			if($userdetails){
				if (!$this->User->hasAny(array('User.id' => $userdetails["User"]["id"], 'User.password' => AuthComponent::password($currentPassword)))) {
					return $this->_returnJson(false, 'Invalid current password. Please enter a correct password.');
				}
				
				$data=array("User" => array(
					"id" => $userdetails['User']['id'],
					"password" => $newPassword,
					"first_name" => $first_name,
					"last_name" => $last_name,
					"security_code" => $security_code
				));
				
				$this->User->save($data);
				return $this->_returnJson(true, 'Profile has been updated successfully.');
			}else {
				return $this->_returnJson(false, 'Error, Invalid User.');
			}
		}
	}

	/**
	 * Forgot password method
	 * 
	 * @access public
	 * 
	 * @param $username
	 * OR
	 * @param $email
	 * 
	 */
	 
	public function forgot_password(){
		if($this->request->is('post')){
			extract($this->request->data);
			$username = isset($username)?$username:'';
			$email = isset($email)?$email:'';
			$useremail='';
			if(empty($username) && empty($email))
			return $this->_returnJson(false, 'Please enter username or email.');
			$this->User->contain();
			if(!empty($username)){
				$userData = $this->User->find('first', array('conditions' => array('User.username' => $username)));
				if (!$userData)
				return $this->_returnJson(false, 'Username doesn\'t exist.');
				$useremail=$userData['User']['email'];
			}
			
			if(!empty($email)){
				if (!filter_var($email, FILTER_VALIDATE_EMAIL))
				return $this->_returnJson(false, 'Please enter valid email id.');
				$userData = $this->User->find('first', array('conditions' => array('User.email' => $email)));
				if (!$userData)
				return $this->_returnJson(false, 'Email id doesn\'t exist.');
				$useremail=$email;
			}
			
			
			   // Generate a 6 digit random integer
				$generatedPassword = rand(100000, 999999);
				// Make an array to hold data
				$temp = array(
					'User' => array(
						'id' => $userData['User']['id'],
						'password' => $generatedPassword,
					),
				);
				// 
				$messagedata=array(
					'first_name'=>$userData['User']['first_name'],
					'last_name'=>$userData['User']['last_name'],
					'new_password'=>$generatedPassword);
				$this->User->save($temp);
				$this->_sendemail(Configure::read('FROM'),$useremail,'Forgot Password',$messagedata,'forgot_password');
			
			return $this->_returnJson(true, 'Please check your email for new password.');
			
		}
	}




	/**
	 * Add Child method
	 * 
	 * @access public
	 * 
	 * @param $child_name
	 * @param $yob
	 * @param $language
	 * @param $parent_emailid
	 * @param $level
	 */
	 
	 public function add_child(){
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$parent_emailid = isset($parent_emailid)?$parent_emailid:'';
			$child_name = isset($child_name)?$child_name:'';
			$yob = isset($yob)?$yob:'';
			$language = isset($language)?$language:'';
			$level = isset($level)?$level:'';
			
			if (empty($parent_emailid))
			return $this->_returnJson(false, 'Please enter Parent\'s email id.');
			
			if(!$this->User->hasAny(array('User.email'=>$parent_emailid,'User.type'=>'parent')))
			return $this->_returnJson(false, 'Parent\'s email id does not exist.');
			
			if (empty($child_name))
			return $this->_returnJson(false, 'Please enter child name.');
			
			$child_name= explode(' ', trim($child_name));
            
			$first_name= isset($child_name[0])?$child_name[0]:'';
			$last_name= isset($child_name[1])?$child_name[1]:'';
            
			
			if (empty($yob))
			return $this->_returnJson(false, 'Please enter child\'s year of birth.');
			
			if (empty($language))
			return $this->_returnJson(false, 'Please enter language.');
			
			if(!$this->Language->hasAny(array('Language.language_name'=>$language)))
			return $this->_returnJson(false, 'Language does not exist.');
			
			if (empty($level))
			return $this->_returnJson(false, 'Please enter level.');
			
			if(!$this->Level->hasAny(array('Level.level_name'=>$level)))
			return $this->_returnJson(false, 'Level does not exist.');
			
			$this->User->contain(false);
			$parent_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$parent_emailid)));
						
			$this->Language->contain();
			$language_id=$this->Language->find('first',array('fields'=>array('Language.id'),'conditions'=>array('Language.language_name'=>$language)));
			
			$this->Level->contain();
			$level_id=$this->Level->find('first',array('fields'=>array('Level.id'),'conditions'=>array('Level.level_name'=>$level)));
			$user_relations['UserRelation']['user1_id']=$parent_id['User']['id'];
			
			$child_data['User']['yob']=date('Y',strtotime($yob));
			$child_data['User']['first_name']=$first_name;
            if(!empty($last_name)){
                $child_data['User']['last_name']=$last_name;
            }
			$child_data['User']['language_id']=$language_id['Language']['id'];
			$child_data['User']['level_id']=$level_id['Level']['id'];
			$child_data['User']['type']='child';
			$child_data['User']['is_verified']='1';
			
			$unique_id= strtolower($first_name).'.'.strtolower($last_name);
           
			$this->User->create();
			if($this->User->save($child_data)){
				$last_insert_id=$this->User->getLastInsertId();
				$update['User']['id']=$last_insert_id;
				$update['User']['username']=$child_data['User']['username']=$unique_id.$last_insert_id;
                $this->User->validator()->remove('username');
				$this->User->save($update);
				$user_relation['UserRelation']['user1_id']=$parent_id['User']['id'];
				$user_relation['UserRelation']['user2_id']=$last_insert_id;
				$this->UserRelation->create();
				$this->UserRelation->save($user_relation);
				$this->_returnJson(true, 'Child has been added successfully.',$child_data);
			}else{
				// $log = $this->User->getDataSource()->getLog(false, false);       
				// debug($log);
				// die;
				$this->_returnJson(false, 'Error, something went wrong.');
			}
			
		}
	 }


	/**
	 * Add Old Child method
	 * 
	 * @access public
	 * 
	 * @param $child_name
	 * @param $dob
	 * @param $language
	 * @param $parent_emailid
	 * @param $level
	 */
	 
	 public function old_add_child(){
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$parent_emailid = isset($parent_emailid)?$parent_emailid:'';
			$child_name = isset($child_name)?$child_name:'';
			$age = isset($age)?$age:'';
			$language = isset($language)?$language:'';
			$level = isset($level)?$level:'';
			
			if (empty($parent_emailid))
			return $this->_returnJson(false, 'Please enter Parent\'s email id.');
			
			if(!$this->User->hasAny(array('User.email'=>$parent_emailid)))
			return $this->_returnJson(false, 'Parent\'s email id does not exist.');
			
			if (empty($child_name))
			return $this->_returnJson(false, 'Please enter child name.');
			
			if (empty($dob))
			return $this->_returnJson(false, 'Please enter child\'s date of birth.');
			
			if (empty($language))
			return $this->_returnJson(false, 'Please enter language.');
			
			if(!$this->Language->hasAny(array('Language.language_name'=>$language)))
			return $this->_returnJson(false, 'Language does not exist.');
			
			if (empty($level))
			return $this->_returnJson(false, 'Please enter level.');
			
			if(!$this->Level->hasAny(array('Level.level_name'=>$level)))
			return $this->_returnJson(false, 'Level does not exist.');
			
			$this->User->contain(false);
			$parent_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$parent_emailid)));
						
			$this->Language->contain();
			$language_id=$this->Language->find('first',array('fields'=>array('Language.id'),'conditions'=>array('Language.language_name'=>$language)));
			
			$this->Level->contain();
			$level_id=$this->Level->find('first',array('fields'=>array('Level.id'),'conditions'=>array('Level.level_name'=>$level)));
			
			$child_data['Child']['user_id']=$parent_id['User']['id'];
			$child_data['Child']['dob']=$dob;
			$child_data['Child']['name']=$child_name;
			$child_data['Child']['language_id']=$language_id['Language']['id'];
			$child_data['Child']['level_id']=$level_id['Level']['id'];
			
			$this->Child->create();
			if($this->Child->save($child_data)){
				$children=$this->Child->find('count',array('conditions'=>array('Child.user_id'=>$child_data['Child']['user_id'])));
				$user['User']['id']=$child_data['Child']['user_id'];
				$user['User']['children']=$children;
				$this->User->save($user);
				$this->_returnJson(true, 'Child has been added successfully.',$user);
			}else{
				$this->_returnJson(false, 'Error, something went wrong.');
			}
			
		}
	 }


	/**
	 * User Invites method
	 * 
	 * @param $invitee_email
	 * @param $invited_email
	 * Just put these values in the table. And also send invite email to that id. With URL (My ID)
	 */
	 
	 public function user_invites(){
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$invitee_email =isset($invitee_email)?$invitee_email:'';
			$invited_email =isset($invited_email)?$invited_email:'';
			$userdata=$messagedata=array();
			
			if(empty($invitee_email))
			return $this->_returnJson(false,'Please enter invitee email id');
			
			if (!filter_var($invitee_email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid invitee email address.');
			
			if (!$this->User->hasAny(array('User.email' => $invitee_email)))
			return $this->_returnJson(false, 'Invitee email does not exist.');
			
			if(empty($invited_email))
			return $this->_returnJson(false,'Please enter invited email id');
			
			if (!filter_var($invited_email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid invited email address.');
			
			if ($this->User->hasAny(array('User.email' => $invited_email, 'User.is_verified' => 1)))
            return $this->_returnJson(false, 'An account already exists for this email id.');
			
			
			$this->User->contain();
			$userdata=$this->User->find('first',array('conditions'=>array('User.email'=>$invitee_email)));
			$fromemail=$userdata['User']['email'];
			$password=$this->_getAccessToken(rand(100000, 999999));
			$messagedata=array('email'=>$invited_email,'password'=>$password);
			
			$userinvites=array();
			$userinvites['UserInvite']['user_id']=$userdata['User']['id'];
			$userinvites['UserInvite']['invite_emailid']=$invited_email;
			
			$this->UserInvite->create();
			if($this->UserInvite->save($userinvites)){
				$userinvite_id=$this->UserInvite->getLastInsertId();
				$messagedata["activation_url"]=Router::url('/user_invites/activate?invitestatus=true&invite_id='.$userinvite_id.'&passkey='.base64_encode($password).'&invitedmail='.base64_encode($invited_email), true );
				$this->_sendemail($invitee_email,$invited_email,'Invitation Mail',$messagedata,'user_invite');
				$this->_returnJson(true, "Invitation has been sent successfully.");
			}else{
				$this->_returnJson(false, "Invitation was not sent. Please try again.");
			}
			
				
		}else{
			$this->_returnJson(false, "Invalid Post Request");
		}
	 }


	/**
	 * User Old Invites method
	 * 
	 * @param $username
	 * @param $fromemail
	 * @param $invite_email
	 * 
	 */
	 
	 public function old_user_invites(){
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$username =isset($username)?$username:'';
			$fromemail =isset($fromemail)?$fromemail:'';
			$invite_email =isset($invite_email)?$invite_email:'';
			
			if(empty($username) && empty($fromemail))
			return $this->_returnJson(false,'Please enter username or email');
			$userdata=$messagedata=array();
			if(!empty($username)){
				$this->User->contain();
				$userdata=$this->User->find('first',array('conditions'=>array('User.username'=>$username)));
				$fromemail=$userdata['User']['email'];
				$messagedata=$userdata['User'];
			}
			
			if(empty($invite_email))
			return $this->_returnJson(false,'Please enter invitation email id');
			
			if (!filter_var($invite_email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid invitation email address.');
			
			if ($this->User->hasAny(array('User.email' => $invite_email, 'User.is_verified' => 1)))
            return $this->_returnJson(false, 'An account already exists for this email id.');
			
			
			
			$userinvites=array();
			$userinvites['UserInvite']['user_id']=$userdata['User']['id'];
			$userinvites['UserInvite']['invite_emailid']=$invite_email;
			
			$this->UserInvite->create();
			if($this->UserInvite->save($userinvites)){
				$userinvite_id=$this->UserInvite->getLastInsertId();
				$messagedata["activation_url"]="Activation URL: ".Router::url('/user_invites/activate?invitestatus=true&invite_id='.$userinvite_id, true );
				$this->_sendemail($fromemail,$invite_email,'Invitation Mail',$messagedata,'user_invite');
				$this->_returnJson(true, "Invitation has been sent successfully.");
			}else{
				$this->_returnJson(false, "Invitation was not sent. Please try again.");
			}
			
				
		}else{
			$this->_returnJson(false, "Invalid Post Request");
		}
	 }

    /**
     * verificationcode method
     * 
     * @param $verify_code
     * @param $email
     * 
     */

    public function verifycode(){
        if($this->request->is('post')){
            extract($this->request->data);
            $verify_code =isset($verify_code)?$verify_code:'';
            $email =isset($email)?$email:'';
            
            if(empty($verify_code) && empty($verify_code))
            return $this->_returnJson(false,'Please enter verification code');
            
            if(empty($email) && empty($email))
            return $this->_returnJson(false,'Something went wrong.');
            
            if (!$this->User->hasAny(array('User.email' => $email)))
            return $this->_returnJson(false, 'Registered email does not exist.');
            
            
            $user=$this->User->find('first',array('conditions'=>array('User.email'=>$email,'User.password'=>AuthComponent::password($verify_code))));
             
            if(!empty($user)){
                $this->_returnJson(true, "Verification code is correct");
            }else{
                $this->_returnJson(false, "Verification code is incorrect");
            }
            
        }else{
            $this->_returnJson(false, "Invalid Post Request");
        }
    }
	  
	
    public function reset(){
        $this->User->query('Delete from `users` where id > 11 ');
        $this->User->query('ALTER TABLE `users` AUTO_INCREMENT =12;');
		$this->_returnJson(true, "Database Reset");
	}

}
?>
