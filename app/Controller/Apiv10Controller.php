<?php
ob_start();
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');

/**
 * Apiv10 Controller
 *
 */
class Apiv10Controller extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('StoryTranslation','AssignFreebook','MathObject','Variant','Grids','Number','SharedBooks','Alphabet','GeneralSettings','Scene','Word','Question','Apitoken','User','Level','Story','Language','UserInvite','UserRelation','School','Country','Book','UserPurchase','UserStoryLevel','Mode','Result','ModeQuestionAnswer','UserModeStatus','Coupon','UserCoupon','Freebook','Grade','UserModeGrade');
	public $components = array('RequestHandler','Cookie');
	public $autoRender = false; // Do not render any view in any action of this controller
	public $layout = null; // Set layout to null to every action of this controller
	public $autoLayout = false; // Set to false to disable automatically rendering the layout around views.
	public $jsonArray = array('status' => false, 'message' => 'Something went wrong'); // Set status & message.
	
	public function beforeFilter() {
    	parent::beforeFilter();
    	$this->Auth->allow();
		
		// Allow user to access these function without 'Authorization' header
		$allowedFunctions = array('StoryPlay3','DeleteUser','GetAllBooks1','Books_List','Download_story','StoryPlay2','purchase_book2','Share_Book','count_purchased_books','purchase_book_by_credit','SetBookStatus','StoryPlay','SpellGame','QuestionGame','MathsGame','get_stars','login_with_facebook','logout','set_password','TotalUsers','AddFacebookUser','AddCoupon','GetPopularBooks','DeactivateFreeBook','ActivateFreeBook','GetStrings','InsertStrings','login_user','add_user','BooksToShare','GetAllStrings','GetStoryIntroText','GetStoryIntroduction','ChangeAppLanguage','SetStatus','GetAllBooks','AddPaidBooks','GetPurchasedBooks','AddFreeBooks','GetPaidBooks','GetUnloadedBooks','GetFreeBooks','get_overall_performance','get_country_names','get_languages','get_SchoolNames','change_language','get_minigames','get_story_statements','get_all_story','get_story','get_grade','add_grades','add_student','get_child_details','get_purchased_story','reset', 'login', 'registration', 'forgot_password','profile_edit','add_child','user_invites','getCountryByIp','uniqueid','verifycode','teacher_registration','secret_code','fetchStudentData','uniqueid_login_settings','GetStudentDetailsByUid','getbooks','add_books','listStudentByTeacherId','student_signin','edit_teacher','level_change','language_change','purchase_book','user_story_level','results','user_mode_status','mode_question_answer','get_number_of_users','storywise_performance','apply_coupon_code','add_credits','update_credits','get_credits','save_stars');
		
		$BeforeAllowedFunctions=array('Books_List','Download_story','StoryPlay2','login_with_facebook','logout','set_password','AddFacebookUser','login_user','add_user','login','registration','forgot_password','verifycode','teacher_registration','secret_code','uniqueid_login_settings','student_signin');
		
		$AdminAllowedFunctions=array('Books_List','Download_story','StoryPlay2','purchase_book2','Share_Book','count_purchased_books','purchase_book_by_credit','SetBookStatus','StoryPlay','SpellGame','QuestionGame','MathsGame','get_stars','logout','set_password','AddFacebookUser','login_user','add_user','login','registration','forgot_password','verifycode','teacher_registration','secret_code','uniqueid_login_settings','student_signin','reset','TotalUsers','AddCoupon','GetPopularBooks','GetStrings','InsertStrings','GetAllStrings','GetStoryIntroText','GetStoryIntroduction','SetStatus','GetAllBooks','GetPaidBooks','GetUnloadedBooks','GetFreeBooks','get_overall_performance','get_country_names','get_languages','get_SchoolNames','change_language','get_minigames','get_story_statements','get_all_story','get_story','get_grade','add_grades','add_student','get_purchased_story','profile_edit','add_child','getCountryByIp','uniqueid','fetchStudentData','uniqueid_login_settings','GetStudentDetailsByUid','getbooks','add_books','listStudentByTeacherId','level_change','language_change','purchase_book','results','get_number_of_users','add_credits','update_credits','get_credits','save_stars');

		$ParentAllowedFunctions=array('Books_List','Download_story','StoryPlay2','purchase_book2','Share_Book','count_purchased_books','purchase_book_by_credit','SetBookStatus','StoryPlay','SpellGame','QuestionGame','MathsGame','get_stars','logout','set_password','AddFacebookUser','login_user','add_user','login','registration','forgot_password','verifycode','teacher_registration','secret_code','uniqueid_login_settings','student_signin','GetPopularBooks','DeactivateFreeBook','ActivateFreeBook','GetStrings','GetAllStrings','GetStoryIntroText','GetStoryIntroduction','ChangeAppLanguage','SetStatus','GetAllBooks','AddPaidBooks','GetPurchasedBooks','AddFreeBooks','GetPaidBooks','GetUnloadedBooks','GetFreeBooks','get_overall_performance','get_country_names','get_languages','get_SchoolNames','get_minigames','get_story_statements','get_all_story','get_story','get_grade','add_grades','get_child_details','get_purchased_story','profile_edit','add_child','user_invites','getCountryByIp','uniqueid','uniqueid_login_settings','getbooks','add_books','level_change','language_change','purchase_book','user_story_level','results','storywise_performance','apply_coupon_code','add_credits','update_credits','get_credits','save_stars');

		$TeacherAllowedFunctions=array('Books_List','Download_story','StoryPlay2','purchase_book2','Share_Book','count_purchased_books','purchase_book_by_credit','SetBookStatus','StoryPlay','SpellGame','QuestionGame','MathsGame','get_stars','logout','set_password','AddFacebookUser','login_user','add_user','login','registration','forgot_password','verifycode','teacher_registration','secret_code','uniqueid_login_settings','student_signin','GetPopularBooks','DeactivateFreeBook','ActivateFreeBook','GetStrings','GetAllStrings','GetStoryIntroText','GetStoryIntroduction','ChangeAppLanguage','SetStatus','GetAllBooks','AddPaidBooks','AddFreeBooks','GetPaidBooks','GetUnloadedBooks','GetFreeBooks','get_overall_performance','get_country_names','get_languages','get_SchoolNames','get_minigames','get_story_statements','get_all_story','get_story','get_grade','add_grades','add_student','get_purchased_story','profile_edit','user_invites','getCountryByIp','uniqueid','fetchStudentData','uniqueid_login_settings','GetStudentDetailsByUid','getbooks','add_books','listStudentByTeacherId','edit_teacher','level_change','language_change','purchase_book','user_story_level','results','storywise_performance','apply_coupon_code','add_credits','update_credits','get_credits','save_stars');

		$ChildAllowedFunctions=array('Books_List','Download_story','StoryPlay2','purchase_book2','Share_Book','count_purchased_books','purchase_book_by_credit','SetBookStatus','StoryPlay','SpellGame','QuestionGame','MathsGame','get_stars','logout','set_password','AddFacebookUser','login_user','add_user','login','registration','forgot_password','verifycode','teacher_registration','secret_code','uniqueid_login_settings','student_signin','GetPopularBooks','DeactivateFreeBook','ActivateFreeBook','GetStrings','GetAllStrings','GetStoryIntroText','GetStoryIntroduction','ChangeAppLanguage','SetStatus','GetAllBooks','AddPaidBooks','GetPurchasedBooks','AddFreeBooks','GetPaidBooks','GetUnloadedBooks','GetFreeBooks','get_overall_performance','get_country_names','get_languages','get_SchoolNames','get_minigames','get_story_statements','get_all_story','get_story','get_grade','add_grades','get_child_details','get_purchased_story','profile_edit','add_child','user_invites','getCountryByIp','uniqueid','uniqueid_login_settings','getbooks','add_books','level_change','language_change','purchase_book','user_story_level','results','storywise_performance','apply_coupon_code','add_credits','update_credits','get_credits','save_stars');

		$StudentAllowedFunctions=array('Books_List','Download_story','StoryPlay2','purchase_book2','Share_Book','count_purchased_books','purchase_book_by_credit','SetBookStatus','StoryPlay','SpellGame','QuestionGame','MathsGame','get_stars','logout','set_password','AddFacebookUser','login_user','add_user','login','registration','forgot_password','verifycode','teacher_registration','secret_code','uniqueid_login_settings','student_signin','GetPopularBooks','DeactivateFreeBook','ActivateFreeBook','GetStrings','GetAllStrings','GetStoryIntroText','GetStoryIntroduction','ChangeAppLanguage','SetStatus','GetAllBooks','AddPaidBooks','AddFreeBooks','GetPaidBooks','GetUnloadedBooks','GetFreeBooks','get_overall_performance','get_country_names','get_languages','get_SchoolNames','get_minigames','get_story_statements','get_all_story','get_story','get_grade','add_grades','add_student','get_purchased_story','profile_edit','user_invites','getCountryByIp','uniqueid','fetchStudentData','uniqueid_login_settings','GetStudentDetailsByUid','getbooks','add_books','listStudentByTeacherId','edit_teacher','level_change','language_change','purchase_book','user_story_level','results','storywise_performance','apply_coupon_code','add_credits','update_credits','get_credits','save_stars');

		if (!method_exists($this, $this->params['action'])) {
			header('HTTP/1.0 404 Not Found');
			exit(json_encode(array('status' => false, 'message' => 'The requested api endpoint doesn\'t exist.')));
		}
		
		// Process all requests
		if (!in_array($this->params['action'], $allowedFunctions)) {

			$requestHeaders = apache_request_headers();

			if (isset($requestHeaders['Authorization']) && !empty($requestHeaders['Authorization'])) {
				$authorizationHeader = $requestHeaders['Authorization'];

				if (!$this->_authenticate($authorizationHeader)) {
					header('HTTP/1.0 401 Unauthorized');
					exit(json_encode(array('status' => false, 'message' => 'No valid access token provided.')));
				}
			} else {
					header('HTTP/1.0 401 Unauthorized');
					exit(json_encode(array('status' => false, 'message' => 'No authorization header sent.')));
			}
		}

		$freebook=$this->AssignFreebook->find('first',array('conditions'=>array('status'=>1)));
		$assignfreebook=$freebook['AssignFreebook']['title'];
		$this->Session->write('free_book', $assignfreebook);

		// if(!in_array($this->params['action'], $BeforeAllowedFunctions) )
		// {
		// 	if(!empty($this->Session->read('user_type')))
		// 	{
		// 		if(($this->Session->read('user_type')=='admin') && (!in_array($this->params['action'], $AdminAllowedFunctions)) )
		// 		{
		// 			exit(json_encode(array('status' => false, 'message' => 'You are not Authorized for this service.')));
		// 		}
		// 		else if(($this->Session->read('user_type')=='parent') && (!in_array($this->params['action'],$ParentAllowedFunctions)))
		// 		{
		// 			exit(json_encode(array('status' => false, 'message' => 'You are not Authorized for this service.')));
		// 		}
		// 		else if(($this->Session->read('user_type')=='child') && (!in_array($this->params['action'],$ChildAllowedFunctions)))
		// 		{
		// 			exit(json_encode(array('status' => false, 'message' => 'You are not Authorized for this service.')));
		// 		}
		// 		else if(($this->Session->read('user_type')=='teacher') && (!in_array($this->params['action'],$TeacherAllowedFunctions)))
		// 		{
		// 			exit(json_encode(array('status' => false, 'message' => 'You are not Authorized for this service.')));
		// 		}
		// 		else if(($this->Session->read('user_type')=='student') && (!in_array($this->params['action'],$StudentAllowedFunctions)))
		// 		{
		// 			exit(json_encode(array('status' => false, 'message' => 'You are not Authorized for this service.')));
		// 		}
		// 	}
		// 	else
		// 	{
		// 		exit(json_encode(array('status' => false, 'message' => 'Please login first.')));
		// 	}
			
		// }
		

		
		if ($this->request->is('post') || $this->request->is('put')) {
			array_walk_recursive($this->request->data, array($this, '_trimElementOfArray'));
		}

	}
	// Remove whitespaces from start and end of a string from element
	protected function _trimElementOfArray(&$item, &$key) {
		$item = trim($item);
	}
	
	/**
	 * Output given data in JSON format.
	 *
	 * @access protected
	 *
	 * @param bool $status true|false
	 * @param string $message
	 * @param array $data The data to output in JSON format
	 * @return object
	 */
	protected function _returnJson($status = false, $message = null, $data = array()) {
		// Set status
		$this->jsonArray['status'] = $status ? 'success' : 'failure';

		// Set message
		$this->jsonArray['message'] = $message;

		// Set data (if any)
		if ($data) {
			// Replace all the 'null' values with blank string
			array_walk_recursive($data, array($this, '_replaceNullWithEmptyString'));
			// Remove whitespaces from start and end of a string from element
			array_walk_recursive($data, array($this, '_trimElementOfArray'));
			$this->jsonArray['data'] = $data;
		}

		// Set the json-encoded data to be the body
		$this->response->body(json_encode($this->jsonArray));
		$this->response->statusCode(200);
		$this->response->type('application/json');

		return $this->response;
	}
	
	/**
	 * Replace null value to blank string from all elements of associative array.
	 * This function call recursively
	 *
	 * @access protected
	 *
	 * @param string|int|null $item
	 * @param string $key
	 * @return void
	 */
	protected function _replaceNullWithEmptyString(&$item, $key) {
		if ($item === null) {
			$item = '';
		}
	}
	
	
	/**
	 * Return long-lived access token.
	 *
	 * @access protected
	 *
	 * @param int $id
	 * @return string $token
	 */
	protected function _getAccessToken($id) {
		try {
			// Make call to "_generateAccessToken" to get long-lived access token
			$access_token = $this->_generateAccessToken($id);

			return $access_token;
		} catch (Exception $e) {
			return $this->_returnJson(false, $e->getMessage());
		}
	}
	
	/**
	 * Generate unique access token to access APIs
	 * It uses openssl_random_pseudo_bytes & SHA1 for generating access token
	 *
	 * @access protected
	 *
	 * @param int $id
	 * @return string $token
	 */
	protected function _generateAccessToken($id) {
		try {
			// Generate a random token
			$token = bin2hex(openssl_random_pseudo_bytes(5)) . SHA1(($id * time()));
			return substr($token, 0,5);
//            return $id;
			//            return $id;
		} catch (Exception $e) {
			return $this->_returnJson(false, $e->getMessage());
		}
	}
	
	public function getCountryByIp(){
	    $ip = $_SERVER['REMOTE_ADDR'];
		$country_details = json_decode(file_get_contents("http://ipinfo.io/14.141.58.163"));
		pr($country_details);
	}
	
	protected function _IsUnique($unique_id){
		if($this->User->hasAny(array('User.username'=>$unique_id))){
			return $this->_IsUnique($unique_id);
		}else{
			return $unique_id.rand(0, 10);
		}
	}
	
	
	public function uniqueid($unique_id){
	    $u=$this->_IsUnique($unique_id);
		echo $u;
	}

	public function Books_List()
	{
		$books=$this->Story->find('all',array('conditions'=>array('language_id'=>$this->Language->getEnglishLanguage())));
		$books_array=array();
		foreach($books as $book)
		{
			array_push($books_array,$book['Story']['story_name']);
		}
		return $this->_returnJson(true, $books_array);

	}

	/**
	 * Registration Method.
	 * 
	 * @access $email 
	 * @access $keyword , keyword ="New" for registration and keyword ="Old" for forgot password
     * @access $type , type =parent/teacher/child/student
	 */
	 
	 public function registration(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$email = isset($email)?$email:'';
			$keyword = isset($keyword)?$keyword:'';
			
			// Email should not be left blank
			if (empty($email))
            return $this->_returnJson(false, 'Please enter email address.');
			
			// Check if email address is valid
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			// Keyword should not be left blank
			if (empty($keyword))
            return $this->_returnJson(false, 'Please enter keyword.');
			
			if($keyword=="New"){
				// Check if email address already exist in database
				if ($this->User->hasAny(array('User.email' => $email, 'User.is_verified' => 1)))
            	return $this->_returnJson(false, 'An account already exists for this email id.');
			}
			
			$userArray=$userData=$messagedata=$emaildata=array();
			$password=$this->_getAccessToken(rand(100000, 999999));
			
			if($keyword=="New"){
				$emaildata=array('template'=>'registration','subject'=>'Tagsom- Registration','message'=>'You\'ve registered successfully.');
				$messagedata=array('email'=>$email,'password'=>$password);
                $type  = isset($type)?$type:'';
                if (empty($type))
                return $this->_returnJson(false, 'Type cannot be left blank.');
                $total_free_books=0;
				if($type=='parent'){
					$total_free_books=1;
					$credits=3;
				}
				else
				{
					$credits=0;
				}
				$userArray=array('User'=>array('email'=>$email,'password'=>$password,'is_verified'=>'1','type'=>$type,'free_book'=>$total_free_books,'invite_target'=>'1','credits'=>$credits));
			}elseif($keyword=="Old"){
				if (!$this->User->hasAny(array('User.email' => $email)))
				return $this->_returnJson(false, 'Email is not registered with ID');
				$this->User->contain();
				$userdata = $this->User->find('first', array('conditions' => array('User.email' => $email)));
				$messagedata = $userdata['User'];
				$messagedata['password']=$password;
				$userArray=array('User'=>array('id'=>$messagedata['id'],'password'=>$password));
				$emaildata=array('template'=>'forgot_password','subject'=>'Tagsom- Forgot Password','message'=>'Please check your email for new password.');
			}else{
				return $this->_returnJson(false, 'Please enter right keyword');
			}
			
			if ($this->User->save($userArray)) {
				$this->_sendemail(Configure::read('FROM'),$email,$emaildata['subject'],$messagedata,$emaildata['template']);
            	return $this->_returnJson(true, $emaildata['message'], array('userID' => $this->User->id,'email'=>$email,'free_books'=>$total_free_books,'credits'=>$credits));
        	} else {
            	return $this->_returnJson(false, 'Error, please try again.');
        	}
	 	}else{
	 		return $this->_returnJson(false, 'Error Post request');
	 	}
	 }
	


/**
	 * Delete User Student Method.
	 * 
	 * @param $teacher_email 
	 * @param $firstname 
     * @param $lastname 
	 */
 	public function DeleteUser(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$email = isset($email)?$email:'';

			if (empty($email))
            return $this->_returnJson(false, 'Please enter user email.');

        	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');

        	$user=$this->User->find('first',array('conditions'=> array('User.email'=>$email)));
        	if(!empty($user))
        	{
        		$type=$user['User']['type'];	
        		if($type!='admin' && $type!='voice_actor')
        		{
        			$this->User->id=$user['User']['id'];
        			$this->User->delete();
        			return $this->_returnJson(true, "User has been deleted successfully.");
        		}
        		else
        		{
        			return $this->_returnJson(false, "You have no permission to delete this user.");
        		}
        	}
        	else
        	{
        		return $this->_returnJson(false, "No Such User Exist.");
        	}
		}
	}

/**
	 * Add Student Method.
	 * 
	 * @param $teacher_email 
	 * @param $firstname 
     * @param $lastname 
	 */
	 
	 public function add_student(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$email = isset($email)?$email:'';
			$firstname = isset($firstname)?$firstname:'';
			$lastname = isset($lastname)?$lastname:'';
			$yob = isset($yob)?$yob:'';
			// firstname should not be left blank
			if (empty($firstname))
            return $this->_returnJson(false, 'Please enter your firstname.');

        	// lastname should not be left blank
			if (empty($lastname))
            return $this->_returnJson(false, 'Please enter your lastname.');

        	// teacher_email should not be left blank
			if (empty($yob))
            return $this->_returnJson(false, 'Please enter age of year.');

        	// firstname should not be left blank
			if (empty($firstname))
            return $this->_returnJson(false, 'Please enter your firstname.');

        	if (strlen($yob) != 4)
        	{
        		return $this->_returnJson(false, 'Please enter valid 4 digit year.');
        	}
        	if(!is_numeric($yob))
        	{
        		return $this->_returnJson(false, 'Please enter valid 4 digit year.');
        	}	
			
			// Check if email address is valid
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			$user=$this->User->find('first',array('conditions'=> array('User.email'=>$email,'type' => 'teacher'),
            'fields'=> array('User.id','User.email'),
            'contain'=> array('Student'=>array(
            'fields' => array('Student.user2_id')
            ))));
            $teacher_id=$user['User']['id'];
            if(empty($teacher_id))
            {
            	return $this->_returnJson(false, "Invalid Teacher Email_id");
            }
       	    else
            {
            	$this->User->create();
            	$array=array('first_name'=>$firstname,'last_name'=>$lastname,'username'=>$firstname.".".$lastname,'type'=>'student','yob'=>$yob);
            	if($this->User->save($array))
            	{
            		$student=$this->User->find('first',array('conditions'=> array('first_name'=>$firstname,'last_name'=>$lastname,'type' => 'student')));
            		$student_id=$student['User']['id'];
            		$this->UserRelation->create();
            		$array1=array('user1_id'=>$teacher_id,'user2_id'=>$student_id);
            		if($this->UserRelation->save($array1))
            		{
            			return $this->_returnJson(true, "Student registered successfully");
            		}
            	}
            }
	 	}
	}
	



	/**
	 * Add Facebook User Method.
	 * 
	 * @param $teacher_email 
	 * @param $firstname 
     * @param $lastname 
	 */
	 
	 public function AddFacebookUser(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$email = isset($email)?$email:'';
			
        	// email should not be left blank
			if (empty($email))
            return $this->_returnJson(false, 'Please enter your email_id.');

			// Check if email address is valid
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			$user=$this->User->find('first',array('conditions'=> array('User.email'=>$email)));
            $parent_id=$user['User']['id'];
            if(empty($parent_id))
            {
            	$this->User->create();
            	$array=array('email'=>$email,'type'=>'parent');
            	if($this->User->save($array))
            	{
            		return $this->_returnJson(true, "User registered successfully");
            	}
            }
       	    else
            {
            	return $this->_returnJson(true, "User already exist");
            }
	 	}
	}



	 /**
	 * Count All Users Method.
	 * 
	 * @param $teacher_email 
	 * @param $firstname 
     * @param $lastname 
	 */
	 
	 public function TotalUsers(){ 
	 	
		$user=$this->User->find('all');
		$length=sizeof($user);
		$this->jsonArray['Total_Users'] = $length;
	    return $this->_returnJson(true,'Total Number of users:');
	 	
	 }



	/**
	 * Login method.
	 *
	 * @access public
	 *
	 * @param string $email
	 * @param string $password
	 * @return json object
	 * 
	 * "You also need to check if new user email id is present in ""Invite"" Table or not.
	 *	If present in invite table then award his invitee as free book. 
	 * But You also need to check the counter ragarding the free books in the User table. 
	 * And also delete the data from the invites tables"
	 * 
	 * If user has free book or not. 
	 * User can have a free book on invite also. (If his invited friend accepts his invitation and create a account on our app)
	 * 
	 */

	public function login(){
		if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$email = isset($email)?$email:'';
			$password = isset($password)?$password:'';
			
			if(empty($email) || empty($password))
				return $this->_returnJson(false, 'Please enter your login credentials.');
			
			// if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			// 	return $this->_returnJson(false, 'Please enter a valid email address.');
			
				$result = $this->User->find('first', array(
				'conditions' => array(
					'User.email' => $email,
					'User.password' => AuthComponent::password($password),
				),
				'recursive' => -1,
			)
			);
			if($result['User']['type']=='teacher' || $result['User']['type']=='parent')
			{
				$uniqueid1=$result['User']['id'];
			}
		
			// Check whether a user exists
			if ($result) {
				
				if($this->UserInvite->hasAny(array('UserInvite.invite_emailid'=>$email))){
					$userinvite=$this->UserInvite->find('first',array('conditions'=>array('UserInvite.invite_emailid'=>$email)));
					$invitee_details=$userinvite['User'];
					$this->UserInvite->deleteAll(array('UserInvite.invite_emailid'=>$email));
					
					$update_user=array();
					$update_user['User']['id']=$invitee_details['id'];
					$update_user['User']['total_invited']=$invitee_details['total_invited']+1;
					$update_user['User']['free_book_available']='0';
					
					if($invitee_details['invite_target']==$update_user['User']['total_invited']){
						$update_user['User']['free_book_available']='1';
						$update_user['User']['free_book']=$invitee_details['free_book']+1;
						// Update next invite target for the user
						$get_invites=$this->Freebook->find('first',array('conditions'=>array('Freebook.invited >'=> $invitee_details['invite_target'])));
						if(empty($get_invites)){
							$update_user['User']['invite_target']=$update_user['User']['total_invited']*2;
						}else{
							$update_user['User']['invite_target']=$get_invites['Freebook']['invited'];
						}
						$update_user['User']['total_invited']='0';
					}
					$this->User->save($update_user);
				}
						
				$data = array();
				$data["User"]["id"] = $result['User']['id'];
				$data["User"]["last_login"] = date("Y-m-d H:i:s");
				$firstname=$result['User']['first_name'];
				$credits=$result['User']['credits'];
				$type=$result['User']['type'];
				$this->User->Save($data);
				$this->Session->write('user_type', $type);
				if(empty($uniqueid1))
				{
					if($type=='teacher'){
						$msg=array("You have logged-in successfully.",'first_name => '.$firstname,'credits => '.$credits);
						$this->Session->write('user_type', $type);
					}else{
						$msg=array("You have logged-in successfully.",'first_name => '.$firstname);
						$this->Session->write('user_type', $type);
					}
				}
				else
				{
					if($type=='teacher'){
						$msg=array("You have logged-in successfully.",'Unique_id => '.$uniqueid1,'first_name => '.$firstname,'credits => '.$credits);
						$this->Session->write('user_type', $type);
					}else{
						$this->Session->write('user_type', $type);
						$msg=array("You have logged-in successfully.",'Unique_id => '.$uniqueid1,'first_name => '.$firstname);
					}
				}

				return $this->_returnJson(true, $msg);
			}else {
				return $this->_returnJson(false, 'Invalid login credentials.');
			}
		}
	}

	/**
	 * Logout method
	 * 
	 * @param no parameters are there
	 */
	 
	 public function logout(){
	 	if(!empty($this->Session->read('user_type')))
	 	{
 		 	$this->Session->delete('user_type');
	 		return $this->_returnJson(true, 'User Logout Successfully.');	
	 	}
	 	else
	 	{
	 		return $this->_returnJson(false, 'No User Loggedin.');	
	 	}
	 }


	 	/**
	 * Logout method
	 * 
	 * @param no parameters are there
	 */
	 
	 public function login_with_facebook(){
	 	if($this->request->is('post')){
			extract($this->request->data);
			$email = isset($email)?$email:'';
		
			if(empty($email))
            {
            	return $this->_returnJson(false, "Email can not be empty");
            }
        	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');

        	$users=$this->User->find('first',array('conditions'=>array('email'=>$email)));
 
        	if(empty($users))
        	{
        		$password=rand(1111,9999);
			  	$Email = new CakeEmail('default');
        		$Email->from(array('demo.narola@gmail.com' => 'Tagsom App'))
                ->to($email)
                ->subject('You Login Credentials')
                ->send('Your Email_id = '.$email . ', Your password = '.$password);
        		$array=array('email'=>$email,'password'=>$password,'type'=>'parent','credits'=>3);
        		$this->User->create();
        		$this->User->save($array);
        		$credits=3;
        		$msg=array("User Successfully Registered.",'credits => '.$credits);
        		return $this->_returnJson(true, $msg);
        	}
        	else
        	{
        		return $this->_returnJson(false, 'User already exist.');
        	}

	 	}
	 	else
	 	{
	 		return $this->_returnJson(false, 'Invalid Post Method.');
	 	}
	 }





	/**
	 * Get school names method
	 * 
	 * @param no parameters are there
	 */
	 
	 public function get_SchoolNames(){ 
			$schools=$this->School->find('all',array('fields'=>array('School.name')));
            if(empty($schools))
            {
            	return $this->_returnJson(false, "No Schools are there");
            }
            else
            {
            	return $this->_returnJson(true, $schools);
            } 	  
	 }


	 /**
	 * Get Strings for Apps label
	 * 
	 * @param no parameters are there
	 */
	 
	 public function GetStrings(){ 
		if($this->request->is('post')){
			extract($this->request->data);
			$title = isset($title)?$title:'';
			$language = isset($language)?$language:'';
			if(empty($title))
            {
            	return $this->_returnJson(false, "Title is empty");
            }
			if(empty($language))
            {
            	return $this->_returnJson(false, "Language is empty");
            }
            else
            {
				$english_id=$this->Language->getEnglishLanguage();
            	$languages=$this->Language->find('first',array('conditions'=>array('language_name'=>$language)));
            	$language_id=$languages['Language']['id'];

            	$english_strings=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$english_id,'title'=>$title)));
            	$array=array();
            	
            	if($language_id==$english_id)
            	{
            	$strings=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'title'=>$title)));
        		array_push($array, $strings);
            	}
            	else
            	{
            		foreach($english_strings as $english_string)
	            	{
	            		$strings=$this->GeneralSettings->find('first',array('conditions'=>array('language_id'=>$language_id,'setting_id'=>$english_string['GeneralSettings']['id'])));
	            		array_push($array, $strings);
	            	}
            	}
            	return $this->_returnJson(true, $array);
         
            }
		}
	}



	 /**
	 * insert Strings for Apps label method
	 * 
	 * @param no parameters are there
	 */
	 
	 public function InsertStrings(){ 
		if($this->request->is('post')){
			$api=$this->Apitoken->get_active_api();
			extract($this->request->data);
			$title = isset($title)?$title:'';
			$string = isset($string)?$string:'';

			if(empty($title))
            {
            	return $this->_returnJson(false, "Title is empty");
            }
			if(empty($string))
            {
            	return $this->_returnJson(false, "String is empty");
            }
            else
            {
            	//get project id from poeditor
				$project_array1=array('api_token' => $api,'action'=>'list_projects');
				$HttpSocket = new HttpSocket();
				$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				$list_projects1 = json_decode($list_project1, true);
				foreach($list_projects1['list'] as $project)
				{
					if($project['name']=='Tagsom Project'){
						$projectid=$project['id'];
					}
				}

				//add title in poeditor
	    		$arr11=array("term"=>$title,"context"=>"Title","plural"=>"General Settings");
			    $data11 = array(
		           "api_token" => $api,
		           "action" => "add_terms",
		           "id" => $projectid,
		           "data" => json_encode(array($arr11),true));
		    	$request=array();
		    	$response22 = $HttpSocket->post('https://poeditor.com/api/', $data11,$request);



            	$english=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
				$english_id=$english['Language']['id'];
				$strings=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$english_id)));
				$array=array();
				foreach($strings as $str)
				{
					array_push($array, $str['GeneralSettings']['string']);
				}

            	foreach($string as $s)
            	{
            		if(!in_array($s,$array))
            		{
	            		$this->GeneralSettings->create();
	            		$file_name=str_replace(' ', '_', $s).".wav";
						$array=array('language_id'=>$english_id,'title'=>$title,'string'=>$s,'audio_clip'=>$file_name);
	            		$this->GeneralSettings->save($array);
						$setting_id=$this->GeneralSettings->getLastInsertId();
						$languages=$this->Language->find('all');
						
						//add Strings in poeditor
			    		$arr12=array("term"=>$s,"context"=>"(".$title.")","plural"=>"Strings");
					    $data12 = array(
				           "api_token" => $api,
				           "action" => "add_terms",
				           "id" => $projectid,
				           "data" => json_encode(array($arr12),true));
				    	$request=array();
				    	$response23 = $HttpSocket->post('https://poeditor.com/api/', $data12,$request);

						foreach($languages as $language)
						{
							if($language['Language']['id']!=$english_id)
							{
								$this->GeneralSettings->create();
								$array=array('language_id'=>$language['Language']['id'],'title'=>'No Translation','string'=>'No Translation','setting_id'=>$setting_id,'audio_clip'=>$file_name);
								$this->GeneralSettings->save($array);
							}
						}
					}
					else
					{
						return $this->_returnJson(false, 'String is already exist');
					}
            	}
            	 return $this->_returnJson(true, 'String is added successfully');
            } 	
		}
	}



	 /**
	 * Get Languages names method
	 * 
	 * @param no parameters are there
	 */
	 public function get_languages(){ 
			$languages=$this->Language->find('all',array('fields'=>array('Language.id','Language.language_name','Language.flag')));
            if(empty($languages))
            {
            	return $this->_returnJson(false, "No Languages are there");
            }
            else
            {
            	$array=array();
            	$flag_array=array();
            	foreach($languages as $language)
            	{
            		array_push($array, $language['Language']['language_name']);
            		array_push($flag_array,$language['Language']['language_name'],$language['Language']['flag']);
            	}
            	return $this->_returnJson(true, $languages);
            } 	  
	 }



	/**
	* Get Story Introduction method
	* 
	* @param no parameters are there
	*/
	public function GetStoryIntroduction(){ 	
 		$languages=$this->Language->find('all');
      	$length=sizeof($languages);
      	
      	$array=array();
  		for($i=0;$i<$length;$i++)
      	{
      		$lang_id=$languages[$i]['Language']['id'];
      		$stories=$this->Story->find('all',array('fields'=>array('story_name','audio_clip','active'),'conditions'=>array('Story.language_id'=>$lang_id)));
      		$array[$i]['Language']=$languages[$i]['Language']['language_name'];
      		$ar1=array();
      		foreach($stories as $story)
      		{
     			$translation=$this->StoryTranslation->find('first',array('conditions'=>array('story_name'=>$story['Story']['story_name'],'language_id'=>$lang_id)));
				if($lang_id==2)
				{
					$local_name=$translation['StoryTranslation']['story_name'];
				}
				else
				{	
				$local_name=$translation['StoryTranslation']['translation'];
				}
      			if($story['Story']['active']==1){ $status='Disabled';}else{ $status='Enabled';}
      			$story_array=array('story_name'=>$story['Story']['story_name'],'audio_clip'=>$story['Story']['audio_clip'],'story_status'=>$status,'local_name'=>$local_name);
      			array_push($ar1,$story_array);
      		}
      		$array[$i]['Introduction']=$ar1;
      	}
        return $this->_returnJson(true, $array);
	 }




	  /**
	 * Get Story Introduction method
	 * 
	 * @param no parameters are there
	 */
	 public function GetStoryIntroText(){ 
 		if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$language = isset($language)?$language:'';
			$story_name = isset($story_name)?$story_name:'';

		
			if(empty($language))
			{
				return $this->_returnJson(false, 'Please enter language.');	
			}

			if(empty($story_name))
			{
				return $this->_returnJson(false, 'Please enter story name.');	
			}

			$languages=$this->Language->find('first',array('conditions'=>array('language_name'=>$language)));
			$find_story=$this->Story->find('first',array('conditions'=>array('language_id'=>$languages['Language']['id'],'story_name'=>$story_name),'fields'=>array('Story.introduction')));
			if(empty($find_story))
            {
            	return $this->_returnJson(false, "No Languages/Story are there");
            }
            else
            {
            	return $this->_returnJson(true, $find_story);
            }

		}
             	  
	 }
	 


	/**
	 * Get App Strings for all languages for (Level 1-3)
	 * 
	 * @param no parameters are there
	 */
	 public function ChangeAppLanguage(){ 
          
        $languages=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
        $lang_id=$languages['Language']['id'];
        $id_array=array();
        $settings1=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$lang_id,'or'=>array('GeneralSettings.title'=>'Level 1'))));
        foreach($settings1 as $setting)
        {
        	array_push($id_array, $setting['GeneralSettings']['id']);
        }
        $settings2=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$lang_id,'or'=>array('GeneralSettings.title'=>'Level 2'))));
        foreach($settings2 as $setting)
        {
        	array_push($id_array, $setting['GeneralSettings']['id']);
        }
        $settings3=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$lang_id,'or'=>array('GeneralSettings.title'=>'Level 3'))));
     	foreach($settings3 as $setting)
        {
        	array_push($id_array, $setting['GeneralSettings']['id']);
        }

        $all_langs=$this->Language->find('all');
        foreach($all_langs as $l)
        {
        	$arr[$l['Language']['language_name']]=array();
        }
       
        foreach($id_array as $id)
        {
        	$settings=$this->GeneralSettings->find('all',array('fields'=>array('title','language_id','string','audio_clip'),'conditions'=>array('OR'=>array('setting_id'=>$id,'id'=>$id))));
        	$length4=sizeof($settings);

 			for($i=0;$i<$length4;$i++)
 			{
 				$langs=$this->Language->find('first',array('conditions'=>array('id'=>$settings[$i]['GeneralSettings']['language_id'])));
    			$language_name=$langs['Language']['language_name'];
    			array_push($arr[$language_name], $settings[$i]['GeneralSettings']);
 			}
        }
    	return $this->_returnJson(true, $arr);
	 }



	 /**
	 * Get Apps string for all languages(Except Level 1-3)
	 * 
	 * @param no parameters are there
	 */
	 public function GetAllStrings(){ 
          
       
        $id_array=array();
       	$settings1=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$this->Language->getEnglishLanguage())));
        foreach($settings1 as $setting)
        {
        	if($setting['GeneralSettings']['title']!='Level 1' && $setting['GeneralSettings']['title']!='Level 2' && $setting['GeneralSettings']['title']!='Level 3' )
        	{
        		array_push($id_array, $setting['GeneralSettings']['id']);
        	}
        }
  
  		$all_langs=$this->Language->find('all');
        foreach($all_langs as $l)
        {
        	$arr[$l['Language']['language_name']]=array();
        }

        foreach($id_array as $id)
        {
        	$settings=$this->GeneralSettings->find('all',array('fields'=>array('title','language_id','string','audio_clip'),'conditions'=>array('OR'=>array('setting_id'=>$id,'id'=>$id))));
        	foreach($settings as $setting)
    		{
    			$langs=$this->Language->find('first',array('conditions'=>array('id'=>$setting['GeneralSettings']['language_id'])));
    			$language_name=$langs['Language']['language_name'];
    			array_push($arr[$language_name], $setting);
    		}
        	
        }

    	return $this->_returnJson(true, $arr);
	 }



	/**
	 * Add list of free books to create and share links from tagsom
	 * 
	 * @param no parameters are there
	 */
	 public function Share_Book()
	 {    
	 	if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
			$book=isset($book)?$book:'';
          
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');

			if(empty($book))
				return $this->_returnJson(false, 'Please enter books to share');

			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
					foreach($book as $b)
					{

						$books=$this->Story->find('first',array('conditions'=>array('story_name'=>$b)));
						$story_id=$books['Story']['id'];
			            $userid=$this->User->getUserIdbyEmail($email);
						$books1=$this->Book->find('first',array('conditions'=>array('status'=>'Purchased','user_relation_id'=>$userid,'story_id'=>$story_id)));
						
						if(empty($books1))
						{
							return $this->_returnJson(false, "No purchased books are found.");
						}
						else
						{
							$book_id=$books1['Book']['story_id'];
							$invites=$this->SharedBooks->find('first',array('conditions'=>array('user_id'=>$userid,'book_id'=>$book_id)));
							if(empty($invites))
							{
								$this->SharedBooks->create();
								$this->SharedBooks->save(array('user_id'=>$userid,'book_id'=>$book_id));
								return $this->_returnJson(true,'Books is successfully added to share');
							}
							else
							{
								return $this->_returnJson(true, 'This book is already shared');
							}
						}
					}
				}
					
				}	
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	 }

	  /**
	 * Get Country list names method
	 * 
	 * @param no parameters are there
	 */
	 
	 public function get_country_names(){ 
			$countries=$this->Country->find('all');
            if(empty($countries))
            {
            	return $this->_returnJson(false, "No countries are there");
            }
            else
            {
            	$array=array();
            	foreach($countries as $country)
            	{
            		array_push($array, $country['Country']['country_name']);
            	}
            	return $this->_returnJson(true, $array);
            } 	  
	 }


	/**
	 * Profile edit method.
	 * 
	 * @access public
	 * 
	 * @param $username
	 * @param $email
	 * @param $currentPassword
	 * @param $newPassword
	 * @param $first_name
	 * @param $last_name
	 * @param $security_code
	 * @param $previous_security_code
	 */
	 
	public function oldprofile_edit(){
		if($this->request->is('post')){
			extract($this->request->data);
			$username = isset($username)?$username:'';
			$email = isset($email)?$email:'';
			$newPassword = isset($newPassword)?$newPassword:'';
			$currentPassword = isset($currentPassword)?$currentPassword:'';
			$first_name = isset($first_name)?$first_name:'';
			$last_name = isset($last_name)?$last_name:'';
			$security_code = isset($security_code)?$security_code:'';
			$previous_security_code =isset($previous_security_code)?$previous_security_code:'';
			
			if(empty($email))
			return $this->_returnJson(false, 'Please enter email address.');
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
			
			if(empty($currentPassword))
			return $this->_returnJson(false, 'Please enter your current password.');
			
			if (empty($newPassword))
			return $this->_returnJson(false, 'Please enter your new password.');
			
			if (empty($first_name))
			return $this->_returnJson(false, 'Please enter your first name.');
			
			if (empty($last_name))
			return $this->_returnJson(false, 'Please enter your last name.');
			
			if(empty($previous_security_code))
			return $this->_returnJson(false, 'Please enter your current security code.');
			
			if (!$this->User->hasAny(array('User.email' => $email, 'User.security_code' => $previous_security_code)))
            return $this->_returnJson(false, 'Current security code does not match, please try again.');
			
			if (empty($security_code))
			return $this->_returnJson(false, 'Please enter your new security_code.');
			
			$userdetails=$this->User->find('first',array(
			'conditions' => array(
				'User.email' => $email
			),
			'recursive' => -1
			));
			
			
			if($userdetails){
				if (!$this->User->hasAny(array('User.id' => $userdetails["User"]["id"], 'User.password' => AuthComponent::password($currentPassword)))) {
					return $this->_returnJson(false, 'Invalid current password. Please enter a correct password.');
				}
				
				$data=array("User" => array(
					"id" => $userdetails['User']['id'],
					"password" => $newPassword,
					"first_name" => $first_name,
					"last_name" => $last_name,
					"security_code" => $security_code
				));
				
				$this->User->save($data);
				return $this->_returnJson(true, 'Profile has been updated successfully.');
			}else {
				return $this->_returnJson(false, 'Error, Invalid User.');
			}
		}
	}

	/**
	 * Forgot password method
	 * 
	 * @access public
	 * 
	 * @param $username
	 * OR
	 * @param $email
	 * 
	*/
	 
	public function forgot_password(){
		if($this->request->is('post')){
			extract($this->request->data);
			$username = isset($username)?$username:'';
			$email = isset($email)?$email:'';
			$useremail='';
			if(empty($username) && empty($email))
			return $this->_returnJson(false, 'Please enter username or email.');
			$this->User->contain();
			if(!empty($username)){
				$userData = $this->User->find('first', array('conditions' => array('User.username' => $username)));
				if (!$userData)
				return $this->_returnJson(false, 'Username doesn\'t exist.');
				$useremail=$userData['User']['email'];
			}
			
			if(!empty($email)){
				if (!filter_var($email, FILTER_VALIDATE_EMAIL))
					return $this->_returnJson(false, 'Please enter valid email id.');
				$userData = $this->User->find('first', array('conditions' => array('User.email' => $email)));
				if (!$userData)
					return $this->_returnJson(false, 'Email id doesn\'t exist.');
				$useremail=$email;
			}
			
			
			   // Generate a 6 digit random integer
				$generatedPassword = rand(100000, 999999);
				// Make an array to hold data
				$temp = array(
					'User' => array(
						'id' => $userData['User']['id'],
						'password' => $generatedPassword,
					),
				);
				// 
				$messagedata=array(
					'first_name'=>$userData['User']['first_name'],
					'last_name'=>$userData['User']['last_name'],
					'new_password'=>$generatedPassword);
				$this->User->save($temp);
				$this->_sendemail(Configure::read('FROM'),$useremail,'Forgot Password',$messagedata,'forgot_password');
			
			return $this->_returnJson(true, 'Please check your email for new password.');
			
		}
	}



/**
	 * Set Password of user from tagsom.com
	 * POST method
	 * @access public
	 * 
	 * @param $first_name
	 * @param $last_name
	 * @param $email
	 * @param $password
	*/
	public function set_password($email=null,$password=null){

			$user=$this->User->find('first',array('conditions'=>array('email'=>$email)));
			$user_id=$user['User']['id'];
			$this->User->id=$user_id;

			if ($this->User->save(array('password'=>$password))) {
				return $this->_returnJson(true, 'Password Set Succesfully');
			
			} else {
				return $this->_returnJson(false, 'Password Not Set Succesfully');
			}
		
	}



	/**
	 * Register user from tagsom.com
	 * POST method
	 * @access public
	 * 
	 * @param $first_name
	 * @param $last_name
	 * @param $email
	 * @param $password
	*/
	public function add_user($first_name=null,$last_name=null,$email=null,$password=null){

			$this->User->create();

			if ($this->User->save(array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'password'=>$password))) {
				return $this->_returnJson(true, 'User Registered Succesfully');
			
			} else {
				return $this->_returnJson(false, 'User Not Registered Succesfully');
			}
		
	}


	/**
	 * For login user from tagsom.com
	 * POST method
	 * @access public
	 * 
	 * @param $email for email id
	 * @param $password for password
	*/
	public function login_user($email=null,$password=null){

			if(empty($email) || empty($password))
				return $this->_returnJson(false, 'Please enter your login credentials.');
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL))
				return $this->_returnJson(false, 'Please enter a valid email address.');
			
				$result = $this->User->find('first', array(
				'conditions' => array(
					'User.email' => $email,
					'User.password' => AuthComponent::password($password),
				),
				'recursive' => -1,
			)
			);
			if(!empty($result))
			{
				return $result['User']['email'];
			}

	}


	/**
	 * get stories name with translation
	 * POST method
	 * @access public
	 * 
	 * @param $story
	*/
	public function get_story(){
		if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$story = isset($story)?$story:'';
			if(empty($story))
			{
				return $this->_returnJson(false, 'Please enter storyname.');
			}
			$stories = $this->Story->find('first', array('conditions' => array('Story.story_name' => $story)));
			if(empty($stories))
			{
				return $this->_returnJson(false, 'No storyname exist.');
			}
			else
			{
				$api=$this->Apitoken->get_active_api();
				$storyname=$stories['Story']['story_name'];
				$language_id=$stories['Story']['language_id'];
				$languages=$this->Language->find('first', array('conditions' => array('Language.id' => $language_id)));
				$language_name=$languages['Language']['language_name'];
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language_name);
			
				$project_array1=array('api_token' => $api,'action'=>'list_projects');
				$HttpSocket = new HttpSocket();
				$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				$list_projects1 = json_decode($list_project1, true);
				foreach($list_projects1['list'] as $project)
				{
					if($project['name']==$storyname){
						$story_id=$project['id'];
					}
				}

				$terms_arr=array('api_token' => $api,'action'=>'view_terms','id'=>$story_id,'language'=>$language_code);
				$terms=$HttpSocket->post('https://poeditor.com/api/', $terms_arr);
				$list_terms = json_decode($terms, true);
				foreach($list_terms['list'] as $term)
				{
					if($term['term']==$storyname && $term['context']=='story_name')
					{
						if(empty($term['definition']['form']))
						{
							echo "There is no translation for this storyname";
						}
						else
						{
							echo $term['definition']['form'];
						}
					}
				}
			}
		}
	}




	/**
	 * get all stories name with translation
	 * GET method
	 * @access public
	 * 
	*/
	public function get_all_story(){
	
			$stories = $this->Story->find('all');
			if(empty($stories))
			{
				return $this->_returnJson(false, 'No story exist.');
			}
			else
			{
				$api=$this->Apitoken->get_active_api();
				$array=array();
				$story_array=array();
				foreach($stories as $story)
				{
					$storyname=$story['Story']['story_name'];
					$language_id=$story['Story']['language_id'];
					$languages=$this->Language->find('first', array('conditions' => array('Language.id' => $language_id)));
					$language_name=$languages['Language']['language_name'];
					$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language_name);
				
					$project_array1=array('api_token' => $api,'action'=>'list_projects');
					$HttpSocket = new HttpSocket();
					$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
					$list_projects1 = json_decode($list_project1, true);
					foreach($list_projects1['list'] as $project)
					{
						if($project['name']==$storyname){
							$story_id=$project['id'];
						}
					}

					$terms_arr=array('api_token' => $api,'action'=>'view_terms','id'=>$story_id,'language'=>$language_code);
					$terms=$HttpSocket->post('https://poeditor.com/api/', $terms_arr);
					$list_terms = json_decode($terms, true);

					foreach($list_terms['list'] as $term)
					{
						if($term['term']==$storyname && $term['context']=='story_name')
						{
							$array=array($storyname => $term['definition']['form']);
							array_push($story_array, $array);
						}
					}
				}

				return $this->_returnJson(true, $story_array);
			}
	}



	/**
	 * get statements translation of selected story
	 * POST method
	 * @access public
	 * 
	 * @param $story
	*/
	public function get_story_statements(){
	
			if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$story = isset($story)?$story:'';
			$statement_array=array();
			if(empty($story))
			{
				return $this->_returnJson(false, 'Please enter storyname.');
			}
			$stories = $this->Story->find('first', array('conditions' => array('Story.story_name' => $story)));
			if(empty($stories))
			{
				return $this->_returnJson(false, 'No storyname exist.');
			}
			else
			{
				$api=$this->Apitoken->get_active_api();
				$storyname=$stories['Story']['story_name'];
				$language_id=$stories['Story']['language_id'];
				$languages=$this->Language->find('first', array('conditions' => array('Language.id' => $language_id)));
				$language_name=$languages['Language']['language_name'];
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language_name);
			
				$project_array1=array('api_token' => $api,'action'=>'list_projects');
				$HttpSocket = new HttpSocket();
				$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				$list_projects1 = json_decode($list_project1, true);
				foreach($list_projects1['list'] as $project)
				{
					if($project['name']==$storyname){
						$story_id=$project['id'];
					}
				}

				$terms_arr=array('api_token' => $api,'action'=>'view_terms','id'=>$story_id,'language'=>$language_code);
				$terms=$HttpSocket->post('https://poeditor.com/api/', $terms_arr);
				$list_terms = json_decode($terms, true);

				foreach($list_terms['list'] as $term)
				{
					if($term['context']=='statement')
					{
						$array=array($term['term'] => $term['definition']['form']);
						array_push($statement_array, $array);
					}
				}
				print_r($statement_array);
			}
		}
	}



	/**
	 * change story_language
	 * POST method
	 * @access public
	 * 
	 * @param $story
	*/
	public function change_language(){
	
			if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$story = isset($story)?$story:'';
			$language = isset($language)?$language:'';
			if(empty($story))
			{
				return $this->_returnJson(false, 'Please enter storyname.');
			}
			if(empty($language))
			{
				return $this->_returnJson(false, 'Please enter language name.');
			}
			$stories = $this->Story->find('first', array('conditions' => array('Story.story_name' => $story),'contain'=>'Language'));
			$storyid=$stories['Story']['id'];
			if(empty($stories))
			{
				return $this->_returnJson(false, 'No storyname exist.');
			}
			if($stories['Language']['language_name']==$language)
			{
				return $this->_returnJson(false, 'Language already exist for story');
			}

			$api=$this->Apitoken->get_active_api();
			$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language);
			if(empty($language_code))
			{
				return $this->_returnJson(false, 'Please enter valid language');
			}

			$project_array1=array('api_token' => $api,'action'=>'list_projects');
			$HttpSocket = new HttpSocket();
			$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
			$list_projects1 = json_decode($list_project1, true);
			foreach($list_projects1['list'] as $project)
			{
				if($project['name']==$story){
					$story_id=$project['id'];
				}
			}

			//list all languages
			$all_lang=array('api_token' => $api,'id'=>$story_id,'action'=>'list_languages');
			$HttpSocket = new HttpSocket();
			$all_languages= $HttpSocket->post('https://poeditor.com/api/', $all_lang);
			$list_languages = json_decode($all_languages, true);
			foreach($list_languages['list'] as $list)
			{
				if($list['code']!='en')
				{
				$selected_lang=array('api_token' => $api,'id'=>$story_id,'action'=>'delete_language','language'=>$list['code']);
				$HttpSocket->post('https://poeditor.com/api/', $selected_lang);	
				}
				else
				{
				$project_array1=array('api_token' => $api,'id'=>$story_id,'action'=>'add_language','language'=>$language_code);
				$add_lang= $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				}
			}
			$languages=$this->Language->find('first', array('conditions' => array('Language.language_name' => $language)));
			$lang_id=$languages['Language']['id'];
			$array=array('language_id'=>$lang_id);
			$this->Story->id=$storyid;
			if($this->Story->save($array))
			{
				return $this->_returnJson(true, 'Language changed successfully');
			}
		}
	}



	/**
	 * get minigames (Questions/Answers/Options) translation for selected story
	 * POST method
	 * @access public
	 * 
	 * @param $story
	*/
	public function get_minigames(){
	
			if($this->request->is('post')){
			// Declare & assign values to variables
			extract($this->request->data);
			$story = isset($story)?$story:'';
			$statement_array=array();
			if(empty($story))
			{
				return $this->_returnJson(false, 'Please enter storyname.');
			}
			$stories = $this->Story->find('first', array('conditions' => array('Story.story_name' => $story)));
			if(empty($stories))
			{
				return $this->_returnJson(false, 'No storyname exist.');
			}
			else
			{
				$minigames_array=array();
				$api=$this->Apitoken->get_active_api();
				$storyname=$stories['Story']['story_name'];
				$language_id=$stories['Story']['language_id'];
				$languages=$this->Language->find('first', array('conditions' => array('Language.id' => $language_id)));
				$language_name=$languages['Language']['language_name'];
				$language_code=$this->Language->getLocaleCodeForDisplayLanguage($language_name);
			
				$project_array1=array('api_token' => $api,'action'=>'list_projects');
				$HttpSocket = new HttpSocket();
				$list_project1 = $HttpSocket->post('https://poeditor.com/api/', $project_array1);
				$list_projects1 = json_decode($list_project1, true);
				foreach($list_projects1['list'] as $project)
				{
					if($project['name']==$storyname){
						$story_id=$project['id'];
					}
				}

				$terms_arr=array('api_token' => $api,'action'=>'view_terms','id'=>$story_id,'language'=>$language_code);
				$terms=$HttpSocket->post('https://poeditor.com/api/', $terms_arr);
				$list_terms = json_decode($terms, true);
				$arr=array();
				foreach($list_terms['list'] as $term)
				{
					if (strpos($term['context'], 'Answer') !== false) {
						$cnt=substr($term['context'], -3);
						array_push($arr, $cnt);	
					}
				}

				foreach($arr as $count)
				{
					$length=$count;
					foreach($list_terms['list'] as $term)
					{
						if($term['context']=='Question'.$count)
						{
							$que_translation=$term['definition']['form'];
							$que=$term['term'];
						}
						if($term['context']=='Answer'.$count)
						{
							$ans_translation=$term['definition']['form'];
							$ans=$term['term'];
						}
						if($term['context']=='Option1(Question'.$count.')')
						{
							$op1_translation=$term['definition']['form'];
							$op1=$term['term'];
						}
						if($term['context']=='Option2(Question'.$count.')')
						{
							$op2_translation=$term['definition']['form'];
							$op2=$term['term'];
						}
						if($term['context']=='Option3(Question'.$count.')')
						{
							$op3_translation=$term['definition']['form'];
							$op3=$term['term'];
						}
					}

					$array3=array($que,$ans,$op1,$op2,$op3);
					$op1='';
					$op2='';
					$op3='';
					$array4=array($que_translation,$ans_translation,$op1_translation,$op2_translation,$op3_translation);
					$op1_translation='';
					$op2_translation='';
					$op3_translation='';

					for($i=0;$i<5;$i++)
					{
						$array5=array($array3[$i] => $array4[$i]);
						array_push($minigames_array, $array5);
					}
				}
			    return $this->_returnJson(true, $minigames_array);
			}
		}
	}


	/**
	 * Add Child method
	 * 
	 * @access public
	 * 
	 * @param $child_name
	 * @param $yob
	 * @param $language
	 * @param $parent_emailid
	 * @param $level
	 */
	 
	 public function add_child(){
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$parent_emailid = isset($parent_emailid)?$parent_emailid:'';
			$child_name = isset($child_name)?$child_name:'';
			$yob = isset($yob)?$yob:'';
			$language = isset($language)?$language:'';
			$level = isset($level)?$level:'';
			
			if (empty($parent_emailid))
			return $this->_returnJson(false, 'Please enter Parent\'s email id.');
			
			if(!$this->User->hasAny(array('User.email'=>$parent_emailid,'User.type'=>'parent')))
			return $this->_returnJson(false, 'Parent\'s email id does not exist.');
			
			if (empty($child_name))
			return $this->_returnJson(false, 'Please enter child name.');
			
			$child_name= explode(' ', trim($child_name));
            
			$first_name= isset($child_name[0])?$child_name[0]:'';
			$last_name= isset($child_name[1])?$child_name[1]:'';
            
			
			if (empty($yob))
			return $this->_returnJson(false, 'Please enter child\'s year of birth.');
			
			if (empty($language))
			return $this->_returnJson(false, 'Please enter language.');
			
			if(!$this->Language->hasAny(array('Language.language_name'=>$language)))
			return $this->_returnJson(false, 'Language does not exist.');
			
			if (empty($level))
			return $this->_returnJson(false, 'Please enter level.');
			
			if(!$this->Level->hasAny(array('Level.level_name'=>$level)))
			return $this->_returnJson(false, 'Level does not exist.');
			
			$this->User->contain(false);
			$parent_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$parent_emailid)));
						
			$this->Language->contain();
			$language_id=$this->Language->find('first',array('fields'=>array('Language.id'),'conditions'=>array('Language.language_name'=>$language)));
			
			$this->Level->contain();
			$level_id=$this->Level->find('first',array('fields'=>array('Level.id'),'conditions'=>array('Level.level_name'=>$level)));
			$user_relations['UserRelation']['user1_id']=$parent_id['User']['id'];
			
			$child_data['User']['yob']=$yob;
			$child_data['User']['first_name']=$first_name;
            if(!empty($last_name)){
                $child_data['User']['last_name']=$last_name;
            }
			$child_data['User']['language_id']=$language_id['Language']['id'];
			$child_data['User']['level_id']=$level_id['Level']['id'];
			$child_data['User']['type']='child';
			$child_data['User']['is_verified']='1';
			
			$unique_id= strtolower($first_name).'.'.strtolower($last_name);
           	if(!empty($username)){
				if($this->User->hasAny(array('User.username'=>$username)))
				{
              		return $this->_returnJson(false,'Child already exist.');
				}
			}

			$this->User->create();
			if($this->User->save($child_data)){
				$last_insert_id=$this->User->getLastInsertId();
				$update['User']['id']=$last_insert_id;
				$update['User']['username']=$child_data['User']['username']=$unique_id.$last_insert_id;
                $this->User->validator()->remove('username');
				$this->User->save($update);
				$user_relation['UserRelation']['user1_id']=$parent_id['User']['id'];
				$user_relation['UserRelation']['user2_id']=$last_insert_id;
				$this->UserRelation->create();
				$this->UserRelation->save($user_relation);
				$relation_id=$this->UserRelation->getLastInsertId();
				$story=$this->Story->getStoryIdByStoryName($this->Session->read('free_book'));
				$this->Book->create();
				$this->Book->save(array('user_relation_id'=>$relation_id,'story_id'=>$story,'status'=>'Free'));
				$this->_returnJson(true, 'Child has been added successfully.',$child_data);

			}else{
				// $log = $this->User->getDataSource()->getLog(false, false);       
				// debug($log);
				// die;
				$this->_returnJson(false, 'Error, something went wrong.');
			}
			
		}
	 }

/**
	 * Add Grades Method.
	 * 
	 * @param no parameters are there 
	 * 
	 */
	 
	 public function add_grades(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$username = isset($username)?$username:'';
			$level = isset($level)?$level:'';
			$story = isset($story)?$story:'';
			$language = isset($language)?$language:'';
			$mode = isset($mode)?$mode:'';
			$grade = isset($grade)?$grade:'';
			$score = isset($score)?$score:'';

			// username should not be left blank
			if (empty($username))
            return $this->_returnJson(false, 'Please enter username.');

        	// level should not be left blank
			if (empty($level))
            return $this->_returnJson(false, 'Please enter user level');

        	// story should not be left blank
			if (empty($story))
            return $this->_returnJson(false, 'Please enter story name.');

        	// language should not be left blank
			if (empty($language))
            return $this->_returnJson(false, 'Please enter language.');

        	// mode should not be left blank
			if (empty($mode))
            return $this->_returnJson(false, 'Please enter mode.');

        	// grade should not be left blank
			if (empty($grade))
            return $this->_returnJson(false, 'Please enter grade.');

        	// mode should not be left blank
			if (empty($score))
            return $this->_returnJson(false, 'Please enter score.');

        	$users=$this->User->find('first',array('conditions'=>array('User.username'=>$username)));
        	if(empty($users)){
        		return $this->_returnJson(false, 'No Such User Exist');
        	}else {
        		$user_id=$users['User']['id'];
        	}
        	$levels=$this->Level->find('first',array('conditions'=>array('Level.level_name'=>$level)));
        	if(empty($levels)){
        		return $this->_returnJson(false, 'No Such Level Exist');
        	}else{
        		$level_id=$levels['Level']['id'];
        	}
        	$stories=$this->Story->find('first',array('conditions'=>array('Story.story_name'=>$story)));
        	if(empty($stories)){
        		return $this->_returnJson(false, 'No Such Story Exist');
        	}else{
        		$story_id=$stories['Story']['id'];
        	}
        	$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
        	if(empty($languages)){
        		return $this->_returnJson(false, 'No Such Language Exist');
        	}else{
        		$language_id=$languages['Language']['id'];
        	}
        	$modes=$this->Mode->find('first',array('conditions'=>array('Mode.mode_name'=>$mode)));
        	if(empty($modes)){
        		return $this->_returnJson(false, 'No Such Mode Exist');
        	}else{
        		$mode_id=$modes['Mode']['id'];
        	}
        	
    		$this->Grade->create();
    		$array=array('score'=>$score,'grade_name'=>$grade);
    		if($this->Grade->save($array))
    		{
	        	$grades=$this->Grade->find('first',array('order'=>array('Grade.id DESC')));
	        	$grade_id=$grades['Grade']['id'];
	        	$this->UserModeGrade->create();
	        	$array=array('user_id'=>$user_id,'level_id'=>$level_id,'story_id'=>$story_id,'language_id'=>$language_id,'mode_id'=>$mode_id,'grade_id'=>$grade_id);
				if($this->UserModeGrade->save($array))
				{
					return $this->_returnJson(true, 'User Mode Grade Successfully Added');
				}
			}
	 	}
	 	else
	 	{
	 		return $this->_returnJson(false, 'Invalid Post Method.');
	 	}
	 }
	


/**
	 * Get Grades Method.
	 * 
	 * @param no parameters are there 
	 * 
	 */
	 
	 public function get_grade(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$username = isset($username)?$username:'';
			$email = isset($email)?$email:'';
			$story = isset($story)?$story:'';


			// username should not be left blank
			if (empty($username))
            	return $this->_returnJson(false, 'Please enter username.');

        	// level should not be left blank
			if (empty($email))
            	return $this->_returnJson(false, 'Please enter teacher/parent email_id');

        	// story should not be left blank
			if (empty($story))
            	return $this->_returnJson(false, 'Please enter story name');

        	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
				return $this->_returnJson(false, 'Please enter valid teacher/parent email id.'); 		

			$student=$this->User->find('first',array('conditions'=>array('User.username'=>$username)));

        	if(empty($student)){
        		return $this->_returnJson(false, 'There are no student/child with that username');
        	}else {
        		$student_id=$student['User']['id'];
        	}

        	$stories=$this->Story->find('first',array('conditions'=>array('Story.story_name'=>$story)));
        	$story_id=$stories['Story']['id'];

        	$teachers=$this->User->find('first',array('conditions'=>array('User.email'=>$email)));
        	if(empty($teachers)){
        		return $this->_returnJson(false, 'There are no teachers/parents with that email_id');
        	}else {
        		$teacher_id=$teachers['User']['id'];
        		$relation=$this->UserRelation->find('first',array('conditions'=>array('UserRelation.user1_id'=>$teacher_id,'UserRelation.user2_id'=>$student_id)));
        		if(empty($relation))
        		{
        			return $this->_returnJson(false, 'There are no relation between teacher/parent and student/child');
        		}
        		else
        		{
        			$grades=$this->UserModeGrade->find('all',array('conditions'=>array('UserModeGrade.user_id'=>$student_id,'UserModeGrade.story_id'=>$story_id)));
        			if(empty($grades))
        			{
        				return $this->_returnJson(false, 'There are no grades stored for that student/child');
        			}
        			else
        			{
        				$msg=array();
        				foreach($grades as $grade)
        				{
	        				$levels=$this->Level->find('first',array('conditions'=>array('Level.id'=>$grade['UserModeGrade']['level_id'])));
	        				$stories=$this->Story->find('first',array('conditions'=>array('Story.id'=>$grade['UserModeGrade']['story_id'])));
	        				$languages=$this->Language->find('first',array('conditions'=>array('Language.id'=>$grade['UserModeGrade']['language_id'])));
	        				$modes=$this->Mode->find('first',array('conditions'=>array('Mode.id'=>$grade['UserModeGrade']['mode_id'])));
	        				$grades=$this->Grade->find('first',array('conditions'=>array('Grade.id'=>$grade['UserModeGrade']['grade_id'])));
	        				$student_name=$student['User']['username'];
	        				$level_name=$levels['Level']['level_name'];
							$story_name=$stories['Story']['story_name'];
	        				$grade_name=$grades['Grade']['grade_name'];
	        				$mode_name=$modes['Mode']['mode_name'];
	        				$language_name=$languages['Language']['language_name'];
	        				$score=$grades['Grade']['score'];
	        				$msg1=array($student_name,$level_name,$story_name,$language_name,$mode_name,$grade_name,$score);
	        				array_push($msg, $msg1);
        				}
        				return $this->_returnJson(true, $msg);
        			}
        		}
        	}
	 	}
	 }
	


	
	/**
	 * Get Overall Performance Method.
	 * 
	 * @param no parameters are there 
	 * 
	 */
	 
	 public function get_overall_performance(){ 
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$username = isset($username)?$username:'';

			// username should not be left blank
			if (empty($username))
            return $this->_returnJson(false, 'Please enter username.');

			$student=$this->User->find('first',array('conditions'=>array('User.username'=>$username)));

        	if(empty($student)){
        		return $this->_returnJson(false, 'There are no student/child with that username');
        	}else {
        		$student_id=$student['User']['id'];
        	}
        	$sum1=0;
        	$sum2=0;
        	$sum3=0;
        	$length1=0;
        	$length2=0;
        	$length3=0;
        	$user_grades=$this->UserModeGrade->find('all',array('conditions'=>array('UserModeGrade.user_id'=>$student_id),'order'=>'UserModeGrade.mode_id ASC'));
        	foreach($user_grades as $grade)
        	{
        		if($grade['UserModeGrade']['mode_id']==1)
        		{
        			$Spelling_mode=array();
        			$grade_id=$grade['UserModeGrade']['grade_id'];
        			$grades=$this->Grade->find('all',array('conditions'=>array('Grade.id'=>$grade_id)));
        			foreach($grades as $grade1)
        			{
        			array_push($Spelling_mode,$grade1['Grade']['score']);
        			}
        			
        			$length1++;
        			
        			for($i=0;$i<$length1;$i++)
        			{
        				$sum1=$sum1+$Spelling_mode[$i];
        			}
        			

        		}

        		if($grade['UserModeGrade']['mode_id']==2)
        		{
        			$Maths_mode=array();
        			$grade_id=$grade['UserModeGrade']['grade_id'];
        			$grades=$this->Grade->find('all',array('conditions'=>array('Grade.id'=>$grade_id)));
        			foreach($grades as $grade1)
        			{
        			array_push($Maths_mode,$grade1['Grade']['score']);
        			}
        			
        			$length2++;
        			for($i=0;$i<$length1;$i++)
        			{
        				$sum2=$sum2+$Maths_mode[$i];
        			}
        		}
        		if($grade['UserModeGrade']['mode_id']==3)
        		{
        			$Questions_mode=array();
        			$grade_id=$grade['UserModeGrade']['grade_id'];
        			$grades=$this->Grade->find('all',array('conditions'=>array('Grade.id'=>$grade_id)));
        			foreach($grades as $grade1)
        			{
        			array_push($Questions_mode,$grade1['Grade']['score']);
        			}
        			
        			$length3++;
        			for($i=0;$i<$length1;$i++)
        			{
        				$sum3=$sum3+$Questions_mode[$i];
        			}
        		}
	
        	}   

        	$per1=$sum1==0?0:$sum1/$length1;
        	$per2=$sum2==0?0:$sum2/$length2;
        	$per3=$sum3==0?0:$sum3/$length3;
        
        	$array=array('SpellingMode'=>number_format($per1,2),'MathsMode'=>number_format($per2,2),'QuestionsMode'=>number_format($per3,2));
			return $this->_returnJson(false, $array);
	 	}
	 }


    /**
     * Secret Code method
     * 
     * @param $parent_emailid 
     * @param $secret_code
     * In this api user will set secret code for accessing parent section. (Password for parent will reset)
     */

    public function secret_code(){
        if($this->request->is('post')){
            extract($this->request->data);
            $parent_emailid =isset($parent_emailid)?$parent_emailid:'';
            $secret_code =isset($secret_code)?$secret_code:'';
            
            if (empty($parent_emailid))
            return $this->_returnJson(false, 'Please enter Parent\'s email id.');
            
            if(!$this->User->hasAny(array('User.email'=>$parent_emailid,'User.type'=>'parent')))
            return $this->_returnJson(false, 'Parent\'s email id does not exist.');
            
            if (empty($secret_code))
            return $this->_returnJson(false, 'Please enter secret code.');
            
            $this->User->contain();
            $user_id=$this->User->find('first',array('fields'=>array('User.id','User.password'),'conditions'=>array('User.email'=>$parent_emailid)));
            $user['User']['security_code']=$secret_code;
            $user['User']['password']=$secret_code;
            $secret=AuthComponent::password($secret_code);
            $pwd=$user_id['User']['password'];
            $this->User->id=$user_id['User']['id'];
            if($pwd==$secret)
            {
            	$this->_returnJson(false, "Secret Code must be different then password.");
            }
            else
            {
	            if($this->User->save($user)){
	                $this->_returnJson(true, 'Secret Code has been saved successfully.');
	            }else{
	                $this->_returnJson(false, "Something went wrong.");
	            }
	        }
            
        }else{
            $this->_returnJson(false, "Invalid Post Request");
        }
    }


	/**
	 * User Invites method
	 * 
	 * @param $invitee_email
	 * @param $invited_email
	 * Just put these values in the table. And also send invite email to that id. With URL (My ID)
	 */
	 
	 public function user_invites(){
	 	if($this->request->is('post')){
	 		extract($this->request->data);
			$invitee_email =isset($invitee_email)?$invitee_email:'';
			$invited_email =isset($invited_email)?$invited_email:'';
			$userdata=$messagedata=array();
			
			if(empty($invitee_email))
			return $this->_returnJson(false,'Please enter invitee email id');
			
			if (!filter_var($invitee_email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid invitee email address.');
			
			if (!$this->User->hasAny(array('User.email' => $invitee_email)))
			return $this->_returnJson(false, 'Invitee email does not exist.');
			
			if(empty($invited_email))
			return $this->_returnJson(false,'Please enter invited email id');
			
			if (!filter_var($invited_email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid invited email address.');
			
			if ($this->User->hasAny(array('User.email' => $invited_email, 'User.is_verified' => 1)))
            return $this->_returnJson(false, 'An account already exists for this email id.');
			
			
			$this->User->contain();
			$userdata=$this->User->find('first',array('conditions'=>array('User.email'=>$invitee_email)));
			$fromemail=$userdata['User']['email'];
			$password=$this->_getAccessToken(rand(100000, 999999));
			$messagedata=array('email'=>$invited_email,'password'=>$password);
			
			$userinvites=array();
			$userinvites['UserInvite']['user_id']=$userdata['User']['id'];
			$userinvites['UserInvite']['invite_emailid']=$invited_email;
			
			$this->UserInvite->create();
			if($this->UserInvite->save($userinvites)){
				$userinvite_id=$this->UserInvite->getLastInsertId();
				$messagedata["activation_url"]=Router::url('/user_invites/activate?invitestatus=true&invite_id='.$userinvite_id.'&passkey='.base64_encode($password).'&invitedmail='.base64_encode($invited_email), true );
				$this->_sendemail($invitee_email,$invited_email,'Invitation Mail',$messagedata,'user_invite');
				$this->_returnJson(true, "Invitation has been sent successfully.");
			}else{
				$this->_returnJson(false, "Invitation was not sent. Please try again.");
			}
			
				
		}else{
			$this->_returnJson(false, "Invalid Post Request");
		}
	 }


	
    /**
     * verificationcode method
     * 
     * @param $verify_code
     * @param $email
     * 
     */

    public function verifycode(){
        if($this->request->is('post')){
            extract($this->request->data);
            $verify_code =isset($verify_code)?$verify_code:'';
            $email =isset($email)?$email:'';
            
            if(empty($verify_code))
            return $this->_returnJson(false,'Please enter verification code.');
            
            if(empty($email))
            return $this->_returnJson(false,'Please enter email.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $email)))
            return $this->_returnJson(false, 'Registered email does not exist.');
            
            
            $user=$this->User->find('first',array('conditions'=>array('User.email'=>$email,'User.password'=>AuthComponent::password($verify_code))));
             
            if(!empty($user)){
                $this->_returnJson(true, "Verification code is correct");
            }else{
                $this->_returnJson(false, "Verification code is incorrect");
            }
            
        }else{
            $this->_returnJson(false, "Invalid Post Request");
        }
    }

    /**
     * teacher_registration method
     * 
     * @param $email
     * @param $first_name
     * @param $last_name
     * @param $school_name
     * @param $contact_num
     * @param $dob
     * @param $gender
     * @param $country
     *  
     */
     
    public function teacher_registration(){
        if($this->request->is('post')){
            extract($this->request->data);
            $email =isset($email)?$email:'';
            $first_name =isset($first_name)?$first_name:'';
            $last_name =isset($last_name)?$last_name:'';
            $school_name =isset($school_name)?$school_name:'';
            $contact_num =isset($contact_num)?$contact_num:'';
            $dob =isset($dob)?$dob:'';
            $gender =isset($gender)?$gender:'';
            $country =isset($country)?$country:'';
            
            if(empty($email))
            return $this->_returnJson(false,'Email cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.email'=>$email,'type'=>'teacher')))
            return $this->_returnJson(false,'Teacher email does not exist.');
            
            if(empty($first_name))
            return $this->_returnJson(false,'Please enter your first name.');
            
            if(empty($last_name))
            return $this->_returnJson(false,'Please enter your last name.');
            
            if(empty($school_name))
            return $this->_returnJson(false,'Please enter your school name.');
            
            if(!$this->School->hasAny(array('School.name'=>$school_name)))
            return $this->_returnJson(false,'School does not exist.');
            
            if(empty($contact_num))
            return $this->_returnJson(false,'Please enter your contact number.');
            
            if(!is_numeric($contact_num))
            return $this->_returnJson(false,'Only numbers are allowed.');
            
            // Y-m-d
            if(empty($dob))
            return $this->_returnJson(false,'Please enter your date of birth.');
            
            if(empty($gender))
            return $this->_returnJson(false,'Please select your gender.');
            
            if(empty($country))
            return $this->_returnJson(false,'Please select country.');
            
            if(!$this->Country->hasAny(array('Country.country_name'=>$country)))
            return $this->_returnJson(false,'Country does not exist.');
            
            
            $this->School->contain();
            $school_id=$this->School->find('first',array('fields'=>array('School.id'),'conditions'=>array('School.name'=>$school_name)));
            
            
            $this->Country->contain();
            $country_id=$this->Country->find('first',array('fields'=>array('Country.id'),'conditions'=>array('Country.country_name'=>$country)));
            
            $this->User->updateAll(array('User.first_name' => "'".$first_name."'",
                                        'User.last_name' => "'".$last_name."'",
                                        'User.school_id' => $school_id['School']['id'],
                                        'User.country_id' => $country_id['Country']['id'],
                                        'User.contact_no' => $contact_num,
                                        'User.dob' => "'".date('Y-m-d',strtotime($dob))."'",
                                        'User.gender' => "'".$gender."'"),array('User.email'=>$email));
            $this->_returnJson(true, "Teacher has been registered successfully.");
        }else{
            $this->_returnJson(false, "Invalid Post Request");
        }
    }


    /**
     * fetchStudentData method
     *  
     * @param $email
     * @param $key_search
     */
     
     public function fetchStudentData(){
         if($this->request->is('post')){
             extract($this->request->data);
             $email =isset($email)?$email:'';
             $key_search = isset($key_search)?$key_search:'';
             
             $conditions=array();
             
             if(empty($email))
             return $this->_returnJson(false,'Email cannot be left blank.');
             
             if(!$this->User->hasAny(array('User.email'=>$email,'type'=>'teacher')))
             return $this->_returnJson(false,'Teacher email does not exist.');
             
             if(!empty($key_search)){
                 $conditions=array('OR' => array(
                    array('first_name LIKE' => '%'.$key_search.'%'),
                    array('last_name LIKE' => '%'.$key_search.'%')
                ));
             }
             
             $user=$this->User->find('first',array('conditions'=> array('User.email'=>$email),
                'fields'=> array('User.id','User.email'),
                'contain'=> array('Student'=>array(
                'fields' => array('Student.user1_id')
                ),'Student.User2'=>array(
                'fields' => array('id','first_name','last_name','yob','username as uniqueid','is_unique'),
                'conditions' => $conditions
                ))
             ));
             
             foreach ($user['Student'] as $key => $value) {
                 unset($user['Student'][$key]['user1_id'],$user['Student'][$key]['user2_id']);
                 if(empty($value['User2'])){
                     unset($user['Student'][$key]);
                 }else{
                     $user['Student'][$key]=$user['Student'][$key]['User2'];
                 }
                 
             }
             print_r($user);
             if(empty($user['Student'])){
                $this->_returnJson(true, "Students data not found",$user);
             }else{
                 $this->_returnJson(true, "Students data",$user);
             }
             
         }else{
            return $this->_returnJson(false, "Invalid Post Request");
        }
     }

     /**
     * To get child details of parents
     *  
     * @param no parameters are there
     * 
     */
    public function get_child_details()
    {
    	if($this->request->is('post'))
    	{
            extract($this->request->data);
            $email =isset($email)?$email:'';
            if(empty($email))
            {
             	return $this->_returnJson(false, "Please Enter Email_ID");
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
            	return $this->_returnJson(false, 'Please Enter a Valid Email Address.');
            }
            $user=$this->User->find('first',array('conditions'=> array('User.email'=>$email,'type' => 'parent'),
	            'fields'=> array('User.id','User.email'),
	            'contain'=> array('Student'=>array('fields' => array('Student.user2_id')))));
            if(empty($user))
            {
            	return $this->_returnJson(false, "Invalid Parent Email_id");
            }
            if(empty($user['Student']))
            {
            	return $this->_returnJson(false, "There are no childs for this parent");
            }
            $arr=array();
            // foreach($user['Student'] as $child)
            // {
            	
            // 	$child_id=$child['user2_id'];
            // 	$childs=$this->User->find('all',array('conditions'=> array('User.id'=>$child_id)));
            // 	array_push($arr,$childs);

            // }
            foreach($user['Student'] as $child)
            {
            	
            	$child_id=$child['user2_id'];
            	$childs=$this->User->find('all',array('fields'=>array('id','first_name','last_name','yob','username','language_id','level_id','credits'),'conditions'=> array('User.id'=>$child_id)));
            	array_push($arr,$childs);

            }
            $arr2=array();
            foreach($arr as $child)
            {
            	foreach($child as $c)
            	{
            		$language=$this->Language->find('first',array('conditions'=>array('id'=>$c['User']['language_id'])));
            		$level=$this->Level->find('first',array('conditions'=>array('id'=>$c['User']['level_id'])));
            		$c['User']['language_id']=$language['Language']['language_name'];
            		$c['User']['level_id']=$level['Level']['level_name'];

            		array_push($arr2, $c);
            	}

            }
    
            return $this->_returnJson(true, $arr2);
        }
        else
        {
            return $this->_returnJson(false, "Invalid Post Request");
        }
    }

    /**
     * Uniqueid Login Settings method
     *  
     * @param $uniqueid[unique_id]
     * 
     */

    public function uniqueid_login_settings(){
        if($this->request->is('post')){
            extract($this->request->data);
			$username =isset($username)?$username:array();
			$is_unique =isset($is_unique)?$is_unique:array();
			
			if(empty($username))
             return $this->_returnJson(false,'Username cannot be left blank.');

           	$arr=array(0,1);
           	if(!in_array($is_unique, $arr))
           	{
           		return $this->_returnJson(false,'is_unique must be 0 or 1.');
           	}
           
			$users=$this->User->find('all',array('conditions'=>array('User.username'=>$username)));
			$id=$users[0]['User']['id'];
			if(empty($users))
			{
				return $this->_returnJson(false,'No such user exist');
			}
			else
			{
				$this->User->id=$id;
				$ar1=array('is_unique'=>$is_unique);
				$this->User->save($ar1);
				return $this->_returnJson(true,'User settings changed');
			}
			
       }
    }
	  
	 /**
     * Get Student Details By Unique id method
     *  
     * @param $uniqueid
     * 
     */
    
    public function GetStudentDetailsByUid(){
        if($this->request->is('post')){
            extract($this->request->data);
            $uniqueid =isset($uniqueid)?$uniqueid:'';
            
            if(empty($uniqueid))
             return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid,'User.type'=>'student')))
            return $this->_returnJson(false,'Student unique id does not exist.');
            
            //$this->User->contain();
            $user_details=$this->User->find('first',array('conditions'=>array('username'=>$uniqueid,'type'=>'student'),
            'fields'=>array('User.first_name','User.last_name','User.gender','User.yob','User.username as uniqueid','User.is_unique','User.is_verified','User.language_id'),
            'contain'=>array(
            'Language.language_name',
            'Level.level_name',
            'School' => array('fields'=>array('School.name','School.email')),
            'Teacher.User1' => array('fields'=>array('User1.first_name','User1.last_name','User1.email')))
            ));
           return $this->_returnJson(true, "Student details",$user_details);
            
        }else{
            return $this->_returnJson(false, "Invalid Post Request");
        }
    }


    /**
     * Get Books method
     *  
     * @param $uniqueid
     * 
     */
     
    public function getbooks(){
        if($this->request->is('post')){
            extract($this->request->data);
            
            $uniqueid =isset($uniqueid)?$uniqueid:'';
            
            if(empty($uniqueid))
             return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid,'User.type'=>'student')))
            return $this->_returnJson(false,'Student unique id does not exist.');
            
           $user_details=$this->User->find('first',array('conditions'=>array('User.username'=>$uniqueid),
                                                          'contain' => array('Teacher.id','Teacher.Book.Story.story_name','Teacher.Book.story_id')));
           $book_data=array(); 
           foreach ($user_details['Teacher']['Book'] as $key => $bookdetails) {
               $book_data[]=array('story_id'=>$bookdetails['story_id'],'story_name'=>$bookdetails['Story']['story_name']);
           }
           $student_book_details['Book']=$book_data;
           if(empty($book_data)){
               return $this->_returnJson(false,"Student has not added any book",$student_book_details);
           }
           return $this->_returnJson(true,"Book List",$student_book_details);
            
        }else{
            // GET method
            $this->Story->contain();
            $stories=$this->Story->find('all',array('fields'=>array('Story.id','Story.story_name')));
            $this->_returnJson(true, "Books List",$stories);
        }
    } 
     
     /**
     * Add Books method
     *  
     * @param $uniqueid
     * @param $stories_id[]
     * 
     * In this api user will enter unique id of student to add books using post method. 
     */
     
    public function add_books(){
        if($this->request->is('post')){
            extract($this->request->data);
            
            $uniqueid =isset($uniqueid)?$uniqueid:'';
            $stories_id =isset($stories_id)?$stories_id:array();
            
            if(empty($uniqueid))
             return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid,'User.type'=>'student')))
            return $this->_returnJson(false,'Student unique id does not exist.');
            
            if(empty($stories_id))
             return $this->_returnJson(false,'Please select at least one Story.');
            
            $student_details=$this->User->find('first',array('conditions'=>array('User.username'=>$uniqueid),'contain'=>array('Teacher.id','Teacher.User1.id')));

            $user_relation_id=$student_details['Teacher']['id'];
            $books=$this->Book->find('all',array('conditions'=>array('Book.user_relation_id'=>$user_relation_id)));
            foreach($books as $book)
            {
            	$book_id=$book['Book']['id'];
            	$this->Book->id=$book_id;
            	$this->Book->delete();
            }
            if(!empty($student_details['Teacher']))
            {
            	$user_relation_id=$student_details['Teacher']['id'];
            	foreach($stories_id as $story_id)
            	{
            		$this->Book->saveAll(array('user_relation_id'=>$user_relation_id,'story_id'=>$story_id));
            	}
            }
            // $books['Book']['user_relation_id']=$student_details['Teacher']['id']; // User Relationship Id
            // foreach ($stories_id as $storyid) {
            //     $this->_saveBook($books['Book']['user_relation_id'], $storyid);
            // }
            
            return $this->_returnJson(true, "Books saved");
            
        }else{
            return $this->_returnJson(false, "Invalid Post Request");
        }
    }

    
    /**
     * list Student By Teacher id method
     *  
     * @param $teacher_emailid
     * 
     * In this api user will get list of students by teacher email id using post method. 
     */
     
    public function listStudentByTeacherId(){
        if($this->request->is('post')){
            extract($this->request->data);
            $teacher_emailid =isset($teacher_emailid)?$teacher_emailid:'';
            
            if(empty($teacher_emailid))
            return $this->_returnJson(false,'Please enter teacher email id.');
            
            if (!filter_var($teacher_emailid, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $teacher_emailid,'User.type'=>'teacher')))
            return $this->_returnJson(false, 'Teacher email does not exist.');
            
            $students_list=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$teacher_emailid),
                                            'contain'=>array('Student','Student.User2'=>array('fields'=>array(
                                            'User2.first_name','User2.last_name','User2.username','User2.is_unique'
                                            )))));
            $data=array();                                
            foreach ($students_list['Student'] as $key => $list) {
                $data['Student'][]=array('first_name'=>$list['User2']['first_name'],'last_name'=>$list['User2']['last_name'],'uniqueid'=>$list['User2']['username'],'is_unique'=>$list['User2']['is_unique']);
            }
            
            if(empty($data))
            return $this->_returnJson(false, "No records found for Students");
            
            return $this->_returnJson(true, "Students List",$data);
            
        }else{
            return $this->_returnJson(false, "Invalid Post Request");
        }
    } 
  
    /**
     * Sign in with student ID method
     *  
     * @param $uniqueid
     * @param $teacher_emailid
     * 
     * In this api student will be able to sign in with unique id if a teacher doesn't disable that unique id
     */
    
    public function student_signin(){
        if($this->request->is('post')){
            extract($this->request->data);
            $uniqueid =isset($uniqueid)?$uniqueid:'';
            $teacher_emailid =isset($teacher_emailid)?$teacher_emailid:'';
            
            if(empty($teacher_emailid))
            return $this->_returnJson(false,'Please enter teacher email id.');
            
            if (!filter_var($teacher_emailid, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $teacher_emailid,'User.type'=>'teacher')))
            return $this->_returnJson(false, 'Teacher email does not exist.');
            
            if(empty($uniqueid))
             return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            $student=$this->User->find('first',array('fields'=>array('User.is_unique'),'conditions'=>array('User.username'=>$uniqueid,'User.type'=>'student'),
            'contain'=>array('Teacher.User1.email')));
            
            if(empty($student))
            return $this->_returnJson(false, 'Student Unique ID does not exist.');
            
            if($student['User']['is_unique']==1)
            return $this->_returnJson(false, 'Teacher has disabled the Unique Id for this Student.');
            
            $student['Teacher']['User1']['email'] = isset($student['Teacher']['User1']['email'])?$student['Teacher']['User1']['email']:'';
            
            if(empty($student['Teacher']['User1']['email']))
            return $this->_returnJson(false, 'Teacher does not exist.');
            
            if(trim($student['Teacher']['User1']['email'])==trim($teacher_emailid)){
                return $this->_returnJson(true, 'Valid login.');
            }else{
                return $this->_returnJson(false, 'Invalid login.');
            }
            
        }else{
            return $this->_returnJson(false, "Invalid Post Request");
        }
    }

    
    /**
     * Edit Teacher method
     *  
     * @param $teacher_emailid
     * @param $first_name
     * @param $last_name
     * @param $yob
     * @param $school_name
     * @param $country
     * @param $contact_no
     * 
     * In this api teacher will be able to edit his profile using post method.
     */
     
     public function edit_teacher(){
         if($this->request->is('post')){
             extract($this->request->data);
             $teacher_emailid =isset($teacher_emailid)?$teacher_emailid:'';
             $first_name =isset($first_name)?$first_name:'';
             $last_name =isset($last_name)?$last_name:'';
             $yob =isset($yob)?$yob:'';
             $contact_no =isset($contact_no)?$contact_no:'';
             $school_name =isset($school_name)?$school_name:'';
             $country =isset($country)?$country:'';
             $user=array();
             
            if(empty($teacher_emailid))
            return $this->_returnJson(false,'Please enter teacher email id.');
            
            if (!filter_var($teacher_emailid, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $teacher_emailid,'User.type'=>'teacher')))
                return $this->_returnJson(false, 'Teacher email does not exist.');
             
            if(!empty($first_name))
             $user['User']['first_name']=$first_name;
             
            if(!empty($last_name)) 
             $user['User']['last_name']=$last_name;
            
            if(!empty($yob)) 
             $user['User']['yob']=date('Y',strtotime($yob));
            
            if(!empty($contact_no)) 
             $user['User']['contact_no']=$contact_no;
            
            if(!empty($school_name)){
                $school_id=$this->School->getSchoolIdbySchoolName($school_name);
                if($school_id==null)
                    return $this->_returnJson(false, 'School does not exist.');
                $user['User']['school_id']=$school_id;
            }
            
            if(!empty($country)){
                $country_id=$this->Country->find('first',array('fields'=>array('id'),'conditions'=>array('Country.country_name'=>$country)));
                if(empty($country_id))
                    return $this->_returnJson(false, 'Country does not exist.');
                $user['User']['country_id']=$country_id['Country']['id'];
            }
            
            if(empty($user))
            return $this->_returnJson(false, 'No field saved.');
            
            $this->User->contain();
            $user_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$teacher_emailid)));
            
            $this->User->id=$user_id['User']['id'];
            
            if($this->User->save($user)){
                return $this->_returnJson(true, 'Teacher saved successfully.');
            }
         }else{
            return $this->_returnJson(false, "Invalid Post Request");
        }
     }


    /**
     * Profile edit method.
     * 
     * @access public
     * 
     * @param $email
     * @param $current_password
     * @param $new_password
     * @param $first_name
     * @param $last_name
     * @param $yob
     * @param $dob
     * @param $contact_no
     * @param $gender
     *  
     */
     
    public function profile_edit(){
        if($this->request->is('post')){
            extract($this->request->data);
            $uniqueid = isset($uniqueid)?$uniqueid:'';
            $email = isset($email)?$email:'';
            $current_password = isset($current_password)?$current_password:'';
            $new_password = isset($new_password)?$new_password:'';
            $first_name = isset($first_name)?$first_name:'';
            $last_name = isset($last_name)?$last_name:'';
            $yob= isset($yob)?$yob:'';
            $dob= isset($dob)?$dob:'';
            $contact_no =isset($contact_no)?$contact_no:'';
            $gender =isset($gender)?$gender:'';
            
            $user=array();
            
            if(empty($email))
            return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return $this->_returnJson(false, 'Please enter a valid email address.');
            
            $this->User->contain();
            $userdetails=$this->User->find('first',array('conditions' => array('User.email' => $email)));
            
            if(empty($userdetails))
            return $this->_returnJson(false, 'Profile does not exist.');
            
            
            if(!empty($current_password)){
                 if (!$this->User->hasAny(array('User.id' => $userdetails["User"]["id"], 'User.password' => AuthComponent::password($current_password)))) {
                    return $this->_returnJson(false, 'Invalid current password. Please enter a correct password.');
                }
                
                if (empty($new_password))
                return $this->_returnJson(false, 'Please enter your new password.');
                
                $user['User']['password']=$new_password;
            }
            
            
            if (!empty($first_name))
                $user['User']['first_name']=$first_name;
            
            if (!empty($last_name))
                $user['User']['last_name']=$last_name;
            
            if (!empty($yob))
                $user['User']['yob']=date('Y',strtotime($yob));
            
            if (!empty($dob))
                $user['User']['dob']=date('Y-m-d',strtotime($dob));
            
            if (!empty($contact_no))
                $user['User']['contact_no']=$contact_no;
            
            if (!empty($gender))
                $user['User']['gender']=$gender;
			
			if(!empty($uniqueid)){
				if($this->User->hasAny(array('User.username'=>$uniqueid))){
					return $this->_returnJson(false, 'Unique Id already exist. Please enter different unique id.');
				}
					$user['User']['username']=$uniqueid;
			}
			
            
            if(empty($user)){
                return $this->_returnJson(false, 'Please enter at least one field.');
            }
            
            $this->User->id=$userdetails["User"]["id"];
            
            if($this->User->save($user)){
                return $this->_returnJson(true, 'Profile has been updated successfully.');
            }else {
                return $this->_returnJson(false, 'Error, Something went wrong.');
            }
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    }

    /**
     * Level change method.
     * 
     * @access public
     * 
     * @param $uniqueid
     * @param $email
     * @param $level
     * @param $type
     *  
     * In this API user will be able to change level by uniqueid or email
     */


    public function level_change(){
        if($this->request->is('post')){
            extract($this->request->data);
            $uniqueid = isset($uniqueid)?$uniqueid:'';
            $email = isset($email)?$email:'';
            $level = isset($level)?$level:'';
            $type =  isset($type)?$type:'';
            
            if(empty($uniqueid) && empty($email))
            return $this->_returnJson(false, 'Please enter unique id or email address.');
            
            if(empty($level))
            return $this->_returnJson(false, 'Please enter level you want to change.');
            
            if(empty($type))
            return $this->_returnJson(false, 'Type cannot be null.');
            
            $this->Level->contain();
            $level_id=$this->Level->find('first',array('fields'=>array('Level.id'),'conditions'=>array('Level.level_name'=>$level)));
            if(empty($level_id))
                return $this->_returnJson(false,'Level does not exist.');
            
            if(!empty($uniqueid)){
                if(!$this->User->hasAny(array('User.username'=>$uniqueid,'User.type'=>$type)))
                    return $this->_returnJson(false,'Unique id does not exist.');
                
                $this->User->contain();
                $user_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.username'=>$uniqueid)));
                $user['User']['level_id']=$level_id['Level']['id'];
                $this->User->id=$user_id['User']['id'];
                if($this->User->save($user)){
                    return $this->_returnJson(true,'Level changed successfully!');
                }else{
                    return $this->_returnJson(false, 'Error, Something went wrong.');
                }
            }

            if(!empty($email)){
                if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                    return $this->_returnJson(false, 'Please enter a valid email address.');
                
                if(!$this->User->hasAny(array('User.email'=>$email,'User.type'=>$type)))
                    return $this->_returnJson(false,'Email does not exist.');
             
                $this->User->contain();
                $user_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.email'=>$email)));
                $user['User']['level_id']=$level_id['Level']['id'];
                $this->User->id=$user_id['User']['id'];
                if($this->User->save($user)){
                    return $this->_returnJson(true,'Level changed successfully!');
                }else{
                    return $this->_returnJson(false, 'Error, Something went wrong.');
                }
            }
            
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    }


    /**
     * Language change method.
     * 
     * @access public
     * 
     * @param $uniqueid
     * @param $email
     * @param $language
     * @param $type
     *  
     * In this API user will be able to change language by uniqueid or email
     */
     
     
     public function language_change(){
         if($this->request->is('post')){
            extract($this->request->data);
            $uniqueid = isset($uniqueid)?$uniqueid:'';
            $email = isset($email)?$email:'';
            $language = isset($language)?$language:'';
            $type =  isset($type)?$type:'';
            
            if(empty($uniqueid) && empty($email))
            return $this->_returnJson(false, 'Please enter unique id or email address.');
            
            if(empty($language))
            return $this->_returnJson(false, 'Please enter language you want to change.');
            
            $language_id=$this->Language->getIdbyLanguageName($language);
            
            if($language_id==null)
                return $this->_returnJson(false,'Please enter correct language');
            
            if(empty($type))
            return $this->_returnJson(false, 'Type cannot be null.');
            
            if(!empty($uniqueid)){
                if(!$this->User->hasAny(array('User.username'=>$uniqueid,'User.type'=>$type)))
                    return $this->_returnJson(false,'Unique id does not exist.');
                
                $this->User->contain();
                $user_id=$this->User->find('first',array('fields'=>array('User.id'),'conditions'=>array('User.username'=>$uniqueid)));
                $user['User']['language_id']=$language_id;
                $this->User->id=$user_id['User']['id'];
                if($this->User->save($user)){
                    return $this->_returnJson(true,'Language changed successfully!');
                }else{
                    return $this->_returnJson(false, 'Error, Something went wrong.');
                }
            }
        }
        else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
     }

    /**
    * Purchase book method.
    * 
    * @access public
    * 
    * @param $email
    * @param $uniqueid
    * @param $story_name
    * @param $language
    *  
    * In this API user will be able to add book to their account
    */
     
    public function purchase_book(){
        if($this->request->is('post')){
            extract($this->request->data);
            $email = isset($email)?$email:'';
            $story_name = isset($story_name)?$story_name:'';
            $language = isset($language)?$language:'';
            
            if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $email)))
                return $this->_returnJson(false, 'Email does not exist.');
            
                    if(empty($language))
                        return $this->_returnJson(false,'Please enter the book language');
                    
                    $language_id=$this->Language->getIdbyLanguageName($language);
                    
                    if($language_id==null)
                        return $this->_returnJson(false,'Please enter correct language');
                    
                    if(empty($story_name))
                        return $this->_returnJson(false,'Please enter the story name');
                    
                    $story2=$this->Story->find('first',array('fields'=>array('Story.id'),'conditions'=>array('story_name'=>$story_name,'language_id'=>$language_id)));
                    $story_id=$story2['Story']['id'];
                    if($story_id==null)
                        return $this->_returnJson(false,'Story does not exist.');
                    
                    $user1_id=$this->User->getUserIdbyEmail($email);
                    
                    $userpurchase=array();
                    $userpurchase['UserPurchase']['language_id']=$language_id;
                    $userpurchase['UserPurchase']['purchase_status']='1';
                    $userpurchase['UserPurchase']['story_id']=$story_id;
                    $userpurchase['UserPurchase']['user_relation_id']=$user1_id;
                    
                    $books_purchased= $this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Free')));
                   
					$books_purchased1= $this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Paid')));


                    if(!empty($books_purchased1))
                    {
					  	$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='Purchased';
	                    $this->Book->id=$books_purchased1['Book']['id'];
					} 
					elseif(!empty($books_purchased))
					{
						$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='FreePurchased';
	                    $this->Book->id=$books_purchased['Book']['id'];
					}      
					else if((empty($books_purchased)) && empty($books_purchased1))
					{ 
						$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='Purchased';
					}
					
					
                    $available_userpurchase=$this->UserPurchase->find('first',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id,'user_relation_id'=>$user1_id)));
                    if(empty($available_userpurchase))
                    {
	                    $this->UserPurchase->create();
	                    $this->UserPurchase->save($userpurchase);
	                    $this->Book->save($books);
	                    return $this->_returnJson(true, "Your Story '".$story_name."' has been added to your account.");
                    }
                    else
                    {
                    	$book2=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id)));
                    	if(!empty($book2))
                    	{
                    		$this->Book->id=$book2['Book']['id'];
                    		$this->Book->save($books);
                    	}
                    	else
                    	{
                    		$this->Book->create();
                    		$this->Book->save($books);
                    	}
 						return $this->_returnJson(true, "Your Story '".$story_name."' has been added to your account.");
                    } 
                	
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    } 
    
    

    /**
    * Purchase book method.
    * 
    * @access public
    * 
    * @param $email
    * @param $uniqueid
    * @param $story_name
    * @param $language
    *  
    * In this API user will be able to add book to their account
    */
     
    public function purchase_book2(){
        if($this->request->is('post')){
            extract($this->request->data);
            $email = isset($email)?$email:'';
            $story_name = isset($story_name)?$story_name:'';
            $language = isset($language)?$language:'';
            
            if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $email)))
                return $this->_returnJson(false, 'Email does not exist.');
            
                    if(empty($language))
                        return $this->_returnJson(false,'Please enter the book language');
                    
                    $language_id=$this->Language->getIdbyLanguageName($language);
                    
                    if($language_id==null)
                        return $this->_returnJson(false,'Please enter correct language');
                    
                    if(empty($story_name))
                        return $this->_returnJson(false,'Please enter the story name');
                    
                    $story2=$this->Story->find('first',array('fields'=>array('Story.id'),'conditions'=>array('story_name'=>$story_name,'language_id'=>$language_id)));
                    $story_id=$story2['Story']['id'];
                    if($story_id==null)
                        return $this->_returnJson(false,'Story does not exist.');
                    
                    $user1_id=$this->User->getUserIdbyEmail($email);
                    
                    $userpurchase=array();
                    $userpurchase['UserPurchase']['language_id']=$language_id;
                    $userpurchase['UserPurchase']['purchase_status']='1';
                    $userpurchase['UserPurchase']['story_id']=$story_id;
                    $userpurchase['UserPurchase']['user_relation_id']=$user1_id;
                    
                    $books_purchased= $this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Free')));
                   
					$books_purchased1= $this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Paid')));


                    if(!empty($books_purchased1))
                    {
					  	$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='Purchased';
	                    $this->Book->id=$books_purchased1['Book']['id'];
					} 
					elseif(!empty($books_purchased))
					{
						$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='FreePurchased';
	                    $this->Book->id=$books_purchased['Book']['id'];
					}      
					else if((empty($books_purchased)) && empty($books_purchased1))
					{ 
						$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='Purchased';
					}
					
					
                    $available_userpurchase=$this->UserPurchase->find('first',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id,'user_relation_id'=>$user1_id)));
                    if(empty($available_userpurchase))
                    {
           				$user1=$this->User->find('first',array('conditions'=>array('id'=>$user1_id)));
                    	$credits=$user1['User']['credits']-3;
                    	$this->User->id=$user1_id;
                    	$this->User->save(array('credits'=>$credits));
	                    $this->UserPurchase->create();
	                    $this->UserPurchase->save($userpurchase);
	                    $this->Book->save($books);
	                    return $this->_returnJson(true, "Your Story '".$story_name."' has been added to your account.");
                    }
                    else
                    {
                    	$user1=$this->User->find('first',array('conditions'=>array('id'=>$user1_id)));
                    	$credits=$user1['User']['credits']-3;
                    	$this->User->id=$user1_id;
                    	$this->User->save(array('credits'=>$credits));
                    	$book2=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id)));
                    	if(!empty($book2))
                    	{
                    		$this->Book->id=$book2['Book']['id'];
                    		$this->Book->save($books);
                    	}
                    	else
                    	{
                    		$this->Book->create();
                    		$this->Book->save($books);
                    	}
 						return $this->_returnJson(true, "Your Story '".$story_name."' has been added to your account.");
                    } 
                	
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    } 



    /**
    * Purchase book by credit method.
    * 
    * @access public
    * 
    * @param $email
    * @param $uniqueid
    * @param $story_name
    * @param $language
    *  
    * In this API user will be able to add book to their account
    */
     
    public function purchase_book_by_credit(){
        if($this->request->is('post')){
            extract($this->request->data);
            $email = isset($email)?$email:'';
            $story_name = isset($story_name)?$story_name:'';
            $language = isset($language)?$language:'';
            
            if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $email)))
                return $this->_returnJson(false, 'Email does not exist.');
            
 
                    if(empty($language))
                        return $this->_returnJson(false,'Please enter the book language');
                    
                    $language_id=$this->Language->getIdbyLanguageName($language);
                    
                    if($language_id==null)
                        return $this->_returnJson(false,'Please enter correct language');
                    
                    if(empty($story_name))
                        return $this->_returnJson(false,'Please enter the story name');
                    
                    $story2=$this->Story->find('first',array('fields'=>array('Story.id'),'conditions'=>array('story_name'=>$story_name,'language_id'=>$language_id)));
                    $story_id=$story2['Story']['id'];
                    if($story_id==null)
                        return $this->_returnJson(false,'Story does not exist.');
                    
                    $user1_id=$this->User->getUserIdbyEmail($email);
                    
                    $books_purchased= $this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id)));
                   	
					if(!empty($books_purchased))
                    {
					  	$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='FreePurchased';
	                    $this->Book->id=$books_purchased['Book']['id'];
	                    $this->Book->save($books);
	                    return $this->_returnJson(true, "Your Story '".$story_name."' has been added to your account.");
					} 
					else
					{
						$books['Book']['user_relation_id']=$user1_id;
	                    $books['Book']['story_id']=$story_id;
	                    $books['Book']['status']='FreePurchased';
	                    $this->Book->create();
	                    $this->Book->save($books);
	                    return $this->_returnJson(true, "Your Story '".$story_name."' has been added to your account.");
					}
                	
           
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    } 



    /**
    * Set book status method.
    * 
    * @access public
    * 
    * @param $email
    * @param $story_name
    * @param $status
    * @param $language
    *  
    * In this API books status will be added to database
    */
     
    public function SetBookStatus(){
        if($this->request->is('post')){
            extract($this->request->data);
            $email = isset($email)?$email:'';
            $story_name = isset($story_name)?$story_name:'';
            $language = isset($language)?$language:'';
            $status = isset($status)?$status:'';

            
            if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
            
            if (!$this->User->hasAny(array('User.email' => $email)))
                return $this->_returnJson(false, 'Email does not exist.');
            
 			if(empty($status))
                return $this->_returnJson(false, 'Please enter status.');


            if(empty($language))
                return $this->_returnJson(false,'Please enter the book language');
            
            $language_id=$this->Language->getIdbyLanguageName($language);
            
            if($language_id==null)
                return $this->_returnJson(false,'Please enter correct language');
            
            if(empty($story_name))
                return $this->_returnJson(false,'Please enter the story name');
            
            $story2=$this->Story->find('first',array('fields'=>array('Story.id'),'conditions'=>array('story_name'=>$story_name,'language_id'=>$language_id)));
            $story_id=$story2['Story']['id'];
            if($story_id==null)
                return $this->_returnJson(false,'Story does not exist.');
            
            $user1_id=$this->User->getUserIdbyEmail($email);
            
            $books_purchased= $this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id)));
           
            if(!empty($books_purchased))
            {
			  	$books['Book']['user_relation_id']=$user1_id;
                $books['Book']['story_id']=$story_id;
                $books['Book']['status']=$status;
                $this->Book->id=$books_purchased['Book']['id'];
                $this->Book->save($books);
                return $this->_returnJson(true, "Status of story has been successfully saved.");
			} 
			else
			{
				$books['Book']['user_relation_id']=$user1_id;
                $books['Book']['story_id']=$story_id;
                $books['Book']['status']=$status;
                $this->Book->create();
                $this->Book->save($books);
                return $this->_returnJson(true, "Status of story has been successfully saved.");
			}
                	
           
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    } 
    




 	public function get_purchased_story(){
 		$this->loadModel('StoryImage');
 		$this->loadModel('Story');
 		$this->loadModel('Language');
 		$this->loadModel('UserPurchase');
 		$this->loadModel('Mode');
        if($this->request->is('post')){
            extract($this->request->data);
            $language = isset($language)?$language:'';

            if(empty($language))
            {
                return $this->_returnJson(false, 'Please enter language_name.');
            }

            $purchase_stories=array();
     		$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];

		    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
		    $numbers=$this->Number->find('all',array('conditions'=>array('language_id'=>$language_id)));

 			$l1=sizeof($alphabets);
 			for($i=0;$i<$l1;$i++)
 			{
 				unset($alphabets[$i]['Alphabet']['language_id']);
 				$alphabets[$i]['Alphabet']['language_name']=$language;
 				$purchase_stories[0]['Alphabet'][$i]=$alphabets[$i]['Alphabet'];
 			}

 			$l3=sizeof($numbers);
 			for($i=0;$i<$l3;$i++)
 			{
 				$purchase_stories[0]['Number'][$i]=$numbers[$i]['Number'];
 			}
 			//general settings
 		
	        $id_array=array();
	        $settings1=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 1'))));
	        foreach($settings1 as $setting)
	        {
	        	array_push($id_array, $setting['GeneralSettings']['id']);
	        }
	        $settings2=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 2'))));
	        foreach($settings2 as $setting)
	        {
	        	array_push($id_array, $setting['GeneralSettings']['id']);
	        }
	        $settings3=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 3'))));
	     	foreach($settings3 as $setting)
	        {
	        	array_push($id_array, $setting['GeneralSettings']['id']);
	        }
	       	$arr=array();
	       
	        foreach($id_array as $id)
	        {
	        	$settings=$this->GeneralSettings->find('all',array('fields'=>array('title','language_id','string','audio_clip'),'conditions'=>array('id'=>$id)));
	        	$length4=sizeof($settings);
	 			for($i=0;$i<$length4;$i++)
	 			{
	    			array_push($arr, $settings[$i]['GeneralSettings']);
	 			}
	        }
	       
			$purchase_stories[0]['General_Settings']=$arr;
			$data=$purchase_stories;
    		return $this->_returnJson(true,$story,$data);
         		
         	
        }
    }




    public function StoryPlay3()
    {
	 	if($this->request->is('post')){
            extract($this->request->data);
            $story = isset($story)?$story:'';
            $language = isset($language)?$language:'';
            
            if(empty($story))
                return $this->_returnJson(false, 'Please enter story name.');

            if(empty($language))
                return $this->_returnJson(false, 'Please enter language_name.');

            $languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];
     		$stories=$this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id,'story_name'=>$story)));
    
			$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story,'language_id'=>$this->Language->getEnglishLanguage())));
			$english_story_id=$english_story['Story']['id'];

			foreach ($stories as $val) {
 				$story_name=$val['Story']['story_name'];
 				$story_id=$val['Story']['id'];
     		}

			if(empty($story_name))
         	{
         		echo 'Either Story_name or Language is Incorrect';
         	}
         	else
         	{
         		$purchase_stories=$this->Story->find('all', array(
			    'contain' => array(
			        'StoryImage' => array(
			            'order' => 'StoryImage.image_number ASC'
			        ),
			        'StoryStatement'),
			    'conditions'=>array('Story.id'=>$story_id)
			    ));

     			$words=$this->Word->find('all',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id)));
			    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
			    $numbers=$this->Number->find('all');
			
				
			    unset($purchase_stories[0]['Story']);
     			
     			foreach($purchase_stories[0]['StoryImage'] as $image)
     			{
     				unset($image['story_id']);
     				$image['story_name']=$purchase_stories[0]['Story']['story_name'];
     			}
     	
				$arr1=array();
     			foreach($purchase_stories[0]['StoryImage'] as $img1)
				{
					foreach($purchase_stories[0]['StoryStatement'] as $statement)
					{
					
						if($statement['story_image_id']==$img1['id'])
						{
							array_push($arr1,$statement);
							
						}
	
					}
				}
				
				unset($purchase_stories[0]['StoryStatement']);
				$purchase_stories[0]['StoryStatement']=$arr1;
				
     			foreach($purchase_stories[0]['StoryStatement'] as $statement)
     			{
					
     				unset($statement['story_id']);
     				unset($statement['story_image_id']);
     				$statement['story_name']=$purchase_stories[0]['Story']['story_name'];
     			}

				$l=sizeof($purchase_stories[0]['StoryStatement']);
				
     			for($i=0;$i<$l;$i++)
     			{
     				$stm=$purchase_stories[0]['StoryStatement'][$i]['statement'];
     				if(strpos($stm,"\n"))
     				{
     					$stm=str_replace("\n", "", $stm);
     		


     				}
     			
     				$purchase_stories[0]['StoryStatement'][$i]['statement']=$stm."*";
     				$purchase_stories[0]['StoryStatement'][$i]['audio_clip']=$purchase_stories[0]['StoryStatement'][$i]['audio_clip']."*";
     			}


     			$data=$purchase_stories;
         		return $this->_returnJson(true,$story,$data);
    
         	}


        }
        else
        {
        	return $this->_returnJson(false, 'Invalid Post Method.');
        }

    }



    public function StoryPlay2()
    {
	 	if($this->request->is('post')){
            extract($this->request->data);
            $story = isset($story)?$story:'';
            $language = isset($language)?$language:'';
            
            if(empty($story))
                return $this->_returnJson(false, 'Please enter story name.');

            if(empty($language))
                return $this->_returnJson(false, 'Please enter language_name.');

            $languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];
     		$stories=$this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id,'story_name'=>$story)));
    
			$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story,'language_id'=>$this->Language->getEnglishLanguage())));
			$english_story_id=$english_story['Story']['id'];

			foreach ($stories as $val) {
 				$story_name=$val['Story']['story_name'];
 				$story_id=$val['Story']['id'];
     		}

			if(empty($story_name))
         	{
         		echo 'Either Story_name or Language is Incorrect';
         	}
         	else
         	{
         		$purchase_stories=$this->Story->find('all', array(
			    'contain' => array(
			        'StoryImage' => array(
			            'order' => 'StoryImage.image_number ASC'
			        )),
			    'conditions'=>array('Story.id'=>$story_id)
			    ));

     			$words=$this->Word->find('all',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id)));
			    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
			    $numbers=$this->Number->find('all');
			
				
			    unset($purchase_stories[0]['Story']);
     			
     			foreach($purchase_stories[0]['StoryImage'] as $image)
     			{
     				unset($image['story_id']);
     				$image['story_name']=$purchase_stories[0]['Story']['story_name'];
     			}
     	
				// $arr1=array();
    //  			foreach($purchase_stories[0]['StoryImage'] as $img1)
				// {
				// 	foreach($purchase_stories[0]['StoryStatement'] as $statement)
				// 	{
					
				// 		if($statement['story_image_id']==$img1['id'])
				// 		{
				// 			array_push($arr1,$statement);
							
				// 		}
	
				// 	}
				// }
				
				// unset($purchase_stories[0]['StoryStatement']);
				// $purchase_stories[0]['StoryStatement']=$arr1;
				
    //  			foreach($purchase_stories[0]['StoryStatement'] as $statement)
    //  			{
					
    //  				unset($statement['story_id']);
    //  				unset($statement['story_image_id']);
    //  				$statement['story_name']=$purchase_stories[0]['Story']['story_name'];
    //  			}

				// $l=sizeof($purchase_stories[0]['StoryStatement']);
				
    //  			for($i=0;$i<$l;$i++)
    //  			{
    //  				$stm=$purchase_stories[0]['StoryStatement'][$i]['statement'];
    //  				if(strpos($stm,"\n"))
    //  				{
    //  					$stm=str_replace("\n", "", $stm);
    //  				}
     			
    //  				$purchase_stories[0]['StoryStatement'][$i]['statement']=$stm."*";
    //  				$purchase_stories[0]['StoryStatement'][$i]['audio_clip']=$purchase_stories[0]['StoryStatement'][$i]['audio_clip']."*";
    //  			}

     			$data=$purchase_stories;
        		return $this->_returnJson(true,$story,$data);
         	}


        }
        else
        {
        	return $this->_returnJson(false, 'Invalid Post Method.');
        }

    }





public function StoryPlay()
    {
	 	if($this->request->is('post')){
            extract($this->request->data);
            $story = isset($story)?$story:'';
            $language = isset($language)?$language:'';
            
            if(empty($story))
                return $this->_returnJson(false, 'Please enter story name.');

            if(empty($language))
                return $this->_returnJson(false, 'Please enter language_name.');

            $languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];
     		$stories=$this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id,'story_name'=>$story)));
    
			$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story,'language_id'=>$this->Language->getEnglishLanguage())));
			$english_story_id=$english_story['Story']['id'];

			foreach ($stories as $val) {
 				$story_name=$val['Story']['story_name'];
 				$story_id=$val['Story']['id'];
     		}

			if(empty($story_name))
         	{
         		echo 'Either Story_name or Language is Incorrect';
         	}
         	else
         	{
         		$purchase_stories=$this->Story->find('all', array(
			    'contain' => array(
			        'StoryImage' => array(
			            'order' => 'StoryImage.image_number ASC'
			        ),
			        'StoryStatement'),
			    'conditions'=>array('Story.id'=>$story_id)
			    ));

     			$words=$this->Word->find('all',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id)));
			    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
			    $numbers=$this->Number->find('all');
			
				
			    unset($purchase_stories[0]['Story']);
     			
     			foreach($purchase_stories[0]['StoryImage'] as $image)
     			{
     				unset($image['story_id']);
     				$image['story_name']=$purchase_stories[0]['Story']['story_name'];
     			}
     	
				$arr1=array();
     			foreach($purchase_stories[0]['StoryImage'] as $img1)
				{
					foreach($purchase_stories[0]['StoryStatement'] as $statement)
					{
					
						if($statement['story_image_id']==$img1['id'])
						{
							array_push($arr1,$statement);
							
						}
	
					}
				}
				
				unset($purchase_stories[0]['StoryStatement']);
				$purchase_stories[0]['StoryStatement']=$arr1;
				
     			foreach($purchase_stories[0]['StoryStatement'] as $statement)
     			{
					
     				unset($statement['story_id']);
     				unset($statement['story_image_id']);
     				$statement['story_name']=$purchase_stories[0]['Story']['story_name'];
     			}

				$l=sizeof($purchase_stories[0]['StoryStatement']);
				
     			for($i=0;$i<$l;$i++)
     			{
     				$stm=$purchase_stories[0]['StoryStatement'][$i]['statement'];
     				if(strpos($stm,"\n"))
     				{
     					$stm=str_replace("\n", "", $stm);
     				}
     			
     				$purchase_stories[0]['StoryStatement'][$i]['statement']=$stm."*";
     				$purchase_stories[0]['StoryStatement'][$i]['audio_clip']=$purchase_stories[0]['StoryStatement'][$i]['audio_clip']."*";
     			}

     			$data=$purchase_stories;
        		return $this->_returnJson(true,$story,$data);
         	}


        }
        else
        {
        	return $this->_returnJson(false, 'Invalid Post Method.');
        }

    }



     /**
 	* SpellGame Method 
 	* 
 	* To get all details of story images and its statements.
 	*/
    public function SpellGame()
    {
    	if($this->request->is('post')){
            extract($this->request->data);
            $story = isset($story)?$story:'';
            $language = isset($language)?$language:'';
            
            if(empty($story))
                return $this->_returnJson(false, 'Please enter story name.');

            if(empty($language))
                return $this->_returnJson(false, 'Please enter language_name.');

            $languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];
     	
			$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story,'language_id'=>$this->Language->getEnglishLanguage())));
			$story_id=$english_story['Story']['id'];
			$story_name=$english_story['Story']['story_name'];


			if(empty($story_name))
         	{
         		echo 'Either Story_name or Language is Incorrect';
         	}
         	else
         	{
         		$purchase_stories=$this->Story->find('all', array(
			    'contain' => array(
			        'StoryStatement' => array(
			            'order' => 'StoryStatement.story_image_id ASC'
			        ),
			        'StoryImage'),
			    'conditions'=>array('Story.id'=>$story_id)
			    ));

     			$words=$this->Word->find('all',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id)));
			    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
			    $numbers=$this->Number->find('all');
				
			    unset($purchase_stories[0]['Story']);
			    unset($purchase_stories[0]['StoryImage']);
			    unset($purchase_stories[0]['StoryStatement']);

     			$l2=sizeof($words);
     			for($i=0;$i<$l2;$i++)
     			{
     				unset($words[$i]['Word']['language_id']);
     				$words[$i]['Word']['language_name']=$language;
     				$purchase_stories[0]['Word'][$i]=$words[$i]['Word'];
     			}

     			//general settings
         		
		        $id_array=array();
		        $settings1=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 1'))));
		        foreach($settings1 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		        $settings2=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 2'))));
		        foreach($settings2 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		        $settings3=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 3'))));
		     	foreach($settings3 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		       	$arr=array();
		       
		        foreach($id_array as $id)
		        {
		        	$settings=$this->GeneralSettings->find('all',array('fields'=>array('title','language_id','string','audio_clip'),'conditions'=>array('id'=>$id)));
		        	$length4=sizeof($settings);
		 			for($i=0;$i<$length4;$i++)
		 			{
		    			array_push($arr, $settings[$i]['GeneralSettings']);
		 			}
		        }
			       
  				$purchase_stories[0]['General_Settings']=$arr;

     			$data=$purchase_stories;
        		return $this->_returnJson(true,$story,$data);


         	}


        }
        else
        {
        	return $this->_returnJson(false, 'Invalid Post Method.');
        }
    }


    /**
 	* QuestionGame Method 
 	* 
 	* To get all details of story images and its statements.
 	*/
    public function QuestionGame()
    {
    	if($this->request->is('post')){
            extract($this->request->data);
            $story = isset($story)?$story:'';
            $language = isset($language)?$language:'';
            
            if(empty($story))
                return $this->_returnJson(false, 'Please enter story name.');

            if(empty($language))
                return $this->_returnJson(false, 'Please enter language_name.');

            $languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];
			$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story,'language_id'=>$this->Language->getEnglishLanguage())));
			$story_id=$english_story['Story']['id'];
			$story_name=$english_story['Story']['story_name'];

			if(empty($story_name))
         	{
         		echo 'Either Story_name or Language is Incorrect';
         	}
         	else
         	{
         		$purchase_stories=$this->Story->find('all', array(
			    'contain' => array(
			        'StoryStatement' => array(
			            'order' => 'StoryStatement.story_image_id ASC'
			        ),
			        'StoryImage','Question'),
			    'conditions'=>array('Story.id'=>$story_id)
			    ));

     			$words=$this->Word->find('all',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id)));
			    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
			    $numbers=$this->Number->find('all');
				
			    unset($purchase_stories[0]['Story']);
			    unset($purchase_stories[0]['StoryImage']);
			    unset($purchase_stories[0]['StoryStatement']);
     			
     			//general settings
         		
		        $id_array=array();
		        $settings1=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 1'))));
		        foreach($settings1 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		        $settings2=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 2'))));
		        foreach($settings2 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		        $settings3=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 3'))));
		     	foreach($settings3 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		       	$arr=array();
		       
		        foreach($id_array as $id)
		        {
		        	$settings=$this->GeneralSettings->find('all',array('fields'=>array('title','language_id','string','audio_clip'),'conditions'=>array('id'=>$id)));
		        	$length4=sizeof($settings);
		 			for($i=0;$i<$length4;$i++)
		 			{
		    			array_push($arr, $settings[$i]['GeneralSettings']);
		 			}
		        }
		       
  				$purchase_stories[0]['General_Settings']=$arr;

     			$data=$purchase_stories;
        		return $this->_returnJson(true,$story,$data);


         	}


        }
        else
        {
        	return $this->_returnJson(false, 'Invalid Post Method.');
        }

    }


    /**
 	* MathsGame Method 
 	* 
 	* To get all details of story images and its statements.
 	*/
    public function MathsGame()
    {
    	if($this->request->is('post')){
            extract($this->request->data);
            $story = isset($story)?$story:'';
            $language = isset($language)?$language:'';
            
            if(empty($story))
                return $this->_returnJson(false, 'Please enter story name.');

            if(empty($language))
                return $this->_returnJson(false, 'Please enter language_name.');

            $languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
     		$language_id=$languages['Language']['id'];
     		$stories=$this->Story->find('all',array('conditions'=>array('Story.language_id'=>$language_id,'story_name'=>$story)));
     		
			$english_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story,'language_id'=>$this->Language->getEnglishLanguage())));
			$story_id=$english_story['Story']['id'];
			$story_name=$english_story['Story']['story_name'];
			$scene_array=array();
			$scenes=$this->Scene->find('all',array('conditions'=>array('story_id'=>$story_id)));
			foreach($scenes as $scene)
			{
				array_push($scene_array, $scene['Scene']['id']);
			}
			$cnt1=0;
			$arr3=array();
			foreach($scene_array as $scene1)
			{
				$grids=$this->Grids->find('all',array('fields'=>array('scene_id','x','y','src','row','col'),'conditions'=>array('scene_id'=>$scene1,'src !='=>'')));
				$total_objects=sizeof($grids);
				$grid_array[$cnt1]=array();
				$arr2=array();
				foreach($grids as $grid)
				{
					$scene1=$this->Scene->find('first',array('conditions'=>array('id'=>$grid['Grids']['scene_id'])));
					$scene_image=$scene1['Scene']['image'];
					$variant_img=$grid['Grids']['src'];
					$variant=$this->Variant->find('first',array('conditions'=>array('image'=>$variant_img)));
					$object_id=$variant['Variant']['object_id'];
					$objects=$this->MathObject->find('first',array('conditions'=>array('id'=>$object_id)));
					$object_name=$objects['MathObject']['title'];
					$object_audio=$objects['MathObject']['audio_clip'];
					$grid_size=$grid['Grids']['row'] .'*'.$grid['Grids']['col'];
					$arr1=array('variant_image'=>$grid['Grids']['src'],'x'=>$grid['Grids']['x'],'y'=>$grid['Grids']['y']);
					array_push($arr2,$arr1);
				}
				$grid_array[$cnt1]=array('scene_image'=>$scene_image,'object_name'=>$object_name,'object_audio'=>$object_audio,'total_objects'=>$total_objects,'grid_size'=>$grid_size);
				$grid_array[$cnt1]+=$arr2;
				$cnt1++;
			}
			

			if(empty($story_name))
         	{
         		echo 'Either Story_name or Language is Incorrect';
         	}
         	else
         	{
         		$purchase_stories=$this->Story->find('all', array(
			    'contain' => array(
			        'StoryStatement' => array(
			            'order' => 'StoryStatement.story_image_id ASC'
			        ),
			        'StoryImage'),
			    'conditions'=>array('Story.id'=>$story_id)
			    ));

     			$words=$this->Word->find('all',array('conditions'=>array('language_id'=>$language_id,'story_id'=>$story_id)));
			    $alphabets=$this->Alphabet->find('all',array('conditions'=>array('language_id'=>$language_id)));
			    $numbers=$this->Number->find('all');
				
			    unset($purchase_stories[0]['Story']);
			    unset($purchase_stories[0]['StoryImage']);
			    unset($purchase_stories[0]['StoryStatement']);

			    $l4=sizeof($grid_array);
     			for($i=0;$i<$l4;$i++)
     			{
     				$purchase_stories[0]['Maths'][$i]=$grid_array[$i];
     			}

     			//general settings
         		
		        $id_array=array();
		        $settings1=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 1'))));
		        foreach($settings1 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		        $settings2=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 2'))));
		        foreach($settings2 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		        $settings3=$this->GeneralSettings->find('all',array('conditions'=>array('language_id'=>$language_id,'or'=>array('GeneralSettings.title'=>'Level 3'))));
		     	foreach($settings3 as $setting)
		        {
		        	array_push($id_array, $setting['GeneralSettings']['id']);
		        }
		       	$arr=array();
		       
		        foreach($id_array as $id)
		        {
		        	$settings=$this->GeneralSettings->find('all',array('fields'=>array('title','language_id','string','audio_clip'),'conditions'=>array('id'=>$id)));
		        	$length4=sizeof($settings);
		 			for($i=0;$i<$length4;$i++)
		 			{
		    			array_push($arr, $settings[$i]['GeneralSettings']);
		 			}
		        }
		       
  				$purchase_stories[0]['General_Settings']=$arr;
     			
     			
     			$data=$purchase_stories;
        		return $this->_returnJson(true,$story,$data);


         	}


        }
        else
        {
        	return $this->_returnJson(false, 'Invalid Post Method.');
        }
    }


    /**
     * User Story level method.
     * 
     * @access public
     * 
     * @param $email
     * @param $uniqueid
     * @param $story_name
     * @param $mode
     * @param $status
     *  
     * This API will help us to know which child or student has played or is playing which story, language, level and mode
     */
    
     
    public function user_story_level(){
        if($this->request->is('post')){
            extract($this->request->data);
            $email = isset($email)?$email:'';
            $uniqueid = isset($uniqueid)?$uniqueid:'';
            $language = isset($language)?$language:'';
            $level = isset($level)?$level:'';
            $story_name = isset($story_name)?$story_name:'';
            $mode = isset($mode)?$mode:'';
            $status = isset($status)?$status:'';
            
            if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
        
            if (!$this->User->hasAny(array('User.email' => $email)))
                return $this->_returnJson(false, 'Email does not exist.');
            
            if(empty($uniqueid))
                return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid)))
                return $this->_returnJson(false,'Unique id does not exist.');
            
            $story_id=$this->Story->getStoryIdByStoryName($story_name);
            
            if($story_id==null)
                return $this->_returnJson(false,'Please enter correct story name');
            
            if(empty($mode))
                return $this->_returnJson(false,'Mode cannot be left blank.');
            
            // Teacher.id indicates User Relation id here
            $user_details=$this->User->find('first',array('fields'=>array('id','level_id','language_id'),'conditions'=>array('User.username'=>$uniqueid),'contain'=>array('Teacher.id','Parent.id')));
            
            $mode_id=$this->Mode->getIdbyModeName($mode);
            if(empty($mode_id))
                return $this->_returnJson(false,'Please enter correct mode.');
            
            if(empty($status))
                return $this->_returnJson(false,'Status can not be blank.');
            
            if($status!='played' && $status != 'playing')
                return $this->_returnJson(false,'Please enter the correct Status: played or playing.');
                
            $userstorylevels=array();
            
            if(!empty($user_details['Teacher']['id']))
            {
            	$userstorylevels['UserStoryLevel']['user_relation_id']=$user_details['Teacher']['id'];
            	$usid=$user_details['Teacher']['id'];
            }
            else
            {
            	$userstorylevels['UserStoryLevel']['user_relation_id']=$user_details['Parent']['id'];
            	$usid=$user_details['Parent']['id'];
        	}
            $userstorylevels['UserStoryLevel']['user_id']=$user_details['User']['id'];
            $userstorylevels['UserStoryLevel']['level_id']=$user_details['User']['level_id'];
            $userstorylevels['UserStoryLevel']['language_id']=$user_details['User']['language_id'];
            $userstorylevels['UserStoryLevel']['mode_id']=$mode_id;
            $userstorylevels['UserStoryLevel']['story_id']=$story_id;
            $userstorylevels['UserStoryLevel']['status']=$status;
            
            $user_story_level_exist=$this->UserStoryLevel->find('first',array('conditions'=>array(
                'UserStoryLevel.user_relation_id'=>$usid, 
                'UserStoryLevel.user_id'=>$user_details['User']['id'],
                'UserStoryLevel.level_id'=>$user_details['User']['level_id'],
                'UserStoryLevel.language_id'=>$user_details['User']['language_id'],
                'UserStoryLevel.story_id'=>$story_id,
                'UserStoryLevel.mode_id'=>$mode_id)));
                
            if(empty($user_story_level_exist)){
                $this->UserStoryLevel->create();
                if($this->UserStoryLevel->save($userstorylevels)){
                    $data=array('user_story_level_id'=>$this->UserStoryLevel->getLastInsertId());
                    return $this->_returnJson(true,'User story level mode saved successfully.',$data);
                }else{
                    return $this->_returnJson(false,'Error. Something went wrong.');
                }
            }else{
                // Update status to played or playing if same request is sent
                $this->UserStoryLevel->contain();
                $this->UserStoryLevel->id=$user_story_level_exist['UserStoryLevel']['id'];
                $update['UserStoryLevel']['status']=$status;
                $this->UserStoryLevel->save($update);
                $data=array('user_story_level_id'=>$user_story_level_exist['UserStoryLevel']['id']);
                return $this->_returnJson(true,'User story level mode saved successfully.',$data);
            }
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    }
   
    /**
     * Results method.
     * 
     * @access public
     * 
     * @param $user_story_level_id
     * @param $item : Flower/Daddy
     * @param $answer
     * @param $asked
     * @param $reply
     * @param $result
     * This API will help us to know which child or student attempted which story level, mode, item question and what answer student/child replied.
     */
     
     public function results(){
         if($this->request->is('post')){
             extract($this->request->data);
             $user_story_level_id = isset($user_story_level_id)?$user_story_level_id:'';
             $item = isset($item)?$item:'';
             $answer = isset($answer)?$answer:'';
             $asked = isset($asked)?$asked:'';
             $reply = isset($reply)?$reply:'';
			 $result = isset($result)?$result:'';
			 
             if(empty($user_story_level_id))
                return $this->_returnJson(false,'Please enter user_story_level_id which was sent after saving user story level.');
                
             if(!$this->UserStoryLevel->hasAny(array('UserStoryLevel.id'=>$user_story_level_id)))
                return $this->_returnJson(false,'user_story_level_id does not exist.');
                
             if(empty($item))
                return $this->_returnJson(false,'Please enter item.');
                
             if(empty($answer))
                return $this->_returnJson(false,'Please enter answer.');
             
             if(empty($asked))
                return $this->_returnJson(false,'Please enter the question asked.');
             
             if(empty($reply))
                return $this->_returnJson(false,'Please enter the answer you replied.');
             
			 if(empty($result))
                return $this->_returnJson(false,'Please enter the result is right or wrong.');
             
             $results=array('Result'=>array(
                'user_story_level_id'=>$user_story_level_id,
                'item'=>$item,
                'answer'=>$answer,
                'asked'=>$asked,
                'reply'=>$reply,
				'result'=> $result));
             
             $this->Result->create();
             if($this->Result->save($results)){
                 return $this->_returnJson(true, "Results saved.");
             }else{
                 return $this->_returnJson(false, "Error. Something went wrong.");
             }
             
             
          }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
     }

    /**
     * User Mode Status method.
     * 
     * @access public
     * 
     * @param $uniqueid
     * @param $email
     * @param $story_name
     * @param $mode_question_answer_id
     * @param $answer
     *  
     * This API will help us to know which child or student attempted how many times same question and what answer was given for each attempt.
     */
     
    
    public function user_mode_status(){
        if($this->request->is('post')){
            extract($this->request->data);
			$email = isset($email)?$email:'';
			$uniqueid = isset($uniqueid)?$uniqueid:'';
			$story_name = isset($story_name)?$story_name:'';
			$mode_question_answer_id = isset($mode_question_answer_id)?$mode_question_answer_id:'';
			$answer = isset($answer)?$answer:'';
			
            if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
        
            if (!$this->User->hasAny(array('User.email' => $email)))
                return $this->_returnJson(false, 'Email does not exist.');
            
            if(empty($uniqueid))
                return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid)))
                return $this->_returnJson(false,'Unique id does not exist.');
            
            if(empty($story_name))
                return $this->_returnJson(false,'Story name cannot be left blank.');
            
            $story_id=$this->Story->getStoryIdByStoryName($story_name);
            
            if($story_id==null)
                return $this->_returnJson(false,'Please enter correct story name.');
            
            if(empty($mode_question_answer_id))
                return $this->_returnJson(false,'Please enter mode_question_answer_id.');
                
            if(!$this->ModeQuestionAnswer->hasAny(array('ModeQuestionAnswer.id'=>$mode_question_answer_id)))
                return $this->_returnJson(false,'mode_question_answer_id does not exist.');
            
            if(empty($answer))
                return $this->_returnJson(false, 'Please provide answer to the question.');
            
            // Teacher.id indicates User Relation id here
            $user_details=$this->User->find('first',array('fields'=>array('id','level_id','language_id'),'conditions'=>array('User.username'=>$uniqueid),'contain'=>array('Teacher.id')));
            
            $user_mode_status=array();
            $user_mode_status['UserModeStatus']['user_relation_id']=$user_details['Teacher']['id'];
            $user_mode_status['UserModeStatus']['language_id']=$user_details['User']['language_id'];
            $user_mode_status['UserModeStatus']['level_id']=$user_details['User']['level_id'];
            $user_mode_status['UserModeStatus']['user_id']=$user_details['User']['id'];
            $user_mode_status['UserModeStatus']['story_id']=$story_id;
            $user_mode_status['UserModeStatus']['mode_question_answer_id']=$mode_question_answer_id;
            $user_mode_status['UserModeStatus']['answer']=$answer;
            
            
            // Calculate total attempt 
            $user_attempt=1;
            $checkusermode=array('UserModeStatus.user_relation_id'=>$user_details['Teacher']['id'],
                                                   'UserModeStatus.language_id'=>$user_details['User']['language_id'],
                                                   'UserModeStatus.level_id'=>$user_details['User']['level_id'],
                                                   'UserModeStatus.user_id'=>$user_details['User']['id'],
                                                   'UserModeStatus.story_id'=>$story_id,
                                                   'UserModeStatus.mode_question_answer_id'=>$mode_question_answer_id,
                                                   'UserModeStatus.answer'=>$answer
                                                   );
            
            
            if($this->UserModeStatus->hasAny($checkusermode)){
                   $data=$this->UserModeStatus->find('first',array('conditions'=>$checkusermode,'fields'=>array('id','user_attempt')));
                   $user_attempt=$data['UserModeStatus']['user_attempt']+1;
                   $update['UserModeStatus']['user_attempt']=$user_attempt;
                   $this->UserModeStatus->id=$data['UserModeStatus']['id'];
                   $this->UserModeStatus->save($update);
                   return $this->_returnJson(true, "User Mode updated successfully.");
            }

            $user_mode_status['UserModeStatus']['user_attempt']=$user_attempt;
            $this->UserModeStatus->create();
            if($this->UserModeStatus->save($user_mode_status)){
                return $this->_returnJson(true, "User Mode status saved successfully.");
            }
            
            
        }else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
    }

	/**
     * Mode Question Answer method.
     * 
     * @access public
     * 
     *  
     * This API will return list of mode question answers.
     */

	public function mode_question_answer(){
		if($this->request->is('post')){
			extract($this->request->data);
			$mode_name = isset($mode_name)?$mode_name:'';
			$story_name = isset($story_name)?$story_name:'';
			
			if(empty($mode_name))
                return $this->_returnJson(false,'Mode name cannot be left blank.');
			
			if(empty($story_name))
                return $this->_returnJson(false,'Story name cannot be left blank.');
			
			$mode_id=$this->Mode->getIdbyModeName($mode_name);
			if($mode_id==null)
				return $this->_returnJson(false,'Mode name is incorrect.');
			$story_id=$this->Story->getStoryIdByStoryName($story_name);
			if($story_id==null)
				return $this->_returnJson(false,'Story name is incorrect.');
			
			$mode_question_answers=$this->ModeQuestionAnswer->find('all',array('conditions'=>array('mode_id'=>$mode_id,'story_id'=>$story_id),'fields'=> array('id','mode_question','mode_answer'),
					'contain'=>array(
						'Mode.mode_name',
						'Story.story_name',
						'Language.language_name')));
			
			$data=array();
			foreach($mode_question_answers as $mqa){
				$data['ModeQuestionAnswer'][]=array('mode_question_answer_id'=>$mqa['ModeQuestionAnswer']['id'],'mode_question'=>$mqa['ModeQuestionAnswer']['mode_question'],'mode_answer'=>$mqa['ModeQuestionAnswer']['mode_answer'],'mode_name'=>$mqa['Mode']['mode_name'],'story_name'=>$mqa['Story']['story_name'],'language_name'=>$mqa['Language']['language_name']);
			}
			return $this->_returnJson(true, "Mode Question Answers",$data);
		}else{
			
			$mode_question_answers=$this->ModeQuestionAnswer->find('all',array('fields'=> array('id','mode_question','mode_answer'),
					'contain'=>array(
						'Mode.mode_name' ,
						'Story.story_name',
						'Language.language_name')));
			$data=array();
			foreach($mode_question_answers as $mqa){
				$data['ModeQuestionAnswer'][]=array('mode_question_answer_id'=>$mqa['ModeQuestionAnswer']['id'],'mode_question'=>$mqa['ModeQuestionAnswer']['mode_question'],'mode_answer'=>$mqa['ModeQuestionAnswer']['mode_answer'],'mode_name'=>$mqa['Mode']['mode_name'],'story_name'=>$mqa['Story']['story_name'],'language_name'=>$mqa['Language']['language_name']);
			}
			
			return $this->_returnJson(true, "Mode Question Answers",$data);
        }
	}

	public function get_number_of_users(){
		$users=array('users'=>$this->User->find('count'));
		return $this->_returnJson(true, "Number of users globally",$users);
	}

	/**
     * Storywise performance method
     * 
     * @access public
     * 
     * @param $uniqueid
	 * @param $story_name
	 * @param $level
	 * @param $status
	 *  
     * This API will get storywise performance
     */

	public function storywise_performance(){
		if($this->request->is('post')){
			extract($this->request->data);
			$uniqueid=isset($uniqueid)?$uniqueid:'';
			$story_name=isset($story_name)?$story_name:'';
			$level=isset($level)?$level:'';
			$status=isset($status)?$status:'';
			
			if(empty($uniqueid))
                return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid)))
                return $this->_returnJson(false,'Unique id does not exist.');
			
			if(empty($story_name))
                return $this->_returnJson(false,'Story name cannot be left blank.');
            
            $story_id=$this->Story->getStoryIdByStoryName($story_name);
            
            if($story_id==null)
                return $this->_returnJson(false,'Please enter correct story name.');
			
			if(empty($level))
                return $this->_returnJson(false,'Level cannot be left blank.');
			
			$level_id=$this->Level->getIdbyLevelName($level);
			
			if($level_id==null)
                return $this->_returnJson(false,'Please enter correct level.');
			
			if(empty($status))
				return $this->_returnJson(false,'Please enter status.');
			
			if($status!='played' && $status!='playing')
				return $this->_returnJson(false,'Please enter status played or playing.');
			
			$user_details=$this->User->getUserData(array('User.username'=>$uniqueid),array('User.id','User.language_id'),array('Teacher.id'));
			
			$user_story_level=$this->UserStoryLevel->find('all',array('conditions'=>array('user_relation_id'=>$user_details['Teacher']['id'],'user_id'=>$user_details['User']['id'],'UserStoryLevel.language_id'=>$user_details['User']['language_id'],'UserStoryLevel.status'=>$status),
			'contain'=>array('Result'=>array('fields'=>array('id','item','answer','asked','reply','result')),'Mode.mode_name')));
			
			$data=array();
			foreach($user_story_level as $usl){
				if(!empty($usl['Result']))
				$data[$usl['Mode']['mode_name']]=$usl['Result'];
			}
			return $this->_returnJson(true, "Storywise Performance",$data);
			
		}else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
	}


	
	/**
     * Add Coupon code method
     * 
     * @access public
     * 
     * @param $discount
	 * @param $story_name
	 * @param $coupon
	 *  
     * This API will be used to apply coupon code for parents
	 * Note: coupons are Not for teachers. 
     */


	public function AddCoupon(){
		if($this->request->is('post')){
			extract($this->request->data);
			$discount=isset($discount)?$discount:'';
			$story_name=isset($story_name)?$story_name:'';
			$coupon=isset($coupon)?$coupon:'';
			$start_date=isset($start_date)?$start_date:'';
			$end_date=isset($end_date)?$end_date:'';
			
			if(empty($discount))
                return $this->_returnJson(false, 'Please enter discount in (%).');
			
			if(empty($story_name))
                return $this->_returnJson(false,'Story name cannot be left blank.');

            if(empty($start_date))
                return $this->_returnJson(false,'Start date cannot be left blank.');

            if(empty($end_date))
                return $this->_returnJson(false,'End date cannot be left blank.');
            
            $story_id=$this->Story->getStoryIdByStoryName($story_name);
            
            if($story_id==null)
                return $this->_returnJson(false,'Please enter correct story name.');
			
			if(empty($coupon))
                return $this->_returnJson(false,'Coupon cannot be left blank.');
			
            $timestamp1 = strtotime($start_date);
	    	$timestamp2 = strtotime($end_date);
			
			if($timestamp2>$timestamp1)
			{
				$this->Coupon->create();
				$this->Coupon->save(array('story_id'=>$story_id,'code'=>$coupon,'discount'=>$discount,'start_date'=>$start_date,'end_date'=>$end_date));
				return $this->_returnJson(true,'Coupon successfully stored.');
			}
			else
			{
				return $this->_returnJson(false, "End_date should be greater than start_date.");
			}
			
		}else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
	}




	/**
     * Apply Coupon code method
     * 
     * @access public
     * 
     * @param $email
	 * @param $story_name
	 * @param $coupon
	 *  
     * This API will be used to apply coupon code for parents
	 * Note: coupons are Not for teachers. 
     */


	public function apply_coupon_code(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
			$story_name=isset($story_name)?$story_name:'';
			$coupon=isset($coupon)?$coupon:'';
			
			if(empty($email))
                return $this->_returnJson(false, 'Please enter email address.');
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                return $this->_returnJson(false, 'Please enter a valid email address.');
        
            if (!$this->User->hasAny(array('User.email' => $email,'User.type <>'=>'teacher')))
                return $this->_returnJson(false, 'Email does not exist.');
			
			if(empty($story_name))
                return $this->_returnJson(false,'Story name cannot be left blank.');
            
            $story_id=$this->Story->getStoryIdByStoryName($story_name);
            
            if($story_id==null)
                return $this->_returnJson(false,'Please enter correct story name.');
			
			if(empty($coupon))
                return $this->_returnJson(false,'Coupon cannot be left blank.');
			
			$conditions=array('Coupon.story_id'=>$story_id,'Coupon.code'=>$coupon);
			
			if(!$this->Coupon->hasAny($conditions))
				return $this->_returnJson(false,'Invalid Coupon.');
			
			 
	 		$coupon_details=$this->Coupon->find('first',array('conditions'=>$conditions));
			 
		 	$current_date=date('Y-m-d',time());
	 		if($current_date > $coupon_details['Coupon']['end_date']){
		 		return $this->_returnJson(false,'Coupon expired. Please try another coupon.');
		 	}
			 
		 	$user_id=$this->User->getUserIdbyEmail($email);
			 
		 	if($this->UserCoupon->hasAny(array('UserCoupon.user_id'=>$user_id,'UserCoupon.coupon_id'=>$coupon_details['Coupon']['id']))){
			 	return $this->_returnJson(false,'Coupon already used.');
		 	}
			
		 	$user_coupon=array('UserCoupon'=>array('user_id'=>$user_id,'coupon_id'=>$coupon_details['Coupon']['id']));
			
		 	if($this->UserCoupon->save($user_coupon)){
			 	$data['Coupon']=array('code'=>$coupon_details['Coupon']['code'],'discount'=>$coupon_details['Coupon']['discount'],'start_date'=>$coupon_details['Coupon']['start_date'],'end_date'=>$coupon_details['Coupon']['end_date']);
			 	return $this->_returnJson(true,'Coupon has been applied.',$data);
		 	}
		
		}else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
	}
	
	
	/**
     * Add Credits method
     * 
     * @access public
     *
	 * @param $email 
	 * @param $uniqueid
     * @param $credits
     * 
	 *  This API will be used to add credits to user account.
     */
	public function add_credits(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
			$credits=isset($credits)?$credits:'';
			$user_data=array();
			
			if(empty($email) )
				return $this->_returnJson(false, 'Please enter email address.');
			
			if(!empty($email)){
				if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                	return $this->_returnJson(false, 'Please enter a valid email address.');
				
            	if (!$this->User->hasAny(array('User.email' => $email)))
                	return $this->_returnJson(false, 'Email does not exist.');
				
				$user_data=$this->User->getUserData(array('User.email'=>$email),array('User.id','User.credits'));
			}
			
			if($credits=='')
				return $this->_returnJson(false, 'Please enter credits.');
			
			$user=array('User'=>array('credits'=>($credits)));
			
			$this->User->id=$user_data['User']['id'];
			
			if($this->User->save($user)){
				return $this->_returnJson(true, "Credits added successfully.",$user);
			}else{
				return $this->_returnJson(false, "Error, something went wrong.");
			}
			
		}else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
	}

	/**
     * Update Credits method
     * 
     * @access public
     *
	 * @param $email 
	 * @param $uniqueid
     * @param $credits
     * 
	 *  This API will be used to update credits to user account.
     */

	public function update_credits(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
			$credits=isset($credits)?$credits:'';
			$user_data=array();
			
			if(empty($email))
				return $this->_returnJson(false, 'Please enter unique id or email address.');
			
			if(!empty($email)){
				if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                	return $this->_returnJson(false, 'Please enter a valid email address.');
				
            	if (!$this->User->hasAny(array('User.email' => $email)))
                	return $this->_returnJson(false, 'Email does not exist.');
				
				$user_data=$this->User->getUserData(array('User.email'=>$email),array('User.id'));
			}
			
			if(empty($credits))
				return $this->_returnJson(false, 'Please enter credits.');
			
			if(!is_numeric($credits))
				return $this->_returnJson(false, 'Please enter digits.');
			
			$user=array('User'=>array('credits'=>($credits)));
			$this->User->id=$user_data['User']['id'];
			
			if($this->User->save($user)){
				return $this->_returnJson(true, "Credits updated successfully.",$user);
			}else{
				return $this->_returnJson(false, "Error, something went wrong.");
			}
			
		}else{
            return $this->_returnJson(false, "Invalid Post Request.");
        }
	}
	
	/**
     * Get Credits method
     * 
     * @access public
     *
	 * @param $email 
	 * @param $uniqueid
     * 
	 *  This API will get credits of user by email id or unique id
     */
	
	public function get_credits(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
            $user_data=array();
			
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email address.');
			
			if(!empty($email)){
				if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                	return $this->_returnJson(false, 'Please enter a valid email address.');
				
            	if (!$this->User->hasAny(array('User.email' => $email)))
                	return $this->_returnJson(false, 'Email does not exist.');
				
				$user_data=$this->User->getUserData(array('User.email'=>$email),array('User.credits'));
			}
            
			
			if(!empty($user_data)){
				return $this->_returnJson(true, "User has ".$user_data['User']['credits']." credits available",$user_data);
			}else{
				return $this->_returnJson(true, "Error, something went wrong.");
			}
			
			
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}


	/**
 	* Get Popular Books method
 	* 
 	* @access public
 	*/
	
	public function GetPopularBooks(){
		           
		$conditions=array('status'=>'Purchased');
	 	$fields = array('Book.story_id','COUNT(`Book`.`story_id`) AS count');
		$books = $this->Book->find('all',array(
			'fields'=>$fields,
		    'conditions'=>$conditions,
		    'recursive'=>0,
		    'group' => 'Book.story_id',
		    'order' => 'count DESC',
		    'limit' => 3
		));
		$story_array['Popular_books']=array();
		foreach($books as $book)
		{
			$story_id=$book['Book']['story_id'];
			$book_name=$this->Story->find('first',array('conditions'=>array('id'=>$story_id)));
			array_push($story_array['Popular_books'],$book_name['Story']['story_name']);
		}
		return $this->_returnJson(true, $story_array);
						
	}




	/**
     * Get Free Books method
     * 
     * @access public
     */
	
	public function GetFreeBooks(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');
			
			if(!empty($email))
			{
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);
		         
					$free_books=array();
					$books=$this->Book->find('all',array('conditions'=>array('user_relation_id'=>$userid)));
					foreach($books as $book)
					{
						if($book['Book']['status']=='Purchased')
						{
							$stories=$this->Story->find('first',array('conditions'=>array('Story.id'=>$book['Book']['story_id'])));
							if(!in_array($stories['Story']['story_name'], $free_books))
							{
								array_push($free_books,$stories['Story']['story_name']);
							}
						}
					}
					return $this->_returnJson(true, $free_books);
				}	
			}
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}


	/**
     * Get Unloaded Books method
     * 
     * @access public
     */
	public function GetUnloadedBooks(){
		if($this->request->is('post')){
			extract($this->request->data);
			$username=isset($username)?$username:'';
          
			if(empty($username))
				return $this->_returnJson(false, 'Please enter username');
			
			if(!empty($username)){
				if(!$this->User->hasAny(array('User.username'=>$username)))
				{
              		return $this->_returnJson(false,'Username does not exist.');
				}
				else
				{
					$stories=$this->Story->find('all',array('fields'=>'Story.story_name'));
					return $this->_returnJson(true, $stories);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}



	/**
     * Count Purchased Books method
     * 
     * @access public
     */
	
	public function count_purchased_books(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');
			
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);
					$books=$this->Book->find('count',array('conditions'=>array('user_relation_id'=>$userid,'status'=>'Purchased')));
					return $this->_returnJson(true, $books);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}



	/**
     * Books To Share method
     * 
     * @access public
     */
	
	public function BooksToShare(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');
			
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);
	
					$books_to_share=array();
				
					$books=$this->Book->find('all',array('conditions'=>array('user_relation_id'=>$userid)));
					foreach($books as $book)
					{
						if($book['Book']['status']=='Purchased' || $book['Book']['status']=='FreePurchased')
						{
							$stories=$this->Story->find('first',array('conditions'=>array('Story.id'=>$book['Book']['story_id'])));
							if(!in_array($stories['Story']['story_name'], $books_to_share))
							{
								$find_shared_books=$this->SharedBooks->find('first',array('conditions'=>array('book_id'=>$book['Book']['story_id'],'user_id'=>$userid)));
								if(empty($find_shared_books))
								{
									 array_push($books_to_share,$stories['Story']['story_name']);
								}
							}
						}
					}
					
					return $this->_returnJson(true, $books_to_share);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}




	/**
     * Get Purchased Books method
     * 
     * @access public
     */
	
	public function GetPurchasedBooks(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');
			
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);
	
					$free_books=array();
				
					$books=$this->Book->find('all',array('conditions'=>array('user_relation_id'=>$userid)));
					foreach($books as $book)
					{
						if($book['Book']['status']=='Purchased' || $book['Book']['status']=='FreePurchased')
						{
							$stories=$this->Story->find('first',array('conditions'=>array('Story.id'=>$book['Book']['story_id'])));
							if(!in_array($stories['Story']['story_name'], $free_books))
							{
								array_push($free_books,$stories['Story']['story_name']);
							}
						}
					}
					
					return $this->_returnJson(true, $free_books);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}




	/**
 	* Get Add Paid Books method
 	* @access public
 	*/
	public function AddPaidBooks(){
		if($this->request->is('post')){
			extract($this->request->data);

			$email=isset($email)?$email:'';
			$story_name=isset($story_name)?$story_name:'';
			$language=isset($language)?$language:'';

			if(empty($email))
				return $this->_returnJson(false, 'Please enter email of Teacher/Parent');

			if(empty($story_name))
				return $this->_returnJson(false, 'Please enter story_name');

			if(empty($language))
				return $this->_returnJson(false, 'Please enter language');
			
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Parent/Teacher does not exist.');
				}
			}

			if(!empty($story_name)){
				if(!$this->Story->hasAny(array('Story.story_name'=>$story_name)))
				{
              		return $this->_returnJson(false,'Story does not exist.');
				}
			}

			$user1_id=$this->User->getUserIdbyEmail($email);
   
          
        	$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
			$language_id=$languages['Language']['id'];
            $story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name,'language_id'=>$language_id)));
            $story_id=$story['Story']['id'];  
            if(empty($story_id))
            {
            	return $this->_returnJson(false, "Story does not exist.");
            }
            else
            {
	        	if(!empty($user1_id))
	        	{
	        		$paid_book_available=$this->Book->find('first',array('conditions'=>array('story_id'=>$story_id,'status'=>'Paid','user_relation_id'=>$user1_id)));
	        		if(empty($paid_book_available))
	        		{
	        			$book_available=$this->Book->find('first',array('conditions'=>array('story_id'=>$story_id,'user_relation_id'=>$user1_id)));
			    		if(!empty($book_available))
			    		{
			    			if($book_available['Book']['status']=='Purchased' || $book_available['Book']['status']=='FreePurchased')
			    			{
								return $this->_returnJson(false, "This book is already purchased.");
			    			}
			    			else
			    			{
				    			$this->Book->id=$book_available['Book']['id'];
				    			$this->Book->save(array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Paid'));
					    		return $this->_returnJson(true, "Paid Book added successfully.");
			    			}
			    		}
			    		else
			    		{
				    		$this->Book->save(array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Paid'));
				    		return $this->_returnJson(true, "Paid Book added successfully.");
				    	}
	        		}
	        		else
	        		{
	        			return $this->_returnJson(false, "Paid book already exist.");
	        		}
	        	}
        	}
        
		}
		else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}



/**
     * Get GetAll Books method
     * 
     * @access public
     */
	
	public function GetAllBooks1(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          	$language=isset($language)?$language:'';
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');

			if(empty($language))
				return $this->_returnJson(false, 'Please enter language');
			
			$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
			$language_id=$languages['Language']['id'];
			if(empty($language_id))
			{
				return $this->_returnJson(false, 'Please enter valid language');
			}

			$english_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
			$english_language_id=$english_language['Language']['id'];

			$english_stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$english_language_id)));
			$array1=array();
			foreach($english_stories as $st)
			{
				array_push($array1, $st['Story']['story_name']);
			}
			// print_r($array1);exit;

			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);

					$free_books['FreeBooks']=array();
					$paid_books['PaidBooks']=array();
					$purchased_books['PurchasedBooks']=array();
					$story_array=array();
					$book_array=array();
					$arr=array();
					$status_array=array('Free','Paid','Purchased','FreePurchased','PaidPurchased','Loaded','Unloaded','FreeByAds');
				
					$stories_all=$this->Story->find('all',array('conditions'=>array('language_id'=>$language_id)));
					$books=$this->Book->find('all',array('conditions'=>array('user_relation_id'=>$userid)));
					foreach($books as $book)
					{
						$stories=$this->Story->find('first',array('conditions'=>array('Story.id'=>$book['Book']['story_id'])));
						array_push($story_array,$stories['Story']['id']);
					}
		
					foreach($array1 as $str)
					{
						$t=1;
						$story_find=$this->Story->find('all',array('conditions'=>array('story_name'=>$str)));
						foreach($story_find as $s)
						{
							$sid=$s['Story']['id'];
							if(in_array($sid,$story_array))
							{
								$t=2;
								$find_status=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$userid,'story_id'=>$sid)));
							}
						}

							
						if($language=='English')
						{
							if($t==2)
							{
								array_push($arr,$str,$find_status['Book']['status']);
							}
							else
							{
								array_push($arr,$str,'Paid');
							}
						}
						else
						{
							$selected_language=$this->Language->find('first',array('conditions'=>array('language_name'=>$language)));
							$selected_language_id=$selected_language['Language']['id'];
							$translation=$this->StoryTranslation->find('first',array('conditions'=>array('story_name'=>$str,'language_id'=>$selected_language_id)));
							$str=$translation['StoryTranslation']['translation'];
							if($t==2)
							{
								array_push($arr,$str,$find_status['Book']['status']);
							}
							else
							{
								array_push($arr,$str,'Paid');
							}
						}

					}
					
	
					return $this->_returnJson(true,$arr);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}



/**
     * Get GetAll Books method
     * 
     * @access public
     */
	
	public function GetAllBooks(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          	$language=isset($language)?$language:'';
			if(empty($email))
				return $this->_returnJson(false, 'Please enter email');

			if(empty($language))
				return $this->_returnJson(false, 'Please enter language');
			
			$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
			$language_id=$languages['Language']['id'];
			if(empty($language_id))
			{
				return $this->_returnJson(false, 'Please enter valid language');
			}

			$english_stories=$this->Story->find('all',array('conditions'=>array('language_id'=>$this->Language->getEnglishLanguage())));
			$array1=array();
			foreach($english_stories as $st)
			{
				array_push($array1, $st['Story']['story_name']);
			}
			// print_r($array1);exit;

			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);

					$free_books['FreeBooks']=array();
					$paid_books['PaidBooks']=array();
					$purchased_books['PurchasedBooks']=array();
					$story_array=array();
					$book_array=array();
					$arr=array();
					$status_array=array('Free','Paid','Purchased','FreePurchased','PaidPurchased','Loaded','Unloaded','FreeByAds');
				
					$stories_all=$this->Story->find('all',array('conditions'=>array('language_id'=>$language_id)));
					$books=$this->Book->find('all',array('conditions'=>array('user_relation_id'=>$userid)));
					foreach($books as $book)
					{
						$stories=$this->Story->find('first',array('conditions'=>array('Story.id'=>$book['Book']['story_id'])));
						array_push($story_array,$stories['Story']['id']);
					}
		
					foreach($array1 as $str)
					{
						$t=1;
						$story_find=$this->Story->find('all',array('conditions'=>array('story_name'=>$str)));
						foreach($story_find as $s)
						{
							$sid=$s['Story']['id'];
							if(in_array($sid,$story_array))
							{
								$t=2;
								$find_status=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$userid,'story_id'=>$sid)));
							}
						}

						if($t==2)
						{
							array_push($arr,$str,$find_status['Book']['status']);
						}
						else
						{
							array_push($arr,$str,'Paid');
						}

					}
					
	
					return $this->_returnJson(true,$arr);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}



	/**
     * Get Paid Books method
     * 
     * @access public
     *
     */
	
	public function GetPaidBooks(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
          
			if(empty($email))
				return $this->_returnJson(false, 'Please enter username');
			
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Email does not exist.');
				}
				else
				{
		            $userid=$this->User->getUserIdbyEmail($email);
					$free_books=array();
					$books=$this->Book->find('all',array('conditions'=>array('user_relation_id'=>$userid)));
					$status_array=array('Free','FreePurchased','Purchased');
					$arr=array();
					foreach($books as $book)
					{
						if(in_array($book['Book']['status'], $status_array))
						{
							array_push($arr,$book['Book']['story_id'] );
						}
					}
					$english_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
					$english_language_id=$english_language['Language']['id'];
					$stories=$this->Story->find('all',array('conditions'=>array('Story.language_id'=>$english_language_id)));
					foreach ($stories as $story) {
						if(!in_array($story['Story']['id'], $arr))
						{
							if($story['Story']['story_name']!=$this->Session->read('free_book'))
							{
							array_push($free_books,$story['Story']['story_name']);
							}
						}
					}
					return $this->_returnJson(true, $free_books);
				}	
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}




	/**
     * Set Status of Book method
     * 
     * @access public
     * 
	 *  This API will get credits of user by email id or unique id
     */
	
	public function SetStatus(){
		if($this->request->is('post')){
			extract($this->request->data);
			$username=isset($username)?$username:'';
			$email=isset($email)?$email:'';
			$story_name=isset($story_name)?$story_name:'';
			$language_name=isset($language_name)?$language_name:'';
			$status=isset($status)?$status:'';

			if(empty($username))
				return $this->_returnJson(false, 'Please enter username of Child/Student');

			if(empty($email))
				return $this->_returnJson(false, 'Please enter email of Teacher/Parent');

			if(empty($story_name))
				return $this->_returnJson(false, 'Please enter story_name');

			if(empty($language_name))
				return $this->_returnJson(false, 'Please enter language_name');

			if(empty($status))
				return $this->_returnJson(false, 'Please enter status of book');

			if(!in_array($status,array('Loaded','Unloaded')))
			{
				return $this->_returnJson(false, 'Book status must be "Loaded" or "Unloaded"');
			}
			
			if(!empty($username))
			{
				if(!$this->User->hasAny(array('User.username'=>$username)))
				{
              		return $this->_returnJson(false,'Child/Student does not exist.');
				}
			}
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Parent/Teacher does not exist.');
				}
			}
			if(!empty($story_name)){
				if(!$this->Story->hasAny(array('Story.story_name'=>$story_name)))
				{
              		return $this->_returnJson(false,'Story does not exist.');
				}
				else
				{
					$stories=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name)));
					$story_id=$stories['Story']['id'];
				}
			}

			$userid=$this->User->getUserIdbyEmail($email);
			$find_language=$this->Language->find('first',array('conditions'=>array('language_name'=>$language_name)));
			$lang_id=$find_language['Language']['id'];
            $story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name,'language_id'=>$lang_id)));
            $story_id=$story['Story']['id'];     
        	
			if(empty($userid))
			{
				return $this->_returnJson(false,'No user relation exist.');
			}
			else 
			{
				$books=$this->Book->find('first',array('conditions'=>array('story_id'=>$story_id,'user_relation_id'=>$userid)));
				
				$this->Book->id=$books['Book']['id'];
				$this->Book->save(array('status'=>$status));
				return $this->_returnJson(true, "The status of the book changed by ".$status.' successfully');
			}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}



/**
     * Add Free Books method
     * 
     * @access public
     */
	
	public function AddFreeBooks(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
			$story_name=isset($story_name)?$story_name:'';
			$language=isset($language)?$language:'';

			if(empty($email))
				return $this->_returnJson(false, 'Please enter email of Teacher/Parent');

			if(empty($story_name))
				return $this->_returnJson(false, 'Please enter story_name');

			if(empty($language))
				return $this->_returnJson(false, 'Please enter language');
			
			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Parent/Teacher does not exist.');
				}
			}

			if(!empty($story_name)){
				if(!$this->Story->hasAny(array('Story.story_name'=>$story_name)))
				{
              		return $this->_returnJson(false,'Story does not exist.');
				}
			}

			
			$languages=$this->Language->find('first',array('conditions'=>array('Language.language_name'=>$language)));
			$language_id=$languages['Language']['id'];

			$user1_id=$this->User->getUserIdbyEmail($email);
            $story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name,'language_id'=>$language_id)));
            $story_id=$story['Story']['id'];  
            if(empty($story_id))
            {
				return $this->_returnJson(false,'Story does not exist.');
            }   
            else
            {
	        	if(!empty($user1_id))
	        	{
	        		$free_book_available=$this->Book->find('first',array('conditions'=>array('story_id'=>$story_id,'status'=>'Free','user_relation_id'=>$user1_id)));

	        		if(empty($free_book_available))
	        		{
	        			$book_available=$this->Book->find('first',array('conditions'=>array('story_id'=>$story_id,'user_relation_id'=>$user1_id)));

			    		if(!empty($book_available))
			    		{
			    			if($book_available['Book']['status']=='Purchased' || $book_available['Book']['status']=='FreePurchased')
				    		{
				    			return $this->_returnJson(false, "This book is already purchased.");
				    		}
				    		else
				    		{
				    			$this->Book->id=$book_available['Book']['id'];
				    			$this->Book->save(array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Free'));
					    		return $this->_returnJson(true, "Free Book added successfully.");
				    		}
			    		}
			    		else
			    		{
			    			$this->Book->create();
			    			$book=array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Free');
	        				$this->Book->save($book);
	        				return $this->_returnJson(true, "Free Book added successfully.");
				    	}
	        		}
	        		else
	        		{
	        			return $this->_returnJson(false, "Free book already exist.");
	        		}
	        	}
	        }
		    

		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}




/**
     * Add Free Books method
     * 
     * @access public
     */
	
	public function ActivateFreeBook(){
		if($this->request->is('post')){
			extract($this->request->data);
			$email=isset($email)?$email:'';
			$story_name=isset($story_name)?$story_name:'';

			if(empty($email))
				return $this->_returnJson(false, 'Please enter email of Teacher/Parent');

			if(empty($story_name))
				return $this->_returnJson(false, 'Please enter story_name');
			

			if(!empty($email)){
				if(!$this->User->hasAny(array('User.email'=>$email)))
				{
              		return $this->_returnJson(false,'Parent/Teacher does not exist.');
				}
			}
			
			$user1_id=$this->User->getUserIdbyEmail($email);

            $story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name)));
            $story_id=$story['Story']['id'];       
       
        	if(!empty($user1_id))
        	{
        		$free_book_available=$this->Book->find('first',array('conditions'=>array('story_id'=>$story_id,'status'=>'Free','user_relation_id'=>$user1_id)));
        		if(empty($free_book_available))
        		{
        			$get_freebook=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'FreeByAds')));
        			$this->Book->id=$get_freebook['Book']['id'];
        			$this->Book->delete();
        			$get_freebook1=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'Paid')));
        			$this->Book->id=$get_freebook1['Book']['id'];
        			$this->Book->delete();
        			$this->Book->save(array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'FreeByAds'));
			    	$find_books=$this->Book->find('first',array('conditions'=>array('user_relation_id'=>$user1_id,'story_id'=>$story_id,'status'=>'FreeByAds')));
			    	$doc=$find_books['Book']['created'];
			    	$timestamp = strtotime($doc) + 60*60;
					$time = date('Y-m-d H:i:s', $timestamp);
			    	return $this->_returnJson(true, "Free Book Activated Successfully for 1 hour.");
        		}
        		else
        		{
        			return $this->_returnJson(false, "Free book already exist.");
        		}
        	}
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}


/**
     * Deactivate Free Books method
     * 
     * @access public
     */
	
	public function DeactivateFreeBook(){
		
		$find_books=$this->Book->find('all',array('conditions'=>array('status'=>'FreeByAds')));
    	foreach($find_books as $book)
    	{
    		$doc=$book['Book']['created'];
	    	$currentTime = time();
           	$hour = date('H', $currentTime);
			$minute = date('i', $currentTime);

	    	$timestamp2 = strtotime($doc) + 60;
			$hour2 = date('H', $timestamp2);
			$minute2 = date('i', $timestamp2);
			$mins = ($currentTime - $timestamp2) / 60;

			if($mins>60)
			{
				$this->Book->id=$book['Book']['id'];
				$this->Book->save(array('status'=>'Paid'));
			}
    	}
	}




	/**
     * Save Stars method
     * 
     * @access public
     *
	 * @param $uniqueid
     * @param $stars
	 * 
	 *  This API will save stars of user by unique id
     */
     
     public function save_stars(){
     	if($this->request->is('post')){
     		extract($this->request->data);
			$uniqueid =isset($uniqueid)?$uniqueid:'';
			$stars= isset($stars)?$stars:'';
			if(empty($uniqueid))
                return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid)))
                return $this->_returnJson(false,'Unique id does not exist.');
			
			if(empty($stars))
				return $this->_returnJson(false,'Please enter total number of stars earned.');
			
			
			$find_user=$this->User->find('first',array('conditions'=>array('username'=>$uniqueid)));
			if(!empty($find_user))
			{
				$star=$find_user['User']['stars'];
			}
			
			$cnt=substr_count($star,"|");
			if($cnt<=4)
			{
				if($cnt==4)
				{
					$total=$star.'|Done';
					$user=array('stars'=>$total);
					$this->User->id=$this->User->getUserIdbyUserName($uniqueid);
					$this->User->save($user);
					if($this->User->save($user)){
						return $this->_returnJson(true, "Stars saved.");
					}else{
						return $this->_returnJson(false, "Error, something went wrong.");
					}
				}
				else
				{
					$total=$star.'|'.$stars;
					$user=array('stars'=>$total);
					$this->User->id=$this->User->getUserIdbyUserName($uniqueid);
					if($this->User->save($user)){
						return $this->_returnJson(true, "Stars saved.");
					}else{
						return $this->_returnJson(false, "Error, something went wrong.");
					}
				}
			}
			else 
			{
				return $this->_returnJson(false, "You can't play this app more then 5 times.");
			}
	
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}


		/**
     * Get Stars method
     * 
     * @access public
     *
	 * @param $uniqueid
     * @param $stars
	 * 
	 *  This API will save stars of user by unique id
     */
     
     public function get_stars(){
     	if($this->request->is('post')){
     		extract($this->request->data);
			$uniqueid =isset($uniqueid)?$uniqueid:'';
			if(empty($uniqueid))
                return $this->_returnJson(false,'Unique id cannot be left blank.');
            
            if(!$this->User->hasAny(array('User.username'=>$uniqueid)))
                return $this->_returnJson(false,'Unique id does not exist.');

			
			
			$find_user=$this->User->find('first',array('conditions'=>array('username'=>$uniqueid),'fields'=>'stars'));
			
				
			return $this->_returnJson(true, $find_user);

			
			
		}else{
			return $this->_returnJson(false, "Invalid Post Request.");
		}
	}

	     
	public function reset(){
        $this->User->query('Delete from `users` where id > 11 ');
        $this->User->query('ALTER TABLE `users` AUTO_INCREMENT =12;');
        $this->Book->query('ALTER TABLE `books` AUTO_INCREMENT =1;');
		$this->_returnJson(true, "Database Reset");
	}

    protected function _saveBook($user_relation_id,$story_id){
      	$booksdata=$this->Book->find('first',array('conditions'=>array(
            'Book.user_relation_id'=>$user_relation_id,
            'Book.story_id' => $story_id)));
      	if(empty($booksdata)){
          $book=array('Book'=>array(
            'user_relation_id' => $user_relation_id,
            'story_id' => $story_id));
        $this->Book->create();
        $this->Book->save($book);
      }
  }
    
    
    

}
?>
