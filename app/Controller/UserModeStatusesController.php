<?php
App::uses('AppController', 'Controller');
/**
 * UserModeStatuses Controller
 *
 * @property UserModeStatus $UserModeStatus
 * @property PaginatorComponent $Paginator
 */
class UserModeStatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserModeStatus->recursive = 0;
		$this->set('userModeStatuses', $this->UserModeStatus->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserModeStatus->exists($id)) {
			throw new NotFoundException(__('Invalid user mode status'));
		}
		$options = array('conditions' => array('UserModeStatus.' . $this->UserModeStatus->primaryKey => $id));
		$this->set('userModeStatus', $this->UserModeStatus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('User');
		$this->loadModel('Story');
		$this->loadModel('Language');
		$this->loadModel('Question');
		$this->loadModel('UserRelation');
		$users = $this->User->find('all');
		$user_array=array();
	  	foreach($users as $user)
	  	{
	  		if($user['User']['type']=='child' || $user['User']['type']=='student')
	  		{
	    		if(!empty($user['User']['email']))
		    	{
		      		array_push($user_array,$user['User']['email']);
		    	}
		    	else
		    	{
		    		if(!empty($user['User']['username']))
			    	{
			      		array_push($user_array,$user['User']['username']);
			    	}
		    	}
		    }
	  	}

	  	if ($this->request->is('ajax')) {

	  		$story_id=$_POST['st1'];
	  		$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id)));
			$question_array=array();
			foreach($questions as $question)
			{
				array_push($question_array, $question['Question']['question']);
			}
			$first_question=$this->Question->find('first',array('conditions'=>array('story_id'=>$story_id)));
			$answer_array=array($first_question['Question']['option1'],$first_question['Question']['option2'],$first_question['Question']['option3']);
			$data=array();
			$data['questions']=$question_array;
			$data['answers']=$answer_array;
			$question_id=$_POST['q_id'];
			if(!empty($question_id))
			{
				$first_question=$this->Question->find('first',array('conditions'=>array('question'=>$question_id)));
				$answer_array=array($first_question['Question']['option1'],$first_question['Question']['option2'],$first_question['Question']['option3']);
				$data['answers']=$answer_array;
			}
			echo json_encode($data);exit;
	  	}

		if ($this->request->is('post')) {
			
			$story_id=$this->request->data['UserModeStatus']['story_id'];
			$question_hidden=$this->request->data['UserModeStatus']['h1'];
			$this->request->data['UserModeStatus']['answer']=$this->request->data['UserModeStatus']['h2'];
			$questions=$this->Question->find('first',array('conditions'=>array('question'=>$question_hidden)));
			$this->request->data['UserModeStatus']['mode_question_answer_id']=$questions['Question']['id'];
		
		    $user_name=$user_array[$this->request->data['UserModeStatus']['user_id']];
			foreach($users as $user)
	  		{
	  			if($user['User']['email']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  			if($user['User']['username']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  		}

	  		$find_relation=$this->UserRelation->find('first',array('conditions'=>array('user2_id'=>$userid)));
	  		$relation_id=$find_relation['UserRelation']['id'];
	  		$this->request->data['UserModeStatus']['user_id']=$userid;
	  		if(!empty($relation_id))
	  		{
	  			$this->request->data['UserModeStatus']['user_relation_id']=$relation_id;
	  		}
            $this->UserRelation=new UserRelation();
            $userRelations=$this->UserRelation->find('first',array('conditions'=>array('UserRelation.user2_id'=>$this->request->data['UserModeStatus']['user_id'])));
            $this->request->data['UserModeStatus']['user_relation_id']=$userRelations['UserRelation']['id'];
			$this->UserModeStatus->create();
			
			
			if ($this->UserModeStatus->save($this->request->data)) {
				$this->Flash->success(__('The user mode status has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} 
		}
		$english_id=$this->Language->getEnglishLanguage();
		$languages = $this->UserModeStatus->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->UserModeStatus->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->UserModeStatus->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('language_id'=>$english_id)));
		$modeQuestionAnswers = $this->UserModeStatus->ModeQuestionAnswer->find('list',array('fields'=>array('ModeQuestionAnswer.mode_question')));
		if(empty($story_id))
		{
			$first_story=$this->Story->find('first',array('conditions'=>array('language_id'=>$english_id)));
			$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$first_story['Story']['id'])));
			$question_array=array();

			foreach($questions as $question)
			{
				array_push($question_array, $question['Question']['question']);
			}
			$first_question=$this->Question->find('first',array('conditions'=>array('story_id'=>$first_story['Story']['id'])));
			$answer_array=array($first_question['Question']['option1'],$first_question['Question']['option2'],$first_question['Question']['option3']);
			$first_option=$first_question['Question']['option1'];
			$first_question=$first_question['Question']['question'];
		}
		$this->set(compact('first_question','first_option','languages', 'levels', 'users', 'stories', 'modeQuestionAnswers','user_array','question_array','answer_array'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserModeStatus->exists($id)) {
			throw new NotFoundException(__('Invalid user mode status'));
		}
		$this->loadModel('User');
		$this->loadModel('Story');
		$this->loadModel('Language');
		$this->loadModel('Question');
		$this->loadModel('UserRelation');
		$users = $this->User->find('all');
		$user_array=array();
	  	foreach($users as $user)
	  	{
	  		if($user['User']['type']=='child' || $user['User']['type']=='student')
	  		{
	    		if(!empty($user['User']['email']))
		    	{
		      		array_push($user_array,$user['User']['email']);
		    	}
		    	else
		    	{
		    		if(!empty($user['User']['username']))
			    	{
			      		array_push($user_array,$user['User']['username']);
			    	}
		    	}
		    }
	  	}

	  	$find_mode_status=$this->UserModeStatus->find('first',array('conditions'=>array('id'=>$id)));
	  	$first_answer=$find_mode_status['UserModeStatus']['answer'];

	  	$story_id=$find_mode_status['UserModeStatus']['story_id'];
	  	$question_find=$this->Question->find('first',array('conditions'=>array('id'=>$find_mode_status['UserModeStatus']['mode_question_answer_id'])));
	  	$question1=$question_find['Question']['question'];
	  	$first_question=$question1;
	  	$user_find=$this->User->find('first',array('conditions'=>array('id'=>$find_mode_status['UserModeStatus']['user_id'])));
	  	$user_email=$user_find['User']['email'];
	  	$user_username=$user_find['User']['username'];
	  	$cnt=0;
	  	$answers_array=array($question_find['Question']['option1'],$question_find['Question']['option2'],$question_find['Question']['option3']);
	  	$i=0;
	  	foreach($answers_array as $answer)
	  	{
	  		if($answer==$find_mode_status['UserModeStatus']['answer'])
	  		{
	  			$selected_answer=$i;
	  		}
	  		$i++;
	  	}

	  	$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id)));
		$question_array=array();
		$i=0;
		foreach($questions as $question)
		{
			if($question['Question']['question']==$question1)
			{
				$selected_question=$i;
			}
			$i++;
			array_push($question_array, $question['Question']['question']);
		}
	  
	  	foreach($user_array as $email)
	  	{
	  		if($email==$user_email)
	  		{
	  			$select=$cnt;
	  			break;
	  		}
	  		if($email==$user_username)
	  		{
	  			$select=$cnt;
	  			break;
	  		}
	  		$cnt++;
	  	}



	  	if ($this->request->is('ajax')) {
			$story_id=$_POST['st1'];
	  		$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id)));
			$question_array=array();
			foreach($questions as $question)
			{
				array_push($question_array, $question['Question']['question']);
			}
			$first_question=$this->Question->find('first',array('conditions'=>array('story_id'=>$story_id)));
			$answer_array=array($first_question['Question']['option1'],$first_question['Question']['option2'],$first_question['Question']['option3']);
			$data=array();
			$data['questions']=$question_array;
			$data['answers']=$answer_array;
			$question_id=$_POST['q_id'];
			if(!empty($question_id))
			{
				$first_question=$this->Question->find('first',array('conditions'=>array('question'=>$question_id)));
				$answer_array=array($first_question['Question']['option1'],$first_question['Question']['option2'],$first_question['Question']['option3']);
				$data['answers']=$answer_array;
			}
			echo json_encode($data);exit;
	  	}

		if ($this->request->is(array('post', 'put'))) {
			$user_name=$user_array[$this->request->data['UserModeStatus']['user_id']];
			$story_id1=$this->request->data['UserModeStatus']['story_id'];

			$question_hidden=$this->request->data['UserModeStatus']['h1'];
			$this->request->data['UserModeStatus']['answer']=$this->request->data['UserModeStatus']['h2'];
			$questions=$this->Question->find('first',array('conditions'=>array('question'=>$question_hidden)));
			$this->request->data['UserModeStatus']['mode_question_answer_id']=$questions['Question']['id'];
			
			$question_id=$this->request->data['UserModeStatus']['mode_question_answer_id'];
			$questions=$this->Question->find('all',array('conditions'=>array('story_id'=>$story_id1)));
			$question_array=array();
			foreach($questions as $question)
			{
				array_push($question_array, $question['Question']['question']);
			}

			foreach($users as $user)
	  		{
	  			if($user['User']['email']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  			if($user['User']['username']==$user_name)
	  			{
	  				$userid=$user['User']['id'];
	  			}
	  		}

	  		$find_relation=$this->UserRelation->find('first',array('conditions'=>array('user2_id'=>$userid)));
	  		$relation_id=$find_relation['UserRelation']['id'];
	  		$this->request->data['UserModeStatus']['user_id']=$userid;
	  		if(!empty($relation_id))
	  		{
	  			$this->request->data['UserModeStatus']['user_relation_id']=$relation_id;
	  		}
	  		
	  			  	
            $this->UserRelation=new UserRelation();
            $userRelations=$this->UserRelation->find('first',array('conditions'=>array('UserRelation.user2_id'=>$this->request->data['UserModeStatus']['user_id'])));
            $this->request->data['UserModeStatus']['user_relation_id']=$userRelations['UserRelation']['id'];
			if ($this->UserModeStatus->save($this->request->data)) {
				$this->Flash->success(__('The user mode status has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user mode status could not be saved. Please, try again.'));
			
			}

		} else {
			$options = array('conditions' => array('UserModeStatus.' . $this->UserModeStatus->primaryKey => $id));
			$this->request->data = $this->UserModeStatus->find('first', $options);
		}
		$english_id=$this->Language->getEnglishLanguage();
		$languages = $this->UserModeStatus->Language->find('list',array('fields'=>array('Language.language_name')));
		$levels = $this->UserModeStatus->Level->find('list',array('fields'=>array('Level.level_name')));
		$stories = $this->UserModeStatus->Story->find('list',array('fields'=>array('Story.story_name'),'conditions'=>array('language_id'=>$english_id)));
		
		$this->set(compact('first_question','first_answer','selected_answer','answers_array','selected_question','languages', 'levels', 'users', 'stories', 'question_array','user_array','select'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserModeStatus->id = $id;
		if (!$this->UserModeStatus->exists()) {
			throw new NotFoundException(__('Invalid user mode status'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserModeStatus->delete()) {
			$this->Flash->success(__('The user mode status has been deleted.'));
		} else {
			$this->Flash->error(__('The user mode status could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
