<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	 public $components = array(
        'Flash',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'users',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login'
            ),
            // 'authenticate' => array(
                // 'Form' => array(
                    // 'passwordHasher' => 'Blowfish'
                // )
            // )
        ),
        'Session',
        'Cookie'
    );
	
	public function isAuthorized($user) {
    // Admin can access every action
 	   if (isset($user['role']) && $user['role'] === 'admin' || $user['role'] === 'voice_actor') {
        	return true;
    	}

    // Default deny
    	return false;
	}
	
	public function _sendemail($from,$to,$subject,$data=array(),$template=''){
		$this->layout=false;
		$this->render(false);
		App::uses('CakeEmail', 'Network/Email');
		$Email = new CakeEmail();
		$Email->from(array($from => 'Tagsom'));
		$Email->to($to);
		$Email->subject($subject);
		$Email->template($template,null);
		$Email->emailFormat('html');
		$Email->viewVars($data);
		$Email->send();
	}

    
    public function beforeFilter() {
    	

    	 $this->layout='admin'; 
		 $this->set('loggedIn', $this->Auth->loggedIn());
		 
		 // Add pagination for all pages globally, set paginate limit here
		 $this->paginate = array('limit'=>10);
		 
		 // set cookie options
	    $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
	    $this->Cookie->httpOnly = true;
	
	    if (!$this->Auth->loggedIn() && $this->Cookie->read('remember_me_cookie')) {
	        $cookie = $this->Cookie->read('remember_me_cookie');
			App::Import('Model','User');
			$this->User=new User();
	        $user = $this->User->find('first', array(
	            'conditions' => array(
	                'User.username' => $cookie['username'],
	                'User.password' => $cookie['password']
	            )
	        ));
	
	        if ($user && !$this->Auth->login($user['User'])) {
	            $this->redirect('/users/logout'); // destroy session & cookie
	        }
	    }
		 
         //$this->Auth->allow('index', 'view');
    }

    
}
