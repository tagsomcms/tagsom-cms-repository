<?php
App::uses('AppController', 'Controller');
/**
 * Grades Controller
 *
 * @property Grade $Grade
 * @property PaginatorComponent $Paginator
 */
class GradesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$grades_array=array();
		$grades1=$this->Grade->find('all',array('fields'=>array('DISTINCT grade_name')));
		foreach($grades1 as $grade)
		{
			$find_grades=$this->Grade->find('first',array('conditions'=>array('grade_name'=>$grade['Grade']['grade_name'])));
			array_push($grades_array, $find_grades['Grade']['id']);
		}

		$this->paginate = array('conditions'=>array('id'=>$grades_array));
		$grades = $this->paginate('Grade');
		$this->set(compact('grades'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Grade->exists($id)) {
			throw new NotFoundException(__('Invalid grade'));
		}
		$options = array('conditions' => array('Grade.' . $this->Grade->primaryKey => $id));
		$this->set('grade', $this->Grade->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$grades=$this->Grade->find('first',array('conditions'=>array('grade_name'=>$this->request->data['Grade']['grade_name'])));
			if(empty($grades))
			{
				$this->Grade->create();
				if ($this->Grade->save($this->request->data)) {
					$this->Flash->success(__('The grade has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The grade could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The grade is already exist.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Grade->exists($id)) {
			throw new NotFoundException(__('Invalid grade'));
		}
		$grades=$this->Grade->find('all',array('conditions'=>array('id !=' =>$id)));
		$grade_array=array();
		foreach($grades as $grade)
		{
			array_push($grade_array,$grade['Grade']['grade_name']);
		}

		if ($this->request->is(array('post', 'put'))) {
			if(!in_array($this->request->data['Grade']['grade_name'], $grade_array))
			{
				if ($this->Grade->save($this->request->data)) {
					$this->Flash->success(__('The grade has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The grade could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The grade is already exist.'));
			}
		} else {
			$options = array('conditions' => array('Grade.' . $this->Grade->primaryKey => $id));
			$this->request->data = $this->Grade->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Grade->id = $id;
		if (!$this->Grade->exists()) {
			throw new NotFoundException(__('Invalid grade'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Grade->delete()) {
			$this->Flash->success(__('The grade has been deleted.'));
		} else {
			$this->Flash->error(__('The grade could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
