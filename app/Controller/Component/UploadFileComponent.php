<?php
class UploadFileComponent extends Component {
	
	public function start_upload($file_Details) {
		
		$filename = ($file_Details['name']);
		$filename = time().$filename;
		$file_path = WWW_ROOT . 'uploads/profile/'.$filename;
		move_uploaded_file($file_Details['tmp_name'],$file_path);
		return $filename;
	}
}
?>