<?php
App::uses('AppController', 'Controller');
/**
 * Freebooks Controller
 *
 * @property Freebook $Freebook
 * @property PaginatorComponent $Paginator
 */
class FreebooksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Freebook->recursive = 0;
		$this->set('freebooks', $this->Paginator->paginate());
		

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Freebook->exists($id)) {
			throw new NotFoundException(__('Invalid free book'));
		}
		$options = array('conditions' => array('Freebook.' . $this->Freebook->primaryKey => $id));
		$this->set('freebook', $this->Freebook->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$free_book=$this->Freebook->find('first',array('conditions'=>array('title'=>$this->request->data['Freebook']['title'])));
			if(empty($free_book))
			{
				$this->Freebook->create();
				if ($this->Freebook->save($this->request->data)) {
					$this->Flash->success(__('The free book has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The freebook could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The freebook already exist'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Freebook->exists($id)) {
			throw new NotFoundException(__('Invalid free book'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$free_book=$this->Freebook->find('first',array('conditions'=>array('title'=>$this->request->data['Freebook']['title'])));
			if(empty($free_book))
			{
				if ($this->Freebook->save($this->request->data)) {
					$this->Flash->success(__('The free book has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The free book could not be saved. Please, try again.'));
				}
			}
			else
			{
				$this->Flash->error(__('The free book already exist'));
			}
		} else {
			$options = array('conditions' => array('Freebook.' . $this->Freebook->primaryKey => $id));
			$this->request->data = $this->Freebook->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Freebook->id = $id;
		if (!$this->Freebook->exists()) {
			throw new NotFoundException(__('Invalid free book'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Freebook->delete()) {
			$this->Flash->success(__('The free book has been deleted.'));
		} else {
			$this->Flash->error(__('The free book could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}




/**
 * assing freebook method
 *
 * @throws NotFoundException
 * @return void
 */
	// public function assign() {
	// 	$this->loadModel('AssignFreebook');
	// 	$this->loadModel('Story');
	// 	$this->loadModel('Language');
	// 	$get_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
	// 	$english_id1=$get_language['Language']['id'];

	// 	$this->paginate = array(
	//  	'conditions' => array('Story.language_id' => $english_id1),
 //        'limit' => 15
	//     );
	     
	//     $freebooks=$this->AssignFreebook->find('all');
	//     $books = $this->paginate('Story');
	// 	$this->set(compact('books','freebooks'));
	// }	

/**
 * active freebook method
 *
 * @throws NotFoundException
 * @return void
 */
	// public function active($id=null) {
	// 	$this->loadModel('AssignFreebook');
	// 	$this->loadModel('Story');
	// 	$this->loadModel('Language');
	// 	$get_language=$this->Language->find('first',array('conditions'=>array('language_name'=>'English')));
	// 	$english_id1=$get_language['Language']['id'];

	// 	$find_book=$this->AssignFreebook->find('first',array('conditions'=>array('story_id'=>$id)));
	// 	if(empty($find_book))
	// 	{
	// 		$story=$this->Story->find('first',array('conditions'=>array('id'=>$id)));
	// 		$storyname=$story['Story']['story_name'];
	// 		$this->AssignFreebook->create();
	// 		$this->AssignFreebook->save(array('story_id'=>$id,'title'=>$storyname,'status'=>1));
	// 		$lastid=$this->AssignFreebook->getLastInsertId();
	// 		$this->AssignFreebook->updateAll(  array('status' => 0), array('id !=' => $lastid));
	// 	}
	// 	else
	// 	{
	// 		$book=$this->AssignFreebook->find('first',array('conditions'=>array('story_id'=>$id)));
	// 		$this->AssignFreebook->updateAll(  array('status' => 1), array('id' => $book['AssignFreebook']['id']));
	// 		$this->AssignFreebook->updateAll(  array('status' => 0), array('id !=' => $book['AssignFreebook']['id']));
	// 	}
	// 	$this->Flash->success(__('The Free Book Activate Successfully.'));
	// 	return $this->redirect(array('action' => 'assign'));
	// }	


 /**
 * assing freebook method
 *
 * @throws NotFoundException
 * @return void
 */
	// public function inactive($id=null) {
	// 	$this->loadModel('AssignFreebook');
	// 	$this->loadModel('Story');
	// 	$this->loadModel('Language');
		
	// 	$book=$this->AssignFreebook->find('first',array('conditions'=>array('story_id'=>$id)));
	// 	$this->AssignFreebook->updateAll(  array('status' => 0), array('id' => $book['AssignFreebook']['id']));
	// 	$this->Flash->success(__('The Free Book Deactivate Successfully.'));
	// 	return $this->redirect(array('action' => 'assign'));
	// }	


}
