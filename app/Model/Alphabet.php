<?php
App::uses('AppModel', 'Model');
/**
 * Alphabet Model
 */
class Alphabet extends AppModel {
	public $actsAs = array('Containable');

 //        public $validate = array(
	// 	'alphabet' => array(
	// 		'Only Alphabets' => array(
 //                'rule' => array('custom', '/^[a-zA-Z\s]+$/'),
		
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// );
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(

		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'language_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);




}
