<?php
App::uses('AppModel', 'Model');
/**
 * UserRelation Model
 *
 * @property User1 $User1
 * @property User2 $User2
 */
class StoryStatement extends AppModel {

public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'statement' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please enter statement'
			),
		),
		// 'audio_clip' => array(
		// 	'notBlank' => array(
		// 		'rule' => array('notBlank'),
		// 		'message' => 'Please select audio file'
		// 	),
		// ),
	);


	/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
	
		// 'StoryImage' => array(
		// 	'className' => 'StoryImage',
		// 	'foreignKey' => 'story_image_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// ),
		'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'story_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		
	);



}
