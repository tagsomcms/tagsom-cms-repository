<?php
App::uses('AppModel', 'Model');
/**
 * Mode Model
 *
 * @property ModeQuestionAnswer $ModeQuestionAnswer
 * @property UserModeGrade $UserModeGrade
 * @property UserStoryLevel $UserStoryLevel
 */
class Mode extends AppModel {
    public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'mode_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ModeQuestionAnswer' => array(
			'className' => 'ModeQuestionAnswer',
			'foreignKey' => 'mode_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserModeGrade' => array(
			'className' => 'UserModeGrade',
			'foreignKey' => 'mode_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserStoryLevel' => array(
			'className' => 'UserStoryLevel',
			'foreignKey' => 'mode_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    
     public function getIdbyModeName($mode){
        $this->contain();
        $mode_id=$this->find('first',array('fields'=>array('id'),'conditions'=>array('Mode.mode_name'=>trim($mode))));
        if(empty($mode_id))
            return null;
        
        return $mode_id['Mode']['id'];
        
    }

}
