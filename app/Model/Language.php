<?php
App::uses('AppModel', 'Model');
/**
 * Language Model
 *
 * @property ModeQuestionAnswer $ModeQuestionAnswer
 * @property Story $Story
 * @property UserModeGrade $UserModeGrade
 * @property UserModeStatus $UserModeStatus
 * @property UserPurchase $UserPurchase
 * @property UserStoryLevel $UserStoryLevel
 */
class Language extends AppModel {
	public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'language_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ModeQuestionAnswer' => array(
			'className' => 'ModeQuestionAnswer',
			'foreignKey' => 'language_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'language_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserModeGrade' => array(
			'className' => 'UserModeGrade',
			'foreignKey' => 'language_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserModeStatus' => array(
			'className' => 'UserModeStatus',
			'foreignKey' => 'language_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserPurchase' => array(
			'className' => 'UserPurchase',
			'foreignKey' => 'language_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserStoryLevel' => array(
			'className' => 'UserStoryLevel',
			'foreignKey' => 'language_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'language_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
         'Alphabet' => array(
            'className' => 'Alphabet',
            'foreignKey' => 'language_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
         'Word' => array(
            'className' => 'Word',
            'foreignKey' => 'language_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
         'Question' => array(
            'className' => 'Word',
            'foreignKey' => 'language_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
          'GeneralSettings' => array(
            'className' => 'GeneralSettings',
            'foreignKey' => 'language_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);
    
    public function getIdbyLanguageName($language){
        $this->contain();
        $language_id=$this->find('first',array('fields'=>array('id'),'conditions'=>array('Language.language_name'=>trim($language))));
        if(empty($language_id))
            return null;
        
        return $language_id['Language']['id'];   
    }

 	public function getEnglishLanguage(){
	    $english=$this->find('first',array('conditions'=>array('language_name'=>'English')));
		return $english['Language']['id'];
	}

    	public	function getLocaleCodeForDisplayLanguage($name){
		    $languageCodes = array(
		    "aa" => "Afar",
		    "ab" => "Abkhazian",
		    "ae" => "Avestan",
		    "af" => "Afrikaans",
		    "ak" => "Akan",
		    "am" => "Amharic",
		    "an" => "Aragonese",
		    "ar" => "Arabic",
		    "as" => "Assamese",
		    "av" => "Avaric",
		    "ay" => "Aymara",
		    "az" => "Azerbaijani",
		    "ba" => "Bashkir",
		    "be" => "Belarusian",
		    "bg" => "Bulgarian",
		    "bh" => "Bihari",
		    "bi" => "Bislama",
		    "bm" => "Bambara",
		    "bn" => "Bengali",
		    "bo" => "Tibetan",
		    "br" => "Breton",
		    "bs" => "Bosnian",
		    "ca" => "Catalan",
		    "ce" => "Chechen",
		    "ch" => "Chamorro",
		    "co" => "Corsican",
		    "cr" => "Cree",
		    "cs" => "Czech",
		    "cu" => "Church Slavic",
		    "cv" => "Chuvash",
		    "cy" => "Welsh",
		    "da" => "Danish",
		    "de" => "German",
		    "dv" => "Divehi",
		    "dz" => "Dzongkha",
		    "ee" => "Ewe",
		    "el" => "Greek",
		    "en" => "English",
		    "eo" => "Esperanto",
		    "es" => "Spanish",
		    "et" => "Estonian",
		    "eu" => "Basque",
		    "fa" => "Persian",
		    "ff" => "Fulah",
		    "fi" => "Finnish",
		    "fj" => "Fijian",
		    "fo" => "Faroese",
		    "fr" => "French",
		    "fy" => "Western Frisian",
		    "ga" => "Irish",
		    "gd" => "Scottish Gaelic",
		    "gl" => "Galician",
		    "gn" => "Guarani",
		    "gu" => "Gujarati",
		    "gv" => "Manx",
		    "ha" => "Hausa",
		    "he" => "Hebrew",
		    "hi" => "Hindi",
		    "ho" => "Hiri Motu",
		    "hr" => "Croatian",
		    "ht" => "Haitian",
		    "hu" => "Hungarian",
		    "hy" => "Armenian",
		    "hz" => "Herero",
		    "ia" => "Interlingua (International Auxiliary Language Association)",
		    "id" => "Indonesian",
		    "ie" => "Interlingue",
		    "ig" => "Igbo",
		    "ii" => "Sichuan Yi",
		    "ik" => "Inupiaq",
		    "io" => "Ido",
		    "is" => "Icelandic",
		    "it" => "Italian",
		    "iu" => "Inuktitut",
		    "ja" => "Japanese",
		    "jv" => "Javanese",
		    "ka" => "Georgian",
		    "kg" => "Kongo",
		    "ki" => "Kikuyu",
		    "kj" => "Kwanyama",
		    "kk" => "Kazakh",
		    "kl" => "Kalaallisut",
		    "km" => "Khmer",
		    "kn" => "Kannada",
		    "ko" => "Korean",
		    "kr" => "Kanuri",
		    "ks" => "Kashmiri",
		    "ku" => "Kurdish",
		    "kv" => "Komi",
		    "kw" => "Cornish",
		    "ky" => "Kirghiz",
		    "la" => "Latin",
		    "lb" => "Luxembourgish",
		    "lg" => "Ganda",
		    "li" => "Limburgish",
		    "ln" => "Lingala",
		    "lo" => "Lao",
		    "lt" => "Lithuanian",
		    "lu" => "Luba-Katanga",
		    "lv" => "Latvian",
		    "mg" => "Malagasy",
		    "mh" => "Marshallese",
		    "mi" => "Maori",
		    "mk" => "Macedonian",
		    "ml" => "Malayalam",
		    "mn" => "Mongolian",
		    "mr" => "Marathi",
		    "ms" => "Malay",
		    "mt" => "Maltese",
		    "my" => "Burmese",
		    "na" => "Nauru",
		    "nb" => "Norwegian Bokmal",
		    "nd" => "North Ndebele",
		    "ne" => "Nepali",
		    "ng" => "Ndonga",
		    "nl" => "Dutch",
		    "nn" => "Norwegian Nynorsk",
		    "no" => "Norwegian",
		    "nr" => "South Ndebele",
		    "nv" => "Navajo",
		    "ny" => "Chichewa",
		    "oc" => "Occitan",
		    "oj" => "Ojibwa",
		    "om" => "Oromo",
		    "or" => "Oriya",
		    "os" => "Ossetian",
		    "pa" => "Panjabi",
		    "pi" => "Pali",
		    "pl" => "Polish",
		    "ps" => "Pashto",
		    "pt" => "Portuguese",
		    "qu" => "Quechua",
		    "rm" => "Raeto-Romance",
		    "rn" => "Kirundi",
		    "ro" => "Romanian",
		    "ru" => "Russian",
		    "rw" => "Kinyarwanda",
		    "sa" => "Sanskrit",
		    "sc" => "Sardinian",
		    "sd" => "Sindhi",
		    "se" => "Northern Sami",
		    "sg" => "Sango",
		    "si" => "Sinhala",
		    "sk" => "Slovak",
		    "sl" => "Slovenian",
		    "sm" => "Samoan",
		    "sn" => "Shona",
		    "so" => "Somali",
		    "sq" => "Albanian",
		    "sr" => "Serbian",
		    "ss" => "Swati",
		    "st" => "Southern Sotho",
		    "su" => "Sundanese",
		    "sv" => "Swedish",
		    "sw" => "Swahili",
		    "ta" => "Tamil",
		    "te" => "Telugu",
		    "tg" => "Tajik",
		    "th" => "Thai",
		    "ti" => "Tigrinya",
		    "tk" => "Turkmen",
		    "tl" => "Tagalog",
		    "tn" => "Tswana",
		    "to" => "Tonga",
		    "tr" => "Turkish",
		    "ts" => "Tsonga",
		    "tt" => "Tatar",
		    "tw" => "Twi",
		    "ty" => "Tahitian",
		    "ug" => "Uighur",
		    "uk" => "Ukrainian",
		    "ur" => "Urdu",
		    "uz" => "Uzbek",
		    "ve" => "Venda",
		    "vi" => "Vietnamese",
		    "vo" => "Volapuk",
		    "wa" => "Walloon",
		    "wo" => "Wolof",
		    "xh" => "Xhosa",
		    "yi" => "Yiddish",
		    "yo" => "Yoruba",
		    "za" => "Zhuang",
		    "zh-CN" => "Chinese",
		    "zu" => "Zulu"
		    );
		    return array_search($name, $languageCodes);
		}

}
