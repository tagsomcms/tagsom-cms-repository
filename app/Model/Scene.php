<?php
App::uses('AppModel', 'Model');
/**
 * Scene Model
 *
 */
class Scene extends AppModel {
	public $actsAs = array('Containable');

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(

		'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'story_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	
	);



}
