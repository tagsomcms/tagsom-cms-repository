<?php
App::uses('AppModel', 'Model');
/**
 * Grade Model
 *
 */
class Grade extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'grade_name' => array(
			'Please Enter valid Data' => array(
				'rule' => array('custom', '/^[a-zA-Z+-\s]+$/'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
