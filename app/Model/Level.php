<?php
App::uses('AppModel', 'Model');
/**
 * Level Model
 *
 * @property UserModeGrade $UserModeGrade
 * @property UserModeStatus $UserModeStatus
 */
class Level extends AppModel {
public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'level_name' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'UserModeGrade' => array(
			'className' => 'UserModeGrade',
			'foreignKey' => 'level_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserModeStatus' => array(
			'className' => 'UserModeStatus',
			'foreignKey' => 'level_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'level_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'ModeQuestionAnswer' => array(
            'className' => 'ModeQuestionAnswer',
            'foreignKey' => 'level_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )

        
	);
    
    public function getIdbyLevelName($level){
        $this->contain();
        $level_id=$this->find('first',array('fields'=>array('id'),'conditions'=>array('Level.level_name'=>trim($level))));
        if(empty($level_id))
            return null;
        
        return $level_id['Level']['id'];
        
    }

}
