<?php
App::uses('AppModel', 'Model');
/**
 * ModeQuestionAnswer Model
 *
 * @property Mode $Mode
 * @property Story $Story
 * @property Language $Language
 * @property UserModeStatus $UserModeStatus
 */
class GeneralSettings extends AppModel {
	public $actsAs = array('Containable');

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(

		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'language_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);




}
