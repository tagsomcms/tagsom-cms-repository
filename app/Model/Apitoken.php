<?php
App::uses('AppModel', 'Model');
/**
 * Apitoken Model
 *
 */
class Apitoken extends AppModel {
public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'api' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please Enter api',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function get_active_api()
	{
		
		$options= array('conditions' => array('status'=> 1));
        $api=$this->find('all',$options);
        return $api[0]['Apitoken']['api'];
        
	}


}
