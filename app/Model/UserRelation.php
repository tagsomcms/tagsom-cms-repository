<?php
App::uses('AppModel', 'Model');
/**
 * UserRelation Model
 *
 * @property User1 $User1
 * @property User2 $User2
 */
class UserRelation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user1_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user2_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User1' => array(
			'className' => 'User',
			'foreignKey' => 'user1_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User2' => array(
			'className' => 'User',
			'foreignKey' => 'user2_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Book' => array(
            'className' => 'Book',
            'foreignKey' => 'user_relation_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserModeStatus' => array(
            'className' => 'UserModeStatus',
            'foreignKey' => 'user_relation_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserPurchase' => array(
            'className' => 'UserPurchase',
            'foreignKey' => 'user_relation_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'UserStoryLevel' => array(
            'className' => 'UserStoryLevel',
            'foreignKey' => 'user_relation_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
    public function getUserRelationID($user1,$user2){
        $user_relation_id=$this->find('first',array('fields'=>array('id'),'conditions'=>array('UserRelation.user1_id'=>$user1,'UserRelation.user2_id'=>$user2)));
        if(empty($user_relation_id))
            return null;
        return $user_relation_id['UserRelation']['id'];
    }

    
}
