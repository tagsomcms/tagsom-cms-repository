<?php
App::uses('AppModel', 'Model');
/**
 * UserRelation Model
 *
 * @property User1 $User1
 * @property User2 $User2
 */
class StoryImage extends AppModel {

public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'story_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),

			),
		),
		'image' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please Select Image File',
				
			),
		),
	);



/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'StoryStatement' => array(
			'className' => 'StoryStatement',
			'foreignKey' => 'story_image_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
		);


	/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'story_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
