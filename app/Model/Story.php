<?php
App::uses('AppModel', 'Model');
App::uses('StoryStatement', 'Model');
/**
 * Story Model
 *
 * @property Language $Language
 * @property ModeQuestionAnswer $ModeQuestionAnswer
 * @property UserModeGrade $UserModeGrade
 * @property UserModeStatus $UserModeStatus
 * @property UserPurchase $UserPurchase
 * @property UserStoryLevel $UserStoryLevel
 */
class Story extends AppModel {
public $actsAs = array('Containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'language_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'story_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Please Enter Storyname',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'language_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ModeQuestionAnswer' => array(
			'className' => 'ModeQuestionAnswer',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserModeGrade' => array(
			'className' => 'UserModeGrade',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserModeStatus' => array(
			'className' => 'UserModeStatus',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserPurchase' => array(
			'className' => 'UserPurchase',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserStoryLevel' => array(
			'className' => 'UserStoryLevel',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'StoryImage' => array(
			'className' => 'StoryImage',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'StoryStatement' => array(
			'className' => 'StoryStatement',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Word' => array(
			'className' => 'Word',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
			'Story' => array(
			'className' => 'Story',
			'foreignKey' => 'story_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
	);

    public function getStoryIdByStoryName($storyname){
        $this->contain();
        $story_id=$this->find('first',array('fields'=>array('id'),'conditions'=>array('Story.story_name'=>trim($storyname))));
        if(empty($story_id))
            return null;
        
        return $story_id['Story']['id'];
    }

    public function import($data=null,$count=null)
    {
    	$statement_model = ClassRegistry::init('StoryStatement');
    	for($i=2;$i<=$count;$i++) // Loop to get all sheets in a file.
		{	
			$content=$data->sheets[0]['cells'][$i][1];
			$story_name=$data->sheets[0]['cells'][$i][2];
			$type=$data->sheets[0]['cells'][$i][3];
			$language=$data->sheets[0]['cells'][$i][4];

			//find_language
			$find_language=$this->Language->find('first',array('conditions'=>array('language_name'=>$language)));
			$language_id=$find_language['Language']['id'];
			$story_exist=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name)));
			if(empty($story_exist))
			{
				$this->Story->create();
				$this->Story->save(array('story_name'=>$story_name,'language_id'=>$language_id));
			}
			$find_story=$this->Story->find('first',array('conditions'=>array('story_name'=>$story_name)));
			$story_id=$find_story['Story']['id'];
			$story_language=$find_story['Story']['language_id'];

			if($type=='Statement')
			{
				$statement_find=$this->StoryStatement->find('all',array('conditions'=>array('story_id'=>$story_id,'statement'=>$content)));
			
				if(empty($statement_find))
				{
					$this->StoryStatement->create();
					$this->StoryStatement->save(array('story_id'=>$story_id,'statement'=>$content));
				}
			}
			
		}
        		
    }

}
