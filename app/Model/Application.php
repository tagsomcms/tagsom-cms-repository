<?php
App::uses('AppModel', 'Model');
/**
 * Book Model
 *
 * @property User $User
 * @property Story $Story
 */
class Application extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public function add_application($application_name=null,$client_name=null,$year=null,$image=null,$tmp_name=null)
	{
		if (!file_exists(WWW_ROOT.'uploads/Applications')) {
			mkdir(WWW_ROOT.'uploads/Applications', 0777, true);
	    }
	    chmod(WWW_ROOT.'uploads/Applications',0777);
	    move_uploaded_file($tmp_name, WWW_ROOT.'uploads/Applications/'.$image);
	    chmod(WWW_ROOT.'uploads/Applications/'.$image,0775);

	    $this->create();
		if($this->save(array('application_name'=>$application_name,'client_name'=>$client_name,'year'=>$year,'image'=>$image)))
		{
			return true;
		}
	}


	public function edit_application($application_name=null,$client_name=null,$year=null,$image=null,$tmp_name=null,$old_image=null,$id=null)
	{
		if (!file_exists(WWW_ROOT.'uploads/Applications')) {
				mkdir(WWW_ROOT.'uploads/Applications', 0777, true);
	    }
	    chmod(WWW_ROOT.'uploads/Applications',0777);
	    move_uploaded_file($tmp_name, WWW_ROOT.'uploads/Applications/'.$image);
	    chmod(WWW_ROOT.'uploads/Applications/'.$image,0775);
		rename(WWW_ROOT.'uploads/Applications/'.$old_image,WWW_ROOT.'uploads/Applications/'.$image);

		$this->id=$id;
		
		if($this->save(array('application_name'=>$application_name,'client_name'=>$client_name,'year'=>$year,'image'=>$image)))
		{
			return true;
		}
		
		
	}


	public function delete_application($id=null)
	{
		$this->id=$id;
		if($this->delete())
		{
			return true;
		}
	}


}
