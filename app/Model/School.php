<?php
App::uses('AppModel', 'Model');
/**
 * School Model
 *
 * @property User $User
 */
class School extends AppModel {
    public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'Enter only Alphabets' => array(
				'rule' => array('custom', '/^[a-zA-Z+-\s]+$/'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'Enter Valid Email_ID' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'school_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    public function getSchoolIdbySchoolName($schoolname){
        $this->contain();
        $school_id=$this->find('first',array('fields'=>array('id'),'conditions'=>array('School.name'=>$schoolname)));
        if(empty($school_id))
            return null;
        
        return $school_id['School']['id'];
    }

}
